﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for utility
/// </summary>
public class utility
{
	public utility()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string[] getColumnCodes(string rptName)
    {
        string[] extract1ColCodes = { "A.1", "B.1", "B.3", "E.1", "E.2", "E.3", "E.4", "E.5", "E.6", "E.7", "E.8", "E.9", "E.10", "E.11", "C.1", "C.3", "C.4", "C.5", "D.1", "D.2", "D.3", "D.4", "D.5", "C.6", "C.7", "C.8", "C.9", "C.10", "C.11", "C.12", "C.13", "C.14", "C.15", "C.16", "C.17", "C.18", "C.19", "C.20", "C.21", "C.22", "C.23", "C.24", "C.25", "C.26", "C.28","D.6","E.12","C.27"};
        string[] extract2aColCodes = { "A.1", "B.1", "G.2", "B.3", "F.2", "F.3", "" };
        string[] extract2bColCodes = { "A.1", "B.1", "B.3", "G.2", "F.2", "C.1","" };
        string[] extract2cColCodes = { "A.1", "B.1", "B.3", "F.2", "G.2", "F.8", "F.6", "F.7", "G.19","" };
        string[] extract3aColCodes = { "A.1", "B.1", "B.3", "F.2", "G.1", "G.17", "F.5", "F.8", "G.2", "G.18", "G.3", "G.4", "G.5", "G.7", "G.8", "G.9", "G.10", "G.11", "G.12", "G.13", "G.14", "G.15", "G.16", "H.1", "H.2", "H.3", "H.4", "H.5", "H.6", "H.7", "H.8", "H.9", "H.10", "H.11", "H.12", "H.13", "H.14", "H.15", "H.16", "H.17", "H.18", "H.19", "H.20", "H.21", "I.1", "I.2", "I.3", "I.4", "I.5", "I.6", "I.7", "I.8", "I.9", "J.1", "J.2", "J.3", "J.4", "J.5", "J.6", "K.1", "K.2", "K.3", "K.4", "K.5", "K.6", "K.7", "K.8", "K.9", "K.10", "K.11", "K.12", "K.13", "L.1", "L.2", "L.3", "L.4", "L.5", "L.6", "L.7", "L.8", "L.9", "L.10", "L.11", "M.1", "M.2", "M.3", "M.4", "M.5", "M.6", "M.7", "M.8", "M.9", "N.1", "N.2", "N.3", "N.4", "N.5", "N.6", "N.7", "N.8", "N.9", "N.10", "N.11", "O.1", "O.2", "O.3", "O.4", "O.5", "P.1", "P.2", "P.3", "P.4", "F.3", "" };
        string[] extract3bColCodes = { "A.1", "B.1", "B.3", "G.1", "G.2", "F.2", "G.17", "G.6","" };
        string[] extract4ColCodes = { "A.1", "B.1", "B.3", "G.1", "G.17", "G.2", "Q.1", "G.18","" };
        string[] extract5ColCodes = { "A.1", "B.1", "B.3", "G.1", "G.17", "G.2", "R.1", "R.2", "R.3", "R.4", "R.5", "R.6", "R.7", "R.8", "R.9", "R.10" };
        string[] extract6ColCodes = { "A.1", "B.1", "B.3", "E.1", "C.1", "C.3", "C.4", "D.1", "D.4", "D.5", "" };
        string[] rtnCodes = { };
        switch (rptName)
        {
            case "Extract1":
                rtnCodes = extract1ColCodes;
                break;
            case "Extract2a":
                rtnCodes = extract2aColCodes;
                break;
            case "Extract2b":
                rtnCodes = extract2bColCodes;
                break;
            case "Extract2c":
                rtnCodes = extract2cColCodes;
                break;
            case "Extract3a":
                rtnCodes = extract3aColCodes;
                break;
            case "Extract3b":
                rtnCodes = extract3bColCodes;
                break;
            case "Extract4":
                rtnCodes = extract4ColCodes;
                break;
            case "Extract5":
                rtnCodes = extract5ColCodes;
                break;
            case "Extract6":
                rtnCodes = extract6ColCodes;
                break;
        }
        return rtnCodes;
    }
}