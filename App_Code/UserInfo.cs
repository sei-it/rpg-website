﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserInfo
/// </summary>
///
/// [Serializable]
[Serializable]
public class UserInfo
{
    public int intUserID;
    public string sUserID;
    public int? intUserRoleID;
    public int? intGranteeID;

    public UserInfo()
	{
		//
		// TODO: Add constructor logic here
		//
        intUserRoleID = 0;
        intGranteeID = 0;
	}

}