﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


 
/// <summary>
/// Summary description for Auth
/// </summary>
/// 
public class Auth
{
 
    public Auth()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static UserInfo  getUserInfo(string sUserID)
    {
        UserInfo oUsI;
        oUsI= new UserInfo();
        object objVal = null;

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oUsrs = from u in db.AppUsers
                        where u.sUserID == sUserID                       //&&  u.blAdmin == true 
                        select u ;
            foreach (var oU in oUsrs)
            {
                oUsI.sUserID = oU.sUserID;
                oUsI.intUserID = oU.Userid;
                oUsI.intUserRoleID = oU.AdminRoleId_fk;

            }
            if ((oUsI.intUserRoleID != 4) ||(oUsI.intUserRoleID != 3))
            {
                var oGCon = from gc in db.A_Grantee_Contacts
                            where gc.Email == sUserID
                            select gc;
                foreach (var oG in oGCon)
                {
                    oUsI.sUserID = oG.Email;
                    oUsI.intUserID = oG.GranteeContactId;
                    if (oG.GranteeRole_fk != null)
                    {
                        oUsI.intUserRoleID = oG.GranteeRole_fk;
                    }
                    if (oG.GranteeId_fk != null)
                    {
                        oUsI.intGranteeID = oG.GranteeId_fk;
                    }
                    
                }
            }

        }


        return oUsI;
    }
    
}