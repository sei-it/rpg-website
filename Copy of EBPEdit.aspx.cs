﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EBPEdit : System.Web.UI.Page
{
    int lngPkID;
    int intCaseID;
    int? intGranteeID;
    int GranteeContactid;
    int CaseWorkerid;
    bool blClose  ;
    UserInfo oUI;
    int ebpid1;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;
        

        if (!IsPostBack)
        {
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["intCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
            }

            if (Page.Request.QueryString["CaseWorkerid"] != null)
            {
                CaseWorkerid = Convert.ToInt32(Page.Request.QueryString["CaseWorkerid"]);
            }

            if (Page.Request.QueryString["GranteeContactid"] != null)
            {
                GranteeContactid = Convert.ToInt32(Page.Request.QueryString["GranteeContactid"]);
            }
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
           
            loadrecords();
        }

        DisableBtnSave();
         
        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();
            loadCaseMembers();
            loadCaseWorkers();          
            displayRecords();
           // displayCaseWorkerl();
          
            ViewState["IsLoaded1"] = true;
        }

    }

    private void DisableBtnSave()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oPages = from pg in db.F_ENROLLMENT_EBPs
                         where pg.CaseEBPID == lngPkID
                         select pg;
            foreach (var oCase in oPages)
            {
                if (oCase.EBP_EXIT_DATE!=null)
                {
                    btnSave.Visible = false;
                    btnSaveClose.Visible = false;
                }
            }

        }
    }
    
    private void displayCaseWorkerl(int intEBPEnrollID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.F_EBPCaseWorkers
                         join G in db.A_Grantee_Contacts
                         on pg.GranteeContactId_FK equals G.GranteeContactId
                         where pg.EBPCaseWorker_Assigned!=false && pg.EBPEnrollID_FK == intEBPEnrollID
                         select new { G.GranteeContactId, CaseWorkerName = G.FirstName + " " + G.LastName, G.Phone, G.Email, pg.CaseWorker_StartDate, pg.CaseWorker_ExitDate };

            grdCaseWorkerList.DataSource = oPages;
            grdCaseWorkerList.DataBind();
        }
       
    }

    protected void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                intGranteeID = oCase.GranteeID_fk;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
            }
        }
    }

    protected void displayRecords()
    {
        object objVal = null;
        if (oUI.intUserRoleID == 2)
        {
            litMessagePermissions.Text = "You do not have permissions to edit this page.";
            btnSave.Visible = false;
            btnSaveClose.Visible = false;
        }

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
        var oPages = from pg in db.F_ENROLLMENT_EBPs 
                     where pg.CaseEBPID == lngPkID
                     select pg;
        foreach (var oCase in oPages)
        {
            objVal = oCase.EBPID_FK;
            if (objVal != null)
            {
                cboEBP.SelectedIndex = cboEBP.Items.IndexOf(cboEBP.Items.FindByValue(oCase.EBPID_FK.ToString()));
                ebpid1 = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                cboEBP.Enabled = false;
            }
            objVal = oCase.EBP_ENROLL_DATE;
            if (objVal != null)
            {
                txtEbpenrolldate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
                txtEbpenrolldate.Enabled = false;

            }

            //objVal = oCase.EBP_EXIT_DATE;
            //if (objVal != null)
            //{
            //    txtEbpexitdate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            //    btnSave.Visible = false;
            //    btnSaveClose.Visible = false;
               
            //}

            //--------to hide the Participant Engagement rating questions April 8th-------------
            
                var qry4 = from pg in db.A_GranteeEBPs                          
                           where   pg.GranteeID_FK == intGranteeID && pg.EBPID_FK == ebpid1
                           select new { pg.FocalEBPYN,pg.EBPID_FK };

                foreach (var c in qry4)
                {
                    if (c.FocalEBPYN == false)
                    {
                        PEngFocalEBP.Visible = false;
                    }
                    else
                    {
                        PEngFocalEBP.Visible = true;
                    }
                }
            

            //--------------------------

            /*PE 04/03/2014*/
            objVal = oCase.EBP_EXIT_DATE;
            if (objVal != null)
            {
                txtEbpexitdate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
                btnSave.Visible = false;
                btnSaveClose.Visible = false;
                divEngagementRating.Visible = true;
                btnEBPClose.Text = "Cancel EBP Close"; 
                displayEngagementRatings();
            }
            else
            {
                btnSave.Visible = true;
                btnSaveClose.Visible = true;
                divEngagementRating.Visible = false;
                btnEBPClose.Text = "Exit Case from EBP";
               // displayEngagementRatings();
            }
            /*------------------*/
        }

        }
       //displayCaseWorkers(lngPkID);
       // displayCaseWorkerList(lngPkID);
        displayCaseMembers(lngPkID);
        displayCaseWorkerl(lngPkID);
    }

    /*Participnt Engagemnet Rating 04/03/2014*/
    protected void displayEngagementRatings()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {  
         


            var oPages = from pg in db.F_EBP_ParticipantRatings
                         where pg.CaseEBPID_FK == lngPkID && pg.ServiceLogID_FK==null
                         select pg;
            
            foreach (var oCase in oPages)
            {
                objVal = oCase.ER_ID_FK;
                if (objVal != null)
                {
                    chkERatings.SelectedIndex = chkERatings.Items.IndexOf(chkERatings.Items.FindByValue(oCase.ER_ID_FK.ToString()));
                }
            }

        }
    }

    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {     

            F_ENROLLMENT_EBP oCase = (from c in db.F_ENROLLMENT_EBPs where c.CaseEBPID == lngPkID select c).FirstOrDefault();
             if ((oCase == null))
            {
                oCase = new F_ENROLLMENT_EBP();
                blNew = true;
                oCase.CaseID_FK = intCaseID;
            }

            if (!(cboEBP.SelectedItem == null))
            {
                if (!(cboEBP.SelectedItem.Value.ToString() == ""))
                {
                    oCase.EBPID_FK = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.EBPID_FK = null;
                }
            }

            DateTime.TryParse(txtEbpenrolldate.Text, out dtTmp); 
            if (txtEbpenrolldate.Text!="")
            oCase.EBP_ENROLL_DATE = dtTmp;

            DateTime.TryParse(txtEbpexitdate.Text, out dtTmp);
            if (txtEbpexitdate.Text != "")
            {
                oCase.EBP_EXIT_DATE = dtTmp;
                oCase.EBP_ENROLL_COMPLETED = true;
            }

            if (blNew == true)
            {
                db.F_ENROLLMENT_EBPs.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();
            lngPkID = oCase.CaseEBPID;
            saveCaseMembers();
            saveCaseWorker1();
            /*PE 04/03/2014*/
            if (txtEbpexitdate.Text != "") 
            {
                updateEngagementRating();
                updateCaseWorker1(dtTmp);
            }
            /*--------*/
           // saveCaseWorkers();
          
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;
 

    }

    private void updateCaseWorker1(DateTime dtTmp)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            DateTime dtTmp1;
            DateTime StartdtTmp1;
            object objVal = null;


            if (!(ddlCaseWorkers.SelectedItem.Value == "Select"))
            {
                GranteeContactid = Convert.ToInt32(ddlCaseWorkers.SelectedValue);
            }
            else
            {
                F_EBPCaseWorker oPages = (from pg in db.F_EBPCaseWorkers
                                          where pg.EBPCaseWorker_Assigned != false && pg.EBPEnrollID_FK == lngPkID
                                          select pg).FirstOrDefault();
                GranteeContactid = Convert.ToInt32(oPages.GranteeContactId_FK);
            }

            //F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
            //                         where c.GranteeContactId_FK == GranteeContactid
            //                         && c.EBPEnrollID_FK == lngPkID
            //                         select c).FirstOrDefault();

            F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
                                     where c.EBPEnrollID_FK == lngPkID
                                     select c).FirstOrDefault();

            if ((oCase == null))
            {
                oCase = new F_EBPCaseWorker();
                blNew = true;
                oCase.EBPEnrollID_FK = lngPkID;
                oCase.GranteeContactId_FK = GranteeContactid;
            }
                  
           
           

            if (!(oCase.CaseWorker_ExitDate== null))
            {
                if (oCase.CaseWorker_ExitDate > dtTmp)
                {
                    oCase.CaseWorker_ExitDate = dtTmp;
                }
               
            }
            else
            {
              oCase.CaseWorker_ExitDate = DateTime.Now;               
            }

            if (blNew == true)
            {
                db.F_EBPCaseWorkers.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();

            displayCaseWorkerl(lngPkID);
            ClearDates();

        }        
        
    }

    private void saveCaseWorker1()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            DateTime dtTmp1;

            if (!(ddlCaseWorkers.SelectedItem.Value == "Select"))
            {
                GranteeContactid = Convert.ToInt32(ddlCaseWorkers.SelectedValue);
            }
            else
            {
                F_EBPCaseWorker oPages = (from pg in db.F_EBPCaseWorkers
                                          where pg.EBPCaseWorker_Assigned != false && pg.EBPEnrollID_FK == lngPkID
                                          select pg).FirstOrDefault();
                GranteeContactid = Convert.ToInt32(oPages.GranteeContactId_FK);
            }

            F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
                                     where c.GranteeContactId_FK == GranteeContactid
                                     && c.EBPEnrollID_FK == lngPkID
                                     select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBPCaseWorker();
                blNew = true;
                oCase.EBPEnrollID_FK = lngPkID;
                oCase.GranteeContactId_FK = GranteeContactid;
            }

            DateTime.TryParse(txtCaseWorkerStartDt.Text, out dtTmp1);

            if (txtCaseWorkerStartDt.Text != "")
            {
                oCase.CaseWorker_StartDate = dtTmp1;
                oCase.EBPCaseWorker_Assigned = true;
            }

            DateTime.TryParse(txtCaseWorkerExitDt.Text, out dtTmp1);

            if (txtCaseWorkerExitDt.Text != "")
                oCase.CaseWorker_ExitDate = dtTmp1;

            if (blNew == true)
            {
                db.F_EBPCaseWorkers.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();

          //  displayCaseWorkerl(lngPkID);
           // ClearDates();

        }        
    }

    /*ERating  04/03/2014*/
    public void updateEngagementRating()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBP_ParticipantRating oCase = (from c in db.F_EBP_ParticipantRatings where c.ServiceLogID_FK == null && c.CaseEBPID_FK == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBP_ParticipantRating();
                blNew = true;
                oCase.CaseEBPID_FK = lngPkID;
               // oCase.ServiceLogID_FK = lngPkID; //not needed lets you identify where it is comng from, SL or EBP
            }

            if (!(chkERatings.SelectedItem == null))
            {
                if (!(chkERatings.SelectedItem.Value.ToString() == ""))
                {
                    oCase.ER_ID_FK = Convert.ToInt32(chkERatings.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.ER_ID_FK = null;
                }
            }
            oCase.updatedBy = HttpContext.Current.User.Identity.Name;
            oCase.updatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.F_EBP_ParticipantRatings.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
            //lngPkID = oCase.EBP_Part_ID;

        }

    }
    
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from e in db.F_EBPLists
                      join gebp in db.A_GranteeEBPs
                      on e.EBPID equals gebp.EBPID_FK 
                      join cs1 in db.B_CASE_ENROLLMENTs
                      on gebp.GranteeID_FK equals cs1.GranteeID_fk
                      where cs1.CaseNoId == intCaseID
                      select e;
            cboEBP.DataSource = qry;
            cboEBP.DataTextField = "EBPName";
            cboEBP.DataValueField = "EBPID";
            cboEBP.DataBind();
            cboEBP.Items.Insert(0, "");

            /*ERating  04/03/2014*/
            var qry3 = from e in db.L_ENGAGEMENT_RATINGs
                       select e;
            chkERatings.DataSource = qry3;
            chkERatings.DataTextField = "CASE_EBP_ENGAGEMENT";
            chkERatings.DataValueField = "ER_ID";
            chkERatings.DataBind();
         }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }
        if (((this.ViewState["intGranteeID"] != null)))
        {
            intGranteeID = Convert.ToInt32(this.ViewState["intGranteeID"]);
        }

        if (((this.ViewState["GranteeContactid"] != null)))
        {
            GranteeContactid = Convert.ToInt32(this.ViewState["GranteeContactid"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        if (((this.ViewState["blClose"] != null)))
        {
            blClose = Convert.ToBoolean(this.ViewState["blClose"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["intGranteeID"] = intGranteeID;
        this.ViewState["blClose"] = blClose;
        this.ViewState["GranteeContactid"] = GranteeContactid;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        /*----check Case Worker Assigned----*/
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPCaseWorker oPages = (from pg in db.F_EBPCaseWorkers
                         where pg.EBPCaseWorker_Assigned != false && pg.EBPEnrollID_FK == lngPkID 
                                      select pg).FirstOrDefault();

            if ((oPages == null))
            {
                lblAddCwMessage.Text = "Required a case worker and it's start date for an EBP";
            }
            else
            {
               
                //if (!(ddlCaseWorkers.SelectedItem.Value == "Select") && (txtCaseWorkerStartDt.Text != ""))
              if(!(oPages.CaseWorker_StartDate==null) )
                {

                    if ((blClose == true ) && (txtEbpexitdate.Text != ""))
                    {
                        litEBPMeassage.Text = "";
                        updateUser();
                        displayRecords();
                    }
                    else
                    {
                        litEBPMeassage.Text = "Input EBP Exit Date";
                    }
                    lblAddCwMessage.Text = "";
                }
                else
                {
                    lblAddCwMessage.Text = "Add a case worker and it's start date for an EBP";
                }
            }

        }      
       
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("EBPList.aspx?intCaseID="+ intCaseID );
    }

    protected void btnSaveClose_Click(object sender, EventArgs e)
    {

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPCaseWorker oPages = (from pg in db.F_EBPCaseWorkers
                                      where pg.EBPCaseWorker_Assigned != false && pg.EBPEnrollID_FK == lngPkID
                                      select pg).FirstOrDefault();

            if ((oPages == null))
            {
                lblAddCwMessage.Text = "Add a case worker and it's start date for an EBP";
            }
            else
            {
                if (!(oPages.CaseWorker_StartDate == null))
                {
                    if ((blClose == true) && (txtEbpexitdate.Text != "") || (blClose == false))
                    {
                        litEBPMeassage.Text = "";
                        updateUser();
                        Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                    }
                    else 
                    {
                        litEBPMeassage.Text = "Input EBP Exit Date";
                    }
                    lblAddCwMessage.Text = "";
                }
                else
                {
                    lblAddCwMessage.Text = "Required Case Worker and it's Start Date";
                }
            }

        }      
       

    }
    //--Case Members
    /// <summary>
    /// Case member operations
    /// </summary>
    /// 
    private void displayCaseMembers(int intCaseEBPID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.F_EBPEnrollments
                         where pg.EBPID_FK == intCaseEBPID
                         select pg;
            foreach (var oCase in oPages)
            {
                for (int i = 0; i < chkCaseMembers.Items.Count; i++)
                {
                    if (Convert.ToInt32(chkCaseMembers.Items[i].Value) == oCase.IndividualID_FK)
                    {
                        chkCaseMembers.Items[i].Selected = true;
                        chkCaseMembers.Items[i].Enabled = false;
                    }
                    chkCaseMembers.Items[i].Enabled = false;
                }

            }
        }

    }
    protected void loadCaseMembers()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs where p.CaseNoId_FK==intCaseID select p;
            chkCaseMembers.DataSource = qry;
            chkCaseMembers.DataTextField = "FIRST_NAME";
            chkCaseMembers.DataValueField = "IndividualID";
            chkCaseMembers.DataBind();
        }
    }

    protected void saveCaseMembers()
    {
        for (int i = 0; i < chkCaseMembers.Items.Count; i++)
        {
            if (chkCaseMembers.Items[i].Selected==true )
            {
                updateCaseMembers(Convert.ToInt32  ( chkCaseMembers.Items[i].Value));
            }
            else
            {
                removeCaseMembers(Convert.ToInt32  ( chkCaseMembers.Items[i].Value));
            }
        }
    }
    protected void updateCaseMembers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew=false;
            F_EBPEnrollment oCase = (from c in db.F_EBPEnrollments where c.IndividualID_FK == IndividualID
                                         && c.EBPID_FK ==lngPkID   select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBPEnrollment();
                blNew = true;
                oCase.EBPID_FK = lngPkID;
                oCase.IndividualID_FK = IndividualID;
            }

            if (blNew == true)
            {
                db.F_EBPEnrollments.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();
             
        }
    }
    protected void removeCaseMembers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew=false;
            F_EBPEnrollment oCase = (from c in db.F_EBPEnrollments where c.IndividualID_FK == IndividualID
                                         && c.EBPID_FK ==lngPkID   select c).FirstOrDefault();
            if ((oCase != null))
            {
                db.F_EBPEnrollments.DeleteOnSubmit(oCase);
            }
            db.SubmitChanges();
             
        }
    }
 

    // Case Workers In EBP
    /////////////////
    private void displayCaseWorkers(int intEBPEnrollID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.F_EBPCaseWorkers
                         where pg.EBPEnrollID_FK == intEBPEnrollID
                         select pg;

            foreach (var oCase in oPages)
            {
                for (int i = 0; i < chkCaseWorkers.Items.Count; i++)
                {
                    if (Convert.ToInt32(chkCaseWorkers.Items[i].Value) == oCase.GranteeContactId_FK)
                    {
                        chkCaseWorkers.Items[i].Selected = true;
                    }
                }

            }
        }       
       

    }
    protected void loadCaseWorkers()
    {
        //using (DataClassesDataContext db = new DataClassesDataContext())
        //{
        //    var qry2 = from gc in db.A_Grantee_Contacts
        //              where gc.GranteeId_fk == intGranteeID
        //              select new { gc.GranteeContactId, CaseWorkerName = gc.FirstName + " " + gc.LastName };
        //    chkCaseWorkers.DataSource = qry2;
        //    chkCaseWorkers.DataTextField = "CaseWorkerName";
        //    chkCaseWorkers.DataValueField = "GranteeContactId";
        //    chkCaseWorkers.DataBind();
        //}

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from gc in db.A_Grantee_Contacts
                      where gc.GranteeId_fk == intGranteeID 
                      select new { gc.GranteeContactId, CaseWorkerName = gc.FirstName + " " + gc.LastName };
            ddlCaseWorkers.DataSource = qry;
            ddlCaseWorkers.DataTextField = "CaseWorkerName";
            ddlCaseWorkers.DataValueField = "GranteeContactId";
            ddlCaseWorkers.DataBind();
            ddlCaseWorkers.Items.Insert(0, "Select");

        }

    }

    protected void saveCaseWorkers()
    {
        for (int i = 0; i < chkCaseWorkers.Items.Count; i++)
        {
            if (chkCaseWorkers.Items[i].Selected == true)
            {
                updateCaseWorkers(Convert.ToInt32(chkCaseWorkers.Items[i].Value));
            }
            else
            {
                removeCaseWorkers(Convert.ToInt32(chkCaseWorkers.Items[i].Value));
            }
        }
        
    }
    protected void updateCaseWorkers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            DateTime dtTmp1;
           
            F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
                                     where c.GranteeContactId_FK == IndividualID
                                         && c.EBPEnrollID_FK == lngPkID
                                     select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBPCaseWorker();
                blNew = true;
                oCase.EBPEnrollID_FK = lngPkID;
                oCase.GranteeContactId_FK = IndividualID;
            }


            DateTime.TryParse(txtCaseWorkerStartDt.Text, out dtTmp1);

            if (txtCaseWorkerStartDt.Text != "")
            {
               
                    oCase.CaseWorker_StartDate = dtTmp1;
               
            }

            DateTime.TryParse(txtCaseWorkerExitDt.Text, out dtTmp1);

            if (txtCaseWorkerExitDt.Text != "")
                oCase.CaseWorker_ExitDate = dtTmp1;             



            if (blNew == true)
            {
                db.F_EBPCaseWorkers.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();
            displayCaseWorkerl(lngPkID);
            ClearDates();


        }
    }
    protected void removeCaseWorkers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
                                     where c.GranteeContactId_FK == IndividualID
                                         && c.EBPEnrollID_FK == lngPkID
                                     select c).FirstOrDefault();
            if ((oCase != null))
            {
                db.F_EBPCaseWorkers.DeleteOnSubmit(oCase);
            }
            db.SubmitChanges();

        }
    }
    protected void btnAddCsaeWorker_Click(object sender, EventArgs e)
    {

        updateUser();
        displayRecords();
        lblAddCwMessage.Text = "";
       

    }

    private void ClearDates()
    {
        ddlCaseWorkers.SelectedIndex = 0;
        txtCaseWorkerStartDt.Text = "";
        txtCaseWorkerExitDt.Text = "";
    }
   
    protected void grdCaseWorkerList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RemoveCaseWorker")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                Boolean blNew = false;
                F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
                                         where c.GranteeContactId_FK == intResponseID
                                         select c).FirstOrDefault();
                if ((oCase != null))
                {
                    db.F_EBPCaseWorkers.DeleteOnSubmit(oCase);
                }
                db.SubmitChanges();
            }
        }
        displayCaseWorkerl(lngPkID);
    }
    protected bool NeedPE()
    {
        bool retVal = false;
        return retVal;
    }

    protected void btnEBPClose_Click(object sender, EventArgs e)
    {
      
        if (btnEBPClose.Text != "Cancel EBP Close")
        {
            PEngQuestions();
            chkERatings.SelectedIndex = -1;
            divEngagementRating.Visible = true;
           
            blClose = true;
            btnEBPClose.Text = "Cancel EBP Close";           
        }
        else
        {
            chkERatings.SelectedIndex = -1;
            PEngQuestions();
            removeExitDateAndEnggRating();          
            divEngagementRating.Visible = false ;
            btnSave.Visible = true;
            btnSaveClose.Visible = true;
            blClose = false ;
            btnEBPClose.Text = "Exit Case from EBP";
           
        }
    }

    private void PEngQuestions()
    {
     //--------to hide the Participant Engagement rating questions April 8th-------------
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (!(cboEBP.SelectedItem.Value.ToString() == ""))
            {
                int ebpid = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                var qry4 = from pg in db.A_GranteeEBPs                          
                           where   pg.GranteeID_FK == intGranteeID && pg.EBPID_FK == ebpid
                           select new { pg.FocalEBPYN,pg.EBPID_FK };

                foreach (var c in qry4)
                {
                    if (c.FocalEBPYN == false)
                    {
                        PEngFocalEBP.Visible = false;
                    }
                    else
                    {
                        PEngFocalEBP.Visible = true;
                    }
                }
            }
        }

      //-------------------------------------------------------

    }

    private void removeExitDateAndEnggRating()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;

        
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_ENROLLMENT_EBP oCase = (from c in db.F_ENROLLMENT_EBPs where c.CaseEBPID == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_ENROLLMENT_EBP();
                blNew = true;
                oCase.CaseID_FK = intCaseID;
            }

            if (!(cboEBP.SelectedItem == null))
            {
                if (!(cboEBP.SelectedItem.Value.ToString() == ""))
                {
                    oCase.EBPID_FK = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.EBPID_FK = null;
                }
            }

            DateTime.TryParse(txtEbpenrolldate.Text, out dtTmp);
            if (txtEbpenrolldate.Text != "")
                oCase.EBP_ENROLL_DATE = dtTmp;

            txtEbpexitdate.Text = "";
            DateTime.TryParse(txtEbpexitdate.Text, out dtTmp);
            if (txtEbpexitdate.Text == "")
            {
                oCase.EBP_EXIT_DATE = null;
                oCase.EBP_ENROLL_COMPLETED = null;
            }

            if (blNew == true)
            {
                db.F_ENROLLMENT_EBPs.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();
            lngPkID = oCase.CaseEBPID;
           
            if (txtEbpexitdate.Text == "")
            {
                RemoveEngagementRating();
            }

        }
       
    }
    private void RemoveEngagementRating()
    {

      
            bool blNew = false;
            int strTmp;
            DateTime dtTmp;

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                              
                F_EBP_ParticipantRating oCase = (from c in db.F_EBP_ParticipantRatings
                                                 where c.CaseEBPID_FK == lngPkID && c.ServiceLogID_FK == null  
                                                 select c).FirstOrDefault();

                if ((oCase != null))
                {
                    db.F_EBP_ParticipantRatings.DeleteOnSubmit(oCase);
                }
                db.SubmitChanges();
            }

        }

    protected void grdCaseWorkerList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if ((oUI.intUserRoleID == 1) || (oUI.intUserRoleID == 2))
        {
            e.Row.Cells[7].Visible = false;
            //grdCaseWorkerList.HeaderRow.Cells[5].Visible = false;
        }
    }
}
