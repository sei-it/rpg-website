﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Login2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {


            AppUser  oAU = (from c in db.AppUsers where c.isActive==true && c.sUserID  == txtUserID.Text && c.passcode == txtPassword.Text select c).FirstOrDefault();

            if ((oAU  != null))
            {
                FormsAuthentication.RedirectFromLoginPage(txtUserID.Text, false);
                Session["Userid"] = oAU.Userid;
            }
            else
            {
                litMessage.Text = "Invalid login credentials. Please try again!";
            }



        }

    }
}