﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" EnableEventValidation="false"  AutoEventWireup="true" CodeFile="CaseEdit.aspx.cs" Inherits="CaseEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script>
        $(function () {
            $("#<%=txtEnrollDate.ClientID%>").datepicker({
                 dateFormat: 'mm/dd/yy',
                 minDate: '01/01/2014',
                 maxDate: 'dateToday'
             })

         });

    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h1>Enroll New Case</h1> 
         
    <div class="formLayout"> 
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
        <p>

            <label for="txtCaseID">Case ID</label>

            <asp:TextBox ID="txtCaseID" runat="server" MaxLength="6" AutoPostBack="true" OnTextChanged="txtCaseID_TextChanged"></asp:TextBox>
            <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
        
            <asp:RequiredFieldValidator ID="rfvCaseID" runat="server" ValidationGroup="Save"
                ControlToValidate="txtCaseID" ErrorMessage="Case ID Required" ForeColor="Red">Required!</asp:RequiredFieldValidator>
            
            <%--&nbsp;<asp:RegularExpressionValidator ID="RVCaseID" runat="server" ErrorMessage="Input 6  alphanumeric characters" ValidationGroup="Save"
                ControlToValidate="txtCaseID" ValidationExpression="^[a-zA-Z0-9]{6}$" ForeColor="Red"></asp:RegularExpressionValidator>--%>
        </p>
            </ContentTemplate>
            </asp:UpdatePanel>
        
        <p style="display: none">
            <label for="txtCaseOriginalID">Original Case ID</label>
            <asp:TextBox ID="txtCaseOriginalID" runat="server"></asp:TextBox>
        </p>
        <p>
            <label for="txtSurname">RPG Case Surname </label>

            <asp:TextBox ID="txtSurname" MaxLength="25" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RPG Case Surname required" ForeColor="Red" ControlToValidate="txtSurname" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtEnrollDate">RPG Enrollment Date</label>
            <asp:TextBox ID="txtEnrollDate" runat="server"></asp:TextBox>


            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEnrollDate" ErrorMessage="RPG Enrollment Date required" ForeColor="Red" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="cvRPGEnroll" runat="server"
                ControlToValidate="txtEnrollDate"
                ErrorMessage="RPG Enrollment Date must be greater than or equal to 01/01/2014"
                Operator="GreaterThanEqual" ValidationGroup="Save" SetFocusOnError="True"
                ForeColor="Red" Display="Dynamic" ValueToCompare="01/01/2014" Type="Date">*
            </asp:CompareValidator>
            <asp:CompareValidator ID="cvRCEnroll" runat="server" ValidationGroup="Save" SetFocusOnError="True" ErrorMessage="RPG Enrollment Date must be less than or equal to current date"
                Operator="LessThanEqual" ControlToValidate="txtEnrollDate" ForeColor="Red" Type="date" Display="Dynamic">*</asp:CompareValidator>
        </p>
        <%-- <asp:textbox id="txtEMinDate" runat="server" Visiable="false"></asp:textbox>
         <asp:textbox id="txtEMaxDate" runat="server" Visiable="false"></asp:textbox>--%>


        <p>
            <label for="cboTreatmentControl">Select Treatment OR Comparison</label>
            <%--  <asp:DropDownList id="cboTreatmentControl" runat="server"  RepeatDirection="Horizontal"  > 
		   </asp:DropDownList>--%>
            <asp:RadioButtonList ID="cboTreatmentControl" runat="server" AutoPostBack="false" RepeatDirection="Vertical">
            </asp:RadioButtonList>
        </p>
        <p>
            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;<asp:Button
                ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" CausesValidation="False" />&nbsp;&nbsp;&nbsp; 
                 <asp:Button ID="btnCompleteCaseEnrollmnet" runat="server" Enabled="false" Text="Identify Case Member Relationships" OnClick="btnCompleteCaseEnrollmnet_Click" />
        </p>
        <p>
            <asp:Literal ID="litMessage" Visible="true" runat="server"></asp:Literal>
        </p>
        <p>
            &nbsp;<asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />
    </div>
    <h2>Case Members</h2>
    <div>

        <asp:GridView ID="grdVwList" runat="server" CssClass="gridTbl"
            AutoGenerateColumns="False" AllowPaging="false"
            AllowSorting="True" OnRowCommand="grdVwList_RowCommand" OnRowDataBound="grdVwList_RowDataBound">
            <Columns>
                <asp:BoundField DataField="IndividualID" DataFormatString="{0:g}" HeaderText="ID" Visible="false">
                    <HeaderStyle Width="50px"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="IND_ID" DataFormatString="{0:g}" HeaderText=" Individual ID">
                    <HeaderStyle Width="50px"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="FIRST_NAME" HeaderText="Individual Name" DataFormatString="{0:g}">
                    <HeaderStyle Width="125px"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="PersonType" HeaderText="Person Type" DataFormatString="{0:g}">
                    <HeaderStyle Width="50px"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:MM/dd/yyyy}">
                    <HeaderStyle Width="75px"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Gender" HeaderText="Gender" DataFormatString="{0:g}">
                    <HeaderStyle Width="50px"></HeaderStyle>
                </asp:BoundField>

            <asp:TemplateField>
                    <HeaderTemplate>
                           Race                     
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Repeater ID="repRace" runat="server" >
                            <HeaderTemplate>                              
                            </HeaderTemplate>
                            <ItemTemplate>                             
                                
                                <%# Eval("RACE") %>
                                 
                            </ItemTemplate> 
                         </asp:Repeater>
                    </ItemTemplate>
                     <HeaderStyle width="250px"/>
              </asp:TemplateField>  


		            <asp:BoundField DataField="Ethnicity" HeaderText="Ethnicity" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="175px"></HeaderStyle>
		            </asp:BoundField>
    
                     <asp:TemplateField>
                       <HeaderTemplate>                        
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" visible="true"  CssClass="otherstyle" CommandArgument='<%# Eval("IndividualID") %>'>View/ Edit</asp:LinkButton>
                                                         
                            </ItemTemplate>
                           <HeaderStyle width="50px" />
                        </asp:TemplateField>  

                 <asp:TemplateField>
                       <HeaderTemplate>                        
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnView" runat="server" CommandName="View" visible="false"  CssClass="otherstyle" CommandArgument='<%# Eval("IndividualID") %>'>View</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="50px" />
                        </asp:TemplateField>  

                  <asp:TemplateField>
                       <HeaderTemplate>                        
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnDelete" runat="server" CommandName="Delete" visible="false"  CssClass="otherstyle" CommandArgument='<%# Eval("IndividualID") %>'>Delete</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="50px" />
                        </asp:TemplateField>  

                

            </Columns>
        </asp:GridView>
        <asp:Button ID="btnNew" runat="server" ValidationGroup="Save"  Text="Add Case Member" OnClick="btnNew_Click" />
       
    </div>
</asp:Content>

