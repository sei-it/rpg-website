﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EBPEdit.aspx.cs" Inherits="EBPEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#<%=txtEbpenrolldate.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
                 $("#<%=txtEbpexitdate.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
                 $("#<%=txtCaseWorkerStartDt.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
                 $("#<%=txtCaseWorkerExitDt.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
             });

             function ValidateCaseMembersCheckBoxList(sender, args) {
                 var checkBoxList = document.getElementById("<%=chkCaseMembers.ClientID %>");
                 var checkboxes = checkBoxList.getElementsByTagName("input");
                 var isValid = false;
                 for (var i = 0; i < checkboxes.length; i++) {
                     if (checkboxes[i].checked) {
                         isValid = true;
                         break;
                     }
                 }
                 args.IsValid = isValid;
             }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Manage EBP Enrollment and Exit</h1>
    <h2>Case ID:
        <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname:
        <asp:Literal ID="litSurname" runat="server"></asp:Literal></h2>
    <p>
        <asp:Literal ID="litMessagePermissions" runat="server"></asp:Literal>

    </p>
    <div class="formLayout">
        <p>
            <label for="cboEBP">EBP Name</label>
            <asp:DropDownList ID="cboEBP" runat="server" RepeatDirection="Horizontal">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rqvcboEBPt" runat="server" ValidationGroup="Save1"
                ControlToValidate="cboEBP" ErrorMessage="EBP Name is required"
                ForeColor="Red">Required</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtEbpenrolldate">EBP Enrollment Date</label>
            <asp:TextBox ID="txtEbpenrolldate" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqEbpenrolldate" runat="server" ValidationGroup="Save1"
                ControlToValidate="txtEbpenrolldate" ErrorMessage="EBP Enrollment Date is required"
                ForeColor="Red">Required</asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:CompareValidator ID="cvBPMinEnroll" runat="server"
                ControlToValidate="txtEbpenrolldate"
                ErrorMessage="EBP Enrollment Date must be greater than or equal to 01/01/2014"
                Operator="GreaterThanEqual" ValidationGroup="Save1" SetFocusOnError="True"
                ForeColor="Red" Display="Dynamic" ValueToCompare="01/01/2014" Type="Date">
            </asp:CompareValidator><br />
            <asp:CompareValidator ID="cvBPMaxEnroll" runat="server" ValidationGroup="Save1" SetFocusOnError="True" ErrorMessage="EBP Enrollment Date must be less than or equal to current date"
                Operator="LessThanEqual" ControlToValidate="txtEbpenrolldate" ForeColor="Red" Type="date" Display="Dynamic"></asp:CompareValidator><br />
            <asp:CompareValidator ID="cvEbpEnrollDt" runat="server" ValidationGroup="Save1" SetFocusOnError="True" ErrorMessage="EBP Enrollment Date must be greater than or equal to RPG Enrollment Date"
                Operator="GreaterThanEqual" ControlToValidate="txtEbpenrolldate" ForeColor="Red" Type="date" Display="Dynamic"></asp:CompareValidator>
            <asp:TextBox ID="txtDtCmp" runat="server" Visible="false"></asp:TextBox>


        </p>
        <p>
            <asp:Button ID="btnEBPClose" runat="server" Enabled="false" Text="Exit Case from EBP" OnClick="btnEBPClose_Click" />&nbsp;</p>

    </div>
    <div class="formLayout" id="divEngagementRating" runat="server" visible="false">
        <h3>EBP Exit</h3>
        <asp:Label ID="litEBPMeassage" runat="server" ForeColor="red"></asp:Label>

        <p>
            <label for="txtEbpexitdate">EBP Exit Date</label>
            <asp:TextBox ID="txtEbpexitdate" runat="server"></asp:TextBox>
        </p>
        <%-- <p>
                <asp:CompareValidator 
                    ID="cvEBPExitDate" 
                    runat="server"  
                    ValidationGroup="Save1" 
                    SetFocusOnError="True" 
                    ErrorMessage="EBP Exit date should be greater than or equal to Date of Service"
                    Operator="GreaterThanEqual" 
                    ControlToValidate="txtEbpexitdate" 
                    ForeColor="Red" 
                    Type="date" 
                    Display="Dynamic">*</asp:CompareValidator>
         </p>--%>

        <%--  <asp:RequiredFieldValidator ID="reqEbpexitdate" runat="server" ValidationGroup="Save1"
                     ControlToValidate="txtEbpexitdate" ErrorMessage="EBP Exit Date is required"
                     ForeColor="Red">Required</asp:RequiredFieldValidator>--%>

        <p>
            <asp:CompareValidator
                ID="cmpEBPexitdate"
                ControlToCompare="txtEbpenrolldate"
                ControlToValidate="txtEbpexitdate"
                Type="Date"
                Operator="GreaterThanEqual"
                ForeColor="Red"
                ValidationGroup="Save1"
                ErrorMessage="EBP Exit Date must be greater than or equal to EBP Enrollment Date"
                runat="server">EBP Exit Date must be greater than or equal to EBP Enrollment Date</asp:CompareValidator><br />

            <asp:CompareValidator
                ID="cmpEBPMinExitDate"
                runat="server"
                ControlToValidate="txtEbpexitdate"
                ErrorMessage="EBP Exit Date must be less than or equal to current date"
                Operator="LessThanEqual"
                ValidationGroup="Save1"
                SetFocusOnError="True"
                ForeColor="Red"
                Display="Dynamic"
                Type="Date">EBP Exit Date must be less than or equal to current date</asp:CompareValidator><br />

            <asp:CompareValidator
                ID="cvCaseWStartDt"
                ControlToValidate="txtEbpexitdate"
                Type="Date"
                Operator="GreaterThanEqual"
                ForeColor="Red"
                ValidationGroup="Save1"
                Display="Dynamic"
                SetFocusOnError="True"
                ErrorMessage="EBP Exit Date must be greater than or equal to Caseworker Start Date"
                runat="server">EBP Exit Date must be greater than or equal to Caseworker Start Date</asp:CompareValidator><br />

            <asp:CompareValidator
                ID="cvEBPExitCaseWrkEndDt"
                runat="server"
                ControlToValidate="txtEbpexitdate"
                ErrorMessage="EBP Exit Date must be greater than or equal to Date of Service"
                Operator="GreaterThanEqual"
                ValidationGroup="Save1"
                SetFocusOnError="True"
                ForeColor="Red"
                Display="Dynamic"
                Type="Date">EBP Exit date must be greater than or equal to Date of Service</asp:CompareValidator>
        </p>
        <div id="PEngFocalEBP" runat="server">
            <h3>Participant Engagement Rating</h3>
            <asp:RequiredFieldValidator ID="reqPartEnggRating" runat="server" ValidationGroup="Save1"
                ControlToValidate="chkERatings" ErrorMessage="Participant Engagement Rating is required"
                ForeColor="Red">Required</asp:RequiredFieldValidator>
            <p style="width: 900px">Rate the specified case's engagement to date in evidence based program.(Complete after 2nd Service Log entry for EBP and at EBP Exit Mark only one) </p>

            <asp:RadioButtonList ID="chkERatings" runat="server" RepeatColumns="1"></asp:RadioButtonList>


            <ul>
                <li>
                    <b>4. Participants were consistently highly involved in services:</b> The participants kept most appointments and actively participated in discussions and activities. If homework was assigned, the participants completed it.

                </li>
                <li>
                    <b>3. Participants&#39; involvement varied:</b> The participant(s) sometimes kept appointments and sometimes actively participated in discussions and activities. If homework was assigned, the participants sometimes completed it. At other times, the participants&#39; involvement was low.</li>
                <li>
                    <b>2. Participants&#39; involvement was consistently low:</b> The participant(s) kept some appointments but missed or cancelled frequently. The participant(s) rarely actively participated in discussions and activities. If homework was assigned, the participant(s) frequently did not complete it.

                </li>
                <li>
                    <b>1. Participants were minimally or not involved at all:</b> The participant(s) kept few appointments. The participant(s) did not actively participate in discussions and activities. If homework was assigned, the participant(s) did not complete it.</li>

            </ul>
        </div>
    </div>
    <h2>Case Members Participating in EBP   </h2>
    <asp:CustomValidator ID="cvalParticipating" ErrorMessage="Please select at least one case member."
        ForeColor="Red" ClientValidationFunction="ValidateCaseMembersCheckBoxList" runat="server" ValidationGroup="Save1">Required</asp:CustomValidator>

    <div>
        <asp:CheckBoxList ID="chkCaseMembers" runat="server"></asp:CheckBoxList>

    </div>

    <h2>Caseworkers</h2>
    <div>


        <asp:GridView ID="grdCaseWorkerList" Visible="true" AutoGenerateColumns="False" CssClass="gridTbl"
            AutoPostBack="true" OnRowCommand="grdCaseWorkerList_RowCommand" runat="server" OnRowDataBound="grdCaseWorkerList_RowDataBound">
            <Columns>

                <asp:BoundField Visible="false" DataField="EBPAssignedCaseWorkerID" HeaderText="GranteeContactID" DataFormatString="{0:g}">
                    <HeaderStyle Width="10px"></HeaderStyle>
                </asp:BoundField>

                <asp:BoundField DataField="CaseWorkerName" HeaderText="Name" DataFormatString="{0:g}">
                    <HeaderStyle Width="150px"></HeaderStyle>
                </asp:BoundField>

                <asp:BoundField DataField="Email" HeaderText="Email" DataFormatString="{0:g}">
                    <HeaderStyle Width="100px"></HeaderStyle>
                </asp:BoundField>

                <asp:BoundField DataField="Phone" HeaderText="Phone #" DataFormatString="{0:g}">
                    <HeaderStyle Width="100px"></HeaderStyle>
                </asp:BoundField>

                <asp:BoundField DataField="CaseWorker_StartDate" HeaderText="CaseWorker Start Date" DataFormatString="{0:d}">
                    <HeaderStyle Width="100px"></HeaderStyle>
                </asp:BoundField>

                <asp:BoundField DataField="CaseWorker_ExitDate" HeaderText="CaseWorker End Date" DataFormatString="{0:d}">
                    <HeaderStyle Width="100px"></HeaderStyle>
                </asp:BoundField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        View/Edit                    
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="imgBtnEdits" runat="server" CommandName="View"
                            Visible="true" CssClass="otherstyle" CommandArgument='<%# Eval("EBPAssignedCaseWorkerID") %>'><%# getButtonName((int)Eval("EBPAssignedCaseWorkerID")) %></asp:LinkButton>

                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                </asp:TemplateField>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Remove                    
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="imgBtnRemove" runat="server" CommandName="RemoveCaseWorker" OnClientClick="return confirm('Are you sure you want to delete this record')"
                            Visible='<%# isCaseworkerinUse(Eval("Email").ToString()) %>' CssClass="otherstyle" CommandArgument='<%# Eval("EBPAssignedCaseWorkerID") %>'>Remove</asp:LinkButton>

                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>


    </div>

    <div id="divAssignCaseworker" runat="server">
        <h2>Assign Caseworker(s)</h2>
        <div>
            <p>
                <asp:CheckBoxList ID="chkCaseWorkers" runat="server" Visible="false"></asp:CheckBoxList>
            </p>

            <p>



                <asp:DropDownList ID="ddlCaseWorkers" runat="server" Width="130" OnSelectedIndexChanged="ddlCaseWorkers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                <label for="txtCaseWorkerStartDt">Start Date</label>
                <asp:TextBox ID="txtCaseWorkerStartDt" Width="75" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCaseWorkerStartDt" runat="server" ValidationGroup="Save1" Visible="false"
                    ControlToValidate="txtCaseWorkerStartDt" ErrorMessage="Caseworker Start Date is required"
                    ForeColor="Red">Required</asp:RequiredFieldValidator>


                <asp:CompareValidator ID="cmpVSdt" ControlToCompare="txtEbpenrolldate" ControlToValidate="txtCaseWorkerStartDt"
                    Type="Date" Operator="GreaterThanEqual" ForeColor="Red" ValidationGroup="Save1"
                    ErrorMessage="Caseworker Start Date should be greater than or equal to EBP Enrollment Date" runat="server">*</asp:CompareValidator>
                <asp:CompareValidator ID="cmpVSdt2" ControlToCompare="txtEbpexitdate" ControlToValidate="txtCaseWorkerStartDt"
                    Type="Date" Operator="LessThanEqual" ForeColor="Red" ValidationGroup="Save1"
                    ErrorMessage="Caseworker Start Date should be less than or equal to EBP Exit Date" runat="server">*</asp:CompareValidator>



                <label for="txtCaseWorkerExitDt">End Date</label>
                <asp:TextBox ID="txtCaseWorkerExitDt" Width="75" runat="server"></asp:TextBox>

                <%--<asp:RequiredFieldValidator ID="rqvCaseWorkerExitDt" runat="server" ValidationGroup="Save1"
             ControlToValidate="txtCaseWorkerExitDt" ErrorMessage="CaseWorker Exit date is required"
             ForeColor="Red">*</asp:RequiredFieldValidator>--%>

                <asp:CompareValidator ID="cmpVal1" ControlToCompare="txtCaseWorkerStartDt" ControlToValidate="txtCaseWorkerExitDt"
                    Type="Date" Operator="GreaterThanEqual" ForeColor="Red" ValidationGroup="Save1"
                    ErrorMessage="Caseworker End Date should be greater than or equal to Caseworker Start Date" runat="server">*</asp:CompareValidator>
                <asp:CompareValidator ID="cmpVal2" ControlToCompare="txtEbpexitdate" ControlToValidate="txtCaseWorkerExitDt"
                    Type="Date" Operator="LessThanEqual" ForeColor="Red" ValidationGroup="Save1"
                    ErrorMessage="Caseworker End Date should be less than or equal to EBP Exit Date" runat="server">*</asp:CompareValidator>


                <asp:Button ID="btnAddCsaeWorker" ValidationGroup="Save1" runat="server" Text="Add caseworker assignment" OnClick="btnAddCsaeWorker_Click" /><br />
                <asp:Label ID="lblSelectCWMesage" runat="server" ForeColor="Red"></asp:Label>
            </p>
            <p>

                <asp:CompareValidator ID="cvCWMaxEDt" runat="server" ValidationGroup="Save1" SetFocusOnError="True" ErrorMessage="Caseworker Start Date must be less than or equal to current date"
                    Operator="LessThanEqual" ControlToValidate="txtCaseWorkerStartDt" ForeColor="Red" Type="date" Display="Dynamic"></asp:CompareValidator><br />

                <asp:CompareValidator ID="cwEndMaxDate" runat="server" ValidationGroup="Save1" SetFocusOnError="True" ErrorMessage="Caseworker End Date must be less than or equal to current date"
                    Operator="LessThanEqual" ControlToValidate="txtCaseWorkerExitDt" ForeColor="Red" Type="date" Display="Dynamic"></asp:CompareValidator>



            </p>

        </div>

    </div>
    <p>
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="Save1" />&nbsp;
                <asp:Button ID="btnSaveClose" runat="server" Visible="false" Text="Save And Close" OnClick="btnSaveClose_Click" ValidationGroup="Save1" />&nbsp;                  
                <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 

                   <asp:ValidationSummary ID="valSumm2" runat="server" ForeColor="Red" ValidationGroup="Save1" HeaderText="Please fill in the correct dates for required fields."></asp:ValidationSummary>

        <asp:ValidationSummary ID="vsum1" runat="server" ForeColor="Red" ValidationGroup="Save" HeaderText="Please fill in the correct dates for required fields."></asp:ValidationSummary>

    </p>
    <p>
        <asp:Label ID="lblAddCwMessage" runat="server" ForeColor="Red"></asp:Label>
        <asp:Literal ID="litMessage" runat="server"></asp:Literal>

    </p>
</asp:Content>

