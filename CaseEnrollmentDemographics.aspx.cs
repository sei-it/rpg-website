﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
 


public partial class CaseEnrollmentDemographics : System.Web.UI.Page
{
    int lngPkID;
    int IdenRelationship;
    int lngCaseID;
    int ValInd;
    string ErrorMsg1;
    int year;
    int day;
    UserInfo oUI;
    string RPGenrollmentDate;
    string RPGenrollmentDOB;
    string RPGenrollmentMonth;
    string RPGenrollmentDay;
    string CurrentDateMonth;
    string CurrentDateDay;
    string CurrentDateYear;
    Boolean disabled = false;
    int dobYear1;
    int maxdobYear1;
    int year1=1;
    bool bldobMonth = false;
    bool bldobDay = false;
    int SelectedMonth;
    int selectvalue;

    int dobbottomYear=1;
    int dobtopYear=1;
    int dobbottomMonth=1;
    int dobtopMonth=1;
    int EnrollYear=1;

    protected void Page_Load(object sender, EventArgs e)
    {
        txtDOB.Attributes.Add("readonly", "readonly");  
        string strJS = null;
        hfTodayDate.Value = DateTime.Now.ToShortDateString();

        if (!IsPostBack)
        {
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            if (Page.Request.QueryString["ErrorMsg"] != null)
            {
                lblDateError.Text = Page.Request.QueryString["ErrorMsg"].ToString();
            }
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["lngCaseID"] != null)
            {
                lngCaseID = Convert.ToInt32(Page.Request.QueryString["lngCaseID"]);
                hfEditMode.Value = "1";
            }
            if (Page.Request.QueryString["IdenRelationship"] != null)
            {
                IdenRelationship = Convert.ToInt32(Page.Request.QueryString["IdenRelationship"]);
            }


            if (Page.Request.QueryString["ErrorMsg1"] != null)
            {
                lblError1.Text = Page.Request.QueryString["ErrorMsg1"].ToString();
            }
            if (Page.Request.QueryString["ValInd"] != null)
            {
                ValInd = Convert.ToInt32(Page.Request.QueryString["ValInd"]);
            }
            loadrecords();
            loadDropdown();
             
        }
        //loadrecords();
        if (ViewState["IsLoaded1"] == null)
        {            
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

        Page.MaintainScrollPositionOnPostBack = true;

    }

    protected void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            
            object objValueYear;          

            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == lngCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
                objValueYear = oCase.RPG_ENROLL_DATE;
              
                if (objValueYear != null)
                {
                    RPGenrollmentDate = String.Format("{0:MM/dd/yyyy}", objValueYear);
                    hfEnrollDate.Value = String.Format("{0:MM/dd/yyyy}", objValueYear);
                    
                    objValueYear = DateTime.Parse(RPGenrollmentDate).Year;
                    lblEnrollYear.Text = objValueYear.ToString();
                  
                }

            }
        }
    }
    protected void displayRecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var qryFlag = from Flag in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                          where Flag.CaseNoId_FK == lngCaseID
                          select Flag;

            foreach (var oCase in qryFlag)
            {
                if (oCase.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED == true)
                {

                    if (oUI.intUserRoleID == 3)// added on 27th june
                    {
                        btnSave.Visible = true;
                        // btnSaveandClose.Visible = false;
                        DisplayAllRecords();
                    }
                    else
                    {
                        btnSave.Visible = false;
                        btnSaveandClose.Visible = false;
                        DisabledAllItems();
                    }

                }
                else
                {
                    DisplayAllRecords();
                }

            }
        }
    }    


    private void DisplayAllRecords()
    {
        object objVal = null;
        divAdultOnly.Visible = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            object objValue;
            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == lngCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }

                //--added on 13th july
                objValue = oCase.RPG_ENROLL_DATE;
                if (objValue != null)
                {
                    RPGenrollmentDate = String.Format("{0:MM/dd/yyyy}", objValue);
                    objVal = DateTime.Parse(RPGenrollmentDate).Year;
                    lblEnrollYear.Text = objVal.ToString();

                    objVal = DateTime.Now.Month;
                    //  objVal = DateTime.Now.ToString("MM");
                    lblNowMonth.Text = objVal.ToString();


                }
            }


            var oPages = from pg in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                         where pg.IndividualID == lngPkID
                         select pg;
            foreach (var oCase in oPages)
            {




                objVal = oCase.IND_ID;
                if (objVal != null)
                {
                    txtIndid.Text = objVal.ToString();
                }

                objVal = oCase.FIRST_NAME;
                if (objVal != null)
                {
                    txtFirstname.Text = objVal.ToString();
                }
                objVal = oCase.FIRST_NAME;
                if (objVal != null)
                {
                    txtFirstname.Text = objVal.ToString();
                }

                objVal = oCase.DOB;
                if (objVal != null)
                {
                    txtDOB.Text = String.Format("{0:MM/dd/yyyy}", objVal);
                }


                //added by varsha on feb 21

                //objVal = oCase.DOB;
                //if (objVal != null)
                //{

                //    DateTime d = Convert.ToDateTime(objVal);
                //    DateTime dt = new DateTime();
                //    dt = d;
                //    int day = dt.Day;
                //    int month = dt.Month;
                //    int year = dt.Year;
                //    //ddlDay.SelectedValue = day.ToString();
                //    //ddlMonth.SelectedValue = month.ToString();
                //    //ddlYear.SelectedItem.Text = year.ToString();
                //    //DateOfBirth_Selection(ddlYear.SelectedItem.Text); // added on july 22nd 
                //    //DayYear_Dispaly(ddlYear.SelectedItem.Text);
                //    //MonthDayYear_Dispaly(ddlMonth.SelectedValue);


                //}


                objVal = oCase.GENDER_fk;
                if (objVal != null)
                {
                    rblGender.SelectedIndex = rblGender.Items.IndexOf(rblGender.Items.FindByValue(oCase.GENDER_fk.ToString()));
                }
                objVal = oCase.RACE_AMER_IND_OR_AK_NAT;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkRaceamerindoraknat.Checked = true;
                    }
                    else
                    {
                        chkRaceamerindoraknat.Checked = false;
                    }
                }
                objVal = oCase.RACE_ASIAN;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkRaceasian.Checked = true;
                    }
                    else
                    {
                        chkRaceasian.Checked = false;
                    }
                }
                objVal = oCase.RACE_BLACK;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkRaceblack.Checked = true;
                    }
                    else
                    {
                        chkRaceblack.Checked = false;
                    }
                }
                objVal = oCase.RACE_NAT_HI_OR_OTH_PAC_ISLE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkRacenathiorothpacisle.Checked = true;
                    }
                    else
                    {
                        chkRacenathiorothpacisle.Checked = false;
                    }
                }
                objVal = oCase.RACE_WHITE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkRacewhite.Checked = true;
                    }
                    else
                    {
                        chkRacewhite.Checked = false;
                    }
                }
                objVal = oCase.ETHNICITY_FK;
                if (objVal != null)
                {
                    rblEthnicity.SelectedIndex = rblEthnicity.Items.IndexOf(rblEthnicity.Items.FindByValue(oCase.ETHNICITY_FK.ToString()));
                }
                objVal = oCase.PRIMARY_HOME_LANG_FK;
                if (objVal != null)
                {
                    rblPrimaryhomelang.SelectedIndex = rblPrimaryhomelang.Items.IndexOf(rblPrimaryhomelang.Items.FindByValue(oCase.PRIMARY_HOME_LANG_FK.ToString()));
                }
                objVal = oCase.PRIMARY_HOME_LANG_SPEC;
                if (objVal != null)
                {
                    txtPrimaryhomelangspec.Text = objVal.ToString();
                }
                objVal = oCase.PERSON_TYPE_FK;
                if (objVal != null)
                {
                    rblPersontype.SelectedIndex = rblPersontype.Items.IndexOf(rblPersontype.Items.FindByValue(oCase.PERSON_TYPE_FK.ToString()));
                    if (oCase.PERSON_TYPE_FK.ToString() == "1")
                    {
                        divAdultOnly.Visible = true;

                    }
                    else
                    {
                        divAdultOnly.Visible = false;

                    }
                }
                objVal = oCase.CURRENT_RESIDENCE_FK;
                if (objVal != null)
                {
                    rblCurrentresidence.SelectedIndex = rblCurrentresidence.Items.IndexOf(rblCurrentresidence.Items.FindByValue(oCase.CURRENT_RESIDENCE_FK.ToString()));
                }

                //added by var on feb 18th


                objVal = oCase.CURRENT_RESIDENCE_SPECIFY_IF_OTHER;
                if (objVal != null)
                {
                    txtCurrentresidencOther.Text = objVal.ToString();
                }
                //-------



                objVal = oCase.HIGHEST_EDUCATION_FK;
                if (objVal != null)
                {
                    rblHighesteducation.SelectedIndex = rblHighesteducation.Items.IndexOf(rblHighesteducation.Items.FindByValue(oCase.HIGHEST_EDUCATION_FK.ToString()));
                }
                objVal = oCase.INCOME_LEVEL_FK;
                if (objVal != null)
                {
                    rblIncomelevel.SelectedIndex = rblIncomelevel.Items.IndexOf(rblIncomelevel.Items.FindByValue(oCase.INCOME_LEVEL_FK.ToString()));
                }
                objVal = oCase.INCOME_SOURCE_WAGE_SALARY;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkIncomesourcewagesalary.Checked = true;
                    }
                    else
                    {
                        chkIncomesourcewagesalary.Checked = false;
                    }
                }
                objVal = oCase.INCOME_SOURCE_PUB_ASS;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkIncomesourcepubass.Checked = true;
                    }
                    else
                    {
                        chkIncomesourcepubass.Checked = false;
                    }
                }
                objVal = oCase.INCOME_SOURCE_RETIR_PENS;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkIncomesourceretirpens.Checked = true;
                    }
                    else
                    {
                        chkIncomesourceretirpens.Checked = false;
                    }
                }
                objVal = oCase.INCOME_SOURCE_DISAB;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkIncomesourcedisab.Checked = true;
                    }
                    else
                    {
                        chkIncomesourcedisab.Checked = false;
                    }
                }
                objVal = oCase.INCOME_SOURCE_OTHER;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkIncomesourceother.Checked = true;
                    }
                    else
                    {
                        chkIncomesourceother.Checked = false;
                    }
                }
                objVal = oCase.INCOME_SOURCE_NONE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkIncomesourcenone.Checked = true;
                    }
                    else
                    {
                        chkIncomesourcenone.Checked = false;
                    }
                }


                //added by var on feb 18th

                objVal = oCase.INCOME_SOURCE_SPECIFY_IF_OTHER;
                if (objVal != null)
                {
                    txtIncomesourceother.Text = objVal.ToString();
                }


                //-----------------



                objVal = oCase.EMPLOYMENT_STATUS_FK;
                if (objVal != null)
                {
                    rblEmploymentstatus.SelectedIndex = rblEmploymentstatus.Items.IndexOf(rblEmploymentstatus.Items.FindByValue(oCase.EMPLOYMENT_STATUS_FK.ToString()));
                }
                objVal = oCase.RELATIONSHIP_STATUS_FK;
                if (objVal != null)
                {
                    rblRelationshipstatus.SelectedIndex = rblRelationshipstatus.Items.IndexOf(rblRelationshipstatus.Items.FindByValue(oCase.RELATIONSHIP_STATUS_FK.ToString()));
                }


            }

            db.Dispose();
        }
     }
    
    //private void DayYear_Dispaly(string yeardispaly)
    //{
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {

    //        if (!(ddlMonth.SelectedItem.Value == "Select"))
    //        {
    //            if (!(ddlYear.SelectedItem.Value == "Select"))
    //            {
    //                //year = Convert.ToInt32(ddlYear.SelectedValue.ToString());

    //                //year = Convert.ToInt32(ddlYear.SelectedItem.ToString());

    //                year = Convert.ToInt32(yeardispaly);
    //                year = year % 4;
    //            }

    //            int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);

    //            if (!(ddlDay.SelectedItem.Value == "Select"))
    //            {
    //                day = Convert.ToInt32(ddlDay.SelectedItem.Value);
    //            }

    //            if ((year == 0) && (month == 2) && (day == 28))
    //            {

    //                var qry10 = from p in db.C_Days
    //                            where (p.Days >= 1 && p.Days <= 29)
    //                            select p;
    //                ddlDay.DataSource = qry10;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");

    //            }
    //        }

    //        ddlYear.SelectedItem.Text = yeardispaly;
    //    }
    //}
    //private void MonthDayYear_Dispaly(string monthdisplay)
    //{
    //       string ThisMonthNum = DateTime.Now.ToString("MM");
    //        int month1 = Convert.ToInt32(ThisMonthNum) + 9;
    //        int nextmonth = month1 - 12;
    //        string ThisYearNum = DateTime.Now.ToString("yyyy");
    //        int year2 = Convert.ToInt32(ThisYearNum) + 1;

    //        int LeapYear2= year2 % 4;
    //        string TodayNum = DateTime.Now.ToString("dd");

    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        if (!(ddlMonth.SelectedItem.Value == "Select"))
    //        {
    //            if (!(ddlYear.SelectedItem.Value == "Select"))
    //            {
    //                //year = Convert.ToInt32(ddlYear.SelectedValue.ToString());

    //                year1 = Convert.ToInt32(ddlYear.SelectedItem.ToString());
    //                year1 = year1 % 4;
    //            }
    //        }

    //        //----------------------
         

    //        if (!(ddlYear.SelectedItem.Text == "Select"))
    //        {
    //            if (Convert.ToInt32(ddlYear.SelectedItem.Text) == year2)
    //            {
    //                var qry14 = from p in db.C_Months
    //                            where (p.MonthID <= nextmonth)
    //                            select p;
    //                ddlMonth.DataSource = qry14;
    //                ddlMonth.DataTextField = "Months";
    //                ddlMonth.DataValueField = "MonthID";
    //                ddlMonth.DataBind();
    //                ddlMonth.Items.Insert(0, "Select");                   

    //            }
    //            else
    //            {
    //                var qry15 = from p in db.C_Months select p;
    //                ddlMonth.DataSource = qry15;
    //                ddlMonth.DataTextField = "Months";
    //                ddlMonth.DataValueField = "MonthID";
    //                ddlMonth.DataBind();
    //                ddlMonth.Items.Insert(0, "Select");
    //            }

    //        }

    //        //---------------------
    //        if (!(ddlYear.SelectedItem.Text == "Select"))
    //        {
    //            if (Convert.ToInt32(ddlYear.SelectedItem.Text) != year2)
    //            {

    //                if (!(ddlMonth.SelectedItem.Value == "Select"))
    //                {
    //                    // int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);

    //                    int month = Convert.ToInt32(monthdisplay);
    //                    if (month == 2)
    //                    {

    //                        if (year1 == 0)
    //                        {
    //                            var qry10 = from p in db.C_Days
    //                                        where (p.Days >= 1 && p.Days <= 29)
    //                                        select p;
    //                            ddlDay.DataSource = qry10;
    //                            ddlDay.DataTextField = "Days";
    //                            ddlDay.DataValueField = "DayID";
    //                            ddlDay.DataBind();
    //                            ddlDay.Items.Insert(0, "Select");
    //                        }
    //                        else
    //                        {
    //                            var qry10 = from p in db.C_Days
    //                                        where (p.Days >= 1 && p.Days <= 28)
    //                                        select p;
    //                            ddlDay.DataSource = qry10;
    //                            ddlDay.DataTextField = "Days";
    //                            ddlDay.DataValueField = "DayID";
    //                            ddlDay.DataBind();
    //                            ddlDay.Items.Insert(0, "Select");
    //                        }
    //                    }
    //                    else if ((month == 4) || (month == 6) || (month == 11) || (month == 9))
    //                    {
    //                        var qry10 = from p in db.C_Days
    //                                    where (p.Days >= 1 && p.Days <= 30)
    //                                    select p;
    //                        ddlDay.DataSource = qry10;
    //                        ddlDay.DataTextField = "Days";
    //                        ddlDay.DataValueField = "DayID";
    //                        ddlDay.DataBind();
    //                        ddlDay.Items.Insert(0, "Select");
    //                    }
    //                    else
    //                    {
    //                        var qry10 = from p in db.C_Days
    //                                    where (p.Days >= 1 && p.Days <= 31)
    //                                    select p;
    //                        ddlDay.DataSource = qry10;
    //                        ddlDay.DataTextField = "Days";
    //                        ddlDay.DataValueField = "DayID";
    //                        ddlDay.DataBind();
    //                        ddlDay.Items.Insert(0, "Select");
    //                    }
    //                }
    //            }
    //            else if ((Convert.ToInt32(ddlYear.SelectedItem.Text) == year2))
    //            {
    //                // int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);

    //                int SelectedMonth = Convert.ToInt32(monthdisplay);
    //                if (Convert.ToInt32(ddlMonth.SelectedItem.Value) == nextmonth)
    //                  {
    //                      //if month is feb and todays date is greater than 28

    //                      if (SelectedMonth == 2)
    //                      {
    //                          if (Convert.ToInt32(TodayNum) <= 28)
    //                          {
    //                              var qry15 = from p in db.C_Days
    //                                          where (p.Days <= Convert.ToInt32(TodayNum))
    //                                          select p;
    //                              ddlDay.DataSource = qry15;
    //                              ddlDay.DataTextField = "Days";
    //                              ddlDay.DataValueField = "DayID";
    //                              ddlDay.DataBind();
    //                              ddlDay.Items.Insert(0, "Select");
    //                          }
    //                          else if (Convert.ToInt32(TodayNum) > 28)
    //                          {
    //                              if ((Convert.ToInt32(TodayNum) == 29) && (LeapYear2 != 0))
    //                              {
    //                                  int value1 = Convert.ToInt32(TodayNum) - 1;
    //                                  var qry15 = from p in db.C_Days
    //                                              where (p.Days <= Convert.ToInt32(value1))
    //                                              select p;
    //                                  ddlDay.DataSource = qry15;
    //                                  ddlDay.DataTextField = "Days";
    //                                  ddlDay.DataValueField = "DayID";
    //                                  ddlDay.DataBind();
    //                                  ddlDay.Items.Insert(0, "Select");
    //                              }
    //                              else if ((Convert.ToInt32(TodayNum) == 29) && (LeapYear2 == 0))
    //                              {
    //                                  int value11 = Convert.ToInt32(TodayNum);
    //                                  var qry15 = from p in db.C_Days
    //                                              where (p.Days <= Convert.ToInt32(value11))
    //                                              select p;
    //                                  ddlDay.DataSource = qry15;
    //                                  ddlDay.DataTextField = "Days";
    //                                  ddlDay.DataValueField = "DayID";
    //                                  ddlDay.DataBind();
    //                                  ddlDay.Items.Insert(0, "Select");
    //                              }
    //                              else if (Convert.ToInt32(TodayNum) == 30)
    //                              {
    //                                  int value2 = Convert.ToInt32(TodayNum) - 2;
    //                                  var qry15 = from p in db.C_Days
    //                                              where (p.Days <= Convert.ToInt32(value2))
    //                                              select p;
    //                                  ddlDay.DataSource = qry15;
    //                                  ddlDay.DataTextField = "Days";
    //                                  ddlDay.DataValueField = "DayID";
    //                                  ddlDay.DataBind();
    //                                  ddlDay.Items.Insert(0, "Select");
    //                              }
    //                              else if (Convert.ToInt32(TodayNum) == 31)
    //                              {
    //                                  int value3 = Convert.ToInt32(TodayNum) - 3;
    //                                  var qry15 = from p in db.C_Days
    //                                              where (p.Days <= Convert.ToInt32(value3))
    //                                              select p;
    //                                  ddlDay.DataSource = qry15;
    //                                  ddlDay.DataTextField = "Days";
    //                                  ddlDay.DataValueField = "DayID";
    //                                  ddlDay.DataBind();
    //                                  ddlDay.Items.Insert(0, "Select");
    //                              }
    //                          }
    //                      }

    //                      // if month is 4 or 6 or 11 or 9 day is (30 days)
    //                      else if ((SelectedMonth == 4) || (SelectedMonth == 6) || (SelectedMonth == 11) || (SelectedMonth == 9))
    //                      {
    //                          if (Convert.ToInt32(TodayNum) > 30)
    //                          {
    //                              int value4 = Convert.ToInt32(TodayNum) - 1;
    //                              var qry15 = from p in db.C_Days
    //                                          where (p.Days <= Convert.ToInt32(value4))
    //                                          select p;
    //                              ddlDay.DataSource = qry15;
    //                              ddlDay.DataTextField = "Days";
    //                              ddlDay.DataValueField = "DayID";
    //                              ddlDay.DataBind();
    //                              ddlDay.Items.Insert(0, "Select");
    //                          }
    //                          else
    //                          {
    //                              var qry15 = from p in db.C_Days
    //                                          where (p.Days <= Convert.ToInt32(TodayNum))
    //                                          select p;
    //                              ddlDay.DataSource = qry15;
    //                              ddlDay.DataTextField = "Days";
    //                              ddlDay.DataValueField = "DayID";
    //                              ddlDay.DataBind();
    //                              ddlDay.Items.Insert(0, "Select");
    //                          }
    //                      }
    //                      //if month is 1 or 3 or 5 or 7 or 8 or 10 or 12 day is (31 days )
    //                      else
    //                      {
    //                          var qry15 = from p in db.C_Days
    //                                      where (p.Days <= Convert.ToInt32(TodayNum))
    //                                      select p;
    //                          ddlDay.DataSource = qry15;
    //                          ddlDay.DataTextField = "Days";
    //                          ddlDay.DataValueField = "DayID";
    //                          ddlDay.DataBind();
    //                          ddlDay.Items.Insert(0, "Select");
    //                      }
    //                  }
    //                  else
    //                  {
    //                      DaysByMonth();
    //                  }

    //            }
               
    //        }                 
    //    }
    //}

    //private void DateOfBirth_Selection(string year)
    //{

    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {

    //        object objVal2;
    //        var oPages1 = from pg in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
    //                      where pg.IndividualID == lngPkID
    //                      select pg;

    //        foreach (var oCase1 in oPages1)
    //        {
    //            objVal2 = oCase1.PERSON_TYPE_FK;
    //            if (objVal2 != null)
    //            {
    //                rblPersontype.SelectedIndex = rblPersontype.Items.IndexOf(rblPersontype.Items.FindByValue(oCase1.PERSON_TYPE_FK.ToString()));
    //                if (oCase1.PERSON_TYPE_FK.ToString() == "1")
    //                {
    //                    //--------Year for Age >= 15 years, 0 months-----//
    //                    // dobYear = dobYear - 15;
    //                    divAdultOnly.Visible = true;

    //                    if (!(String.IsNullOrEmpty(lblEnrollYear.Text)))
    //                    {
    //                       int EnrollYear = Convert.ToInt32(lblEnrollYear.Text);
    //                       dobtopYear = EnrollYear - 15;
    //                        var qry13 = from p in db.C_Years
    //                                    where p.Years <= dobtopYear
    //                                    select p;
    //                        ddlYear.DataSource = qry13;
    //                        ddlYear.DataTextField = "Years";
    //                        ddlYear.DataValueField = "YearID";
    //                        ddlYear.DataBind();
    //                        ddlYear.Items.Insert(0, "Select");
    //                    }

    //                    ddlYear.SelectedItem.Text = year;
    //                }
    //                else
    //                {
    //                    divAdultOnly.Visible = false;
                                   
    //                     //--------Year for Age <= 20 years, 0 months-----//
    //                       // dobYear = dobYear - 20 and max = DobYear;


    //                         if (!(String.IsNullOrEmpty(lblEnrollYear.Text)))
    //                           {
    //                              int EnrollYear = Convert.ToInt32(lblEnrollYear.Text);
    //                              dobbottomYear = EnrollYear - 20;
    //                              int EnrollMoth = 0;
    //                              if(!string.IsNullOrEmpty(hfEnrollDate.Value.Trim()))
    //                              {
    //                                  EnrollMoth = Convert.ToDateTime(hfEnrollDate.Value.Trim()).Month;
    //                              }

    //                              int maxdobYear = Convert.ToInt32(lblEnrollYear.Text);
    //                              var qry13 = from p in db.C_Years
    //                                          where (p.Years >= dobbottomYear && p.Years <= (maxdobYear + 1))
    //                                                 select p;
    //                                 ddlYear.DataSource = qry13;
    //                                 ddlYear.DataTextField = "Years";
    //                                 ddlYear.DataValueField = "YearID";
    //                                 ddlYear.DataBind();
    //                                 ddlYear.Items.Insert(0, "Select");
    //                         }
    //                        ddlYear.SelectedItem.Text = year;                                    
    //                 }
    //            }

    //        }

    //    }
    //}   

    private void DisabledAllItems()
    {
        object objVal = null;
            divAdultOnly.Visible = false;
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                var oCases = from css in db.B_CASE_ENROLLMENTs
                             where css.CaseNoId == lngCaseID
                             select css;
                foreach (var oCase in oCases)
                {
                    litCaseID.Text = oCase.CASE_ID;
                    if (oCase.SURNAME != null)
                    {
                        litSurname.Text = Convert.ToString(oCase.SURNAME);

                    }
                    
                }


                var oPages = from pg in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                             where pg.IndividualID == lngPkID
                             select pg;
                foreach (var oCase in oPages)
                {




                    objVal = oCase.IND_ID;
                    if (objVal != null)
                    {
                        txtIndid.Text = objVal.ToString();
                        txtIndid.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }

                    objVal = oCase.FIRST_NAME;
                    if (objVal != null)
                    {
                        txtFirstname.Text = objVal.ToString();
                        txtFirstname.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.FIRST_NAME;
                    if (objVal != null)
                    {
                        txtFirstname.Text = objVal.ToString();
                        txtFirstname.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.DOB;
                    if (objVal != null)
                    {
                        txtDOB.Text = Convert.ToDateTime(objVal).ToShortDateString();
                        if (hfEditMode.Value=="1") txtDOB.Enabled=false;
                        DateTime d = Convert.ToDateTime(objVal);
                        DateTime dt = new DateTime();
                        dt = d;
                        int day = dt.Day;
                        int month = dt.Month;
                        int year = dt.Year;
                        //ddlDay.SelectedValue = day.ToString();
                        //ddlMonth.SelectedValue = month.ToString();
                        //ddlYear.SelectedItem.Text = year.ToString();
                        //ddlDay.Enabled = false;
                        //ddlMonth.Enabled = false;
                        //ddlYear.Enabled = false;
                       

                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }


                    objVal = oCase.GENDER_fk;
                    if (objVal != null)
                    {
                        rblGender.SelectedIndex = rblGender.Items.IndexOf(rblGender.Items.FindByValue(oCase.GENDER_fk.ToString()));
                        rblGender.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.RACE_AMER_IND_OR_AK_NAT;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkRaceamerindoraknat.Checked = true;
                        }
                        else
                        {
                            chkRaceamerindoraknat.Checked = false;
                        }
                        chkRaceamerindoraknat.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.RACE_ASIAN;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkRaceasian.Checked = true;
                        }
                        else
                        {
                            chkRaceasian.Checked = false;
                        }
                        chkRaceasian.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.RACE_BLACK;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkRaceblack.Checked = true;
                        }
                        else
                        {
                            chkRaceblack.Checked = false;
                        }
                        chkRaceblack.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.RACE_NAT_HI_OR_OTH_PAC_ISLE;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkRacenathiorothpacisle.Checked = true;
                        }
                        else
                        {
                            chkRacenathiorothpacisle.Checked = false;
                        }
                        chkRacenathiorothpacisle.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.RACE_WHITE;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkRacewhite.Checked = true;
                        }
                        else
                        {
                            chkRacewhite.Checked = false;
                        }
                        chkRacewhite.Enabled = false;
                    }
                    else
                    {
                        txtIndid.Enabled = false;
                    }
                    objVal = oCase.ETHNICITY_FK;
                    if (objVal != null)
                    {
                        rblEthnicity.SelectedIndex = rblEthnicity.Items.IndexOf(rblEthnicity.Items.FindByValue(oCase.ETHNICITY_FK.ToString()));
                        rblEthnicity.Enabled = false;
                    }
                    else
                    {
                        rblEthnicity.Enabled = false;
                    }
                    objVal = oCase.PRIMARY_HOME_LANG_FK;
                    if (objVal != null)
                    {
                        rblPrimaryhomelang.SelectedIndex = rblPrimaryhomelang.Items.IndexOf(rblPrimaryhomelang.Items.FindByValue(oCase.PRIMARY_HOME_LANG_FK.ToString()));
                        rblPrimaryhomelang.Enabled = false;
                    }
                    else
                    {
                        rblPrimaryhomelang.Enabled = false;
                    }
                    objVal = oCase.PRIMARY_HOME_LANG_SPEC;
                    if (objVal != null)
                    {
                        txtPrimaryhomelangspec.Text = objVal.ToString();
                        txtPrimaryhomelangspec.Enabled = false;
                    }
                    else
                    {
                        txtPrimaryhomelangspec.Enabled = false;
                    }
                    objVal = oCase.PERSON_TYPE_FK;
                    if (objVal != null)
                    {
                        rblPersontype.SelectedIndex = rblPersontype.Items.IndexOf(rblPersontype.Items.FindByValue(oCase.PERSON_TYPE_FK.ToString()));
                        rblPersontype.Enabled = false;

                        if (oCase.PERSON_TYPE_FK.ToString() == "1")
                        {
                            divAdultOnly.Visible = true;
                        }
                        else
                        {
                            divAdultOnly.Visible = false;
                        }

                    }
                    else
                    {
                        rblPersontype.Enabled = false;
                    }
                    objVal = oCase.CURRENT_RESIDENCE_FK;
                    if (objVal != null)
                    {
                        rblCurrentresidence.SelectedIndex = rblCurrentresidence.Items.IndexOf(rblCurrentresidence.Items.FindByValue(oCase.CURRENT_RESIDENCE_FK.ToString()));
                        rblCurrentresidence.Enabled = false;
                    }
                    else
                    {
                        rblCurrentresidence.Enabled = false;
                    }

                    //added by var on feb 18th


                    objVal = oCase.CURRENT_RESIDENCE_SPECIFY_IF_OTHER;
                    if (objVal != null)
                    {
                        txtCurrentresidencOther.Text = objVal.ToString();
                        txtCurrentresidencOther.Enabled = false;
                    }
                    else
                    {
                        txtCurrentresidencOther.Enabled = false;
                    }

                    //-------



                    objVal = oCase.HIGHEST_EDUCATION_FK;
                    if (objVal != null)
                    {
                        rblHighesteducation.SelectedIndex = rblHighesteducation.Items.IndexOf(rblHighesteducation.Items.FindByValue(oCase.HIGHEST_EDUCATION_FK.ToString()));
                        rblHighesteducation.Enabled = false;
                    }
                    else
                    {
                        rblHighesteducation.Enabled = false;
                    }
                    objVal = oCase.INCOME_LEVEL_FK;
                    if (objVal != null)
                    {
                        rblIncomelevel.SelectedIndex = rblIncomelevel.Items.IndexOf(rblIncomelevel.Items.FindByValue(oCase.INCOME_LEVEL_FK.ToString()));
                        rblIncomelevel.Enabled = false;
                    }
                    else
                    {
                        rblIncomelevel.Enabled = false;
                    }
                    objVal = oCase.INCOME_SOURCE_WAGE_SALARY;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkIncomesourcewagesalary.Checked = true;
                        }
                        else
                        {
                            chkIncomesourcewagesalary.Checked = false;
                        }
                        chkIncomesourcewagesalary.Enabled = false;
                    }
                    else
                    {
                        chkIncomesourcewagesalary.Enabled = false;
                    }
                    objVal = oCase.INCOME_SOURCE_PUB_ASS;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkIncomesourcepubass.Checked = true;
                        }
                        else
                        {
                            chkIncomesourcepubass.Checked = false;
                        }
                        chkIncomesourcepubass.Enabled = false;
                    }
                    else
                    {
                        chkIncomesourcepubass.Enabled = false;
                    }
                    objVal = oCase.INCOME_SOURCE_RETIR_PENS;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkIncomesourceretirpens.Checked = true;
                        }
                        else
                        {
                            chkIncomesourceretirpens.Checked = false;
                        }
                        chkIncomesourceretirpens.Enabled = false;
                    }
                    else
                    {
                        chkIncomesourceretirpens.Enabled = false;
                    }
                    objVal = oCase.INCOME_SOURCE_DISAB;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkIncomesourcedisab.Checked = true;
                        }
                        else
                        {
                            chkIncomesourcedisab.Checked = false;
                        }
                        chkIncomesourcedisab.Enabled = false;
                    }
                    else
                    {
                        chkIncomesourcedisab.Enabled = false;
                    }
                    objVal = oCase.INCOME_SOURCE_OTHER;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkIncomesourceother.Checked = true;
                        }
                        else
                        {
                            chkIncomesourceother.Checked = false;
                        }
                        chkIncomesourceother.Enabled = false;
                    }
                    else
                    {
                        chkIncomesourceother.Enabled = false;
                    }
                    objVal = oCase.INCOME_SOURCE_NONE;
                    if (objVal != null)
                    {
                        if (objVal.ToString() == "True")
                        {
                            chkIncomesourcenone.Checked = true;
                        }
                        else
                        {
                            chkIncomesourcenone.Checked = false;
                        }
                        chkIncomesourcenone.Enabled = false;
                    }
                    else
                    {
                        chkIncomesourcenone.Enabled = false;
                    }


                    //added by var on feb 18th

                    objVal = oCase.INCOME_SOURCE_SPECIFY_IF_OTHER;
                    if (objVal != null)
                    {
                        txtIncomesourceother.Text = objVal.ToString();
                        txtIncomesourceother.Enabled = false;
                    }
                    else
                    {
                        txtIncomesourceother.Enabled = false;
                    }


                    //-----------------



                    objVal = oCase.EMPLOYMENT_STATUS_FK;
                    if (objVal != null)
                    {
                        rblEmploymentstatus.SelectedIndex = rblEmploymentstatus.Items.IndexOf(rblEmploymentstatus.Items.FindByValue(oCase.EMPLOYMENT_STATUS_FK.ToString()));
                        rblEmploymentstatus.Enabled = false;
                    }
                    else
                    {
                        rblEmploymentstatus.Enabled = false;
                    }
                    objVal = oCase.RELATIONSHIP_STATUS_FK;
                    if (objVal != null)
                    {
                        rblRelationshipstatus.SelectedIndex = rblRelationshipstatus.Items.IndexOf(rblRelationshipstatus.Items.FindByValue(oCase.RELATIONSHIP_STATUS_FK.ToString()));
                        rblRelationshipstatus.Enabled = false;
                    }
                    else
                    {
                        rblRelationshipstatus.Enabled = false;
                    }

                }

                db.Dispose();
            }
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        string datestr;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            
            C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE oCase = (from c in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs where c.IndividualID == lngPkID select c).FirstOrDefault();
            //ResourceSubmission oCase = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oCase == null))
            {
                oCase = new C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE();
                blNew = true ;
            }

            oCase.IND_ID = txtIndid.Text;
       

            oCase.FIRST_NAME = txtFirstname.Text;

            if (txtDOB.Text != "")
            {
                DateTime.TryParse(txtDOB.Text, out dtTmp);
                oCase.DOB = dtTmp;
            }


            //added by varsha on feb 21

            //datestr = ddlMonth.SelectedValue + "-" + ddlDay.SelectedValue + "-" + ddlYear.SelectedItem;

            //lblDateError.Text="";
            
            //string ErrorMsg;
            //if (DateTime.TryParse(txtDOB.Text, out dtTmp))
            //{
            //    oCase.DOB = dtTmp;
            //}
            //else
            //{
            //    ErrorMsg = "Please select correct month, day and year";
            //    Response.Redirect("CaseEnrollmentDemographics.aspx?ErrorMsg=" + ErrorMsg + "&lngCaseID=" + lngCaseID);
            //}
           

           
            if (!(rblGender.SelectedItem == null))
            {
                if (!(rblGender.SelectedItem.Value.ToString() == ""))
                {
                    oCase.GENDER_fk = Convert.ToInt32(rblGender.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.GENDER_fk = null;
                }
            }
            if (chkRaceamerindoraknat.Checked == true)
            {
                oCase.RACE_AMER_IND_OR_AK_NAT = true;
            }
            else
            {
                oCase.RACE_AMER_IND_OR_AK_NAT = false;
            }
            if (chkRaceasian.Checked == true)
            {
                oCase.RACE_ASIAN = true;
            }
            else
            {
                oCase.RACE_ASIAN = false;
            }
            if (chkRaceblack.Checked == true)
            {
                oCase.RACE_BLACK = true;
            }
            else
            {
                oCase.RACE_BLACK = false;
            }
            if (chkRacenathiorothpacisle.Checked == true)
            {
                oCase.RACE_NAT_HI_OR_OTH_PAC_ISLE = true;
            }
            else
            {
                oCase.RACE_NAT_HI_OR_OTH_PAC_ISLE = false;
            }
            if (chkRacewhite.Checked == true)
            {
                oCase.RACE_WHITE = true;
            }
            else
            {
                oCase.RACE_WHITE = false;
            }
            if (!(rblEthnicity.SelectedItem == null))
            {
                if (!(rblEthnicity.SelectedItem.Value.ToString() == ""))
                {
                    oCase.ETHNICITY_FK = Convert.ToInt32(rblEthnicity.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.ETHNICITY_FK = null;
                }
            }
            if (!(rblPrimaryhomelang.SelectedItem == null))
            {
                if (!(rblPrimaryhomelang.SelectedItem.Value.ToString() == ""))
                {
                    //if (rblPrimaryhomelang.SelectedIndex.ToString() == "2")
                    //{
                    //    Valcount = 1;
                    //}
                    oCase.PRIMARY_HOME_LANG_FK = Convert.ToInt32(rblPrimaryhomelang.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.PRIMARY_HOME_LANG_FK = null;
                }
            }
            oCase.PRIMARY_HOME_LANG_SPEC = txtPrimaryhomelangspec.Text;
            if (!(rblPersontype.SelectedItem == null))
            {
                if (!(rblPersontype.SelectedItem.Value.ToString() == ""))
                {
                    oCase.PERSON_TYPE_FK = Convert.ToInt32(rblPersontype.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.PERSON_TYPE_FK = null;
                }
            }
            if (!(rblCurrentresidence.SelectedItem == null))
            {
                if (!(rblCurrentresidence.SelectedItem.Value.ToString() == ""))
                {
                    
                    oCase.CURRENT_RESIDENCE_FK = Convert.ToInt32(rblCurrentresidence.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.CURRENT_RESIDENCE_FK = null;
                }
            }

            //added by varsha on feb 18th

            oCase.CURRENT_RESIDENCE_SPECIFY_IF_OTHER = txtCurrentresidencOther.Text;

            //--------------

            if (!(rblHighesteducation.SelectedItem == null))
            {
                if (!(rblHighesteducation.SelectedItem.Value.ToString() == ""))
                {
                    oCase.HIGHEST_EDUCATION_FK = Convert.ToInt32(rblHighesteducation.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.HIGHEST_EDUCATION_FK = null;
                }
            }
            if (!(rblIncomelevel.SelectedItem == null))
            {
                if (!(rblIncomelevel.SelectedItem.Value.ToString() == ""))
                {
                    oCase.INCOME_LEVEL_FK = Convert.ToInt32(rblIncomelevel.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.INCOME_LEVEL_FK = null;
                }
            }
            if (chkIncomesourcewagesalary.Checked == true)
            {
                oCase.INCOME_SOURCE_WAGE_SALARY = true;
            }
            else
            {
                oCase.INCOME_SOURCE_WAGE_SALARY = false;
            }
            if (chkIncomesourcepubass.Checked == true)
            {
                oCase.INCOME_SOURCE_PUB_ASS = true;
            }
            else
            {
                oCase.INCOME_SOURCE_PUB_ASS = false;
            }
            if (chkIncomesourceretirpens.Checked == true)
            {
                oCase.INCOME_SOURCE_RETIR_PENS = true;
            }
            else
            {
                oCase.INCOME_SOURCE_RETIR_PENS = false;
            }
            if (chkIncomesourcedisab.Checked == true)
            {
                oCase.INCOME_SOURCE_DISAB = true;
            }
            else
            {
                oCase.INCOME_SOURCE_DISAB = false;
            }
            if (chkIncomesourceother.Checked == true)
            {
                oCase.INCOME_SOURCE_OTHER = true;
            }
            else
            {
                oCase.INCOME_SOURCE_OTHER = false;
            }
            if (chkIncomesourcenone.Checked == true)
            {
                oCase.INCOME_SOURCE_NONE = true;
            }
            else
            {
                oCase.INCOME_SOURCE_NONE = false;
            }


            //added by var on feb 18th

            oCase.INCOME_SOURCE_SPECIFY_IF_OTHER = txtIncomesourceother.Text;
          

            //-----------------
            if (!(rblEmploymentstatus.SelectedItem == null))
            {
                if (!(rblEmploymentstatus.SelectedItem.Value.ToString() == ""))
                {
                    oCase.EMPLOYMENT_STATUS_FK = Convert.ToInt32(rblEmploymentstatus.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.EMPLOYMENT_STATUS_FK = null;
                }
            }
            if (!(rblRelationshipstatus.SelectedItem == null))
            {
                if (!(rblRelationshipstatus.SelectedItem.Value.ToString() == ""))
                {
                    oCase.RELATIONSHIP_STATUS_FK = Convert.ToInt32(rblRelationshipstatus.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.RELATIONSHIP_STATUS_FK = null;
                }
            }
            oCase.UpdatedBy = HttpContext.Current.User.Identity.Name;
            oCase.UpdatedDate = DateTime.Now;




            if (blNew == true)
            {
               
                    oCase.FOCAL_CHILD_YN = false;
                    oCase.FAMILY_FUNCTIONING_ADULT_YN = false;
                    oCase.DOMAIN_RECOVERY_ADULT_YN = false;
                    oCase.CaseNoId_FK = lngCaseID;
                    db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs.InsertOnSubmit(oCase);               

            }
           
                db.SubmitChanges();
            
           
            lngPkID = oCase.IndividualID;
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID + " on " + DateTime.Now.ToString();
        litMessage.Text = "Record saved on " + DateTime.Now.ToString();




    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["lngCaseID"] != null)))
        {
            lngCaseID = Convert.ToInt32(this.ViewState["lngCaseID"]);
        }
        if (((this.ViewState["IdenRelationship"] != null)))
        {
            IdenRelationship = Convert.ToInt32(this.ViewState["IdenRelationship"]);
        }
        if (((this.ViewState["ValInd"] != null)))
        {
            ValInd = Convert.ToInt32(this.ViewState["ValInd"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        

    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.B_Genders select p;
            rblGender.DataSource = qry;
            rblGender.DataTextField =  "Gender";
            rblGender.DataValueField = "GenderId";
            rblGender.DataBind();

            //'''''''''''''
            var qry2 = from p in db.C_CurrentResidences orderby p.orderitby select p;
            rblCurrentresidence.DataSource = qry2;
            rblCurrentresidence.DataTextField = "CurrentResidence";
            rblCurrentresidence.DataValueField = "CurrentResidenceID";
            rblCurrentresidence.DataBind();
 
            //'''''''''''''
            var qry3 = from p in db.C_EmploymentStatus select p;
            rblEmploymentstatus.DataSource = qry3;
            rblEmploymentstatus.DataTextField = "EmploymentStatus";
            rblEmploymentstatus.DataValueField = "EmploymentStatusID";
            rblEmploymentstatus.DataBind();
             //'''''''''''''
            var qry4 = from p in db.C_PersonTypes select p;
            rblPersontype.DataSource = qry4;
            rblPersontype.DataTextField = "PersonType";
            rblPersontype.DataValueField = "PersonTypeID";
            rblPersontype.DataBind();
              //'''''''''''''
            var qry5 = from p in db.C_PrimaryLanguages select p;
            rblPrimaryhomelang.DataSource = qry5;
            rblPrimaryhomelang.DataTextField = "PrimaryLanguage";
            rblPrimaryhomelang.DataValueField = "PrimaryLangID";
            rblPrimaryhomelang.DataBind();
              //'''''''''''''
            var qry6 = from p in db.C_HighestEducations select p;
            rblHighesteducation.DataSource = qry6;
            rblHighesteducation.DataTextField = "HighestEducation";
            rblHighesteducation.DataValueField = "HighestEducationID";
            rblHighesteducation.DataBind();
              //'''''''''''''
            var qry7 = from p in db.C_RelaationshipStatus select p;
            rblRelationshipstatus.DataSource = qry7;
            rblRelationshipstatus.DataTextField = "RelashionShipStatus";
            rblRelationshipstatus.DataValueField = "RelashionshipSttusID";
            rblRelationshipstatus.DataBind();
            var qry8 = from p in db.C_Ethnicities select p;
            rblEthnicity.DataSource = qry8;
            rblEthnicity.DataTextField = "Ethnicity";
            rblEthnicity.DataValueField = "EthinicityID";
            rblEthnicity.DataBind();
            var qry9 = from p in db.C_IncomeLevels select p;
            rblIncomelevel.DataSource = qry9;
            rblIncomelevel.DataTextField = "IncomeLevel";
            rblIncomelevel.DataValueField = "IncomeLevelID";
            rblIncomelevel.DataBind();

            // added by Varsha on feb 21

            //var qry10 = from p in db.C_Days select p;
            //ddlDay.DataSource = qry10;
            //ddlDay.DataTextField = "Days";
            //ddlDay.DataValueField = "DayID";
            //ddlDay.DataBind();
            //ddlDay.Items.Insert(0, "Select");

            //var qry11 = from p in db.C_Months select p;
            //ddlMonth.DataSource = qry11;
            //ddlMonth.DataTextField = "Months";
            //ddlMonth.DataValueField = "MonthID";
            //ddlMonth.DataBind();
            //ddlMonth.Items.Insert(0, "Select");

            //var qry12 = from p in db.C_Years select p;
            //ddlYear.DataSource = qry12;
            //ddlYear.DataTextField = "Years";
            //ddlYear.DataValueField = "Years";
            //ddlYear.DataBind();
            //ddlYear.Items.Insert(0, "Select");

            ////--------Year for Age >= 15 years, 0 months-----//
            //// dobYear = dobYear - 15;
            //dobYear = Convert.ToInt32(lblEnrollYear.Text);
            //dobYear = dobYear - 15;
            //var qry13 = from p in db.C_Years
            //            where p.Years >= dobYear
            //            select p;
            //ddlYear.DataSource = qry13;
            //ddlYear.DataTextField = "Years";
            //ddlYear.DataValueField = "YearID";
            //ddlYear.DataBind();
            //ddlYear.Items.Insert(0, "Select");

            ////--------Year for Age <= 20 years, 0 months-----//
       
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngCaseID"] = lngCaseID;
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["IdenRelationship"] = IdenRelationship;
        this.ViewState["ValInd"] = ValInd;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        //ValidateDOB();
        ValidateIndividualID();

        if (ValInd == 0)
        {
            lblError1.Text = "";
            updateUser();
            displayRecords();
            btnSave.Visible = false;
            btnSaveandClose.Visible = false;
            Response.Redirect("CaseEdit.aspx?lngPkID=" + lngCaseID + "&IdenRelationship=" + IdenRelationship);
        }
        else
        {
            lblError1.Text = ErrorMsg1;
        }

    }

    //private void ValidateDOB()
    //{
           
    //    DateTime dtdob;
    //    DateTime tempdob;
    //    DateTime currentDate = DateTime.Now;
    //    string datedob;

    //    int dtmonthRPG;  
    //     int dtmonthDOB;  
    //    int subMonth;
    //    int subMonthChild; //


    //    int  ddtDayRPG;
    //    int dtDayDOB;
    //    int SubDay;


    //    int dtYearRPG;
    //    int dtYearDob;
    //    int subYear;
    //    int subYearChild; //


    //    RPGenrollmentDOB = hfEnrollDate.Value;       
    //    dtmonthRPG = DateTime.Parse(RPGenrollmentDOB).Month;
    //    ddtDayRPG = DateTime.Parse(RPGenrollmentDOB).Day;
    //    dtYearRPG=DateTime.Parse(RPGenrollmentDOB).Year;

    //    //---current date------//

    //      string TodayNum = DateTime.Now.ToString("dd");        // e.g 07
    //      string ThisMonthNum = DateTime.Now.ToString("MM");    // e.g 03
    //      string ThisYearNum = DateTime.Now.ToString("yyyy");   // e.g 2014



    //    //---------------------//


    //    //datedob = ddlMonth.SelectedValue + "-" + ddlDay.SelectedValue + "-" + ddlYear.SelectedItem;
    //    //tempdob=DateTime.Parse(datedob);

    //    dtYearDob = Convert.ToInt32(ddlYear.SelectedItem.Text);
    //    subYear=dtYearRPG-dtYearDob;
    //    subYearChild = Convert.ToInt32(ThisYearNum) + 1;


    //    dtDayDOB = Convert.ToInt32(ddlDay.SelectedValue);
    //    SubDay = ddtDayRPG - dtDayDOB;

    //    int add9 = 9;
    //    dtmonthDOB = Convert.ToInt32(ddlMonth.SelectedValue);      
    //    subMonth= dtmonthRPG - dtmonthDOB;
    //    subMonthChild = Convert.ToInt32(ThisMonthNum) + add9;
    //    subMonthChild = subMonthChild - dtmonthDOB;

    //    if (rblPersontype.SelectedItem.Value == "1") // person type is Adult
    //    {
    //        if (subYear <= 15)                      // Adult Age= [RPG enroll date - DOB] where [Age >= 15 years, 0 months]
    //        {
    //            if (subMonth >= 0)
    //            {
    //                bldobMonth = true;
    //                bldobDay = true;

    //                if (subMonth == 0)
    //                {
    //                    if (SubDay >= 0)
    //                    {
    //                        bldobDay = true;
    //                    }
    //                    else
    //                    {
    //                        bldobDay = false;
    //                    }
    //                }

    //            }
    //            else
    //            {
    //                bldobMonth = false;
    //                if (SubDay >= 0)
    //                {
    //                    bldobDay = true;
    //                }
    //                else
    //                {
    //                    bldobDay = false;
    //                }
    //            }
    //        }
    //        else
    //        {
    //            bldobDay = true;
    //            bldobMonth = true;

    //        }
    //    }
    //    else                                          // person type is child
    //    {
    //        if (subYear >= 20)                          // Child Age= [RPG enroll date - DOB] where [Age <= 20 years, 0 months]
    //        {
    //            if (subMonth <= 0)
    //            {
    //                bldobMonth = true;
    //                bldobDay = true;
    //                if (subMonth == 0)
    //                {
    //                    if (SubDay <= 0)
    //                    {
    //                        bldobDay = true;
    //                    }
    //                    else
    //                    {
    //                        bldobDay = false;
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                bldobMonth = false;
    //                if (SubDay <= 0)
    //                {
    //                    bldobDay = true;
    //                }
    //                else
    //                {
    //                    bldobDay = false;
    //                }
    //            }
    //        }
    //        else
    //        {
    //            bldobDay = true;
    //            bldobMonth = true;
    //        }
    //    }
    //}
    private void ValidateIndividualID()
    {
      DataClassesDataContext db = new DataClassesDataContext();
        lblError1.Text = "";
        object objVal = null;


        if (lngPkID == 0)
        {
            var oPages = from pg in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                         where pg.CaseNoId_FK == lngCaseID
                         select pg;
            foreach (var oCase1 in oPages)
            {

                objVal = oCase1.IND_ID;
                if (objVal.ToString() == txtIndid.Text)
                {

                    ErrorMsg1 = "Individual ID already exists";
                    lblError1.Text = ErrorMsg1;
                     ValInd = 1;
                     Response.Redirect("CaseEnrollmentDemographics.aspx?ErrorMsg1=" + ErrorMsg1 + "&lngPkID=" + lngPkID + "&lngCaseID=" + lngCaseID);
                }

            }
        }
  
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("CaseEdit.aspx?lngPkID=" + lngCaseID+ "&IdenRelationship=" + IdenRelationship );


    }    
    protected void btnSaveandClose_Click(object sender, EventArgs e)
    {
        //DataClassesDataContext db = new DataClassesDataContext();
        //lblError1.Text = "";
        //object objVal = null;
        //var oPages = from pg in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
        //             select pg;
        //foreach (var oCase1 in oPages)
        //{

        //    objVal = oCase1.IND_ID;
        //    if (objVal.ToString() == txtIndid.Text)
        //    {
        //        string ErrorMsg1 = "Individual ID already exist";
        //        Response.Redirect("CaseEnrollmentDemographics.aspx?ErrorMsg1=" + ErrorMsg1);
        //    }

        //}
        //updateUser();       
        //Response.Redirect("CaseEdit.aspx?lngPkID=" + lngCaseID + "&IdenRelationship=" + IdenRelationship);

        DataClassesDataContext db = new DataClassesDataContext();
        lblError1.Text = "";
        object objVal = null;


        if (lngPkID == 0)
        {
            var oPages = from pg in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                         where pg.CaseNoId_FK == lngCaseID
                         select pg;
            foreach (var oCase1 in oPages)
            {

                objVal = oCase1.IND_ID;
                if (objVal.ToString() == txtIndid.Text)
                {

                    string ErrorMsg1 = "Individual ID already exists";
                    lblError1.Text = ErrorMsg1;
                    Response.Redirect("CaseEnrollmentDemographics.aspx?ErrorMsg1=" + ErrorMsg1 + "&lngPkID=" + lngPkID + "&lngCaseID=" + lngCaseID);
                }

            }

            updateUser();
            Response.Redirect("CaseEdit.aspx?lngPkID=" + lngCaseID + "&IdenRelationship=" + IdenRelationship);
        }
        else
        {
            updateUser();
            Response.Redirect("CaseEdit.aspx?lngPkID=" + lngCaseID + "&IdenRelationship=" + IdenRelationship);
        }
    }


    protected void rblPersontype_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtDOB.Text = "";
        lblNowMonth.Text = DateTime.Now.Month.ToString();
        if (rblPersontype.SelectedItem.Value == "1")
        {
            divAdultOnly.Visible = true;
        }
        else
            divAdultOnly.Visible = false;
    }

    //private void setdobMonthRange(int dobtopMonth, int dobbottomMonth)
    //{
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        var qry14 = new object();
    //        string selectedMonth = ddlMonth.SelectedItem.Value;

    //        if (dobtopMonth == -1 && dobbottomMonth == -1)
    //        {
    //            qry14 = from p in db.C_Months
    //                    select p;
    //        }
    //        else if (dobtopMonth != -1 && dobbottomMonth != -1)
    //        {
    //             qry14 = from p in db.C_Months
    //                        where (p.MonthID <= dobtopMonth && p.MonthID >= dobbottomMonth)
    //                        select p;
    //        }
    //        else if (dobtopMonth == -1)
    //        {
    //            qry14 = from p in db.C_Months
    //                    where (p.MonthID >= dobbottomMonth)
    //                    select p;
    //        }
    //        else if (dobbottomMonth == -1)
    //        {
    //            qry14 = from p in db.C_Months
    //                    where (p.MonthID <= dobtopMonth)
    //                    select p;
    //        }
    //        ddlMonth.SelectedItem.Selected = false;
    //        ddlMonth.Items.Clear();

    //        ddlMonth.DataSource = qry14;
    //        ddlMonth.DataTextField = "Months";
    //        ddlMonth.DataValueField = "MonthID";
    //        ddlMonth.DataBind();
    //        ddlMonth.Items.Insert(0, "Select");

    //        if (ddlMonth.Items.FindByValue(selectedMonth) == null)
    //        {
    //            resetdobDaysRange();
    //        }
    //        else
    //            ddlMonth.SelectedValue = selectedMonth;
    //    }
    //}

    //private void resetdobDaysRange()
    //{
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        var qry10 = from p in db.C_Days
    //                    select p;

    //        ddlDay.SelectedItem.Selected = false;
    //        ddlDay.Items.Clear();
    //        ddlDay.DataSource = qry10;
    //        ddlDay.DataTextField = "Days";
    //        ddlDay.DataValueField = "DayID";
    //        ddlDay.DataBind();
    //        ddlDay.Items.Insert(0, "Select");
    //        ddlDay.SelectedIndex = 0;
    //    }
    //}

    //private void setAdultDOBRange()
    //{
    //    divAdultOnly.Visible = true;
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        //--------Year for Age >= 15 years, 0 months-----//
    //        // dobYear <= EnrollYear - 15;
    //        int SelectedDay = ddlDay.SelectedItem.Value=="Select" ? 0 : Convert.ToInt32(ddlDay.SelectedItem.Text.Trim());
    //        int SelectedMonth = ddlMonth.SelectedItem.Value == "Select" ? 0 : Convert.ToInt32(ddlMonth.SelectedItem.Value.Trim());
    //        int SelectedYear = ddlYear.SelectedItem.Value == "Select" ? 0 : Convert.ToInt32(ddlYear.SelectedItem.Text.Trim());
    //        string SelectedYearID =  ddlYear.SelectedValue;

    //        if (!(String.IsNullOrEmpty(lblEnrollYear.Text)))
    //        {
    //            int EnrollYear = Convert.ToInt32(lblEnrollYear.Text);
    //            dobtopYear = EnrollYear - 15;
    //            int dobtopMonth = Convert.ToDateTime(hfEnrollDate.Value).Month;

    //            var qry13 = from p in db.C_Years
    //                        where p.Years <= dobtopYear
    //                        select p;

    //            ddlYear.Items.Clear();
    //            ddlYear.SelectedIndex = -1;
    //            ddlYear.DataSource = qry13;
    //            ddlYear.DataTextField = "Years";
    //            ddlYear.DataValueField = "YearID";
    //            ddlYear.DataBind();
    //            ddlYear.Items.Insert(0, "Select");

    //            if (SelectedYear == dobtopYear)
    //            {
    //                setdobMonthRange(dobtopMonth, -1);
    //                //ddlYear.SelectedValue = SelectedYearID;
    //            }
    //            //else if (SelectedYear > dobtopYear)
    //            //{
    //            //    ddlMonth.SelectedValue = "Select";
    //            //    ddlDay.SelectedValue = "Select";
    //            //}

    //        }


    //    }

    //}

    //private void setChildDOBRange()
    //{
    //    divAdultOnly.Visible = false;
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        // Age = EnrollmentDate - DOB
    //        //--------Year for Age <= 20 years, 0 months---AND DOB <= TodayDay +9months//
    //        // EnrollmentDay -20 =< DOB <= Today'sDay + 9months;


    //        if (!(String.IsNullOrEmpty(lblEnrollYear.Text)))
    //        {
    //            EnrollYear = Convert.ToInt32(lblEnrollYear.Text);
    //            dobbottomYear = EnrollYear - 20;

    //            if (!(String.IsNullOrEmpty(lblNowMonth.Text.Trim())))
    //            {
    //                int nowMonth = DateTime.Now.Month;

    //                if (nowMonth + 9 > 12)
    //                    dobtopYear = DateTime.Now.Year + 1;
    //                else
    //                    dobtopYear = DateTime.Now.Year;

    //                dobtopMonth = 12 - (nowMonth + 9);
                    

    //                int selectedYear = ddlYear.SelectedItem.Value=="Select" ? 0 : Convert.ToInt32(ddlYear.SelectedItem.Text);
    //                string selectedYearID = ddlYear.SelectedValue;

    //                var qry13 = from p in db.C_Years
    //                            where (p.Years >= dobbottomYear && p.Years <= dobtopYear)
    //                            select p;
    //                ddlYear.SelectedItem.Selected = false;
    //                ddlYear.Items.Clear();
    //                ddlYear.DataSource = qry13;
    //                ddlYear.DataTextField = "Years";
    //                ddlYear.DataValueField = "YearID";
    //                ddlYear.DataBind();
    //                ddlYear.Items.Insert(0, "Select");

    //                if (selectedYear <= dobtopYear)
    //                {
    //                    if (selectedYear > 0 && ddlYear.Items.FindByText(selectedYear.ToString()) != null)
    //                        ddlYear.SelectedValue = selectedYearID;

    //                    if (selectedYear == dobtopYear)
    //                        setdobMonthRange(dobtopMonth, -1);
    //                }
    //                else
    //                    ddlYear.SelectedIndex = 0;

    //            }
    //            else  //old code may not ever go here. but keep it for safe
    //            {
    //                var qry013 = from p in db.C_Years
    //                             where (p.Years >= dobbottomYear && p.Years <= dobtopYear)

    //                             select p;
    //                ddlYear.DataSource = qry013;
    //                ddlYear.DataTextField = "Years";
    //                ddlYear.DataValueField = "YearID";
    //                ddlYear.DataBind();
    //                ddlYear.Items.Insert(0, "Select");
    //            }
    //        }

    //        //--------Year for Age <= 20 years, 0 months-----//
    //    }
    //}

    //private void SetDateRange(bool isAdult)
    //{
    //    if (isAdult)
    //        setAdultDOBRange();
    //    else
    //        setChildDOBRange();
    //}

    
    protected void chkIncomesourceother_CheckedChanged(object sender, EventArgs e)
    {            

       if (chkIncomesourceother.Checked == true)        
        {
            rfvtxtIncomeSourceOther.EnableClientScript = true;
            rfvtxtIncomeSourceOther.Enabled = true;
        }
        else
        {
            rfvtxtIncomeSourceOther.EnableClientScript = false;
            rfvtxtIncomeSourceOther.Enabled = false;
        }
    }
    protected void rblPrimaryhomelang_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = rblPrimaryhomelang.SelectedIndex;
        if (i == 2)
        {         
            valtxtOther.EnableClientScript = true; 
            valtxtOther.Enabled = true;
        }
        else
        {
            valtxtOther.EnableClientScript = false;
            valtxtOther.Enabled = false;
        }
    }
    protected void rblCurrentresidence_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = rblCurrentresidence.SelectedIndex;

        if (i == 6)
        {           
            rvfCurrentResidencOther.EnableClientScript = true;
            rvfCurrentResidencOther.Enabled = true;
        }
        else
        {
            rvfCurrentResidencOther.EnableClientScript = false;
            rvfCurrentResidencOther.Enabled = false;
        }
    }    



    //protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (!(ddlDay.SelectedItem.Value == "Select"))
    //    {
    //        day = Convert.ToInt32(ddlDay.SelectedItem.Value);
    //    }
    //    string strSelectDay = day == 0 ? "Select" : day.ToString();
    //    //ddlDay.Items.Clear();
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {

    //        //if (!(ddlMonth.SelectedItem.Value == "Select"))
    //        if (Convert.ToString(ddlMonth.SelectedItem.Text) != "Select")
    //        {
    //            if (!(ddlYear.SelectedItem.Value == "Select"))
    //            {
    //                //year = Convert.ToInt32(ddlYear.SelectedValue.ToString());

    //                year = Convert.ToInt32(ddlYear.SelectedItem.ToString());
    //                year = year % 4;
    //            }

    //            int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);


    //            if ((year == 0) && (month == 2) && (day == 28)) //leap year with feb-29
    //            {
                   
    //                var qry10 = from p in db.C_Days
    //                            where (p.Days >= 1 && p.Days <= 29)
    //                            select p;
    //                ddlDay.Items.Clear();
    //                ddlDay.SelectedValue = "1";
    //                ddlDay.DataSource = qry10;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");
    //                ddlDay.SelectedValue = strSelectDay;

    //            }
    //            else if ((year != 0) && (month == 2)) //non leap year, feb - 28
    //            {
    //              //  ddlDay.SelectedValue = strSelectDay;
    //                var qry10 = from p in db.C_Days
    //                            where (p.Days >= 1 && p.Days <= 28)
    //                            select p;
    //                ddlDay.Items.Clear();
    //                ddlDay.SelectedValue = "1";
    //                ddlDay.DataSource = qry10;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");
    //                if (strSelectDay == "29")
    //                    ddlDay.SelectedValue = "Select";
    //                else
    //                ddlDay.SelectedValue = strSelectDay;
    //            }
    //            else if ((year == 0) && (month == 2))
    //            {
    //               // ddlDay.SelectedValue = strSelectDay;
    //                var qry10 = from p in db.C_Days
    //                            where (p.Days >= 1 && p.Days <= 29)
    //                            select p;
    //                ddlDay.Items.Clear();
    //                ddlDay.SelectedValue = "1";
    //                ddlDay.DataSource = qry10;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");
    //                ddlDay.SelectedValue = strSelectDay;
    //            }
    //            else if ((month == 4) || (month == 6) || (month == 11) || (month == 9)) //months with 30 days
    //            {
                   
    //                var qry16 = from p in db.C_Days
    //                            where (p.Days <= 30)
    //                            select p;
    //                ddlDay.Items.Clear();
    //                ddlDay.SelectedValue = "1";
    //                ddlDay.DataSource = qry16;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");
    //                ddlDay.SelectedValue = strSelectDay;
    //            }
    //            //if month is 1 or 3 or 5 or 7 or 8 or 10 or 12 day is (31 days )
    //            else
    //            {
                    
    //                var qry17 = from p in db.C_Days
    //                            where (p.Days <= 31)
    //                            select p;
    //                ddlDay.SelectedItem.Selected = false;
    //                ddlDay.Items.Clear();

    //                ddlDay.DataSource = qry17;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");
    //                ddlDay.SelectedValue = strSelectDay;
    //            }


    //            //string ThisMonthNum = DateTime.Now.ToString("MM");
    //            //int month1 = Convert.ToInt32(ThisMonthNum) + 9;
    //            //int nextmonth = month1 - 12;
    //            //string ThisYearNum = DateTime.Now.ToString("yyyy");
    //            //int year01 = Convert.ToInt32(ThisYearNum) + 1;
    //            //string TodayNum = DateTime.Now.ToString("dd");
    //            //int leapYear01 = year % 4;

    //            //if (!(ddlYear.SelectedItem.Text== "Select"))
    //            //{
    //            //    if (Convert.ToInt32(ddlYear.SelectedItem.Text) == year01)
    //            //    {

    //            //        var qry14 = from p in db.C_Months
    //            //                    where (p.MonthID <= nextmonth)
    //            //                    select p;
    //            //        ddlMonth.DataSource = qry14;
    //            //        ddlMonth.DataTextField = "Months";
    //            //        ddlMonth.DataValueField = "MonthID";
    //            //        ddlMonth.DataBind();
    //            //        ddlMonth.Items.Insert(0, "Select");

    //            //    }                   
    //            //    else
    //            //    {
    //            //        ddlMonth.SelectedValue = month.ToString();
    //            //        var qry15 = from p in db.C_Months select p;
    //            //        ddlMonth.DataSource = qry15;
    //            //        ddlMonth.DataTextField = "Months";
    //            //        ddlMonth.DataValueField = "MonthID";
    //            //        ddlMonth.DataBind();
    //            //        ddlMonth.Items.Insert(0, "Select");

    //            //        if (!String.IsNullOrEmpty(ddlMonth.SelectedValue))
    //            //        {
    //            //            int selectedMonth1= Convert.ToInt32(ddlMonth.SelectedValue);
    //            //            if ((selectedMonth1 == 2)&&(leapYear01==0))
    //            //            {
    //            //                var qry10 = from p in db.C_Days
    //            //                            where (p.Days >= 1 && p.Days <= 29)
    //            //                            select p;
    //            //                ddlDay.Items.Clear();
    //            //                ddlDay.SelectedValue = "1";
    //            //                ddlDay.DataSource = qry10;
    //            //                ddlDay.DataTextField = "Days";
    //            //                ddlDay.DataValueField = "DayID";
    //            //                ddlDay.DataBind();
    //            //                ddlDay.Items.Insert(0, "Select");
    //            //                ddlDay.SelectedValue = strSelectDay;
    //            //            }
    //            //            else if ((selectedMonth1 == 2) && (leapYear01 != 0))
    //            //            {
    //            //               // ddlDay.SelectedValue = strSelectDay;
    //            //                var qry10 = from p in db.C_Days
    //            //                            where (p.Days >= 1 && p.Days <= 28)
    //            //                            select p;
    //            //                ddlDay.Items.Clear();
    //            //                ddlDay.SelectedValue = "1";
    //            //                ddlDay.DataSource = qry10;
    //            //                ddlDay.DataTextField = "Days";
    //            //                ddlDay.DataValueField = "DayID";
    //            //                ddlDay.DataBind();
    //            //                ddlDay.Items.Insert(0, "Select");
    //            //                if (strSelectDay == "29")
    //            //                    ddlDay.SelectedValue = "Select";
    //            //                else
    //            //                    ddlDay.SelectedValue = strSelectDay;
    //            //            }
    //            //           // if month is 4 or 6 or 11 or 9 day is (30 days)
    //            //          else if ((selectedMonth1 == 4) || (selectedMonth1 == 6) || (selectedMonth1 == 11) || (selectedMonth1 == 9))
    //            //            {
                             
    //            //                  var qry16 = from p in db.C_Days
    //            //                              where (p.Days <=30)
    //            //                              select p;
    //            //                  ddlDay.Items.Clear();
    //            //                  ddlDay.SelectedValue = "1";
    //            //                  ddlDay.DataSource = qry16;
    //            //                  ddlDay.DataTextField = "Days";
    //            //                  ddlDay.DataValueField = "DayID";
    //            //                  ddlDay.DataBind();
    //            //                  ddlDay.Items.Insert(0, "Select");
    //            //                  ddlDay.SelectedValue = strSelectDay;
    //            //              }
    //            //            //if month is 1 or 3 or 5 or 7 or 8 or 10 or 12 day is (31 days )
    //            //            else
    //            //            {
    //            //                var qry17 = from p in db.C_Days
    //            //                            where (p.Days <= 31)
    //            //                            select p;
    //            //                ddlDay.SelectedItem.Selected = false;
    //            //                ddlDay.Items.Clear();
    //            //                ddlDay.SelectedIndex = -1;
    //            //                ddlDay.DataSource = qry17;
    //            //                ddlDay.DataTextField = "Days";
    //            //                ddlDay.DataValueField = "DayID";
    //            //                ddlDay.DataBind();
    //            //                ddlDay.Items.Insert(0, "Select");
    //            //                ddlDay.SelectedValue = strSelectDay;
    //            //            }
    //            //        }

    //            //        //DaysByAdded_9_Months();
    //            //    }                 

    //            //}
    //        }
    //        //setdobMonthRange(-1, -1);
    //        if (rblPersontype.SelectedItem != null)
    //        {
    //            if (rblPersontype.SelectedItem.Value == "1")  //Adult
    //            {
    //                dobtopMonth = Convert.ToDateTime(hfEnrollDate.Value).Month;
    //                dobtopYear = Convert.ToDateTime(hfEnrollDate.Value).Year - 15;
    //                if (ddlYear.SelectedItem.Text == dobtopYear.ToString())
    //                {
    //                    setdobMonthRange(dobtopMonth, -1);
    //                }
    //                else
    //                    setdobMonthRange(-1, -1);
    //            }
    //            else  //Child
    //            {
    //                dobtopYear = DateTime.Now.Year + ((DateTime.Now.Month + 9) > 12 ? 1 : 0);
    //                dobbottomYear = Convert.ToDateTime(hfEnrollDate.Text).Year - 20;
    //                if (ddlYear.SelectedItem.Text == dobtopYear.ToString())   //top month
    //                {
    //                    dobtopMonth = (DateTime.Now.Month + 9) <= 12 ? (DateTime.Now.Month + 9) : 12 - (DateTime.Now.Month + 9);
    //                    setdobMonthRange(dobtopMonth, -1);
    //                }
    //                else if (ddlYear.SelectedItem.Text == dobbottomYear.ToString())     //bottom month
    //                {
    //                    dobbottomMonth = Convert.ToDateTime(hfEnrollDate.Text).Month;
    //                    setdobMonthRange(-1, dobbottomMonth);
    //                }
    //                else
    //                    setdobMonthRange(-1, -1);
    //            }
    //        }
    //    }
    //}
    //protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    MonthDayYear();

    //}
    //protected void ddlDay_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //  //  DaysByAdded_9_Months();
    //}


    //private void MonthDayYear()
    //{

    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {

    //        DaysByMonth();
    //        //DaysByAdded_9_Months();

    //    }
    //}
    //private void DaysByAdded_9_Months()
    //{
    //    string strSelectedDay = ddlDay.SelectedIndex == 0 ? "Select" : ddlDay.SelectedValue;
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        //----------------------------------------------

    //        string ThisMonthNum = DateTime.Now.ToString("MM");
    //        int month1 = Convert.ToInt32(ThisMonthNum) + 9;
    //        int nextmonth = month1 - 12;
    //        string ThisYearNum = DateTime.Now.ToString("yyyy");
    //        int yearPlus = Convert.ToInt32(ThisYearNum) + 1;
    //        string TodayNum = DateTime.Now.ToString("dd");



    //        int yearLeap = yearPlus % 4;

           


    //        if (Convert.ToString(ddlMonth.SelectedItem.Text) != "Select")
    //        {
    //            SelectedMonth = Convert.ToInt32(ddlMonth.SelectedItem.Value); // selected month from dropdown
    //        }

    //        if (Convert.ToString(ddlMonth.SelectedItem.Text) != "Select")
    //        {
    //            if (!(Convert.ToString(ddlYear.SelectedItem.Text) == "Select"))
    //            {
    //                if ((Convert.ToInt32(ddlYear.SelectedItem.Text) == yearPlus) && (Convert.ToInt32(ddlMonth.SelectedItem.Value) == nextmonth))
    //                {
    //                    //if month is feb and todays date is greater than 28

    //                    if (SelectedMonth == 2)
    //                    {
    //                        if (Convert.ToInt32(TodayNum) <= 28)
    //                        {
    //                            var qry15 = from p in db.C_Days
    //                                        where (p.Days <= Convert.ToInt32(TodayNum))
    //                                        select p;
    //                            ddlDay.Items.Clear();
    //                            //ddlDay.SelectedValue = "1";
    //                            ddlDay.DataSource = qry15;
    //                            ddlDay.DataTextField = "Days";
    //                            ddlDay.DataValueField = "DayID";
    //                            ddlDay.DataBind();
    //                            ddlDay.Items.Insert(0, "Select");
    //                            if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > Convert.ToInt32(TodayNum))
    //                                ddlDay.SelectedValue = "Select";
    //                            else
    //                                ddlDay.SelectedValue = strSelectedDay;
    //                        }
    //                        else if (Convert.ToInt32(TodayNum) > 28)
    //                        {
    //                            if ((Convert.ToInt32(TodayNum) == 29) && (yearLeap != 0))
    //                            {
    //                                int value1 = Convert.ToInt32(TodayNum) - 1;
    //                                var qry15 = from p in db.C_Days
    //                                            where (p.Days <= Convert.ToInt32(value1))
    //                                            select p;
    //                                ddlDay.Items.Clear();
    //                                ddlDay.DataSource = qry15;
    //                                ddlDay.DataTextField = "Days";
    //                                ddlDay.DataValueField = "DayID";
    //                                ddlDay.DataBind();
    //                                ddlDay.Items.Insert(0, "Select");

    //                                if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > value1)
    //                                    ddlDay.SelectedValue = "Select";
    //                                else
    //                                    ddlDay.SelectedValue = strSelectedDay;
    //                            }
    //                            else if ((Convert.ToInt32(TodayNum) == 29) && (yearLeap == 0))
    //                            {
    //                                int value11 = Convert.ToInt32(TodayNum);
    //                                var qry15 = from p in db.C_Days
    //                                            where (p.Days <= Convert.ToInt32(value11))
    //                                            select p;
    //                                ddlDay.Items.Clear();
    //                                ddlDay.DataSource = qry15;
    //                                ddlDay.DataTextField = "Days";
    //                                ddlDay.DataValueField = "DayID";
    //                                ddlDay.DataBind();
    //                                ddlDay.Items.Insert(0, "Select");
    //                                if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > value11)
    //                                    ddlDay.SelectedValue = "Select";
    //                                else
    //                                    ddlDay.SelectedValue = strSelectedDay;
    //                            }
    //                            else if (Convert.ToInt32(TodayNum) == 30)
    //                            {
    //                                int value2 = Convert.ToInt32(TodayNum) - 2;
    //                                var qry15 = from p in db.C_Days
    //                                            where (p.Days <= Convert.ToInt32(value2))
    //                                            select p;

    //                                ddlDay.Items.Clear();
    //                                ddlDay.DataSource = qry15;
    //                                ddlDay.DataTextField = "Days";
    //                                ddlDay.DataValueField = "DayID";
    //                                ddlDay.DataBind();
    //                                ddlDay.Items.Insert(0, "Select");
    //                                if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > value2)
    //                                    ddlDay.SelectedValue = "Select";
    //                                else
    //                                    ddlDay.SelectedValue = strSelectedDay;
    //                            }
    //                            else if (Convert.ToInt32(TodayNum) == 31)
    //                            {
    //                                int value3 = Convert.ToInt32(TodayNum) - 3;
    //                                var qry15 = from p in db.C_Days
    //                                            where (p.Days <= Convert.ToInt32(value3))
    //                                            select p;
    //                                ddlDay.Items.Clear();
    //                                ddlDay.DataSource = qry15;
    //                                ddlDay.DataTextField = "Days";
    //                                ddlDay.DataValueField = "DayID";
    //                                ddlDay.DataBind();
    //                                ddlDay.Items.Insert(0, "Select");
    //                                if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > value3)
    //                                    ddlDay.SelectedValue = "Select";
    //                                else
    //                                    ddlDay.SelectedValue = strSelectedDay;
    //                            }
    //                        }
    //                    }

    //                    // if month is 4 or 6 or 11 or 9 day is (30 days)
    //                    else if ((SelectedMonth == 4) || (SelectedMonth == 6) || (SelectedMonth == 11) || (SelectedMonth == 9))
    //                    {
    //                        if (Convert.ToInt32(TodayNum) > 30)
    //                        {
    //                            int value4 = Convert.ToInt32(TodayNum) - 1;
    //                            var qry15 = from p in db.C_Days
    //                                        where (p.Days <= Convert.ToInt32(value4))
    //                                        select p;
    //                            ddlDay.Items.Clear();
    //                            ddlDay.DataSource = qry15;
    //                            ddlDay.DataTextField = "Days";
    //                            ddlDay.DataValueField = "DayID";
    //                            ddlDay.DataBind();
    //                            ddlDay.Items.Insert(0, "Select");
    //                            if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > value4)
    //                                ddlDay.SelectedValue = "Select";
    //                            else
    //                                ddlDay.SelectedValue = strSelectedDay;
    //                        }
    //                        else
    //                        {
    //                            var qry15 = from p in db.C_Days
    //                                        where (p.Days <= Convert.ToInt32(TodayNum))
    //                                        select p;
    //                            ddlDay.Items.Clear();
    //                            ddlDay.DataSource = qry15;
    //                            ddlDay.DataTextField = "Days";
    //                            ddlDay.DataValueField = "DayID";
    //                            ddlDay.DataBind();
    //                            ddlDay.Items.Insert(0, "Select");
    //                            if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > Convert.ToInt32(TodayNum))
    //                                ddlDay.SelectedValue = "Select";
    //                            else
    //                                ddlDay.SelectedValue = strSelectedDay;
    //                        }
    //                    }
    //                    //if month is 1 or 3 or 5 or 7 or 8 or 10 or 12 day is (31 days )
    //                    else
    //                    {
    //                        var qry15 = from p in db.C_Days
    //                                    where (p.Days <= Convert.ToInt32(TodayNum))
    //                                    select p;
    //                        ddlDay.Items.Clear();
    //                        ddlDay.DataSource = qry15;
    //                        ddlDay.DataTextField = "Days";
    //                        ddlDay.DataValueField = "DayID";
    //                        ddlDay.DataBind();
    //                        ddlDay.Items.Insert(0, "Select");
    //                        if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > Convert.ToInt32(TodayNum))
    //                            ddlDay.SelectedValue = "Select";
    //                        else
    //                            ddlDay.SelectedValue = strSelectedDay;
    //                    }
    //                }
    //                else
    //                {
    //                    DaysByMonth();
    //                }
    //            }
    //            //else
    //            //{
    //            //   DaysByMonth();
    //            //}
    //        }


    //        //----------------
    //    }
    //}
    //private void DaysByMonth()
    //{
    //    string strSelectedDay = ddlDay.SelectedIndex == 0 ? "Select" : ddlDay.SelectedValue;
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        //if (!(ddlMonth.SelectedItem.Value == "Select"))
    //        if (Convert.ToString(ddlMonth.SelectedItem.Text) != "Select")
    //        {            
    //            //if (!(ddlYear.SelectedItem.Value == "Select"))
    //           if (!(Convert.ToString(ddlYear.SelectedItem.Text) == "Select"))                
    //            {
    //                //year = Convert.ToInt32(ddlYear.SelectedValue.ToString());

    //                year1 = Convert.ToInt32(ddlYear.SelectedItem.ToString());
    //                year1 = year1 % 4;
    //            }
    //        }
    //        //if (!(ddlMonth.SelectedItem.Value == "Select"))
    //        if (Convert.ToString(ddlMonth.SelectedItem.Text) != "Select")
    //        {
    //            int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
    //            if (month == 2)
    //            {

    //                if (year1 == 0)
    //                {
    //                    var qry10 = from p in db.C_Days
    //                                where (p.Days >= 1 && p.Days <= 29)
    //                                select p;
    //                    ddlDay.SelectedItem.Selected = false;
    //                    ddlDay.Items.Clear();
    //                    ddlDay.DataSource = qry10;
    //                    ddlDay.DataTextField = "Days";
    //                    ddlDay.DataValueField = "DayID";
    //                    ddlDay.DataBind();
    //                    ddlDay.Items.Insert(0, "Select");
    //                    if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > 29)
    //                        ddlDay.SelectedValue = "Select";
    //                    else
    //                        ddlDay.SelectedValue = strSelectedDay;
    //                }
    //                else
    //                {
    //                    var qry10 = from p in db.C_Days
    //                                where (p.Days >= 1 && p.Days <= 28)
    //                                select p;

    //                    ddlDay.SelectedItem.Selected = false;
    //                    ddlDay.Items.Clear();

    //                    ddlDay.DataSource = qry10;
    //                    ddlDay.DataTextField = "Days";
    //                    ddlDay.DataValueField = "DayID";
    //                    ddlDay.DataBind();
    //                    ddlDay.Items.Insert(0, "Select");
    //                    if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > 28)
    //                        ddlDay.SelectedValue = "Select";
    //                    else
    //                        ddlDay.SelectedValue = strSelectedDay;
    //                }


    //            }
    //            else if ((month == 4) || (month == 6) || (month == 11) || (month == 9))
    //            {
    //                var qry10 = from p in db.C_Days
    //                            where (p.Days >= 1 && p.Days <= 30)
    //                            select p;

    //                ddlDay.SelectedItem.Selected = false;
    //                ddlDay.Items.Clear();

    //                ddlDay.DataSource = qry10;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");
    //                if (strSelectedDay != "Select" && Convert.ToInt32(strSelectedDay) > 30)
    //                    ddlDay.SelectedValue = "Select";
    //                else
    //                    ddlDay.SelectedValue = strSelectedDay;
    //            }
    //            else
    //            {
    //                ddlDay.SelectedItem.Selected = false;
    //                ddlDay.Items.Clear();

    //                var qry10 = from p in db.C_Days
    //                            where (p.Days >= 1 && p.Days <= 31)
    //                            select p;
    //                ddlDay.DataSource = qry10;
    //                ddlDay.DataTextField = "Days";
    //                ddlDay.DataValueField = "DayID";
    //                ddlDay.DataBind();
    //                ddlDay.Items.Insert(0, "Select");
    //                ddlDay.SelectedValue = strSelectedDay;
    //            }
    //        }
    //    }
    //    //ddlDay.SelectedValue = strSelectedDay;
    //}

   
}