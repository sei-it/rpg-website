﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Configuration;
using System.IO;


public partial class CaseActivityTickler : System.Web.UI.Page
{
    private int? intUserGranteeID;
    private int? intUserRole;
    int IdenRelationship;
    int lngPkID;
    int EBPexitdate = 1;
    UserInfo oUI;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
          
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            intUserGranteeID = oUI.intGranteeID;
            intUserRole = oUI.intUserRoleID;
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["IdenRelationship"] != null)
            {
                IdenRelationship = Convert.ToInt32(Page.Request.QueryString["IdenRelationship"]);
            }
            displayRecords();
        }

    }

    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {             
           
            if(intUserRole == 1)//Grantee Admin 
            {
                //var vCases = db.getCaseConfirmTickler_modified(intUserGranteeID);
                var vCases = db.getCaseConfirmTickler(intUserGranteeID);
                            
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
            else if (intUserRole == 2) //Case Worker
            {
               // var vCases = db.getCaseForCaseworker_AssignedListTickler_modifed(intUserGranteeID, HttpContext.Current.User.Identity.Name);
                var vCases = db.getCaseForCaseworker_AssignedListTickler(intUserGranteeID, HttpContext.Current.User.Identity.Name);
                
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
           

        }

    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        if (((this.ViewState["IdenRelationship"] != null)))
        {
            IdenRelationship = Convert.ToInt32(this.ViewState["IdenRelationship"]);
        }

    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["oUI"] = oUI;
        this.ViewState["IdenRelationship"] = IdenRelationship;
        return (base.SaveViewState());
    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "CreateSL")
        {
            //int intCaseID = Convert.ToInt32(e.CommandArgument);
            //Response.Redirect("ServiceLogEdit.aspx?intCaseID=" + intCaseID);

            int CaseEBPID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("ServiceLogEdit.aspx?intCaseID=" + CaseEBPID);

        }

        if (e.CommandName == "CreateCaseConfimAct")
        {
            //int intCaseID = Convert.ToInt32(e.CommandArgument);
            //Response.Redirect("CaseConfirmCaseActivity.aspx?intCaseID=" + intCaseID + "&intEBPID=" + );

        }

        if (e.CommandName == "EBP")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("EBPList.aspx?intCaseID=" + intResponseID);
        }
    }
    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int intCaseID = 0;
        int intCaseEBPID = 0;
        Repeater repCasemember;
        Repeater repServiceLogs;
        LinkButton imgBtnCreate;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //if (DataBinder.Eval(e.Row.DataItem, "CaseNoId") != null)
            //{
            //    intCaseID = (int)DataBinder.Eval(e.Row.DataItem, "CaseNoId");
            //}

            if (DataBinder.Eval(e.Row.DataItem, "CaseEBPID") != null)
            {
                intCaseEBPID = (int)DataBinder.Eval(e.Row.DataItem, "CaseEBPID");
            }

            repCasemember = (Repeater)e.Row.FindControl("repCaseMembers");
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                           join ebp in db.F_EBPEnrollments on p.IndividualID equals ebp.IndividualID_FK
                           where ebp.EBPID_FK == intCaseEBPID
                           select new { p.FIRST_NAME, p.IndividualID };
                repCasemember.DataSource = qry2;
                repCasemember.DataBind();


                //var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                //           where p.CaseNoId_FK == intCaseID
                //           select new { p.FIRST_NAME, p.IndividualID };

                // repCasemember.DataSource = qry2;
                //  repCasemember.DataBind();

                //--------------------Service log date---------------
                repServiceLogs = (Repeater)e.Row.FindControl("repServiceLogs");


              
                    var qry3 = from p in db.G_SERVICE_LOGs
                               where p.CaseEBPID_FK == intCaseEBPID
                               select new { p.ServiceID, p.DATE_OF_SERVICE, p.CaseID_FK, p.CaseEBPID_FK };
                    repServiceLogs.DataSource = qry3;
                    repServiceLogs.DataBind();
               
                //-----Modified on july 13th---------------
                    var qry4 = db.getTicklerServiceLog(intCaseEBPID);
                    repServiceLogs.DataSource = qry4;
                    repServiceLogs.DataBind();

              
            }
            
        }
    }
}