﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="UnAuthorized.aspx.cs" Inherits="Error_UnAuthorized" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Permission Error</h1>
    <p>You do not have privileges to view this page. Please contact System Administrator.</p>
</asp:Content>

