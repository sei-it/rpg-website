﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ServiceLogList.aspx.cs" Inherits="ServiceLogList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <h1>Service Logs for EBPs</h1>
    <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal></h2>
    <div>
        <p>
            <asp:Button ID="btnNew" runat="server"   Text="Create New Service Log" OnClick="btnNew_Click" Visible="False" /> 
        </p>
        </div>
         <asp:GridView ID="grdVwList" runat="server"   CssClass="gridTbl" 
            AutoGenerateColumns="False"     AllowPaging="false"  
            AllowSorting="True" OnRowCommand="grdVwList_RowCommand"   >
            <Columns>
 
		            <asp:BoundField DataField="created_by" HeaderText="Case Worker" DataFormatString="{0:g}">
		            <HeaderStyle Width="200px"></HeaderStyle>
		            </asp:BoundField>  
		            <asp:BoundField DataField="DATE_OF_SERVICE" HeaderText="Date of Session" DataFormatString="{0:MM/dd/yyyy}">
                        		            <HeaderStyle Width="120px"></HeaderStyle>
		            </asp:BoundField>

		            <asp:BoundField DataField="SessionLocation" HeaderText=" Session Location" DataFormatString="{0:g}">
		            <HeaderStyle Width="200px"></HeaderStyle>
		            </asp:BoundField> 
		            <asp:BoundField DataField="EBPName" HeaderText="EBP"  >
                        		            <HeaderStyle Width="120px"></HeaderStyle>
		            </asp:BoundField>
                    <asp:TemplateField>
                       <HeaderTemplate>View Service Log                   
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="ServiceLogs" visible="true"  CssClass="otherstyle" CommandArgument='<%# Eval("ServiceID") %>'>View</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="250px" />
                        </asp:TemplateField>  
            </Columns>
        </asp:GridView>
</asp:Content>

