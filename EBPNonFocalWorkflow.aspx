﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EBPNonFocalWorkflow.aspx.cs" Inherits="EBPNonFocalWorkflow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>EBP Service Log</h1>
    <p>
        The EBP selected for this enrollment is not a focal EBP for this grantee. No Service Logs can be created for EBPs that are not focal.
    </p>
</asp:Content>

