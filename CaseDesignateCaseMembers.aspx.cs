﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CaseDesignateCaseMembers : System.Web.UI.Page
{
    int lngPkID;
    int lngCaseID;
    int count = 0;
    int countf = 0;
    int countD = 0;
    int Countxt = 0;
    int CountOther = 0;
    int IdenRelationship;
    int ValFChild;
    int ValFChildCount;
    int c1;
    int i = 1;
    int j = 1;
    int k = 1;
    int intCaseMemberID, IndividualChildAdultID = 0;
    string ChildValue;
    UserInfo oUI;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        if (!IsPostBack)
        {
            Session["CaseEditlngPkID"] = null;
            Session["CaseEditIdenRelationship"] = null;

            if (Page.Request.QueryString["lngCaseID"] != null)
            {
                lngCaseID = Convert.ToInt32(Page.Request.QueryString["lngCaseID"]);
                Session["CaseEditlngPkID"] = lngCaseID;
            }
            if (Page.Request.QueryString["IdenRelationship"] != null)
            {
                IdenRelationship = Convert.ToInt32(Page.Request.QueryString["IdenRelationship"]);
                Session["CaseEditIdenRelationship"] = IdenRelationship;
            }
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["ValFChild"] != null)
            {
                ValFChild = Convert.ToInt32(Page.Request.QueryString["ValFChild"]);
            }
            if (Page.Request.QueryString["Countxt"] != null)
            {
                Countxt = Convert.ToInt32(Page.Request.QueryString["Countxt"]);
            }
            if (Page.Request.QueryString["ValFChildCount"] != null)
            {
                ValFChildCount = Convert.ToInt32(Page.Request.QueryString["ValFChildCount"]);
            }
            if (Page.Request.QueryString["CountOther"] != null)
            {
                CountOther = Convert.ToInt32(Page.Request.QueryString["CountOther"]);
            }
            if (Page.Request.QueryString["c1"] != null)
            {
                c1 = Convert.ToInt32(Page.Request.QueryString["c1"]);
            }
            if (Page.Request.QueryString["i"] != null)
            {
                i = Convert.ToInt32(Page.Request.QueryString["i"]);
            }
            if (Page.Request.QueryString["j"] != null)
            {
                j = Convert.ToInt32(Page.Request.QueryString["j"]);
            }
            if (Page.Request.QueryString["k"] != null)
            {
                k = Convert.ToInt32(Page.Request.QueryString["k"]);
            }
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            AdultChild.Visible = false; // added on 27th June
            btnSave.Visible = false;
            btnClose.Visible = false;



            displayGrid(lngCaseID);
            getCount(lngCaseID);
            loadDropdown();
            displayChildAdult();
        }
        if (oUI.intUserRoleID != 3)
        {
            DisableBtnSave();
        }
        

       
    }

    private void displayChildAdult()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            object objVal2 = null;
            var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                       where p.IndividualID == IndividualChildAdultID
                       select p;

            foreach (var oCase in qry2)
            {
                if (!(oCase.ChildAdultID_FK == null))
                {
                    objVal2 = oCase.ChildAdultID_FK;
                    if (objVal2 != null)
                    {
                        rblchildAdult.SelectedIndex = rblchildAdult.Items.IndexOf(rblchildAdult.Items.FindByValue(oCase.ChildAdultID_FK.ToString()));
                       
                    }
                }
                
            }
        }
    }

    private void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.C_ChildAdultCares select p;

            rblchildAdult.DataSource = qry;
            rblchildAdult.DataTextField = "ChildAdultCare";
            rblchildAdult.DataValueField = "ChildAdultID";
            rblchildAdult.DataBind();
        }

    }
    private void getCount(int Caseid)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vCases1 = db.getCaseMemberForCaseIDFocal(Caseid);
            ValFChildCount = vCases1.Count();
            CountOther = ValFChildCount;

        }
    }
    private void DisableBtnSave()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var qryFlag = from Flag in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                          where Flag.CaseNoId_FK == lngCaseID
                          select Flag;
            foreach (var oCase in qryFlag)
            {
                if (oCase.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED == true)
                {
                    btnSave.Visible = false;
                    btnSaveClose.Visible = false;
                }
            }

        }
    }

    private void displayGrid(int intCaseID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                litSurname.Text = oCase.SURNAME;
                //intGranteeID = oCase.GranteeID_fk;
            }

            
            //var vCases = db.getCaseMemberForCaseIDFocal(intCaseID);
            //  foreach (var oCase in vCases)
            //    {
            var oPages = from pg in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                         where pg.CaseNoId_FK == intCaseID
                         select pg;
            foreach (var oCase in oPages)
            {

                   objVal = oCase.ChildAdultID_FK;
                    if (objVal != null)
                    {

                        rblchildAdult.SelectedIndex = rblchildAdult.Items.IndexOf(rblchildAdult.Items.FindByValue(oCase.ChildAdultID_FK.ToString()));
                    }
                    AdultChild.Visible = true; // added on 27th June
                    btnSave.Visible = true;
                    btnClose.Visible = true;
              }



            var vCases1 = db.getCaseMemberForCaseIDFocal(intCaseID);
            grdVwList.DataSource = vCases1;
            grdVwList.DataBind();


        }

    }

  protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int intRelationshipToFocalID = 0;
        string strJS;
        CheckBox chkFocalChild;
        CheckBox chkFamilyFuncAdult;
        CheckBox chkDomainRecAdult;
        DropDownList ddlRelashtionship;
        TextBox txtfocalchildIfOther; 

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "IndividualID") != null)
            {
                intCaseMemberID = (int)DataBinder.Eval(e.Row.DataItem, "IndividualID");
                ((HiddenField)e.Row.FindControl("hidIndividualID")).Value = intCaseMemberID.ToString();
                if (DataBinder.Eval(e.Row.DataItem, "ChildAdultID_FK") != null)
                {
                    IndividualChildAdultID = (int)DataBinder.Eval(e.Row.DataItem, "IndividualID");
                }
            }

            chkFocalChild = (CheckBox)e.Row.FindControl("chkGrdFocalchild");     
            chkFamilyFuncAdult = (CheckBox)e.Row.FindControl("chkGrdFamilyfunctioningadult");
            chkDomainRecAdult = (CheckBox)e.Row.FindControl("chkGrdDomainrecoveryadult");
            txtfocalchildIfOther = (TextBox)e.Row.FindControl("txtRelatfocalchildIfOther");
          
            //'Load and populate relation dropdown
            ddlRelashtionship = (DropDownList)e.Row.FindControl("cboRelationshiptofocalchild");

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                var qry2 = from p in db.C_RelationshipFocalChilds select p;
                ddlRelashtionship.DataSource = qry2;
                ddlRelashtionship.DataTextField = "RelashionShipToFocal";
                ddlRelashtionship.DataValueField = "RelFocalChildID";
                ddlRelashtionship.DataBind();
                ddlRelashtionship.Items.Insert(0, "Select");
            }

            //-------Display Check box related to child and Adult-------------

            using (DataClassesDataContext db = new DataClassesDataContext())
            {

                var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                           where p.IndividualID == intCaseMemberID
                           select new { p.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED, p.PERSON_TYPE_FK };

                foreach (var oCase in qry2)
                {
                    if (oCase.PERSON_TYPE_FK == 1)
                    {
                        chkFocalChild.Visible = false;


                        //Adult
                    }
                    else
                    {
                        //txtfocalchildIfOther.Visible = false;
                       // ddlRelashtionship.Visible = false;
                        chkDomainRecAdult.Visible = false;
                        chkFamilyFuncAdult.Visible = false;

                    }
                }
            }
            //----------------------------------------------------------------
           
            
            if (DataBinder.Eval(e.Row.DataItem, "RELATIONSHIP_TO_FOCAL_CHILD_FK") != null)
            {
                intRelationshipToFocalID = (int)DataBinder.Eval(e.Row.DataItem, "RELATIONSHIP_TO_FOCAL_CHILD_FK");
            }

            if (intRelationshipToFocalID != 0)
            {
                ddlRelashtionship.SelectedIndex = ddlRelashtionship.Items.IndexOf(ddlRelashtionship.Items.FindByValue(intRelationshipToFocalID.ToString() ));
               
            }
            
            

            if (chkFocalChild.Checked == true)
            {
               
                txtfocalchildIfOther.Visible = false;
                ddlRelashtionship.Visible = false;
                chkDomainRecAdult.Visible = false;
                chkFamilyFuncAdult.Visible = false;
            }
            else
            {
                txtfocalchildIfOther.Visible = true;
                ddlRelashtionship.Visible = true;
               // chkDomainRecAdult.Visible = true;
               // chkFamilyFuncAdult.Visible = true;
            }

            if (ddlRelashtionship.SelectedItem.Value == "12") // change from 14 to 12 for production data id value
            {                
                txtfocalchildIfOther.Visible = true;
                if (!(String.IsNullOrEmpty(txtfocalchildIfOther.Text)))
                {
                   // Countxt = 1;
                    lblValMessage.Text = "";
                }               
            }
            else 
            {               
                txtfocalchildIfOther.Visible = false;
            }

            if (ddlRelashtionship.SelectedItem.Value == "Select")
            {
               // ValFChild = 1;
            }

            if(!(String.IsNullOrEmpty(txtfocalchildIfOther.Text)))
            {
                //Countxt = 1;
            }
        }
    }    

      private void saveGrid()
      {  
          
        foreach (GridViewRow grdRow in grdVwList.Rows)
        {
            saveGridItems(grdRow);
        }
      }

      public void saveGridItems(GridViewRow di)
      {
          object objTmp = null;
          int intIndividualID;
          int? intRelationshipFocalChild;
          Boolean? blFocalChild  =false ;
          Boolean? blFamilyfunctioningadult=false ;
          Boolean? blDomainrecoveryadult=false ;

          DropDownList cboRelationshiptofocalchild;
          CheckBox chkGrdFocalchild;
          CheckBox chkGrdFamilyfunctioningadult;
          CheckBox chkGrdDomainrecoveryadult;
          TextBox txtRelatfocalchildIfOther;
          HiddenField hidIndividualID;

         
          //----------------------------------------------------------------
          cboRelationshiptofocalchild = (DropDownList)di.FindControl("cboRelationshiptofocalchild");
          if ((cboRelationshiptofocalchild.SelectedItem.Value != "Select"))
          {
              intRelationshipFocalChild = Convert.ToInt32(cboRelationshiptofocalchild.SelectedItem.Value);
          }
          else
          {
              intRelationshipFocalChild = null;
          }
          //---------------------------------------------------------------
          chkGrdFocalchild = (CheckBox)di.FindControl("chkGrdFocalchild");
          if ((chkGrdFocalchild.Checked==true))
          {
              blFocalChild = true;
              
          }
          else
          {
              blFocalChild = false;
          }

          //-------------------------------------------------------------------
          chkGrdFamilyfunctioningadult = (CheckBox)di.FindControl("chkGrdFamilyfunctioningadult");
          if ((chkGrdFamilyfunctioningadult.Checked == true))
          {
              blFamilyfunctioningadult = true;
          }
          else
          {
              blFamilyfunctioningadult = false;
          }
          //------------------------------------------------------------------
          chkGrdDomainrecoveryadult = (CheckBox)di.FindControl("chkGrdDomainrecoveryadult");
          if ((chkGrdDomainrecoveryadult.Checked == true))
          {
              blDomainrecoveryadult = true;
          }
          else
          {
              blDomainrecoveryadult = false;
          }

          //-------------------------------------------------------------
          txtRelatfocalchildIfOther = (TextBox)di.FindControl("txtRelatfocalchildIfOther");


          if (!(rblchildAdult.SelectedItem == null))
            {
               

                if (!(rblchildAdult.SelectedItem.Value.ToString() == ""))
                {
                    ChildValue = rblchildAdult.SelectedItem.Value.ToString();
                }
                else
                {
                    ChildValue = null;
                }
            }


          //--------------------------------------------------------------
          hidIndividualID = (HiddenField )di.FindControl("hidIndividualID");
          intIndividualID = Convert.ToInt32( hidIndividualID.Value);

         //------------------------------------------------------------------
          using (DataClassesDataContext db = new DataClassesDataContext())
          {
              C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE oCase = (from c in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs 
                                                                 where c.IndividualID == intIndividualID select c).FirstOrDefault();



              oCase.RELATIONSHIP_TO_FOCAL_CHILD_FK = intRelationshipFocalChild;
              oCase.RELATIONSHIP_TO_FOCAL_CHILD_IF_OTHER = txtRelatfocalchildIfOther.Text;
              oCase.FOCAL_CHILD_YN = blFocalChild ;
              oCase.FAMILY_FUNCTIONING_ADULT_YN = blFamilyfunctioningadult ;
              oCase.DOMAIN_RECOVERY_ADULT_YN = blDomainrecoveryadult ;
              oCase.ChildAdultID_FK = Convert.ToInt32(ChildValue.ToString());

              oCase.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED = true;
              oCase.UpdatedDate = DateTime.Now;
              oCase.Freeze_DOB_UpdatedDate = DateTime.Now;
              oCase.UpdatedBy=System.Web.HttpContext.Current.User.Identity.Name;
               db.SubmitChanges();
               
          }

 
      }

      protected void btnSave_Click(object sender, EventArgs e)
      {
          bool isValidData = true;
          int intVaild = 0;
          int intFocalChild = 0; 
          int intFFAdult = 0;
          int intDRAdult = 0;
          string sVaidErrorMessage="";
         
          foreach(GridViewRow itm in grdVwList.Rows)
          {
              CheckBox cbxFocalChild = itm.Cells[2].Controls[1] as CheckBox;
              CheckBox cbxFamilyFunctioningAdult = itm.Cells[5].Controls[1] as CheckBox;
              CheckBox cbxDomainrecoveryAdult = itm.Cells[6].Controls[1] as CheckBox;
              if (!cbxFocalChild.Checked)
              {
                  DropDownList ddllist = itm.Cells[3].Controls[1] as DropDownList;
                  if (ddllist.Visible == true)
                  {
                      if (ddllist.SelectedValue.ToLower() == "select")
                      {
                          LiteralControl ltlError = itm.Cells[3].Controls[0] as LiteralControl;
                          ltlError.Text = "<span style='color:red'>Please select one of the items.</span>";
                          isValidData = false;
                      }
                      else if (ddllist.SelectedValue == "12") //Other change from 14 to 12 for production data id value
                      {
                          TextBox txtOther = itm.Cells[4].Controls[1] as TextBox;
                          if (string.IsNullOrEmpty(txtOther.Text))
                          {
                              LiteralControl ltlError = itm.Cells[4].Controls[0] as LiteralControl;
                              ltlError.Text = "<span style='color:red'>Required</span>";
                              isValidData = false;
                          }
                      }
                  }
                  else if (ddllist.Visible == false)
                  {
                      LiteralControl ltlError = itm.Cells[3].Controls[0] as LiteralControl;
                      ltlError.Visible=false;
                      //isValidData = false;
                  }
                  
              }
              else
              {
                  intFocalChild = intFocalChild + 1;
              }
              if (cbxFamilyFunctioningAdult.Checked)
                  {
                      i = i + 1;
                      //LiteralControl ltlError = itm.Cells[4].Controls[0] as LiteralControl;
                      //ltlError.Text = "<span style='color:red'>Required</span>";
                      // isValidData = true;
                      intFFAdult = intFFAdult + 1;
                  }
              if (cbxDomainrecoveryAdult.Checked)
                  {
                      j = j + 1;
                      // LiteralControl ltlError = itm.Cells[5].Controls[0] as LiteralControl;
                      //ltlError.Text = "<span style='color:red'>Required</span>";
                      // isValidData = true;
                  intDRAdult =intDRAdult+1;
                  }
           }
          //////iterate checkboxes/////////////////


          ///Vaildttion Messges//////////////////////////
          if ((intFocalChild < 1))
          {
              sVaidErrorMessage = "ONE Focal Child needs to be selected</br>" ;
          }
          if ((intFocalChild > 1))
          {
              sVaidErrorMessage = "ONLY ONE Focal Child can be selected</br>";
          }
          if (intFFAdult < 1)
          {
              sVaidErrorMessage = "One Family Functioning Adult needs to be selected</br>";
          }
          if (intDRAdult < 1)
          {
              sVaidErrorMessage = "One Recovery Domain Adult needs to be selected.</br>";
          }

          if ((intFocalChild == 1) && (intFFAdult > 0) && (intDRAdult > 0) && (isValidData == true))
          {
              isValidData = true;
              ltErrorMessage.Text = "";
          }
          else
          {
              isValidData = false ;
              ltErrorMessage.Text = sVaidErrorMessage;

          }
          //////////////////////////////////////////

          if (!isValidData)                    
               return;

          if (oUI.intUserRoleID == 3)// added on 27th june
          {
              lblValMessage.Text = "";
              saveGrid();
              IdenRelationship = 1;
              btnSave.Visible = true;
              Response.Redirect("CaseEdit.aspx?IdenRelationship=" + Session["CaseEditIdenRelationship"] + "&lngPkID=" + Session["CaseEditlngPkID"]);

          }
          else
          {
              lblValMessage.Text = "";
              saveGrid();
              IdenRelationship = 1;
              btnSave.Visible = false;
              Response.Redirect("CaseEdit.aspx?IdenRelationship=" + Session["CaseEditIdenRelationship"] + "&lngPkID=" + Session["CaseEditlngPkID"]);
          }
        
       
      }
      protected override void LoadViewState(object savedState)
      {
          base.LoadViewState(savedState);

          if (((this.ViewState["lngCaseID"] != null)))
          {
              lngCaseID = Convert.ToInt32(this.ViewState["lngCaseID"]);
          }

          if (((this.ViewState["IdenRelationship"] != null)))
          {
              IdenRelationship = Convert.ToInt32(this.ViewState["IdenRelationship"]);
          }
          if (((this.ViewState["ValFChild"] != null)))
          {
              ValFChild = Convert.ToInt32(this.ViewState["ValFChild"]);
          }

          if (((this.ViewState["CountOther"] != null)))
          {
              CountOther = Convert.ToInt32(this.ViewState["CountOther"]);
          }
          if (((this.ViewState["Countxt"] != null)))
          {
              Countxt = Convert.ToInt32(this.ViewState["Countxt"]);
          }
          if (((this.ViewState["ValFChildCount"] != null)))
          {
              ValFChildCount = Convert.ToInt32(this.ViewState["ValFChildCount"]);
          }
          if (((this.ViewState["c1"] != null)))
          {
              c1 = Convert.ToInt32(this.ViewState["c1"]);
          }

          if (((this.ViewState["i"] != null)))
          {
              i = Convert.ToInt32(this.ViewState["i"]);
          }

          if (((this.ViewState["j"] != null)))
          {
              j = Convert.ToInt32(this.ViewState["j"]);
          }
          if (((this.ViewState["k"] != null)))
          {
              k= Convert.ToInt32(this.ViewState["k"]);
          }
          if (((this.ViewState["oUI"] != null)))
          {
              oUI = (UserInfo)(this.ViewState["oUI"]);
          }

      }
      protected override object SaveViewState()
      {

          this.ViewState["lngCaseID"] = lngCaseID;
          this.ViewState["IdenRelationship"] = IdenRelationship;
          this.ViewState["ValFChild"] = ValFChild;
          this.ViewState["Countxt"] = Countxt;
          this.ViewState["CountOther"] = CountOther;
          this.ViewState["i"] = i;
          this.ViewState["j"] = j;
          this.ViewState["k"] = k;
          this.ViewState["c1"] = c1;
          this.ViewState["ValFChildCount"] = ValFChildCount;
          this.ViewState["oUI"] = oUI;
          
          return (base.SaveViewState());
      }

      protected void btnSaveClose_Click(object sender, EventArgs e)
      {
          saveGrid();
          IdenRelationship = 1;
          Response.Redirect("CaseEdit.aspx?IdenRelationship=" + Session["CaseEditIdenRelationship"] + "&lngPkID=" + Session["CaseEditlngPkID"]);
      }


      protected void cboRelationshiptofocalchild_SelectedIndexChanged(object sender, EventArgs e)
      {
          DropDownList ddlRelas;
          TextBox txtfocal;


            //ValFChild = 1;
          lblValMessage.Text = "";
          foreach (GridViewRow rowItem in grdVwList.Rows)
          {
              ValFChild = ValFChild + 1;
             // lblValMessage.Text = "";
              ddlRelas = (DropDownList)(rowItem.Cells[0].FindControl("cboRelationshiptofocalchild"));
              txtfocal = (TextBox)(rowItem.Cells[0].FindControl("txtRelatfocalchildIfOther"));

              if (ddlRelas.SelectedItem.Text.ToLower() == "other") //change from 14 to 12 for production data id value
              {
                 
                  txtfocal.Visible = true;

                  if (String.IsNullOrEmpty(txtfocal.Text))
                  {
                      Countxt = 0;
                  }
                  else
                  {
                      Countxt = 1;
                      lblValMessage.Text = "";
                  }

              }
              //else if (!(string.IsNullOrEmpty(ddlRelas.SelectedItem.Value)))
              else //if (ddlRelas.SelectedItem.Value == "Select")
              {
                  //Countxt = 1;
                  ValFChild = 1;
                  txtfocal.Text = "";
                  txtfocal.Visible = false;
              }       
             
          } 

      }    
      protected void chkGrdFocalchild_CheckedChanged(object sender, EventArgs e)
      {
          CheckBox chk;
          CheckBox chkFamily;
          CheckBox chkDomain;
          DropDownList ddlRelas;
          TextBox txtfocal;
          c1 = 0;
        

          foreach (GridViewRow rowItem in grdVwList.Rows)
          {
              chk = (CheckBox)(rowItem.Cells[0].FindControl("chkGrdFocalchild"));
              chkFamily = (CheckBox)(rowItem.Cells[0].FindControl("chkGrdFamilyfunctioningadult"));
              chkDomain = (CheckBox)(rowItem.Cells[0].FindControl("chkGrdDomainrecoveryadult"));
              ddlRelas = (DropDownList)(rowItem.Cells[0].FindControl("cboRelationshiptofocalchild"));
              txtfocal = (TextBox)(rowItem.Cells[0].FindControl("txtRelatfocalchildIfOther"));
              //chk.Checked =((CheckBox)sender).Checked;

              if (count >= 1)
              {

                  chk.Checked = false;
              }

              if (chk.Checked == true)
              {
                  c1 = 1;
                  //ValFChild = 1;
                  //lblValMessage.Text = "";
                  txtfocal.Text = "";
                  ddlRelas.SelectedValue = "Select";
                  chkDomain.Checked = false;
                  chkFamily.Checked = false;

                  txtfocal.Visible = false;
                  ddlRelas.Visible = false;
                  chkDomain.Visible = false;
                  chkFamily.Visible = false;
                  count = +1;
              }
              else
              {
                  if (ddlRelas.SelectedItem.Text.ToLower() != "other") //change from 14 to 12 for production data id value
                  {
                      txtfocal.Visible = false;
                  }
                  else
                  {
                      txtfocal.Visible = true;
                  }
                  ddlRelas.Visible = true;
                 // chkDomain.Visible = true;
                 // chkFamily.Visible = true;
              }
          }

      }         
      protected void chkGrdFamilyfunctioningadult_CheckedChanged(object sender, EventArgs e)
      {
          CheckBox chkFamily;

          foreach (GridViewRow rowItem in grdVwList.Rows)
          {

              chkFamily = (CheckBox)(rowItem.Cells[0].FindControl("chkGrdFamilyfunctioningadult"));


              if (countf >= 1)
              {
                  chkFamily.Checked = false;
              }

              if (chkFamily.Checked == true)
              {
                  countf = +1;
              }
          }

      }
      protected void chkGrdDomainrecoveryadult_CheckedChanged(object sender, EventArgs e)
      {
          CheckBox chkDomain;

          foreach (GridViewRow rowItem in grdVwList.Rows)
          {

              chkDomain = (CheckBox)(rowItem.Cells[0].FindControl("chkGrdDomainrecoveryadult"));


              if (countD >= 1)
              {
                  chkDomain.Checked = false;
              }

              if (chkDomain.Checked == true)
              {
                  countD = +1;
              }
          }

      }

      protected void btnClose_Click(object sender, EventArgs e)
      {
          string strJS = null;
          strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
          strJS = strJS + "opener.location=opener.location; </script>";
          Response.Redirect("CaseEdit.aspx?IdenRelationship=" + Session["CaseEditIdenRelationship"] + "&lngPkID=" + Session["CaseEditlngPkID"]);
      }
      protected void txtRelatfocalchildIfOther_TextChanged(object sender, EventArgs e)
      {
          DropDownList ddlRelas;
          TextBox txtfocal;
          foreach (GridViewRow rowItem in grdVwList.Rows)
          {
              ddlRelas = (DropDownList)(rowItem.Cells[0].FindControl("cboRelationshiptofocalchild"));
              txtfocal = (TextBox)(rowItem.Cells[0].FindControl("txtRelatfocalchildIfOther"));

              if (ddlRelas.SelectedItem.Text.ToLower()=="other") //change from 14 to 12 for production data id value
              {

                  txtfocal.Visible = true;

                  if (String.IsNullOrEmpty(txtfocal.Text))
                  {
                      Countxt = 0;
                  }
                  else
                  {
                      Countxt = 1;
                      lblValMessage.Text = "";
                  }
              }
              else if (ddlRelas.SelectedItem.Value == "Select")
              {
                 
                  txtfocal.Text = "";
                  txtfocal.Visible = false;
              }
          }


      }
}