﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;

public partial class Admin_UsersList : System.Web.UI.Page
{
    UserInfo oUI;
    string Emailadd;
    int Userid;
    protected void Page_Load(object sender, EventArgs e)
    {
  

        if (!IsPostBack)
        {
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            if (!(oUI.intUserRoleID == 4 || oUI.intUserRoleID == 3 )) // changed for SysAdmin role june27)
            {
                Response.Redirect("~/Error/UnAuthorized.aspx");
            }

            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
    }



    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
           
            var vUsers = from p in db.vw_SuperSys_AdminLists
                         where p.isActive
                         orderby p.sUserID descending
                         select p;
            grdVwList.DataSource = vUsers;
            grdVwList.DataBind();
        }

    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

      
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }



    }
    protected override object SaveViewState()
    {

        this.ViewState["oUI"] = oUI;
       
        return (base.SaveViewState());
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("AppUserEdit.aspx?intAppUserID=0"  );
    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            int intID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("AppUserEdit.aspx?intAppUserID=" + intID);
        }

        //if (e.CommandName == "ViewEdit")
        //{
        //    int intID = Convert.ToInt32(e.CommandArgument);
        //    Response.Redirect("AppUserEdit.aspx?intAppUserID=" + intID);
        //}

        if (e.CommandName == "Delete")
        {
            int intID = Convert.ToInt32(e.CommandArgument);
            DeleteAdminUser(intID);
            Response.Redirect("UsersList.aspx");
        }
    }

    private void DeleteAdminUser(int intID)
    {     
       
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                var case0 = (from c in db.AppUsers
                                 where c.Userid == intID
                                 select new { c.sUserID });

                if ((case0 != null))
                {
                    foreach (var var1 in case0)
                    {
                        if (!(var1.sUserID == null))
                        {
                            Emailadd = var1.sUserID.ToString();
                        }
                    }
                }


                AppUser case1 = (from c in db.AppUsers
                                 where c.Userid == intID
                                 select c).FirstOrDefault();
                if ((case1 != null))
                {
                    //db.AppUsers.DeleteOnSubmit(case1);
                    case1.isActive = false;
                }
                db.SubmitChanges();

               
                    A_Grantee_Contact case2 = (from c in db.A_Grantee_Contacts
                                               where c.Email == Emailadd
                                               select c).FirstOrDefault();
                    if ((case2 != null))
                    {
                        //db.A_Grantee_Contacts.DeleteOnSubmit(case2);
                        case2.isActive = false;
                    }
                    db.SubmitChanges();
                
            }     
        
    }
    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        LinkButton imgBtnDelete;
        LinkButton imgBtnViewEdit;
        LinkButton imgBtnEdit;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "Userid") != null)
            {
                Userid = (int)DataBinder.Eval(e.Row.DataItem, "Userid");
            }          

            if (  oUI.intUserRoleID== 3)
            {
                imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                imgBtnDelete.Visible = true;
                imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
                                              "confirm('Are you sure you want to delete this record')");

                imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
                imgBtnEdit.Visible = true;
            }
            else if (oUI.intUserRoleID == 4)
            {
                imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                imgBtnDelete.Visible = false;

                imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
                imgBtnEdit.Visible = true;
            }
            else
            {
                imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                imgBtnDelete.Visible = false;

                imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
                imgBtnEdit.Visible = false;
            }

        }
    }
}