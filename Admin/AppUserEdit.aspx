﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AppUserEdit.aspx.cs" Inherits="Admin_AppUserEdit" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
       <%-- <script>
            function checkEMailID(source, args) {
                args.IsValid = true;

                $.ajax({

                    type: "POST",
                    url: "AppUserService.asmx/checkDuplicateAccount",
                    data: "{strEmail: '" + $('#<%= txtUserID.ClientID%>').val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#duplicate").empty();
                if (response.d != "0") {
                    $("#duplicate").html('E-mail address is already in the system, try a different email address.');
                    args.IsValid = false;
                }
            }
        });
    }</script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <h1>User Edit</h1>
    <div class="formLayout">
         <p>
        <asp:Label ID="lblError1" runat="server" ForeColor="Red" Text="" Visible="false"></asp:Label>
    
    </p>
        <p>
		    <label for="txtUame">User Name 
           <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUame"  ValidationGroup="Save"
               ErrorMessage="User Name" ForeColor="Red">Required!</asp:RequiredFieldValidator>


		    </label>
		    <asp:textbox id="txtUame" runat="server" Width="225px" ></asp:textbox>
            <asp:Label ID="lblUserNameExist" runat="server" ForeColor="Red" Text="" ></asp:Label>
	    </p>
 
	    <p>
		    <label for="txtUserID">UserID (Email Address)

           <asp:RequiredFieldValidator ID="rfvEmailAddress" runat="server" ControlToValidate="txtUserID"  ValidationGroup="Save"
               ErrorMessage="Email Address" ForeColor="Red">Required!</asp:RequiredFieldValidator>
		    </label>
		    <asp:textbox id="txtUserID" runat="server" Width="226px" ></asp:textbox>

            <span id="duplicate" style="color: #FF0000">
                <asp:Literal ID="lblError" runat="server"></asp:Literal>
            </span>
           <%-- <asp:CustomValidator ID="CustomValidator1" runat="server"   ControlToValidate="txtUserID" ValidationGroup="Save"
                     ClientValidationFunction="checkEMailID" ErrorMessage="Email not valid">Email format not valid</asp:CustomValidator>--%>
            
	    </p>
        <div id="Adminrole" runat="server">
          <p >

		        <label for="rblAdminrole">Admin Role
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Save"
                     ControlToValidate="rblAdminRole" ErrorMessage="Admin Role" ForeColor="Red">Required!</asp:RequiredFieldValidator>
                </label>
               
		        &nbsp;<asp:RadioButtonList id="rblAdminRole" AutoPostBack="false" runat="server" RepeatDirection="Horizontal"  > 
		        </asp:RadioButtonList>
	        </p>

        </div>
      
        <p><asp:Label ID="lblErrorAdmin" runat="server" ForeColor="Red" test=""></asp:Label></p>
       <%-- <p style="display:none">
		    <label for="chkAdmin">Admin ?</label>
		    <asp:checkbox id="chkAdmin" runat="server"   Text=""></asp:checkbox>
	    </p>--%>
           <p>
        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" onclick="btnSave_Click" />&nbsp;
       <asp:Button ID="btnSaveClose" runat="server" Visible="false" ValidationGroup="Save" Text="Save And Close" onclick="btnSaveClose_Click" />&nbsp;
        <asp:Button ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnGeneratePassword" runat="server" Text="Reset Password" onclick="btnGeneratePassword_Click" />
          
</p>
        <p>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
          
</p>
        <p>
              &nbsp;<asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red"  HeaderText="Please fill in all the required fields." />

     </div>
</asp:Content>

