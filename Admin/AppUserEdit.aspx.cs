﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using System.Text;

public partial class Admin_AppUserEdit : System.Web.UI.Page
{
    int lngPkID;
    string username;
    string userId;
    string password;
    string url;
    UserInfo oUI;
    bool isDuplicateGc = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        if (!IsPostBack)
        {
            if (Page.Request.QueryString["intAppUserID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["intAppUserID"]);
            }
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            loadDropdown();
           
        }

      
        loadrecords();
       
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            SavebuttonEnable();
            ViewState["IsLoaded1"] = true;
        }

       

    }

    private void SavebuttonEnable()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

           

            String result = Regex.Replace(txtUserID.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");
            var oPages1 = from pg in db.AppUsers
                          where (pg.AdminRoleId_fk == 3 || pg.AdminRoleId_fk == 4) && pg.sUserID == result
                          select pg;
            //if (var oCase1 in oPages1)
            //{
            //    btnSave.Enabled = false;
            //    btnGeneratePassword.Enabled = false;
            //}
           
            foreach (var oCase1 in oPages1)
            {
                if ((oCase1.AdminRoleId_fk == 3)  && (oCase1.sUserID == result))
                {
                   // btnSave.Enabled = true;
                   // btnGeneratePassword.Enabled = true;
                    Adminrole.Visible = true;
                }
                else if( (oCase1.AdminRoleId_fk ==4)  && (oCase1.sUserID == result))
                {
                   // btnSave.Enabled = true;
                  //  btnGeneratePassword.Enabled = true;
                    Adminrole.Visible = true;
                }
                else
                {
                   // btnSave.Enabled = false;
                   // btnGeneratePassword.Enabled = false;
                }
            }

            AppUser oResSubmission = (from c in db.AppUsers where c.AdminRoleId_fk != null && c.Userid == lngPkID select c).FirstOrDefault();
            if ((oResSubmission == null))
            {
               // btnSave.Enabled = false;
               // btnGeneratePassword.Enabled = false;
            }


            if (lngPkID == 0)
            {
                btnSave.Enabled = true;
                btnGeneratePassword.Enabled = true;
            }
           

        }
    }

    //--------Load------------//
    private void loadDropdown()
    {
         using (DataClassesDataContext db = new DataClassesDataContext())
        {

            if (oUI.intUserRoleID == 3)                 // changed for SysAdmin role june27
            {
                var qry = from p in db.A_Admin_Roles select p;
                rblAdminRole.DataSource = qry;
                rblAdminRole.DataTextField = "AdminRole";
                rblAdminRole.DataValueField = "AdminRoleId";
                rblAdminRole.DataBind();

            }
            else
            {
                var qry2 = from p in db.A_Admin_Roles
                          where (p.AdminRoleId == 4)
                          select new { p.AdminRoleId, p.AdminRole };

                rblAdminRole.DataSource = qry2;
                rblAdminRole.DataTextField = "AdminRole";
                rblAdminRole.DataValueField = "AdminRoleId";
                rblAdminRole.DataBind();
                
            }


                //var qry = from p in db.A_Admin_Roles select p;
                //rblAdminRole.DataSource = qry;
                //rblAdminRole.DataTextField ="AdminRole";
                //rblAdminRole.DataValueField ="AdminRoleId";
                //rblAdminRole.DataBind();
             //'''''''''''''
        }
    }
    protected void loadrecords()
    {

    }


    //---Display/updateUser----//
    protected void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.AppUsers
                         where pg.Userid == lngPkID
                         select pg;

           
            foreach (var oResSubmission in oPages)
            {
                if (!((oResSubmission.AdminRoleId_fk == 3) || (oResSubmission.AdminRoleId_fk == 4)))
                {
                    Adminrole.Visible = false;
                    RequiredFieldValidator1.Enabled = false;
                    //btnSave.Enabled = false;
                    //btnGeneratePassword.Enabled = false;
                }
                else
                {
                    RequiredFieldValidator1.Enabled = true;
                    Adminrole.Visible = true;
                    //btnSave.Enabled = false;
                    //btnGeneratePassword.Enabled = false;
                }

                
                objVal = oResSubmission.username;
                if (objVal != null)
                {
                    txtUame.Text = objVal.ToString();
                }
                objVal = oResSubmission.sUserID ;
                if (objVal != null)
                {
                    txtUserID.Text = objVal.ToString();
                }
                //objVal = oResSubmission.blAdmin;
                //if (objVal != null)
                //{
                //    if (objVal.ToString() == "True")
                //    {
                //        chkAdmin.Checked = true;
                //    }
                //    else
                //    {
                //        chkAdmin.Checked = false;
                //    }
                //}

                objVal = oResSubmission.AdminRoleId_fk;
                if (objVal != null)
                {
                    Adminrole.Visible = true;
                    rblAdminRole.SelectedIndex =rblAdminRole.Items.IndexOf(rblAdminRole.Items.FindByValue(oResSubmission.AdminRoleId_fk.ToString()));
                }

               
            }

        }
    }
    public bool updateUser()
    {
        bool blNew = false;
        bool isDirty = false;
        bool isExistUser = false;

        int strTmp;
        DateTime dtTmp;
        string EmailsUserID="";

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.Userid == lngPkID select c).FirstOrDefault();
            AppUser User = (from c in db.AppUsers where c.username == txtUame.Text.Trim() select c).FirstOrDefault();
            string emailinDB = oResSubmission==null ? "" : oResSubmission.sUserID;

            A_Grantee_Contact gc = db.A_Grantee_Contacts.SingleOrDefault(x => x.Email == emailinDB);
            if (gc != null)
            {
                gc.Email = txtUserID.Text;
                db.SubmitChanges();
            }

            var svclog = from sl in db.G_SERVICE_LOGs
                         where sl.CaseworkerEmailID == emailinDB
                         select sl;
            foreach (var item in svclog)
            {
                item.CaseworkerEmailID = txtUserID.Text;
                db.SubmitChanges();
            }

            if (User != null)
                isExistUser = true;

             if ((oResSubmission == null))
            {
                oResSubmission = new AppUser();
                oResSubmission.isActive = true;
                blNew = true;
            }

             if (oResSubmission.sUserID != null && oResSubmission.sUserID.ToLower() != txtUserID.Text.Trim().ToLower())
                 isDirty = true;

            EmailsUserID = oResSubmission.sUserID;
            String result = Regex.Replace(txtUserID.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");
            oResSubmission.username = txtUame.Text;
            oResSubmission.sUserID = result;

            if (blNew == true)
            {
              
                object objVal1 = null;
                var oPages1 = from pg in db.AppUsers
                              where pg.Userid != lngPkID && pg.sUserID == result
                              select pg;
                foreach (var oCase1 in oPages1)
                {

                    objVal1 = oCase1.sUserID;
                    if (objVal1.ToString() == result)
                    {
                        isDuplicateGc = true;
                        string ErrorMsg = "Email already exists";
                        lblError.Text = ErrorMsg;
                    }
                    else
                    {
                       
                        oResSubmission.sUserID = result;
                    }
                }

                if (!(rblAdminRole.SelectedItem == null))
                {
                    if (!(rblAdminRole.SelectedItem.Value.ToString() == ""))
                    {
                        oResSubmission.AdminRoleId_fk = Convert.ToInt32(rblAdminRole.SelectedItem.Value.ToString());
                    }
                    else
                    {
                        oResSubmission.AdminRoleId_fk = null;
                    }
                }
            }
            else
            {
                oResSubmission.sUserID = result;
            }




            //if (!(rblAdminRole.SelectedItem == null))
            //{
            //    if (!(rblAdminRole.SelectedItem.Value.ToString() == ""))
            //    {
            //        oResSubmission.AdminRoleId_fk = Convert.ToInt32(rblAdminRole.SelectedItem.Value.ToString());
            //    }
            //    else
            //    {
            //        oResSubmission.AdminRoleId_fk = null;
            //    }
            //}


            if (blNew == true)
            {

                if (isDuplicateGc == false && !isExistUser)
                {
                     oResSubmission.blAdmin = true;
                     oResSubmission.passcode = CreateRandomPassword(6);
                   // SendEmailNotification();

                   // litMessage.Text = "userId and password have been sent to " + oResSubmission.sUserID;
                   

                    db.AppUsers.InsertOnSubmit(oResSubmission);

                    db.SubmitChanges();
                    lngPkID = oResSubmission.Userid;
                   
                }
                
                if(isDuplicateGc)
                {
                    string ErrorMsg = "Email already exists";
                    lblError.Text = ErrorMsg;
                }

                if (isExistUser)
                {
                    lblUserNameExist.Text = "User's name already exists";
                }

            }
            else
            {

                db.SubmitChanges();
                Adminrole.Visible = true;
                lngPkID = oResSubmission.Userid;
                litMessage.Text = "Updated!";
                
            }
        }
        //return isDirty;
        if (!isDuplicateGc && !isExistUser)
        {
            if (!blNew) //if user updated
            {
                updateGranteeUserEmail((string)result, EmailsUserID);
            }
            return true;
        }
        else
            return false;

    }

    private void updateGranteeUserEmail(string EmailtoUpdate, string EmailsUserID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            A_Grantee_Contact gcontact = (from rcd in db.A_Grantee_Contacts
                                         where rcd.Email == EmailsUserID
                                         select rcd).FirstOrDefault();
            gcontact.Email = EmailtoUpdate;
            gcontact.UpdatedBy = System.Web.HttpContext.Current.User.Identity.Name;
            gcontact.UpdatedDt = DateTime.Now; ;
            db.SubmitChanges();

        }
    }

    //----State view------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }

        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());

    }

    //----Save/Close/SaveClose------------//
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if(lngPkID!=0)  //edit mode
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                var theUser = (from usr in db.AppUsers
                               where usr.Userid == lngPkID
                               select usr).SingleOrDefault();

                var otherUser = from usr in db.AppUsers
                                where usr.Userid != lngPkID && usr.sUserID.Trim().ToLower() == txtUserID.Text.Trim().ToLower()
                                select usr;

                var gcUser = (from gcusr in db.A_Grantee_Contacts
                             where gcusr.Email.Trim().ToLower() == theUser.sUserID.Trim().ToLower()
                             select gcusr).SingleOrDefault();

                var slcsworkers = from cswkr in db.G_SERVICE_LOGs
                                  where cswkr.CaseworkerEmailID.Trim().ToLower() == theUser.sUserID.Trim().ToLower()
                                  select cswkr;

                if (otherUser.Count() > 0)
                {

                    string ErrorMsg = "Sorry, but the email you entered already belongs to the following person: " + otherUser.First().username;
                    //lblError.Text = ErrorMsg;
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "EmailExist", "<script>alert('" + ErrorMsg + "');</script>", false);
                    //string ErrorMsg = "Email already exists";
                    //lblError.Text = ErrorMsg;

                }
                else
                {
                    if (gcUser != null)
                    {
                        gcUser.Email = txtUserID.Text.Trim();
                        db.SubmitChanges();
                    }

                    foreach(var itm in slcsworkers)
                    {
                        itm.CaseworkerEmailID = txtUserID.Text.Trim();
                        db.SubmitChanges();
                    }

                    theUser.username = txtUame.Text.Trim();
                    theUser.sUserID = txtUserID.Text.Trim();
                    db.SubmitChanges();

                    Response.Redirect("UsersList.aspx");
                }

            }
        }
        else if (validateuplicateAppUser() == false)
        {
            if (validateuplicateGranteeContact() == false)
            {
                if (updateUser())   //return email has been changed flag
                {
                    SendEmailNotification();
                }
                else
                    return;

                displayRecords();
                Response.Redirect("UsersList.aspx");
            }
            else
                return;
        }
        else
            return;
     
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("UsersList.aspx");
    }
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        if (validateuplicateAppUser() == false)
        {
            if (validateuplicateGranteeContact() == false)
            {
                updateUser();
                Response.Redirect("UsersList.aspx");
            }
        }

    }

    //-----Validation for duplicat suerid------------//
    private bool validateuplicateAppUser()
    {
        bool isDuplicate = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            object objVal1 = null;
            object objVal2 = null;
            lblError.Text = "";

            // for new Grantee
            String result = Regex.Replace( txtUserID.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");
            var oPages1 = from pg in db.AppUsers
                          where pg.AdminRoleId_fk == 3 && pg.AdminRoleId_fk == 4 && pg.sUserID == result
                          select pg;
            foreach (var oCase1 in oPages1)
            {
                objVal1 = oCase1.sUserID;
                if (objVal1.ToString() == result)
                {
                    isDuplicate = true;
                    string ErrorMsg = "Email already exists";
                    lblError.Text = ErrorMsg;
                }

            }

            //------for updating grantee------
            var oPages2 = from pg in db.AppUsers
                          where pg.Userid == lngPkID
                          select pg;

            foreach (var oCase2 in oPages2)
            {

                objVal2 = oCase2.sUserID;
                if (objVal2.ToString() == result)
                {
                    isDuplicate = false;
                }
                else
                {
                    foreach (var oCase1 in oPages1)
                    {
                        objVal1 = oCase1.sUserID;
                        if (objVal1.ToString() == result)
                        {
                            isDuplicate = true;
                            string ErrorMsg = "Email already exists";
                            lblError.Text = ErrorMsg;
                        }

                    }
                }
            }




        }
        return isDuplicate;
    }
    private bool validateuplicateGranteeContact()
    {
        bool isDuplicateGc = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            object objVal1 = null;
            object objVal2 = null;
            lblError.Text = "";

            String result = Regex.Replace(txtUserID.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");

            // for new grantee contact
            var oPages1 = from pg in db.A_Grantee_Contacts
                          where pg.GranteeContactId == lngPkID && pg.Email == result
                          select pg;
            foreach (var oCase1 in oPages1)
            {

                objVal1 = oCase1.Email;
                if (objVal1.ToString() == result)
                {
                    isDuplicateGc = true;
                    string ErrorMsg = "Email already exists";
                    lblError.Text = ErrorMsg;
                }

            }

            //-------for updating existing GCW

            //var oPages2 = from pg in db.A_Grantee_Contacts
            //             where pg.GranteeContactId == lngPkID 
            //             select pg;
            //foreach (var oCase2 in oPages2)
            //{

            //        objVal2 = oCase2.Email;
            //        if (objVal2.ToString() == result)
            //        {
            //            isDuplicateGc = false;
            //            //string ErrorMsg = "Email already exists";
            //            //lblError.Text = ErrorMsg;
            //        }
            //        else
            //        {

            //              foreach (var oCase1 in oPages2)
            //                {
            //                    objVal1 = oCase1.Email;
            //                    if (objVal1.ToString() == result)
            //                    {
            //                        isDuplicateGc = true;
            //                        string ErrorMsg = "Email already exists";
            //                        lblError.Text = ErrorMsg;
            //                    }
            //                 }
            //          }
            //   }
        }
        return isDuplicateGc;
    }

    //-----Create Random password--------------------//
    private static string CreateRandomPassword(int passwordLength)
    {
        string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
        char[] chars = new char[passwordLength];
        Random rd = new Random();

        for (int i = 0; i < passwordLength; i++)
        {
            chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
        }

        return new string(chars);
    }   
    protected void btnGeneratePassword_Click(object sender, EventArgs e)
    {
        lblErrorAdmin.Text = "";
        ResetPassword();
        SendEmailNotification();

    }

    private void ResetPassword()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.Userid == lngPkID && c.username == txtUame.Text select c).FirstOrDefault();
            //ResourceSubmission oResSubmission = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oResSubmission != null))
            {
                oResSubmission.passcode = CreateRandomPassword(6);
                db.SubmitChanges();
            }
        }
    }

    private void SendEmailNotification()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.Userid == lngPkID && c.username == txtUame.Text select c).FirstOrDefault();
            //ResourceSubmission oResSubmission = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oResSubmission != null))
            {
                //oResSubmission.passcode = CreateRandomPassword(6);
                //  litMessage.Text = "password have been sent to " + oResSubmission.sUserID;
                //                User Name: " + oResSubmission.sUserID + @"<br/>
                //                Password: " + oResSubmission.passcode + @"<br/>
                //                url: http://rpgcl.seiservices.com";
                //                ;

                username = oResSubmission.username;
                userId = oResSubmission.sUserID;
                password = oResSubmission.passcode;

                db.SubmitChanges();
            }

            try
            {

                string strfrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];

                MailMessage objMailMsg = new MailMessage(strfrom, userId);
                objMailMsg.BodyEncoding = System.Text.Encoding.UTF8;
                //objMailMsg.Bcc.Add("FLacerda1@seiservices.com");
                // objMailMsg.CC.Add("VKothale@seiservices.com");

                objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
                url = System.Web.Configuration.WebConfigurationManager.AppSettings["rpgSite"];
                // //-------Email body-----------------------

                String strBody1;
                strBody1 = File.ReadAllText(Server.MapPath("AccountEmailConfirmation.html"));

                strBody1 = strBody1.Replace("[[User]]", username);
                //strBody1 = strBody1.Replace("[[UserId]]", userId);
                strBody1 = strBody1.Replace("[[password]]", password);
                // strBody1 = strBody1.Replace("[[URL]]", url);

                objMailMsg.Body = strBody1;

                // //=============================

                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

                // //--------prepare to send mail via SMTP transport-----//

                SmtpClient objSMTPClient = new SmtpClient(System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"], 587);
                objSMTPClient.EnableSsl = true;
                //objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                NetworkCredential userCredential = new NetworkCredential(strfrom, "R3&ra67@44!sr");

                objSMTPClient.Credentials = userCredential;
                objSMTPClient.Send(objMailMsg);
                litMessage.Text = "password have been sent to " + oResSubmission.sUserID;

            }
            catch(Exception ex)
            {
                litMessage.Visible = true;
                 litMessage.Text="Could not send the e-mail - error: " + ex.Message;
            }
        }
    }


    public object result { get; set; }
}