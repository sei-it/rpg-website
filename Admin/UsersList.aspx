﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="UsersList.aspx.cs" Inherits="Admin_UsersList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

        <h1>User List</h1>
    <h2> <asp:Literal ID="litCaseID" runat="server"></asp:Literal></h2><p></p>
    <div>
            <asp:Button ID="btnNew" runat="server"   Text="Create New User" OnClick="btnNew_Click" /></div><p></p>

         <asp:GridView ID="grdVwList" runat="server"   CssClass="gridTbl" 
            AutoGenerateColumns="False"  
            AllowSorting="True" OnRowCommand="grdVwList_RowCommand"  OnRowDataBound="grdVwList_RowDataBound"  >
            <Columns>
            		<asp:BoundField DataField="Userid" Visible="false" HeaderText="ID" DataFormatString="{0:g}">
		            <HeaderStyle Width="50px"></HeaderStyle>
		            </asp:BoundField>
                   <asp:BoundField DataField="GRANTEE_ID_NO"  HeaderText="GRANTEE ID" DataFormatString="{0:g}">
		            <HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
    		            <asp:BoundField DataField="sUserID" HeaderText="UserID" DataFormatString="{0:g}">
		            <HeaderStyle Width="150px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="Name" HeaderText="User Name" DataFormatString="{0:g}">
		            <HeaderStyle Width="200px"></HeaderStyle>
		            </asp:BoundField>
                    <asp:BoundField DataField="SysAdmin" HeaderText="Admin Role" ReadOnly="True"  />
                   <asp:BoundField DataField="GranteeName" HeaderText="Grantee Name" DataFormatString="{0:g}">
		            <HeaderStyle Width="200px"></HeaderStyle>
		            </asp:BoundField>
                   <asp:BoundField DataField="GranteeRole" HeaderText="Grantee Role" DataFormatString="{0:g}">
		            <HeaderStyle Width="200px"></HeaderStyle>
		            </asp:BoundField> 
 
    
                    <asp:TemplateField>
                       <HeaderTemplate>               
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="View" visible="false"  
                                    CommandArgument='<%# Eval("Userid") %>'>View/Edit</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField>  

                <%-- <asp:TemplateField>
                       <HeaderTemplate>               
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnViewEdit" runat="server" CommandName="ViewEdit" visible=false  CommandArgument='<%# Eval("Userid") %>'>View/Edit</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField>  --%>


                 <asp:TemplateField>
                       <HeaderTemplate>            
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnDelete" runat="server" CommandName="Delete" visible=false  CommandArgument='<%# Eval("Userid") %>'>Delete</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField>  
            </Columns>
        </asp:GridView>
</asp:Content>

