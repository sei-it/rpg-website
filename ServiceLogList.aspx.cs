﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ServiceLogList : System.Web.UI.Page
{
    int intCaseID;
    int intEPBID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["intCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
            }
            if (Page.Request.QueryString["intEPBID"] != null)
            {
                intEPBID = Convert.ToInt32(Page.Request.QueryString["intEPBID"]);
            }
        }

        if (!IsPostBack)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
    }



    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
            }


            var vCases = db.getServiceLogsForCaseID(intCaseID);
            grdVwList.DataSource = vCases;
            grdVwList.DataBind();
        }

    }





    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }
        if (((this.ViewState["intEPBID"] != null)))
        {
            intEPBID = Convert.ToInt32(this.ViewState["intEPBID"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["intEPBID"] = intEPBID;
        return (base.SaveViewState());
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("ServiceLogEdit.aspx?intCaseID=" + intCaseID);
    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ServiceLogs")
        {
            int intC = 1;
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("ServiceLogEdit.aspx?intCaseID=" + intCaseID + "&lngPkID=" + intResponseID + "&intC=" + intC);


        }
    }
}