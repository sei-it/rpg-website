﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO; 
using System.Configuration;
using System.Text;

public partial class GrantAdmins_GranteeContacts : System.Web.UI.Page
{
    int lngPkID, intGranteeID;
    string username;
    string userId;
    string password;
    string compPassword;
    string url;
    UserInfo oUI;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        

        if (!IsPostBack)
        {
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["intGranteeID"] != null)
            {
                intGranteeID = Convert.ToInt32(Page.Request.QueryString["intGranteeID"]);
            }
            if (Page.Request.QueryString["ErrorMsg"] != null)
            {
                lblError.Text = Page.Request.QueryString["ErrorMsg"].ToString();
            }
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            loadDropdown();
        }
        loadrecords();
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

    }

    protected void loadrecords()
    {
    }

    protected void displayRecords()
    {
        object objVal = null;
        DataClassesDataContext db = new DataClassesDataContext();
        var oPages = from pg in db.A_Grantee_Contacts 
                     where pg.GranteeContactId == lngPkID
                     select pg;
        foreach (var oCase in oPages)
        {



            objVal = oCase.FirstName;
            if (objVal != null)
            {
                txtFirstName.Text = objVal.ToString();
            }
            objVal = oCase.LastName;
            if (objVal != null)
            {
                txtLastName.Text = objVal.ToString();
            }
            objVal = oCase.Email;
            if (objVal != null)
            {
                txtEmail.Text = objVal.ToString();
               
            }
            objVal = oCase.Phone;
            if (objVal != null)
            {
                txtPhone.Text = objVal.ToString();
            }
            objVal = oCase.GranteeRole_fk;
            if (objVal != null)
            {
                rblGranteerole.SelectedIndex = rblGranteerole.Items.IndexOf(rblGranteerole.Items.FindByValue(oCase.GranteeRole_fk.ToString()));
            }


        }

        db.Dispose();
         
    }
    public void updateContacts()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            
            A_Grantee_Contact oCase = (from c in db.A_Grantee_Contacts where c.GranteeContactId == lngPkID select c).FirstOrDefault();
           // ResourceSubmission oCase1 = (from c in db.AppUsers where c.sUserID == 0 select c).First();
            if ((oCase == null))
            {
                oCase = new A_Grantee_Contact();
                blNew = true;
            }
           

            // before replaecing get the email 

            compPassword = oCase.Email;

            String result = txtEmail.Text;  // Regex.Replace(txtEmail.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");
            oCase.FirstName = txtFirstName.Text;
            oCase.LastName = txtLastName.Text;            
            oCase.Email = result;
            oCase.Phone = txtPhone.Text;
            oCase.isActive = true;

            if (!(rblGranteerole.SelectedItem == null))
            {
                if (!(rblGranteerole.SelectedItem.Value.ToString() == ""))
                {
                    oCase.GranteeRole_fk = Convert.ToInt32(rblGranteerole.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.GranteeRole_fk = null;
                }
            }
            oCase.UpdatedBy = System.Web.HttpContext.Current.User.Identity.Name;
            oCase.UpdatedDt = DateTime.Now;

            if (blNew == true)
            {
                oCase.GranteeId_fk = intGranteeID;
                db.A_Grantee_Contacts.InsertOnSubmit(oCase);

            }
            db.SubmitChanges();
            updateUsersForLogin(result, txtFirstName.Text + "." + txtLastName.Text, compPassword);
            lngPkID = oCase.GranteeContactId;
        }
        //LitJS.Text = " showSuccessToast();";
       // litMessage.Text = "Record saved! ID=" + lngPkID;



    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            //if ((oUI.intUserRoleID == 4 || oUI.intUserRoleID == 3 )) // changed for SysAdmin role june27
            //{
            //    var qry = from p in db.A_Grantee_Roles select p;
            //    rblGranteerole.DataSource = qry;
            //    rblGranteerole.DataTextField = "GranteeRole";
            //    rblGranteerole.DataValueField = "GranteeRoleId";
            //    rblGranteerole.DataBind();
            //    //'''''''''''''
            //}
            //else
            //{
                var qry = from p in db.A_Grantee_Roles
                          where (p.GranteeRoleId >=1 && p.GranteeRoleId<=2)
                          select new { p.GranteeRoleId, p.GranteeRole };

                 
                        rblGranteerole.DataSource = qry;
                        rblGranteerole.DataTextField = "GranteeRole";
                        rblGranteerole.DataValueField = "GranteeRoleId";
                        rblGranteerole.DataBind();
                   
            //}



        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intGranteeID"] != null)))
        {
            intGranteeID = Convert.ToInt32(this.ViewState["intGranteeID"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }

    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intGranteeID"] = intGranteeID;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (lngPkID != 0)  //edit grantee contact
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                //1.grantee contact
                A_Grantee_Contact oCase = (from c in db.A_Grantee_Contacts where c.GranteeContactId == lngPkID select c).FirstOrDefault();
                var otherGCName = (from c in db.A_Grantee_Contacts
                                   where c.GranteeContactId != lngPkID && c.GranteeId_fk == intGranteeID
                               && (c.FirstName.Trim().ToLower() == txtFirstName.Text.Trim().ToLower()
                                  && c.LastName.Trim().ToLower() == txtLastName.Text.Trim().ToLower()
                               ) select c);
                if (otherGCName.Count() > 0)
                {
                    string ErrorMsg = "Combination of first + last name is currently being used or has previously been used";
                    lblError.Text = ErrorMsg;
                    return;
                }
                var otherGCEmail = (from c in db.A_Grantee_Contacts
                                   where c.GranteeContactId != lngPkID
                                   && (c.Email.Trim().ToLower() == txtEmail.Text.Trim().ToLower() )
                                   select c);
                if (otherGCEmail.Count() > 0)
                {
                    var user = db.AppUsers.SingleOrDefault(x => x.sUserID.Trim().ToLower() == txtEmail.Text.Trim().ToLower());
                    string ErrorMsg = "Sorry, but the email you entered already belongs to the following person: " + user.username;
                    //lblError.Text = ErrorMsg;
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "EmailExist", "<script>alert('" + ErrorMsg + "');</script>", false);
                    return;
                }
                AppUser oSupperAdmin = (from c in db.AppUsers where c.sUserID.Trim().ToLower() == txtEmail.Text.Trim().ToLower() select c).FirstOrDefault();
                var gcEmail =(from c in db.A_Grantee_Contacts
                                   where (c.Email.Trim().ToLower() == txtEmail.Text.Trim().ToLower() )
                                   select c);
                if (oSupperAdmin != null && gcEmail.Count()==0)
                {
                    string ErrorMsg = "Email has been used by a supper admin";
                    lblError.Text = ErrorMsg;
                    return;
                }

                string sLoginUserID = oCase.Email;

                String result = txtEmail.Text;  // Regex.Replace(txtEmail.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");
                oCase.FirstName = txtFirstName.Text.Trim();
                oCase.LastName = txtLastName.Text.Trim();
                oCase.Email = result.Trim();
                oCase.Phone = txtPhone.Text;
                oCase.isActive = true;

                if (!(rblGranteerole.SelectedItem == null))
                {
                    if (!(rblGranteerole.SelectedItem.Value.ToString() == ""))
                    {
                        oCase.GranteeRole_fk = Convert.ToInt32(rblGranteerole.SelectedItem.Value.ToString());
                    }
                    else
                    {
                        oCase.GranteeRole_fk = null;
                    }
                }
                oCase.UpdatedBy = System.Web.HttpContext.Current.User.Identity.Name;
                oCase.UpdatedDt = DateTime.Now;

                db.SubmitChanges();
                //2.AppUser: sUserID
                AppUser oResSubmission = (from c in db.AppUsers where c.sUserID.Trim().ToLower() == sLoginUserID.Trim().ToLower() select c).FirstOrDefault();

                if (oResSubmission != null)
                {
                    oResSubmission.sUserID = result;
                    db.SubmitChanges();
                }
                //3.Servicelog: caseworkerEmailID
                var oSvclog = from svc in db.G_SERVICE_LOGs
                              where svc.CaseworkerEmailID == sLoginUserID
                              select svc;
                foreach (var item in oSvclog)
                {
                    item.CaseworkerEmailID = result;
                    db.SubmitChanges();
                }

            }

            Response.Redirect("GranteeEdit.aspx?lngPkID=" + intGranteeID);
        }
        else if (validateuplicateGranteeContact() == false)
            {
             if (validateuplicateAppUser() == false)
                {
                updateContacts();
                displayRecords();
                //SendEmail();
                }
             }        
       }

    private bool validateuplicateAppUser()
    {
        bool isDuplicate = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            object objVal1 = null;
            object objVal2 = null;
            lblError.Text = "";

            // for new Grantee
            String result = Regex.Replace(txtEmail.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");
            var oPages1 = from pg in db.AppUsers
                         where pg.AdminRoleId_fk != null && pg.sUserID == result
                         select pg;
            foreach (var oCase1 in oPages1)
            {
                objVal1 = oCase1.sUserID;
                if (objVal1.ToString() == result)
                {
                    isDuplicate = true;
                    string ErrorMsg = "Email already exists";
                    lblError.Text = ErrorMsg;
                 }

            }

            //------for updating grantee------
            var oPages2 = from pg in db.AppUsers
                          where pg.Userid == lngPkID  
                         select pg;
                     
            foreach (var oCase2 in oPages2)
            {

                objVal2 = oCase2.sUserID;
                if (objVal2.ToString() == result)
                {
                    isDuplicate = false;
                }
                else
                {
                    foreach (var oCase1 in oPages1)
                    {
                        objVal1 = oCase1.sUserID;
                        if (objVal1.ToString() == result)
                        {
                            isDuplicate = true;
                            string ErrorMsg = "Email already exists";
                            lblError.Text = ErrorMsg;
                        }

                    }
                }
            }
        }
        return isDuplicate;
    }

    private bool validateuplicateGranteeContact()
    {
        bool isDuplicateGc = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            object objVal1 = null;
            object objVal2 = null;
            lblError.Text = "";
            String result = Regex.Replace(txtEmail.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");

            // for new grantee contact
            var oPages1 = from pg in db.A_Grantee_Contacts
                         where pg.GranteeContactId != lngPkID && pg.Email == result
                         select pg;
            foreach (var oCase1 in oPages1)
            {

                objVal1 = oCase1.Email;
                if (objVal1.ToString() == result)
                {
                    isDuplicateGc = true;
                    string ErrorMsg = "Email already exists";
                    lblError.Text = ErrorMsg;
                }
            }

            var fullname = from fname in db.A_Grantee_Contacts
                           where fname.FirstName == txtFirstName.Text.Trim().ToLower()
                              && fname.LastName == txtLastName.Text.Trim().ToLower() && fname.GranteeId_fk == intGranteeID
                           select fname;

            if (fullname.Count() > 0)
            {
                isDuplicateGc = true;
                string ErrorMsg = "Combination of first + last name is currently being used or has previously been used";
                lblError.Text = ErrorMsg;
            }

            //-------for updating existing GCW

            var oPages2 = from pg in db.A_Grantee_Contacts
                         where pg.GranteeContactId == lngPkID 
                         select pg;
            foreach (var oCase2 in oPages2)
            {
                //string firstName_Lastname1 = oCase2.FirstName.Trim().ToLower() + oCase2.LastName.Trim().ToLower();
                //string firstName_Lastname2 = txtFirstName.Text.Trim().ToLower() + txtLastName.Text.Trim().ToLower();
                //if (firstName_Lastname1 == firstName_Lastname2)
                //{
                //    isDuplicateGc = true;
                //    string ErrorMsg = "Caseworker's first name + last name combination must be unique";
                //    lblError.Text = ErrorMsg;
                //    return true;
                //}

                objVal2 = oCase2.Email;
                if (objVal2.ToString() == result)
                {
                    isDuplicateGc = false;
                    //string ErrorMsg = "Email already exists";
                    //lblError.Text = ErrorMsg;
                }
                else
                {

                    foreach (var oCase1 in oPages1)
                    {
                        objVal1 = oCase1.Email;
                        if (objVal1.ToString() == result)
                        {
                            isDuplicateGc = true;
                            string ErrorMsg = "Email already exists";
                            lblError.Text = ErrorMsg;
                        }
                    }
                }
            }
        }
        return isDuplicateGc;
       
    }

    private void SendEmail()
    {

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.sUserID == txtEmail.Text select c).FirstOrDefault();
            var oPasscode = from c in db.AppUsers
                            where c.sUserID == txtEmail.Text
                            select new { c.passcode };

            if ((oResSubmission != null))
            {
                foreach (var pcode in oPasscode)
                {
                    if (pcode.passcode == null)
                    {
                        //oResSubmission.passcode = RandomPassword.Generate(6);
                        //litMessage.Text = "userId and password have been sent to " + oResSubmission.sUserID;
                        ////                litMessage.Text = "Password has been setup for " + oResSubmission.username + @"<br/>
                        //                User Name: " + oResSubmission.sUserID + @"<br/>
                        //                Password: " + oResSubmission.passcode + @"<br/>
                        //                url: http://rpgcl.seiservices.com";
                        //                ;
                        username = oResSubmission.username;
                        userId = oResSubmission.sUserID;
                        password = oResSubmission.passcode;
                        litMessage.Text = "userId and password have been sent to " + oResSubmission.sUserID;

                       
                    }
                    else
                    {
                        username = oResSubmission.username;
                        userId = oResSubmission.sUserID;
                        password = pcode.passcode;
                       // litMessage.Text = "Record saved! ID=" + lngPkID;
                        litMessage.Text = "Record saved!";
                        //litMessage.Text = "userId and password have been sent to " + oResSubmission.sUserID;
                    }
                }

                try
                {

                    string strfrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
                    MailMessage objMailMsg = new MailMessage(strfrom, userId);
                    objMailMsg.BodyEncoding = System.Text.Encoding.UTF8;
                    objMailMsg.Bcc.Add("FLacerda@seiservices.com");
                    objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
                    url = System.Web.Configuration.WebConfigurationManager.AppSettings["rpgSite"];
                    // //-------Email body-----------------------

                    String strBody1;
                    strBody1 = File.ReadAllText(Server.MapPath("AccountEmailConfirmation.html"));
                    strBody1 = strBody1.Replace("[[User]]", username);
                    strBody1 = strBody1.Replace("[[password]]", password);

                    objMailMsg.Body = strBody1;

                    // //=============================

                    objMailMsg.Priority = MailPriority.High;
                    objMailMsg.IsBodyHtml = true;

                    // //--------prepare to send mail via SMTP transport-----//

                    SmtpClient objSMTPClient = new SmtpClient();
                    objSMTPClient.EnableSsl = true;
                    objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                    objSMTPClient.Port = 587;
                    objSMTPClient.Credentials = new NetworkCredential(strfrom, "R3&ra67@44!sr");

                    bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                    if (isEmail)
                      objSMTPClient.Send(objMailMsg);

                }
                catch(Exception ex)
                {
                     litMessage.Text="Could not send the e-mail - error: " + ex.Message;
                }

            }
            db.SubmitChanges();
        }


        //try
        //{

        //    string strfrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
        //    MailMessage objMailMsg = new MailMessage(strfrom, userId);
        //    objMailMsg.BodyEncoding = System.Text.Encoding.UTF8;
        //    objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
        //    url = System.Web.Configuration.WebConfigurationManager.AppSettings["rpgSite"];
        //    // //-------Email body-----------------------

        //    String strBody1;
        //    strBody1 = File.ReadAllText(Server.MapPath("AccountEmailConfirmation.html"));
        //    strBody1 = strBody1.Replace("[[User]]", username);
        //    strBody1 = strBody1.Replace("[[password]]", password);

        //    objMailMsg.Body = strBody1;

        //    // //=============================

        //    objMailMsg.Priority = MailPriority.High;
        //    objMailMsg.IsBodyHtml = true;

        //    // //--------prepare to send mail via SMTP transport-----//

        //    SmtpClient objSMTPClient = new SmtpClient();

        //    objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
        //    NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
        //    objSMTPClient.Send(objMailMsg);

        //}
        //catch
        //{
        //    // litMessage.Text="Could not send the e-mail - error: " + ex.Message;
        //}
        
    }  

    public void updateUsersForLogin(string sLoginUserID, string sLoginName, string Email)
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.sUserID == sLoginUserID select c).FirstOrDefault();
            A_Grantee_Contact oCompPassword = (from c in db.A_Grantee_Contacts where c.GranteeContactId == lngPkID select c).FirstOrDefault();
            AppUser oPasscode = (from c in db.AppUsers      where c.sUserID == Email       select c).FirstOrDefault();


            if ((oResSubmission != null))
            {
              
                oResSubmission.username = sLoginName;
                oResSubmission.sUserID = sLoginUserID;
                oResSubmission.blAdmin = false;
                oResSubmission.isActive = true;
                // db.AppUsers.InsertOnSubmit(oResSubmission);             
                litMessage.Text = "Record saved!";
            }
            else
            {

                if (compPassword != null)
                {
                    if ((oResSubmission == null)) // update AppUser
                    {     
                        oPasscode.username = sLoginName;
                        oPasscode.sUserID = sLoginUserID;
                        oPasscode.blAdmin = false;
                        oPasscode.isActive = true;
                        litMessage.Text = "Record saved!";
                    }                   
                }
                else  //grantee_contact new
                {
                    oResSubmission = new AppUser();
                    blNew = true;

                    oResSubmission.username = sLoginName;
                    oResSubmission.sUserID = sLoginUserID;
                    oResSubmission.blAdmin = false;
                    oResSubmission.isActive = true;
                    oResSubmission.passcode = RandomPassword.Generate(6);
                  
                    if (blNew == true)
                    {
                        db.AppUsers.InsertOnSubmit(oResSubmission);
                        blNew = false;

                    }
                    db.SubmitChanges();
                    //litMessage.Text = "userId and password have been sent to " + sLoginUserID;
                    bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                    if (isEmail)
                    {
                        try
                        {

                            string strfrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
                            MailMessage objMailMsg = new MailMessage(strfrom, sLoginUserID);
                            objMailMsg.BodyEncoding = System.Text.Encoding.UTF8;
                            objMailMsg.Bcc.Add("FLacerda@seiservices.com");
                            objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
                            url = System.Web.Configuration.WebConfigurationManager.AppSettings["rpgSite"];
                            // //-------Email body-----------------------

                            String strBody1;
                            strBody1 = File.ReadAllText(Server.MapPath("AccountEmailConfirmation.html"));
                            strBody1 = strBody1.Replace("[[User]]", sLoginName);
                            strBody1 = strBody1.Replace("[[password]]", oResSubmission.passcode);

                            objMailMsg.Body = strBody1;

                            // //=============================

                            objMailMsg.Priority = MailPriority.High;
                            objMailMsg.IsBodyHtml = true;

                            // //--------prepare to send mail via SMTP transport-----//

                            SmtpClient objSMTPClient = new SmtpClient();
                            objSMTPClient.EnableSsl = true;
                            objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                            objSMTPClient.Port = 587;
                            objSMTPClient.Credentials = new NetworkCredential(strfrom, "R3&ra67@44!sr");

                            if (isEmail)
                                objSMTPClient.Send(objMailMsg);

                        }
                        catch (Exception ex)
                        {
                            litMessage.Text = "Could not send the e-mail - error: " + ex.Message;
                        }
                        if (isEmail)
                        {
                            litMessage.Text = "Password has been sent to " + sLoginUserID;
                            btnSave.Enabled = false;
                        }
                    }
                    else
                        Response.Redirect("GranteeEdit.aspx?lngPkID=" + intGranteeID);

                }
         
            }

            db.SubmitChanges();
             
        }


    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("GranteeList.aspx");
    }
    protected void btnResetPassword_Click(object sender, EventArgs e)
    {
        lblError.Text = "";
        string Firstname = txtFirstName.Text;
        string LastName = txtLastName.Text;  

        string fullname= Firstname + "." + LastName ;
        //updateUser();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.sUserID == txtEmail.Text && c.username==fullname select c).FirstOrDefault();
            //ResourceSubmission oResSubmission = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oResSubmission != null))
            {
                oResSubmission.passcode = RandomPassword.Generate (6);
               // litMessage.Text = "userId and reset password have been sent to " + oResSubmission.sUserID;
                litMessage.Text = "New password has been sent to " + oResSubmission.sUserID;
//                litMessage.Text = "Password has been setup for " + oResSubmission.username + @"<br/>
//                User Name: " + oResSubmission.sUserID + @"<br/>
//                Password: " + oResSubmission.passcode + @"<br/>
//                url: http://rpgcl.seiservices.com";
//                ;
                username = oResSubmission.username;
                userId = oResSubmission.sUserID;
                password = oResSubmission.passcode;

                db.SubmitChanges();
            }
            try
            {
              
                string strfrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
                MailMessage objMailMsg = new MailMessage(strfrom, userId);
                objMailMsg.BodyEncoding = System.Text.Encoding.UTF8;
                objMailMsg.Bcc.Add("FLacerda@seiservices.com");
              //  objMailMsg.CC.Add("VKothale@seiservices.com");

                objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
                url = System.Web.Configuration.WebConfigurationManager.AppSettings["rpgSite"];
              //-------Email body-----------------------

                String strBody1;
                strBody1 = File.ReadAllText(Server.MapPath("AccountEmailConfirmation.html"));

                strBody1 = strBody1.Replace("[[User]]", username);
               // strBody1 = strBody1.Replace("[[UserId]]", userId);
                strBody1 = strBody1.Replace("[[password]]", password);
              //  strBody1 = strBody1.Replace("[[URL]]", url);

                objMailMsg.Body = strBody1;

             //=============================

                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

              //--------prepare to send mail via SMTP transport-----//

                SmtpClient objSMTPClient = new SmtpClient();
                objSMTPClient.EnableSsl = true;
                objSMTPClient.Port = 587;
                objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                objSMTPClient.Credentials = new NetworkCredential(strfrom, "R3&ra67@44!sr");

                bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (isEmail)
                  objSMTPClient.Send(objMailMsg);

            }
            catch
            {
                // litMessage.Text="Could not send the e-mail - error: " + ex.Message;
            }
        }


    }
}