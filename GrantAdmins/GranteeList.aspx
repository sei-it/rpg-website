﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="GranteeList.aspx.cs" Inherits="GrantAdmins_GranteeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

          <h2>Grantee List</h2>
                    <p>
        <asp:Button ID="btnNew" runat="server"   Text="New Grantee" OnClick="btnNew_Click" /></p>
         <asp:GridView ID="grdVwList" runat="server"  CssClass="gridTbl" 
            AutoGenerateColumns="False"     AllowPaging="false"  
            AllowSorting="True" OnRowCommand="grdVwList_RowCommand"  OnRowDataBound="grdVwList_RowDataBound"  >
            <Columns>
            		<asp:BoundField DataField="GranteeId" Visible="false"  HeaderText="Id" DataFormatString="{0:g}">
		            <HeaderStyle Width="50px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="GRANTEE_ID_NO" HeaderText="Grantee ID#" DataFormatString="{0:g}">
		            <HeaderStyle Width="120px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="GranteeName" HeaderText="Grantee Name" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="250px"></HeaderStyle>
		            </asp:BoundField>

                     <asp:BoundField DataField="Phone" HeaderText="Contact Phone#" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="250px"></HeaderStyle>
		            </asp:BoundField>
                  
                     <asp:TemplateField>
                       <HeaderTemplate>  
                            View                     
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Edit" visible=true  CssClass="otherstyle" CommandArgument='<%# Eval("GranteeId") %>'>View/Edit </asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField>  

                  <asp:TemplateField>
                       <HeaderTemplate>  
                            Delete                     
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnDelete" runat="server" CommandName="Delete" visible=true  CssClass="otherstyle" CommandArgument='<%# Eval("GranteeId") %>'>Delete</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField>  
 
            </Columns>
        </asp:GridView>
</asp:Content>

