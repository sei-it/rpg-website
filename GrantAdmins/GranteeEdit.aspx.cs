﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Drawing;


public partial class GrantAdmins_GranteeEdit : System.Web.UI.Page
{
    private int? intUserGranteeID;
    private int? intUserRole;
    UserInfo oUI;
    int GranteeContactId;
    protected int lngPkID=0;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        if (!IsPostBack)
        {

            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            intUserGranteeID = oUI.intGranteeID;
            intUserRole = oUI.intUserRoleID;

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
               
            }
            loadDropdown();
        }


        if ((intUserRole == 4) || (intUserRole == 3)) //RPG Admin
        {
            btnAssignFocalEBP.Visible = true;
           
        }
        else if (intUserRole == 1)//Grantee Admin 
        {
            btnAssignFocalEBP.Visible = false;
        }
        else if (intUserRole == 2) //Case Worker
        {
            btnAssignFocalEBP.Visible = false;
        }

        if (lngPkID != 0)
        {
            //btnSave.Visible = false;
        }






        loadrecords();
        string strJS2;

        strJS2 = "window.open('GranteeFocalEBPs.aspx?lngPkID=" + lngPkID  + "','','width=950,height=700,resizable,scrollbars');";

        //btnAssignFocalEBP.Attributes.Add("onclick", strJS2);
       // displayRecords();
                
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

    }

    protected void loadrecords()
    {
    }

    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.F_EBPLists orderby p.EBPName select p;
            chkEBPs.DataSource = qry;
            chkEBPs.DataTextField = "EBPName";
            chkEBPs.DataValueField = "EBPID";
            chkEBPs.DataBind();
         }
    }

    protected void displayRecords()
    {
        object objVal = null;
        DataClassesDataContext db = new DataClassesDataContext();
        var oPages = from pg in db.A_Grantees
                     where pg.GranteeId == lngPkID
                     select pg;
        foreach (var oCase in oPages)
        {



            objVal = oCase.GRANTEE_ID_NO;
            if (objVal != null)
            {
                txtGranteeidno.Text = objVal.ToString();
            }
            objVal = oCase.GranteeName;
            if (objVal != null)
            {
                txtGranteeName.Text = objVal.ToString();
            }
            objVal = oCase.Address1;
            if (objVal != null)
            {
                txtAddress1.Text = objVal.ToString();
            }
            objVal = oCase.Address2;
            if (objVal != null)
            {
                txtAddress2.Text = objVal.ToString();
            }
            objVal = oCase.City;
            if (objVal != null)
            {
                txtCity.Text = objVal.ToString();
            }
            objVal = oCase.State;
            if (objVal != null)
            {
                txtState.Text = objVal.ToString();
            }
            objVal = oCase.Zip;
            if (objVal != null)
            {
                txtZip.Text = objVal.ToString();
            }
            objVal = oCase.Phone;
            if (objVal != null)
            {
                txtPhone.Text = objVal.ToString();
            }


        }

        db.Dispose();
        displayGrid(lngPkID);
        displayEBPs(lngPkID);
    }

    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            A_Grantee oCase = (from c in db.A_Grantees where c.GranteeId == lngPkID select c).FirstOrDefault();
            //ResourceSubmission oCase = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oCase == null))
            {
                oCase = new A_Grantee();
                blNew = true;
            }

            oCase.GRANTEE_ID_NO = txtGranteeidno.Text;
            oCase.GranteeName = txtGranteeName.Text;
            oCase.Address1 = txtAddress1.Text;
            oCase.Address2 = txtAddress2.Text;
            oCase.City = txtCity.Text;
            oCase.State = txtState.Text;
            oCase.Zip = txtZip.Text;
            oCase.Phone = txtPhone.Text;

            oCase.UpdatedBy = "";

            oCase.UpdatedDt = DateTime.Now;

            if (blNew == true)
            {
                db.A_Grantees.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
            lngPkID = oCase.GranteeId;
        }
        //LitJS.Text = " showSuccessToast();";
        //litMessage.Text = "Record saved! ID=" + lngPkID;
        litMessage.Text = "Record saved!";
        updateGranteeEBPCheckBoxes();


    }
    public void updateGranteeEBPCheckBoxes()
    {
        for (int i = 0; i < chkEBPs.Items.Count; i++)
        {
            if (chkEBPs.Items[i].Selected ==true )
                updateGranteeEBPs(Convert.ToInt32(chkEBPs.Items[i].Value),true);
            else
                updateGranteeEBPs(Convert.ToInt32(chkEBPs.Items[i].Value), false);
         }
    }
    public void updateGranteeEBPs(int intEBPID,bool blAdd)
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            A_GranteeEBP oCase = (from c in db.A_GranteeEBPs where c.GranteeID_FK  == lngPkID && c.EBPID_FK==intEBPID 
                                  select c).FirstOrDefault();
            //ResourceSubmission oCase = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oCase == null) && blAdd == true)  //if not exist, add
            {
                oCase = new A_GranteeEBP();
                blNew = true;

                oCase.GranteeID_FK = lngPkID;
                oCase.EBPID_FK = intEBPID;
                oCase.FocalEBPYN = false;
                db.A_GranteeEBPs.InsertOnSubmit(oCase);
                db.SubmitChanges();
            }
            else if ((oCase != null) && blAdd == false ) //if it exists remove it.
            {
                db.A_GranteeEBPs.DeleteOnSubmit(oCase);

                db.SubmitChanges();
            }
             
        }
         
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }

        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
       
       
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (lngPkID == 0)    //add mode
        {
            if (CheckGranteeIDvalid())
            {
                updateUser();
                displayRecords();
                Response.Redirect("GranteeList.aspx");
            }
        }
        else
        {
            updateUser();
            displayRecords();
            Response.Redirect("GranteeList.aspx");
        }
        
    }   
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("GranteeList.aspx");
    }
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        if (lngPkID == 0)    //add mode
        {
            if (CheckGranteeIDvalid())
            {
                updateUser();
                Response.Redirect("GranteeList.aspx");
            }
        }
        else
        {
            updateUser();
            Response.Redirect("GranteeList.aspx");
        }

    }

    private void displayGrid(int intGranteeID)
    {
        object objVal = null;
        
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var vCases = db.getContactsForGrantee(intGranteeID);

            grdVwList.DataSource = vCases;
            grdVwList.DataBind();
        }

    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Begin")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("GranteeContacts.aspx?lngPkID=" + intResponseID + "&intGranteeID=" + lngPkID);


        }
        if (e.CommandName == "Delete")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            DeleteContact(intResponseID);
            Response.Redirect("GranteeEdit.aspx?lngPkID=" + lngPkID);
        }

    }

    private void DeleteContact(int intResponseID)
    {
        string email = "";
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            A_Grantee_Contact case1 = (from c in db.A_Grantee_Contacts
                                       where c.GranteeContactId == intResponseID
                                       select c).FirstOrDefault();
            if ((case1 != null))
            {
                email = case1.Email;
                //db.A_Grantee_Contacts.DeleteOnSubmit(case1);
                case1.isActive = false;
            }
            db.SubmitChanges();

            AppUser deluser = (from u in db.AppUsers where u.sUserID==email select u).FirstOrDefault();
            if(deluser!=null)
            {
                //db.AppUsers.DeleteOnSubmit(deluser);
                deluser.isActive = false;
            }
           db.SubmitChanges();
        }
    }
    private void displayEBPs(int intGranteeID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
                    var oPages = from pg in db.A_GranteeEBPs
                                 where pg.GranteeID_FK == intGranteeID
                     select pg;
                    foreach (var oCase in oPages)
                    {
                        for (int i = 0; i < chkEBPs.Items.Count; i++)
                        {
                            if (Convert.ToInt32(chkEBPs.Items[i].Value) == oCase.EBPID_FK)
                            {
                                chkEBPs.Items[i].Selected = true;
                            }
                        }

                    }
        }

    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("GranteeContacts.aspx?intGranteeID=" + lngPkID);
    }

   
    protected void btnAssignFocalEBPs_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~/GrantAdmins/GranteeFocalEBPs.aspx?lngPkID=" + lngPkID);
    }
    protected void btnAssignFocalEBP_Click(object sender, EventArgs e)
    {
       // updateUser();
        string strJS = "window.open('GranteeFocalEBPs.aspx?lngPkID=" + lngPkID + "','','width=950,height=700,resizable,scrollbars');";
        Response.Write("<script>" + strJS +  "</script>");

    }
    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
         LinkButton imgBtnDelete;

         if (e.Row.RowType == DataControlRowType.DataRow)
         {
             if (DataBinder.Eval(e.Row.DataItem, "GranteeContactId") != null)
             {
                 GranteeContactId = (int)DataBinder.Eval(e.Row.DataItem, "GranteeContactId");
             }

             if (intUserRole == 3)
             {
                 imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                 imgBtnDelete.Visible = true;
                 imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
                                               "confirm('Are you sure you want to delete this record')"); 
             }
             else
             {
                 imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                 imgBtnDelete.Visible = false;

             }

         }
    }


    private bool CheckGranteeIDvalid()
    {
        bool isvalid = true;
        string pattern = @"^[a-zA-Z0-9]{8}$";

        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            lblError.Text = "";

            //var oPages = from pg in db.A_Grantees
            //             where pg.GRANTEE_ID_NO == txtGranteeidno.Text
            //             select pg;

            //if (oPages != null && oPages.Count() > 0)
            //{
            //    isvalid = false;
            //    string ErrorMsg = "Grantee ID already exists.";
            //    lblError.Text = ErrorMsg;

            //    btnSave.Visible = true;
            //}

            if (string.IsNullOrEmpty(txtGranteeidno.Text.Trim()))
            {
                lblError.Text = "Required!";
                isvalid = false;
            }
            else
            {
                if (!Regex.IsMatch(txtGranteeidno.Text.Trim(), pattern))
                {
                    lblError.Text = "Input 8 alphanumeric characters";
                    isvalid = false;
                }
                else
                {
                    var oPages = from pg in db.A_Grantees
                                 where pg.GRANTEE_ID_NO == txtGranteeidno.Text
                                 select pg;

                    if (oPages != null && oPages.Count() > 0)
                    {
                        isvalid = false;
                        string ErrorMsg = "Grantee ID already exists.";
                        lblError.Text = ErrorMsg;

                        btnSave.Visible = true;
                    }
                }
            }
            return isvalid;
        }
    }

    protected void txtGranteeidno_TextChanged(object sender, EventArgs e)
    {
        //if (CheckGranteeIDvalid())
        //{
        //    btnSave.Enabled = true;
        //    btnSave.BackColor = ColorTranslator.FromHtml("#E58703");
        //}
        //else
        //{
        //    btnSave.Enabled = false;
        //    btnSave.BackColor = Color.Gray;
        //}
        if(lngPkID==0)              //add mode
            CheckGranteeIDvalid();
    }
}