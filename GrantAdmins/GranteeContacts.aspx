﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="GranteeContacts.aspx.cs" Inherits="GrantAdmins_GranteeContacts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
       <h1>Grantee Contact</h1>
    <div class="formLayout">
    <p>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label>
    
    </p>
 
	        <p>
		        <label for="txtFirstName">First Name
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First Name" ForeColor="Red">Required!</asp:RequiredFieldValidator>
                </label>
		        &nbsp;<asp:textbox id="txtFirstName" runat="server" MaxLength="50" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtLastName">Last Name <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last Name" ForeColor="Red">Required!</asp:RequiredFieldValidator>
                </label>
		        &nbsp;<asp:textbox id="txtLastName" runat="server" MaxLength="50" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtEmail">Email <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="true" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email   " ForeColor="Red">Required!</asp:RequiredFieldValidator>
                </label>
		        &nbsp;<asp:textbox id="txtEmail" runat="server" MaxLength="80" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtPhone">Phone</label>
		        <asp:textbox id="txtPhone" runat="server" MaxLength="20" ></asp:textbox>
	        </p>
	        <p>
		        <label for="rblGranteerole">Grantee Role
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rblGranteerole" ErrorMessage="Grantee  Role" ForeColor="Red">Required!</asp:RequiredFieldValidator>
                </label>
		        &nbsp;<asp:RadioButtonList id="rblGranteerole" runat="server"  RepeatDirection="Horizontal"  > 
		        </asp:RadioButtonList>
	        </p>
          <p>
              <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;
              <asp:Button ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click"  CausesValidation="false" />&nbsp;&nbsp;&nbsp; 
              <asp:Button ID="btnResetPassword" runat="server" Visible="true" Text="Reset Password" onclick="btnResetPassword_Click" />
          
        </p>
        <p>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>          
        </p>
    </div>
</asp:Content>

