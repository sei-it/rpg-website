﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GranteeFocalEBPs.aspx.cs" Inherits="GrantAdmins_GranteeFocalEBPs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/forms.css" rel="stylesheet" />
    <link href="../Styles/Site.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
                <div class="main">
    <div>
        <div class="formLayout"> 
        <p>Use this screen to Tag/ Untag Focal EBPs </p>
            </div>
        <div class="formLayout">
            <asp:CheckBoxList ID="chkEBPs" runat="server" RepeatColumns="2"></asp:CheckBoxList>
        </div>
    </div>
            <div class="formLayout">
       <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;
                <asp:Button ID="btnSaveClose" runat="server" Text="Save and Close" onclick="btnSaveClose_Click1" />&nbsp;<asp:Button 
                    ID="btnClose" runat="server" Text="Cancel" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 
          
                 
          
        </p>
        <p>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
          
        </p>
    </div>
                    </div>
    </form>
</body>
</html>
