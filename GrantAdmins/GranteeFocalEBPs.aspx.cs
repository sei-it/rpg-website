﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GrantAdmins_GranteeFocalEBPs : System.Web.UI.Page
{
   public  int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        if (!IsPostBack)
        {
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            loadDropdown();
        }
 
        if (ViewState["IsLoaded1"] == null)
        {
            displayEBPs(lngPkID);
            ViewState["IsLoaded1"] = true;
        }

    }
 
    private void displayEBPs(int intGranteeID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.A_GranteeEBPs
                         where pg.GranteeID_FK == intGranteeID
                         select pg;
            foreach (var oCase in oPages)
            {
                for (int i = 0; i < chkEBPs.Items.Count; i++)
                {
                    if (Convert.ToInt32(chkEBPs.Items[i].Value) == oCase.EBPID_FK)
                    {
                        //chkEBPs.Items[i].Selected = true;
                        if (oCase.FocalEBPYN == true)
                        {
                            chkEBPs.Items[i].Selected = true;
                        }
                    }
                }

            }
        }

    }
    public void updateGranteeEBPCheckBoxes()
    {
        for (int i = 0; i < chkEBPs.Items.Count; i++)
        {
            if (chkEBPs.Items[i].Selected == true)
                updateGranteeEBPs(Convert.ToInt32(chkEBPs.Items[i].Value), true);
            else
                updateGranteeEBPs(Convert.ToInt32(chkEBPs.Items[i].Value), false);
        }
    }
    public void updateGranteeEBPs(int intEBPID, bool blAdd)
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            A_GranteeEBP oCase = (from c in db.A_GranteeEBPs
                                  where c.GranteeID_FK == lngPkID && c.EBPID_FK == intEBPID
                                  select c).FirstOrDefault();
             if (  blAdd == true)  //if not exist, add
            {
                oCase.FocalEBPYN=true;                 
                db.SubmitChanges();
            }
            else if ( blAdd == false) //if nt selected as focal ebp, uncheck it.
            {
                oCase.FocalEBPYN=false ;
                db.SubmitChanges();
            }
        }

    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.A_GranteeEBPs 
                      join ebp in db.F_EBPLists
                      on p.EBPID_FK equals ebp.EBPID
                      where p.GranteeID_FK == lngPkID
                      select ebp;
            chkEBPs.DataSource = qry;
            chkEBPs.DataTextField = "EBPName";
            chkEBPs.DataValueField = "EBPID";
            chkEBPs.DataBind();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        updateGranteeEBPCheckBoxes();
    }
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {

        updateGranteeEBPCheckBoxes(); 
        Response.Write(@"<script  >window.parent.closeIframe();<script>");
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnSaveClose_Click1(object sender, EventArgs e)
    {
        updateGranteeEBPCheckBoxes();
        Response.Write(@"<script language=""JavaScript"">top.returnValue=1;window.close();</script>");
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
       // Response.Write(@"<script  >window.parent.closeIframe();window.parent.$(""#modalDiv"").dialog('close');<script>");
    Response.Write(@"<script language=""JavaScript"">top.returnValue=1;window.close();</script>");
    }
}