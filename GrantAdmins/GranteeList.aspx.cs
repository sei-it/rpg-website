﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GrantAdmins_GranteeList : System.Web.UI.Page
{
    UserInfo oUI;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            if (!(oUI.intUserRoleID == 4 || oUI.intUserRoleID == 1 || oUI.intUserRoleID == 3 )) // changed for SysAdmin role june27
            {
                Response.Redirect("~/Error/UnAuthorized.aspx");
            }
        }
        displayRecords();
        string strJS;
        //strJS = ("window.open(\"UserEdit.aspx?lngPkID=" + ("0" + "\",\'\',\"width=950,height=700,resizable,scrollbars\");"));
        //btnNew.Attributes.Add("onclick", strJS);
    }
    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (oUI.intUserRoleID == 4 || oUI.intUserRoleID == 3)
            {
                var vCases = from p in db.A_Grantees
                             orderby p.GRANTEE_ID_NO descending
                             select p;
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
             else
            {
                var vCases = from p in db.A_Grantees
                             where p.GranteeId == oUI.intGranteeID
                             orderby p.GRANTEE_ID_NO descending
                             select p;
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
                btnNew.Visible = false;
            }
        }

    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            int intCaseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("GranteeEdit.aspx?lngPkID=" + intCaseID);
        }
        if (e.CommandName == "Delete")
        {
            int Granteeid = Convert.ToInt32(e.CommandArgument);

            DeleteGrantee(Granteeid);
            Response.Redirect("GranteeList.aspx");
        }
 
    }

    private void DeleteGrantee(int Granteeid)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            //------------------A_Grantees------------------------//
            A_Grantee oCase1 = (from c in db.A_Grantees
                                where c.GranteeId == Granteeid
                                       select c).FirstOrDefault();
            if ((oCase1 != null))
            {
                db.A_Grantees.DeleteOnSubmit(oCase1);
            }
            db.SubmitChanges();

            //---------------------A_Grantee_Contact---------------//
            var case1 = (from p in db.A_Grantee_Contacts
                        where p.GranteeId_fk == Granteeid
                        select new { p.Email });
            if ((case1 != null))
            {
                foreach (var var1 in case1)
                {
                    if (!(var1.Email == null))
                    {
                        GranteeDeletefrom_A_Grantee_Contact(var1.Email);
                        GranteeDeleteFrom_AppUsers(var1.Email);
                    }
                }
            }

            //-------------------- A_GranteeEBPs--------------------//

            A_GranteeEBP oCase2 = (from c in db.A_GranteeEBPs
                                 where c.GranteeID_FK == Granteeid
                                  select c).FirstOrDefault();
            if ((oCase2 != null))
            {
                db.A_GranteeEBPs.DeleteOnSubmit(oCase2);
            }
            db.SubmitChanges();

            //--------------------B_CASE_ENROLLMENT---------------//

            var case4 = (from p in db.B_CASE_ENROLLMENTs
                         where p.GranteeID_fk == Granteeid
                         select new { p.CaseNoId });
            if ((case4 != null))
            {
                foreach (var var4 in case4)
                {
                    if (!(var4.CaseNoId == null))
                    {
                        GranteeDeletefrom_B_CASE_ENROLLMENTs(var4.CaseNoId);
                        GranteeDeletefrom_C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE(var4.CaseNoId);
                        GranteeDeletefrom_F_ENROLLMENT_EBP(var4.CaseNoId);
                        GranteeDeletefrom_F_ENROLLMENT(var4.CaseNoId);
                        GranteeDeletefrom_F_EBPCaseWorkers_AssignedList(var4.CaseNoId);
                        GranteeDeletefrom_G_ServiceLogCaseMembers(var4.CaseNoId);
                        GranteeDeletefrom_G_SERVICE_LOG(var4.CaseNoId);
                        GranteeDeletefrom_F_EBP_ParticipantRatings(var4.CaseNoId);
                        GranteeDeletefrom_E_CaseClosure(var4.CaseNoId);
                        GreanteeDeletefrom_N_SubstanceAbuseMatrix(var4.CaseNoId);
                        GreanteeDeletefrom_O_ParentingSkillsMatrix(var4.CaseNoId);
                        GreanteeDeletefrom_P_PersonalDevelopmentMatrix(var4.CaseNoId);
                        GreanteeDeletefrom_Q_DevelopmentEducationMatrix(var4.CaseNoId);
                        GreanteeDeletefrom_R_AdultSubstanceAbuseMatrix(var4.CaseNoId);
                        GreanteeDeletefrom_S_OtherFamilySubAbuseMatrix(var4.CaseNoId);
                       
                    }
                }
            }

         

        }
    }

//------------------------Delete---------------------------------------------------//

 //------------------deletee from S_OtherFamilySubAbuseMatrix--------------------------//
    private void GreanteeDeletefrom_S_OtherFamilySubAbuseMatrix(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase27 = (from p in db.vw_deleteFrom_S_OtherFamilySubAbuseMatrixes
                           where p.CaseID_FK == CaseNoId
                           select new { p.ServiceID_FK });
            if ((oCase27 != null))
            {
                foreach (var var10 in oCase27)
                {
                    if (!(var10.ServiceID_FK == null))
                    {
                        deleteFrom_S_OtherFamilySubAbuseMatrixes(Convert.ToInt32(var10.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_S_OtherFamilySubAbuseMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            S_OtherFamilySubAbuseMatrix oCase28 = (from c in db.S_OtherFamilySubAbuseMatrixes
                                                   where c.ServiceID_FK == ServiceID_FK
                                                   select c).FirstOrDefault();
            if ((oCase28 != null))
            {
                db.S_OtherFamilySubAbuseMatrixes.DeleteOnSubmit(oCase28);
            }
            db.SubmitChanges();
        }
    }

   

 //------------------deletee from R_AdultSubstanceAbuseMatrix--------------------------//
 private void GreanteeDeletefrom_R_AdultSubstanceAbuseMatrix(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
               var oCase25 = (from p in db.vw_deleteFrom_R_AdultSubstanceAbuseMatrixes
                         where p.CaseID_FK == CaseNoId
                         select new { p.ServiceID_FK });
            if ((oCase25 != null))
            {
                foreach (var var9 in oCase25)
                {
                    if (!(var9.ServiceID_FK == null))
                    {
                        deleteFrom_R_AdultSubstanceAbuseMatrixes(Convert.ToInt32(var9.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

private void deleteFrom_R_AdultSubstanceAbuseMatrixes(int ServiceID_FK)
{
 	 using (DataClassesDataContext db = new DataClassesDataContext())
        {
            R_AdultSubstanceAbuseMatrix oCase26 = (from c in db.R_AdultSubstanceAbuseMatrixes
                                                    where c.ServiceID_FK == ServiceID_FK
                                                    select c).FirstOrDefault();
            if ((oCase26!= null))
            {
                db.R_AdultSubstanceAbuseMatrixes.DeleteOnSubmit(oCase26);
            }
            db.SubmitChanges();
        }
}

//------------------deletee from Q_DevelopmentEducationMatrix--------------------------//


 private void GreanteeDeletefrom_Q_DevelopmentEducationMatrix(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
             var oCase23 = (from p in db.vw_deleteFrom_Q_DevelopmentEducationMatrixes
                         where p.CaseID_FK == CaseNoId
                         select new { p.ServiceID_FK });
            if ((oCase23 != null))
            {
                foreach (var var8 in oCase23)
                {
                    if (!(var8.ServiceID_FK == null))
                    {
                        deleteFrom_Q_DevelopmentEducationMatrixes(Convert.ToInt32(var8.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

private void deleteFrom_Q_DevelopmentEducationMatrixes(int ServiceID_FK)
{
 	 using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Q_DevelopmentEducationMatrix oCase24 = (from c in db.Q_DevelopmentEducationMatrixes
                                                   where c.ServiceID_FK == ServiceID_FK
                                                   select c).FirstOrDefault();
            if ((oCase24 != null))
            {
                db.Q_DevelopmentEducationMatrixes.DeleteOnSubmit(oCase24);
            }
            db.SubmitChanges();
        }
}

    
  //------------------deletee from P_PersonalDevelopmentMatrix--------------------------//

private void GreanteeDeletefrom_P_PersonalDevelopmentMatrix(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
                var oCase21 = (from p in db.vw_deleteFrom_P_PersonalDevelopmentMatrixes
                         where p.CaseID_FK == CaseNoId
                         select new { p.ServiceID_FK });
            if ((oCase21 != null))
            {
                foreach (var var7 in oCase21)
                {
                    if (!(var7.ServiceID_FK == null))
                    {
                        deleteFrom_P_PersonalDevelopmentMatrixes(Convert.ToInt32(var7.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

private void deleteFrom_P_PersonalDevelopmentMatrixes(int ServiceID_FK)
{
 	 using (DataClassesDataContext db = new DataClassesDataContext())
        {
            P_PersonalDevelopmentMatrix oCase22 = (from c in db.P_PersonalDevelopmentMatrixes
                                               where c.ServiceID_FK == ServiceID_FK
                                               select c).FirstOrDefault();
            if ((oCase22 != null))
            {
                db.P_PersonalDevelopmentMatrixes.DeleteOnSubmit(oCase22);
            }
            db.SubmitChanges();
        }
}

   




 //------------------deletee from O_ParentingSkillsMatrix--------------------------//

 private void GreanteeDeletefrom_O_ParentingSkillsMatrix(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
             var oCase19 = (from p in db.vw_deleteFrom_O_ParentingSkillsMatrixes
                         where p.CaseID_FK == CaseNoId
                         select new { p.ServiceID_FK });
            if ((oCase19 != null))
            {
                foreach (var var6 in oCase19)
                {
                    if (!(var6.ServiceID_FK == null))
                    {
                        deleteFrom_O_ParentingSkillsMatrixes(Convert.ToInt32(var6.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();

        }
    }

    private void deleteFrom_O_ParentingSkillsMatrixes(int ServiceID_FK)
        {
 	         using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    O_ParentingSkillsMatrix oCase20 = (from c in db.O_ParentingSkillsMatrixes
                                                      where c.ServiceID_FK == ServiceID_FK
                                                      select c).FirstOrDefault();
                    if ((oCase20 != null))
                    {
                        db.O_ParentingSkillsMatrixes.DeleteOnSubmit(oCase20);
                    }
                    db.SubmitChanges();
                }
        }

    //------------------deletee from N_SubstanceAbuseMatrix--------------------------//

    private void GreanteeDeletefrom_N_SubstanceAbuseMatrix(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase17 = (from p in db.vw_deleteFrom_N_SubstanceAbuseMatrixes
                           where p.CaseID_FK == CaseNoId
                           select new { p.ServiceID_FK });
            if ((oCase17 != null))
            {
                foreach (var var5 in oCase17)
                {
                    if (!(var5.ServiceID_FK == null))
                    {
                        deleteFrom_N_SubstanceAbuseMatrixes(Convert.ToInt32(var5.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_N_SubstanceAbuseMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            N_SubstanceAbuseMatrix oCase18 = (from c in db.N_SubstanceAbuseMatrixes
                                              where c.ServiceID_FK == ServiceID_FK
                                              select c).FirstOrDefault();
            if ((oCase18 != null))
            {
                db.N_SubstanceAbuseMatrixes.DeleteOnSubmit(oCase18);
            }
            db.SubmitChanges();
        }
    }


   //-------------------delete from E_CaseClosure---------------------------------------//

    private void GranteeDeletefrom_E_CaseClosure(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            E_CaseClosure oCase16 = (from c in db.E_CaseClosures
                                  where c.CaseNoId_FK == CaseNoId
                                  select c).FirstOrDefault();
            if ((oCase16 != null))
            {
                db.E_CaseClosures.DeleteOnSubmit(oCase16);
            }
            db.SubmitChanges();
        }

    }

     //-----------------delete  from vw_deleteFrom_F_EBP_ParticipantRating---------------//


    private void GranteeDeletefrom_F_EBP_ParticipantRatings(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase14 = (from p in db.vw_deleteFrom_F_EBP_ParticipantRatings
                        where p.CaseID_FK == CaseNoId
                        select new { p.CaseEBPID_FK });
            if ((oCase14 != null))
            {
                foreach (var var4 in oCase14)
                {
                    if (!(var4.CaseEBPID_FK == null))
                    {
                        deleteFrom_F_EBP_ParticipantRatings(Convert.ToInt32(var4.CaseEBPID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_F_EBP_ParticipantRatings(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBP_ParticipantRating oCase15 = (from c in db.F_EBP_ParticipantRatings
                                               where c.CaseEBPID_FK == CaseEBPID_FK
                                              select c).FirstOrDefault();
            if ((oCase15 != null))
            {
                db.F_EBP_ParticipantRatings.DeleteOnSubmit(oCase15);
            }
            db.SubmitChanges();
        }
    }


     //----------------delete from G_SERVICE_LOG-------------------------------//

    private void GranteeDeletefrom_G_SERVICE_LOG(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            G_SERVICE_LOG oCase13 = (from c in db.G_SERVICE_LOGs
                                  where c.CaseID_FK == CaseNoId
                                  select c).FirstOrDefault();
            if ((oCase13 != null))
            {
                db.G_SERVICE_LOGs.DeleteOnSubmit(oCase13);
            }
            db.SubmitChanges();
        }
    }


    //----------------- G_ServiceLogCaseMembers---------------//

    private void GranteeDeletefrom_G_ServiceLogCaseMembers(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase11 = (from p in db.vw_deleteFrom_G_ServiceLogCaseMembers
                        where p.CaseID_FK == CaseNoId
                        select new { p.ServiceID });
            if ((oCase11 != null))
            {
                foreach (var var3 in oCase11)
                {
                    if (!(var3.ServiceID == null))
                    {
                        deleteFrom_G_ServiceLogCaseMembers(Convert.ToInt32(var3.ServiceID));
                    }
                }
            }
            db.SubmitChanges();
        }

    }
    private void deleteFrom_G_ServiceLogCaseMembers(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            G_ServiceLogCaseMember oCase12 = (from c in db.G_ServiceLogCaseMembers
                                             where c.ServiceID_FK == ServiceID_FK
                                             select c).FirstOrDefault();
            if ((oCase12 != null))
            {
                db.G_ServiceLogCaseMembers.DeleteOnSubmit(oCase12);
            }
            db.SubmitChanges();
        }
    }
   
    


    //------- F_EBPCaseWorkers_AssignedList---------------------------------------------//

    private void GranteeDeletefrom_F_EBPCaseWorkers_AssignedList(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase9 = (from p in db.vw_deleteFrom_F_EBPCaseWorkers_AssignedLists
                          where p.CaseID_FK == CaseNoId
                          select new { p.CaseID_FK, p.EBPEnrollID_FK });
            if ((oCase9 != null))
            {
                foreach (var var9 in oCase9)
                {
                    if (!(var9.EBPEnrollID_FK == null))
                    {
                        deleteFrom_F_EBPCaseWorkers_AssignedLists(Convert.ToInt32(var9.EBPEnrollID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }

    }

    private void deleteFrom_F_EBPCaseWorkers_AssignedLists(int EBPEnrollID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPCaseWorkers_AssignedList oCase10 = (from c in db.F_EBPCaseWorkers_AssignedLists
                                                    where c.EBPEnrollID_FK == EBPEnrollID_FK
                                                    select c).FirstOrDefault();
            if ((oCase10 != null))
            {
                db.F_EBPCaseWorkers_AssignedLists.DeleteOnSubmit(oCase10);
            }
            db.SubmitChanges();
        }
    }
        
           

    //------------------F_ENROLLMENT---------------------------------------------------//


    private void GranteeDeletefrom_F_ENROLLMENT(int CaseNoId_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase7 = (from p in db.vw_deleteFrom_F_EBPEnrollments
                          where p.CaseNoId_FK == CaseNoId_FK
                          select new { p.IndividualID_FK });
            if ((oCase7 != null))
            {
                foreach (var var7 in oCase7)
                {
                    if (!(var7.IndividualID_FK == null))
                    {
                        deleteF_EBPEnrollment(Convert.ToInt32(var7.IndividualID_FK));
                    }
                }
            }
        }
    }

    private void deleteF_EBPEnrollment(int IndividualID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPEnrollment oCase8 = (from c in db.F_EBPEnrollments
                                      where c.IndividualID_FK == IndividualID_FK
                                      select c).FirstOrDefault();
            if ((oCase8 != null))
            {
                db.F_EBPEnrollments.DeleteOnSubmit(oCase8);
            }
            db.SubmitChanges();
        }
    }



   //------------------F_ENROLLMENT_EBP----------------------//


    private void GranteeDeletefrom_F_ENROLLMENT_EBP(int CaseID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_ENROLLMENT_EBP oCase6 = (from c in db.F_ENROLLMENT_EBPs
                                       where c.CaseID_FK == CaseID_FK
                                       select c).FirstOrDefault();
            if ((oCase6 != null))
            {
                db.F_ENROLLMENT_EBPs.DeleteOnSubmit(oCase6);
            }
            db.SubmitChanges();
        }
    }
    //--------------------------C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE-------------------------------//

    private void GranteeDeletefrom_C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE(int CaseNoId_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE oCase5 = (from c in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                                                                where c.CaseNoId_FK == CaseNoId_FK
                                                                select c).FirstOrDefault();
            if ((oCase5 != null))
            {
                db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs.DeleteOnSubmit(oCase5);
            }
            db.SubmitChanges();

        }
    }

    //-----------------------B_case_enrollments-------------//
    private void GranteeDeletefrom_B_CASE_ENROLLMENTs(int CaseNoId)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            B_CASE_ENROLLMENT oCase4 = (from c in db.B_CASE_ENROLLMENTs
                                        where c.CaseNoId == CaseNoId
                                        select c).FirstOrDefault();
            if ((oCase4 != null))
            {
                db.B_CASE_ENROLLMENTs.DeleteOnSubmit(oCase4);
            }
            db.SubmitChanges();
        }

    }

    //-------------------AppUsers--------------------------//
    private void GranteeDeleteFrom_AppUsers(string AppEmail)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser case3 = (from c in db.AppUsers
                             where c.sUserID == AppEmail
                                       select c).FirstOrDefault();
            if ((case3 != null))
            {
                db.AppUsers.DeleteOnSubmit(case3);
            }
            db.SubmitChanges();
        }
    }

    //------------------A_Grantee_Contac------------------------//
    private void GranteeDeletefrom_A_Grantee_Contact(string Email)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            A_Grantee_Contact case1 = (from c in db.A_Grantee_Contacts
                                  where c.Email == Email
                                  select c).FirstOrDefault();
            if ((case1 != null))
            {
                db.A_Grantee_Contacts.DeleteOnSubmit(case1);
            }
        db.SubmitChanges();
        }
    }
    //---------------------------End------------------------------------//

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("GranteeEdit.aspx?lngPkID=0");
    }
    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int intGranteeId = 0;
        LinkButton imgBtnDelete;

        //Repeater repCasemember;


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "GranteeId") != null)
            {
                intGranteeId = (int)DataBinder.Eval(e.Row.DataItem, "GranteeId");
            }
            //repCasemember = (Repeater)e.Row.FindControl("repCaseMembers");

            //using (DataClassesDataContext db = new DataClassesDataContext())
            //{
            //    var qry2 = from p in db.A_Grantees 
            //               where p.CaseNoId_FK == intCaseID
            //               select new { p.FIRST_NAME, p.IndividualID };
            //    repCasemember.DataSource = qry2;
            //    repCasemember.DataBind();
            //}

            if (!(oUI.intUserRoleID == 3))
            {
                imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                imgBtnDelete.Visible = false;
                grdVwList.HeaderRow.Cells[5].Visible = false;
            }
            else
            {
                imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                imgBtnDelete.Visible = true;
                imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
                                              "confirm('Are you sure you want to delete this record')");
                grdVwList.HeaderRow.Cells[5].Visible = true;
            }


        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }
}