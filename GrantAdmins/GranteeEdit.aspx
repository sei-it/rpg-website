﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" EnableEventValidation="false"  AutoEventWireup="true" CodeFile="GranteeEdit.aspx.cs" Inherits="GrantAdmins_GranteeEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<%--    <link rel="stylesheet" href="css/themes/base/jquery.ui.all.css">
--%>     
<%--    <link href="css/themes/south-street/jquery-ui-1.8.10.custom.css" rel="stylesheet" type="text/css" />--%>
 
     
    <link href="../css/themes/south-street/jquery.ui.all.css" rel="stylesheet" type="text/css" />
   	<script src="../Scripts/jquery-1.4.1.js"></script>
        <script src="../Scripts/jquery-ui-1.8.10.custom.min.js" type="text/javascript"></script>
 
    <script src="../Scripts/ui/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
	<script src="../Scripts/ui/jquery.ui.core.js"></script>
	<script src="../Scripts/ui/jquery.ui.widget.js"></script>
	<script src="../Scripts/ui/jquery.ui.mouse.js"></script>
	<script src="../Scripts/ui/jquery.ui.draggable.js"></script>
	<script src="../Scripts/ui/jquery.ui.position.js"></script>
	<script src="../Scripts/ui/jquery.ui.resizable.js"></script>
<script src="../Scripts/ui/jquery.ui.accordion.js"></script>
	<script src="../Scripts/ui/jquery.ui.dialog.js"></script>
	<script src="../Scripts/ui/jquery.effects.core.js"></script>
	<script src="../Scripts/ui/jquery.effects.blind.js"></script>
	<script src="../Scripts/ui/jquery.effects.explode.js"></script>
   <script src="../Scripts/ui/jquery-ui-sliderAccess.js" type="text/javascript"></script> 
    <script src="../Scripts/ui/jquery-ui-timepicker-addon.js" type="text/javascript"></script>




            <script type="text/javascript">
                $(document).ready(function () {
                    $("#modalDiv").dialog({
                        modal: true,
                        autoOpen: false,
                        height: '500',
                        width: '800px',
                        draggable: true,
                        resizeable: true,
                        title: 'Applicable Evidence Based Programs (EBPs)',
                        buttons: {

                            Close: function () {
                                $(this).dialog("close");
                            }
                        }

                    });
                    $("#"+'<%=btnGrdNewRow02.ClientID %>').click(
                function () {
                    
                    url = 'GranteeFocalEBPs.aspx?lngPkID=' + "3" + '&lngPanelID=' + $("#" + '<%=HFPanelID.ClientID %>').val();
                        $("#modalDiv").dialog("open");
                        $("#modalIFrame").attr('src', url);
                     
                    return false;
                });
                });

                function closeIframe() {
                    $('#modalDiv').dialog("close");
                    
                    return false;
                }

                $('.saveButton').click(function () {
                   // $(this).prop("disabled", true);
                });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
       <h1>Grantee Edit</h1>
    <div class="formLayout">
     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
               <p> 
               <label for="txtGranteeidno">Grantee ID</label>
		        <asp:textbox id="txtGranteeidno" runat="server" MaxLength="8" AutoPostBack="true" OnTextChanged="txtGranteeidno_TextChanged" ></asp:textbox>
                <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>

          <%--  &nbsp;
                  <asp:RequiredFieldValidator ID="rfvGranteeID" runat="server" ValidationGroup="Save" 
            ControlToValidate="txtGranteeidno" ErrorMessage="Grantee ID Required" ForeColor="Red">Required!</asp:RequiredFieldValidator> 
            &nbsp;<asp:RegularExpressionValidator ID="RVGranteeID" runat="server" ErrorMessage="Input 8 alphanumeric characters" 
            ControlToValidate="txtGranteeidno" ValidationGroup="Save" ValidationExpression="^[a-zA-Z0-9]{8}$" ForeColor="Red"></asp:RegularExpressionValidator>--%>
            </p>
 </ContentTemplate>
            </asp:UpdatePanel>

        	 
	        <p>
		        <label for="txtGranteeName">Grantee Name</label>
		        <asp:textbox id="txtGranteeName" runat="server" MaxLength="45" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtAddress1">Address 1</label>
		        <asp:textbox id="txtAddress1" runat="server" MaxLength="50" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtAddress2">Address 2</label>
		        <asp:textbox id="txtAddress2" runat="server" MaxLength="50" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtCity">City</label>
		        <asp:textbox id="txtCity" runat="server" MaxLength="50" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtState">State</label>
		        <asp:textbox id="txtState" runat="server" MaxLength="40" ></asp:textbox>
	        </p>
	        <p>
		        <label for="txtZip">Zip</label>
		        <asp:textbox id="txtZip" runat="server" MaxLength="15"></asp:textbox>
	        </p>
	        <p>
		        <label for="txtPhone">Phone</label>
		
		        <asp:textbox id="txtPhone" runat="server" MaxLength="15" ></asp:textbox>
	        </p>
  

    </div>
    <h2>Applicable Evidence Based Programs (EBPs) </h2>
    <div class="formLayout">
        <asp:CheckBoxList ID="chkEBPs" runat="server" RepeatColumns="2"></asp:CheckBoxList>

        <br /><asp:Button ID="btnAssignFocalEBP" runat="server"    Text="Assign Focal EBPs" OnClick="btnAssignFocalEBP_Click" ValidationGroup="Save"  />
               
         <button id="btnGrdNewRow02" runat="server" Visible="false" class="MEPIbtn" style="margin-top: 4px; 
            margin-bottom: 10px;" type="button" >
            Assign/ Unassign Focal EBPs</button>
                <asp:Button ID="btnAssignFocalEBPs" runat="server" Text="Assign Focal EBPs"  Visible="false" onclick="btnAssignFocalEBPs_Click"  />

    </div>
    <div class="formLayout">
       <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" ValidationGroup="Save" CssClass="saveButton" />&nbsp;
                <asp:Button ID="btnSaveClose" Visible="false" runat="server" Text="Save and Close" onclick="btnSaveClose_Click" ValidationGroup="Save" />&nbsp;<asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp;             
          
        </p>
      </p>
        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in the required fields." />

        <p>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
          
        </p>
    </div>
    <h2>Contacts</h2>
    <div>
         
            <asp:GridView ID="grdVwList" runat="server"  CssClass="gridTbl"  
            AutoGenerateColumns="False"     AllowPaging="false"  
            AllowSorting="True" OnRowCommand="grdVwList_RowCommand" OnRowDataBound="grdVwList_RowDataBound"  >
            <Columns>
            		<asp:BoundField DataField="GranteeContactId" DataFormatString="{0:g}" HeaderText="Id" Visible="false">
		            <HeaderStyle Width="50px"></HeaderStyle>
		            </asp:BoundField>
 
		            <asp:BoundField DataField="FirstName" HeaderText="First Name" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="125px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="LastName" HeaderText="Last Name" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="Email" HeaderText="Email" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="125px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="Phone" HeaderText="Phone" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="125px"></HeaderStyle>
		            </asp:BoundField>
 
		            <asp:BoundField DataField="GranteeRole" HeaderText="Role" DataFormatString="{0:g}">
                        		            <HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>

    
                     <asp:TemplateField>
                       <HeaderTemplate>                        
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" visible="true" 
                                    CssClass="otherstyle" CommandArgument='<%# Eval("GranteeContactId") %>'>View/Edit</asp:LinkButton>                                                          
                            </ItemTemplate>
                           <HeaderStyle width="50px" />
                        </asp:TemplateField> 
                
                
                 <asp:TemplateField>
                       <HeaderTemplate>                        
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnDelete" runat="server" CommandName="Delete" 
                                    visible="false"  CssClass="otherstyle" CommandArgument='<%# Eval("GranteeContactId") %>'>Delete</asp:LinkButton>                                                          
                            </ItemTemplate>
                           <HeaderStyle width="50px" />
                        </asp:TemplateField>  

            </Columns>
        </asp:GridView>
        <asp:Button ID="btnNew" runat="server"   Text="Add a New Contact" OnClick="btnNew_Click" ValidationGroup="Save"  />
    </div>

        <div id="modalDiv" ><iframe id="modalIFrame" width="100%" height="100%" frameBorder="0" scrolling="auto" title="Dialog Title"></iframe></div>
        <asp:HiddenField ID="HFPanelID" runat="server" />
</asp:Content>

