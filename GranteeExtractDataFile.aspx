﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="GranteeExtractDataFile.aspx.cs" Inherits="GranteeExtractDataFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

     <h1>Export data in excel</h1>


      <div class="formLayout">
<%--    <p>
<asp:Button ID="btnExtract1" runat="server"   Text="RPG Enrollment and Exit (Extract 1)" OnClick="btnExtract1_Click" /></p>
  <p>
<asp:Button ID="btnExtract2a" runat="server"   Text="EBP Enrollment and Exit-Case File (Extract 2a)" OnClick="btnExtract2a_Click" /></p>
  <p>
<asp:Button ID="btnExtract2b" runat="server"   Text="EBP Enrollment and Exit-Individual File (Extract 2b)" OnClick="btnExtract2b_Click" /></p>
  <p>
<asp:Button ID="btnExtract2c" runat="server"   Text="EBP Enrollment and Exit-Caseworker File (Extract 2c)" OnClick="btnExtract2c_Click" /></p>
  
  <p>
<asp:Button ID="btnExtract3a" runat="server"   Text="Focal EBP Service Logs-Case File (Extract 3a)" OnClick="btnExtract3a_Click" /></p>
    
    
    <p>
<asp:Button ID="btnExtract3b" runat="server"   Text="Focal EBP Service Logs-Individual File (Extract 3b )" OnClick="btnExtract3b_Click" /></p>
  <p>
<asp:Button ID="btnExtract4" runat="server"   Text="Focal EBP Participant Engagement (Extract 4)" OnClick="btnExtract4_Click" /></p>
  <p>
<asp:Button ID="btnExtract5" runat="server"   Text="Focal EBP Case Activity (Extract 5)" OnClick="btnExtract5_Click" /></p>
  <p>
<asp:Button ID="btnExtract6" runat="server"   Text="ESL Feed to OAISIS (Extract 6)" OnClick="btnExtract6_Click" /></p>--%>

  <table>
          <tr>
              <td><asp:Button ID="btnExtract1" runat="server"   Text="RPG Enrollment and Exit (Extract 1)" OnClick="btnExtract1_Click" /></td>
             <td><asp:Button ID="btnExtract3a" runat="server"   Text="Focal EBP Service Logs-Case File (Extract 3a)" OnClick="btnExtract3a_Click" />
                 
                 </td>
 
               </tr>
          <tr>
                <td><asp:Button ID="btnExtract2a" runat="server"   Text="EBP Enrollment and Exit-Case File (Extract 2a)" OnClick="btnExtract2a_Click" /></td>

              <td><asp:Button ID="btnExtract3b" runat="server"   Text="Focal EBP Service Logs-Individual File (Extract 3b )" OnClick="btnExtract3b_Click" /></td>
              
          
               </tr>
          <tr>
               <td> <asp:Button ID="btnExtract2b" runat="server"   Text="EBP Enrollment and Exit-Individual File (Extract 2b)" OnClick="btnExtract2b_Click" /></td>
             <td> <asp:Button ID="btnExtract4" runat="server"   Text="Focal EBP Participant Engagement (Extract 4)" OnClick="btnExtract4_Click" /></td>
               
             
          </tr>

          <tr>
               <td><asp:Button ID="btnExtract2c" runat="server"   Text="EBP Enrollment and Exit-Caseworker File (Extract 2c)" OnClick="btnExtract2c_Click" /></td>
         <td><asp:Button ID="btnExtract5" runat="server"   Text="Focal EBP Case Activity (Extract 5)" OnClick="btnExtract5_Click" /></td>
                  

          </tr>
          <tr>
                <td><asp:Button ID="btnExtract6" runat="server"  Visible="false"  Text="ESL Feed to OAISIS (Extract 6)" OnClick="btnExtract6_Click" /></td>
          
       
             
          </tr>

      </table>

</div>


  <div class="formLayout">

    <asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" CausesValidation="False" />
</div>
</asp:Content>

