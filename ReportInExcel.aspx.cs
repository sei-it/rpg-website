﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web.Configuration;

using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;

public partial class ReportInExcel : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["RPG_CLConnectionString"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnExtract1_Click(object sender, EventArgs e)
    {
        ExportExtract1();
    }
    private void ExportExtract1()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();           
            //string sql = "Select * from vw_Extract1";
           // string sql = "Select * from vw_LS_RPG_EnrollAndExit_Extract1";
            string sql = "Select [GRANTEE_ID_NO]" +
                         ",[CASE_ID]" +
                         ",[RPG_ENROLL_DATE]" +
                         ",[RPG_CLOSE_DATE]" +
                         ",[COMPLETED_PROGRAM]" +
                         ",[FAMILY_MOVE]" +
                         ",[UNABLE_TO_LOCATE]" +
                         ",[UNRESPONSIVE]" +
                         ",[FAMILY_DECLINED]" +
                         ",[TRANSFERRED]" +
                         ",[MISC_OR_CHILD_DEATH]" +
                         ",[PARENT_DEATH]" +
                         ",[CASE_CLOSE_OTHER]" +
                         ",[SPECIFY_RPG_CASE_CLOSE]" +
                         ",[IND_ID]" +
                         ",[Date of Birth]" +
                         ",[Gender]" +
                         ",[PersonType]" +
                         ",[FOCAL_CHILD_YN]" +
                         ",[RelashionShipToFocal] as  [RelationshipToFocal]" +
                         ",[RELATIONSHIP_TO_FOCAL_CHILD_IF_OTHER]" +
                         ",[FAMILY_FUNCTIONING_ADULT_YN]" +
                         ",[RECOVERY_DOMAIN_ADULT_YN]" +
                         ",[RACE_AMER_IND_OR_AK_NAT]" +
                         ",[RACE_ASIAN]" +
                         ",[RACE_BLACK]" +
                         ",[RACE_NAT_HI_OR_OTH_PAC_ISLE]" +
                         ",[RACE_WHITE]" +
                         ",[Ethnicity]" +
                         ",[PrimaryLanguage]" +
                         ",[PRIMARY_HOME_LANG_SPEC]" +
                         ",[CurrentResidence]" +
                         ",[CURRENT_RESIDENCE_SPECIFY_IF_OTHER]" +
                         ",[HighestEducation]" +
                         ",[IncomeLevel]" +
                         ",[INCOME_SOURCE_WAGE_SALARY]" +
                         ",[INCOME_SOURCE_PUB_ASS]" +
                         ",[INCOME_SOURCE_RETIR_PENS]" +
                         ",[INCOME_SOURCE_DISAB]" +
                         ",[INCOME_SOURCE_OTHER]" +
                         ",[INCOME_SOURCE_NONE]" +
                         ",[INCOME_SOURCE_SPECIFY_IF_OTHER] as [INCOME_SOURCESPECIFY_IF_OTHER]" +
                         ",[EmploymentStatus]" +
                         ",[RelationshipStatus]" +
                         ",[SYSTEM_TIMESTAMP_IND] " +
                         ",FOCAL_CHILD_FFA_CARE_ENRL as  [FOCAL CHILD FFA CARE ENRL]" +
                         ",FOCAL_CHILD_FFA_CARE_EXIT as  [FOCAL CHILD FFA CARE EXIT] " +
                         ",TREATMENT_ASSIGNMENT as [TREATMENT ASSIGNMENT] " +
            
            " from ExA_New_Extract1"; 


           // string sql = "Select * from ExA_New_Extract1DOB"; 
            
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "RPG_EnrollmentAndExit_Extract1");
            }
        }
    }
    private void ExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        int pos = FileName.LastIndexOf('_') + 1;
        int lenght = FileName.Length;
        string strRptName = FileName.Substring(pos, lenght - pos); 

       // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        string ExtractName = "Grantee" + "(" + timestamp + ")" + FileName + " .xlsx";             
      //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");
        string sDest = @"E:\xx\SampleExcel\Docs\padp.xlsx";
        string sTempFileName = Server.MapPath(@"~\tmp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName, strRptName);
        displayExport(sTempFileName, ExtractName);

        //System.IO.StringWriter sw = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(sw);
        //GridView gv = new GridView();
        //gv.DataSource = ds.Tables[TableIndex];
        //gv.DataBind();
        //PrepareGridViewForExport(gv);
        //gv.RenderControl(hw);
        //Response.Write(sw.ToString());
        //Response.End();
    }

    
 
    private void PrepareGridViewForExport(System.Web.UI.Control control)
    {
        string content = "";
        for (int i = control.Controls.Count - 1; i >= 0; i--)
        {
            PrepareGridViewForExport(control.Controls[i]);
        }
        if (!(control is TableCell))
        {
            if (control.GetType().GetProperty("SelectedItem") != null)
            {
                LiteralControl literal = new LiteralControl();
                control.Parent.Controls.Add(literal);
                try
                {
                    literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                }
                catch
                {
                }
                control.Parent.Controls.Remove(control);
            }
            else
                if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    if (control.GetType() == typeof(CheckBox) || control.GetType() == typeof(RadioButton))
                    {
                        literal.Text = (bool)control.GetType().GetProperty("Checked").GetValue(control, null) ? "Yes" : "";
                    }
                    else
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);

                    control.Parent.Controls.Remove(control);
                }
        }
        return;
    }
    public static void ExportDataSet(DataSet ds, string destination, string reportName)
    {
       
        using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            foreach (System.Data.DataTable table in ds.Tables)
            {

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                utility util = new utility();

                DocumentFormat.OpenXml.Spreadsheet.Row headerFieldRow = addHeaderFieldNumbers(util.getColumnCodes(reportName));
                sheetData.AppendChild(headerFieldRow);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        dynamic dc = dsrow[col];
                        if (dc.GetType().ToString() == "System.Boolean")
                        {
                            if (dsrow[col].ToString() == "True")
                            {
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Yes");
                            }

                        }
                        else
                        {
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        }
                        //if (Type.GetType(dc.GetType().ToString()))
                        //
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

            }
        }
    }

    private static Row addHeaderFieldNumbers(string[] extractColCodes)
    {
        DocumentFormat.OpenXml.Spreadsheet.Row FieldRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
        foreach (string code in extractColCodes)
        {
            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(code);
            FieldRow.AppendChild(cell);
        }
        return FieldRow;
    }

  

    private void displayExport(string sTempFileName, string sFilename)
    {
        try
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.spreadsheet";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);

            }
        }
    }

    protected void btnExtract2a_Click(object sender, EventArgs e)
    {
        ExportExtract2a();
    }
    private void ExportExtract2a()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();           
           // string sql = "Select * from vw_Extract2a";
            //string sql = "Select * from vw_LS_EnrollAndExit_Extract2a"; //updated on 20th May

            string sql = "Select [GRANTEE_ID_NO]" +
                            " ,[CASE_ID]" +
                            " ,[EBPName]" +
                            " ,[RPG_ENROLL_DATE]" +
                            " ,[EBP_ENROLL_DATE]" +
                            " ,[EBP_EXIT_DATE]" +
                            " ,[CaseEBPID]" +

            " from Ex2A_New_Extract2a"; //updated on 25th May
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "EBPEnrollment_ExitCaseFile_Extract2a");
            }
        }
    }

    protected void btnExtract2b_Click(object sender, EventArgs e)
    {
        ExportExtract2b();
    }
    private void ExportExtract2b()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();           
           // string sql = "Select * from vw_Extract2b";
           // string sql = "Select * from vw_LS_EnrollAndExit_Extract2b";

            string sql = "Select [GRANTEE_ID_NO]" +
                        ",[CASE_ID]" +
                        ",[RPG_ENROLL_DATE]" +
                        ",[EBPName]" +
                        ",[EBP_ENROLL_DATE]" +
                        ",[IND_ID] " +
                        ",[CaseEBPID] " + 

            " from Ex2b_New_Extract2b";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "EBPEnrollment_ExitIndividual_Extract2b");
            }
        }
    }

    protected void btnExtract2c_Click(object sender, EventArgs e)
    {
        ExportExtract2c();
    }
    private void ExportExtract2c()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();            
            //string sql = "Select * from vw_NewExtract2c";
            //string sql = "Select * from vw_LS_EnrollAndExit_Extract2c";

            string sql = "Select [GRANTEE_ID_NO] " +
                        "  ,[CASE_ID]" +
                        "  ,[RPG_ENROLL_DATE]" +
                        "  ,[EBP_ENROLL_DATE]" +
                        "  ,[EBPName]" +
                        "  ,[CaseworkerID] as [CaseWorkerID]" +
                        "  ,[CaseWorker_StartDate]" +
                        "  ,[CaseWorker_ExitDate]" +
                        "  ,[CaseworkerName]" +
                        ",[CaseEBPID] " + 

            " from Ex2c_New_Extract2c";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "EBPEnrollment_ExitCaseworkerFile_Extract2c");
            }
        }
    }

    protected void btnExtract3a_Click(object sender, EventArgs e)
    {
        ExportExtract3a();
    }
    private void ExportExtract3a()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();            
           //string sql = "Select * from vw_NewExtract3a";
           //string sql = "Select * from vw_LS_FocalEBP_ServiceLogs_CaseFile_Extract3a";
            // string sql = "Select * from vw_Extract3a";

            //string sql = "Select * from Ex3a_New_Extract3a";

            string sql = " SELECT DISTINCT  " +     //"Select * from Ex3a_New_Extract3a where GranteeId= @intUserGranteeID ";
            " dbo.vw_A_serviceLog.GRANTEE_ID_NO, dbo.vw_A_serviceLog.CASE_ID, dbo.vw_A_serviceLog.RPG_ENROLL_DATE,  " +
            " dbo.vw_A_serviceLog.EBP_ENROLL_DATE, dbo.vw_A_serviceLog.DATE_OF_SERVICE, dbo.vw_A_serviceLog.[SYSTEM TIME STAMP] as [SYSTEM_TIMESTAMP_SL],  " +
            " dbo.vw_A_serviceLog.CaseworkerName, dbo.vw_A_serviceLog.CaseworkerID as [CaseWorkerID], dbo.vw_A_serviceLog.EBPName, dbo.vw_A_serviceLog.ServiceID,  " +
            " dbo.vw_A_serviceLog.SESSION_LOCATION_FK as [SESSION_LOCATION], dbo.vw_A_serviceLog.SESSION_LOCATION_SPECIFY_IF_OTHER, dbo.vw_A_serviceLog.SESSION_MINUTES,  " +
            " dbo.vw_A_serviceLog.OTHERS_PRESENT_FOST_PRNT_GUARD_YN, dbo.vw_A_serviceLog.OTHERS_PRESENT_INTERPRETER_YN,  " +
            " dbo.vw_A_serviceLog.OTHERS_PRESENT_GRANT_STAFF_MEMB_YN, dbo.vw_A_serviceLog.OTHERS_PRESENT_OTH_RELAT_YN,  " +
            " dbo.vw_A_serviceLog.OTHERS_PRESENT_RPG_PART_STAFF_YN, dbo.vw_A_serviceLog.OTHERS_PRESENT_FID_OBS_YN,  " +
            " dbo.vw_A_serviceLog.OTHERS_PRESENT_HLTH_PROF_YN, dbo.vw_A_serviceLog.OTHERS_PRESENT_SUPERVIS_YN,  " +
            " dbo.vw_A_serviceLog.OTHERS_PRESENT_OTH_PROF_STAFF_YN, dbo.vw_A_serviceLog.OTHERS_PRESENT_SPECIFY_IF_OTHER,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_GRP_DISC_YN, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_IND_DISC_YN,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_FAM_ACT_INTER, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_FAM_MTNG,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_ROLE_PLAY, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_REENACT,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_EXP_ACT, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_GAMES,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_WORKSHEET, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_WATCH_VID,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_GOAL_SET, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_GUID_PRAC,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_COACH_FEED, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_PROV_EMOT_SUPP,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_CRIS_INTER, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_PRNT_SKILL_SCR,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_CHILD_DEV_SCR, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_HLTH_ASS,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_MENT_HLTH_SUB_ABUSE, dbo.vw_A_serviceLog.SESSION_ACTIVITIES_OTHER,  " +
            " dbo.vw_A_serviceLog.SESSION_ACTIVITIES_SPECIFY_OTHER, " +
            " CASE dbo.vw_A_serviceLog.SESSION_ALIGNMENT_FK WHEN 1 THEN '3' WHEN 2 THEN '2' WHEN 3 THEN '1' ELSE null END AS " +
            " SESSION_ALIGNMENT, " +
            " dbo.vw_A_serviceLog.REASON_NOT_ALIGN_FAM_CRIS, dbo.vw_A_serviceLog.REASON_NOT_ALIGN_PART_NOT_ENGAGE,  " +
            " dbo.vw_A_serviceLog.REASON_NOT_ALIGN_PART_INTER_OTH_TOP, dbo.vw_A_serviceLog.REASON_NOT_ALIGN_PRES_OTH_IND_INHIB_SESS,  " +
            " dbo.vw_A_serviceLog.REASON_NOT_ALIGN_PART_SICK, dbo.vw_A_serviceLog.REASON_NOT_ALIGN_PHYSC_CONST,  " +
            " dbo.vw_A_serviceLog.REASON_NOT_ALIGN_OTHER, dbo.vw_A_serviceLog.SPECIFY_REASON_NOT_ALIGN, dbo.vw_A_serviceLog.ADULT_TOPICS_SUB_ABUSE as ADULT_TOPICS_SUD,  " +
            " dbo.vw_A_serviceLog.ADULT_TOPICS_PRNT_SKILLS, dbo.vw_A_serviceLog.ADULT_TOPICS_PERS_DEV,  " +
            " dbo.vw_A_serviceLog.CHILD_TOPICS_ED_ADULT_SUB_ABUSE AS YOUTH_TOPICS_THERAPY,  " +
            " dbo.vw_A_serviceLog.CHILD_TOPICS_DEV_AND_ED AS YOUTH_TOPICS_SUD_ED, dbo.vw_A_serviceLog.ED_OTH_RELAT as OTHREL_TOPICS_SUD_ED,  " +
            " dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T1 as ADULT_SUD_T1, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T2 as ADULT_SUD_T2, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T3 as ADULT_SUD_T3,  " +
            " dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T4 as ADULT_SUD_T4, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T5 as ADULT_SUD_T5, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T6 as ADULT_SUD_T6,  " +
            " dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T7 as ADULT_SUD_T7, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T8 as ADULT_SUD_T8, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T9 as ADULT_SUD_T9,  " +
            " dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T10 as ADULT_SUD_T10, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T11 as ADULT_SUD_T11, dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T12 as ADULT_SUD_T12,  " +
            " dbo.vw_B_serviceLog.ADULT_SUB_ABUSE_T13 as ADULT_SUD_T13, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T1, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T2,  " +
            " dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T3, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T4, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T5,  " +
            " dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T6, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T7, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T8,  " +
            " dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T9, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T10, dbo.vw_B_serviceLog.ADULT_PARENT_SKILLS_T11,  " +
            " dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T1, dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T2, dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T3,  " +
            " dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T4, dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T5, dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T6,  " +
            " dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T7, dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T8, dbo.vw_B_serviceLog.ADULT_PERSONAL_DEV_T9,  " +
            " dbo.vw_B_serviceLog.YOUTH_THERAPY_T1, dbo.vw_B_serviceLog.YOUTH_THERAPY_T2, dbo.vw_B_serviceLog.YOUTH_THERAPY_T3,  " +
            " dbo.vw_B_serviceLog.YOUTH_THERAPY_T4, dbo.vw_B_serviceLog.YOUTH_THERAPY_T5, dbo.vw_B_serviceLog.YOUTH_THERAPY_T6,  " +
            " dbo.vw_B_serviceLog.YOUTH_THERAPY_T7, dbo.vw_B_serviceLog.YOUTH_THERAPY_T8, dbo.vw_B_serviceLog.YOUTH_THERAPY_T9,  " +
            " dbo.vw_B_serviceLog.YOUTH_THERAPY_T10, dbo.vw_B_serviceLog.YOUTH_THERAPY_T11, dbo.vw_B_serviceLog.EDU_YOUTH_SUB_ABUSE_T1 as YOUTH_SUD_ED_T1, " +
            " dbo.vw_B_serviceLog.EDU_YOUTH_SUB_ABUSE_T2 as YOUTH_SUD_ED_T2, dbo.vw_B_serviceLog.EDU_YOUTH_SUB_ABUSE_T3 as YOUTH_SUD_ED_T3, dbo.vw_B_serviceLog.EDU_YOUTH_SUB_ABUSE_T4 as YOUTH_SUD_ED_T4,  " +
            " dbo.vw_B_serviceLog.EDU_YOUTH_SUB_ABUSE_T5 as YOUTH_SUD_ED_T5, dbo.vw_B_serviceLog.EDU_OTHER_RELATIVES_T1 as OTHREL_SUD_ED_T1, dbo.vw_B_serviceLog.EDU_OTHER_RELATIVES_T2 as OTHREL_SUD_ED_T2,  " +
            " dbo.vw_B_serviceLog.EDU_OTHER_RELATIVES_T3 as OTHREL_SUD_ED_T3, dbo.vw_B_serviceLog.EDU_OTHER_RELATIVES_T4 as OTHREL_SUD_ED_T4,  dbo.vw_A_serviceLog.EBP_EXIT_DATE, " +
            "  dbo.vw_A_serviceLog.CaseEBPID " +
            " FROM         dbo.vw_A_serviceLog INNER JOIN " +
            " dbo.vw_B_serviceLog ON dbo.vw_A_serviceLog.ServiceID = dbo.vw_B_serviceLog.ServiceID " +
            " WHERE     (dbo.vw_A_serviceLog.CASE_ID IS NOT NULL) ";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "FocalEBP_ServiceLogs_CaseFile_Extract3a");
            }
        }
    }

    protected void btnExtract3b_Click(object sender, EventArgs e)
    {
        ExportExtract3b();
    }
    private void ExportExtract3b()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();            
            //string sql = "Select * from vw_NewExtract3b";
            //string sql = "Select * from FoaclEBP_Extract3b";

            string sql = "Select [GRANTEE_ID_NO]" +
                        " ,[CASE_ID]" +
                        " ,[RPG_ENROLL_DATE]" +
                        " ,[DATE_OF_SERVICE]" +
                        " ,[EBPName]" +
                        " ,[EBP_ENROLL_DATE]" +
                        " ,[SYSTEM TIME STAMP] as SYSTEM_TIMESTAMP_SL" +
                        " ,[IND_ID]" +
                        " ,[CaseEBPID]" +
                          " from Ex3b_New_Extract3b";         
          
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "FocalEBP_ServiceLogsIndividual_File_Extract3b");
            }
        }
    }

    protected void btnExtract4_Click(object sender, EventArgs e)
    {
        ExportExtract4();
    }
    private void ExportExtract4()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();
           
           // string sql = "Select * from vw_NewExtract4";
           // string sql = "Select * from ServiceLogExtract4";

            string sql = "Select [GRANTEE_ID_NO]" +
                        " ,[CASE_ID]" +
                        " ,[RPG_ENROLL_DATE]" +
                        " ,[DATE_OF_SERVICE]" +
                        " ,[SYSTEM TIME STAMP] as SYSTEM_TIMESTAMP_SL" +
                        " ,[EBPName]" +
                        " ,[ParticipantsEngagementScale]" +
                        " ,[ServiceID]" +
                         " ,[CaseEBPID]" +
            
            " from Ex4_New_Extract4";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "FocalEBP_ParticipantEngagement_Extract4");
            }
        }
    }

    protected void btnExtract5_Click(object sender, EventArgs e)
    {
        ExportExtract5();
    }
    private void ExportExtract5()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();        
           // string sql = "Select * from vw_Extract5";
           // string sql = "Select * from vw_NewExtract5";


            string sql = "Select [GRANTEE_ID_NO]" +
                            " ,[CASE_ID]" +
                            " ,[RPG_ENROLL_DATE]" +
                            " ,[DATE_OF_SERVICE]" +
                            " ,[SYSTEM TIME STAMP] as SYSTEM_TIMESTAMP_SL" +
                            " ,[EBPName]" +
                            " ,[CONTACT_CONFIRMATION_YN]" +
                            " ,[IN_PERSON_CONFIRMATION_YN]" +
                            " ,[CONTACT_TYPE_PROV_REF_SERV]" +
                            " ,[CONTACT_TYPE_ATT_SCHED_APP]" +
                            " ,[CONTACT_TYPE_CHECK_FAM_NOT_IN_PERS]" +
                            " ,[CONTACT_TYPE_OTHER]" +
                            " ,[SPECIFY_CONTACT_TYPE]" +
                            " ,[REASON_NO_CASE_CONTACT_FK]" +
                            " ,[MISSED_VISIT_COUNT]" +
                            " ,[ABILITY_SCHED_APP_YN]" +
            
            " from Ex5_New_Extract5";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "FocalEBP_CaseActivity_Extract5");
            }
        }
    }

    protected void btnExtract6_Click(object sender, EventArgs e)
    {
        ExportExtract6();
    }
    private void ExportExtract6()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            SqlConnection con = new SqlConnection(connString);
            DataSet ds = new DataSet();           
           // string sql = "Select * from vw_Extract6";
           // string sql = "Select * from ESL_OAISIS_Extract6_new";


            string sql = "Select [RPG_Case_ID]" +
                        " ,[RPG_Enrollment_Date]" +
                        " ,[RPG_Closure_Date]" +
                        " ,[RPG_Member_ID]" +
                        " ,[RPG_Member_DOB]" +
                        " ,[RPG_Member_Gender]" +
                        " ,[RPG_IsFocalChild]" +
                        " ,[RPG_IsFFA]" +
                        " ,[RPG_IsRDA]" +
                        " ,[RPG_IsTreatment]" +
            
            " from Ex6_New_Extract6";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "ESLFeed_to_OAISIS_Extract6");
            }
        }
    }


    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("CaseList.aspx");
    }
}