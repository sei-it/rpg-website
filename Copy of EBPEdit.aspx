﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Copy of EBPEdit.aspx.cs" Inherits="EBPEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
         <script>
             $(function () {
                 $("#<%=txtEbpenrolldate.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
                 $("#<%=txtEbpexitdate.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
                 $("#<%=txtCaseWorkerStartDt.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
                 $("#<%=txtCaseWorkerExitDt.ClientID%>").datepicker({
                     dateFormat: 'mm/dd/yy',
                     minDate: '01/01/2014',
                     maxDate: 'dateToday'
                 })
         });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Manage EBP Enrollment and Exit</h1>
    <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal></h2>
        <p>
            <asp:Literal ID="litMessagePermissions" runat="server"></asp:Literal>
          
        </p>
    <div class="formLayout">
      <p>
		<label for="cboEBP">EBP Name</label>
		<asp:DropDownList id="cboEBP" runat="server"  RepeatDirection="Horizontal"  > 
		</asp:DropDownList>
           <asp:RequiredFieldValidator ID="rqvcboEBPt" runat="server" ValidationGroup="Save1"
             ControlToValidate="cboEBP" ErrorMessage="EBP Name is required"
             ForeColor="Red">Required</asp:RequiredFieldValidator>
	</p>
	<p>
		<label for="txtEbpenrolldate">EBP Enrollment Date</label>
		<asp:textbox id="txtEbpenrolldate" runat="server" ></asp:textbox>
        <asp:RequiredFieldValidator ID="reqEbpenrolldate" runat="server" ValidationGroup="Save1"
             ControlToValidate="txtEbpenrolldate" ErrorMessage="EBP Enrollment Date is required"
             ForeColor="Red">Required</asp:RequiredFieldValidator>
 	           
 	</p>
        <p><asp:Button ID="btnEBPClose" runat="server" Text="Exit Case from EBP" onclick="btnEBPClose_Click"   />&nbsp;</p>
        
    </div>
    <div class="formLayout" id="divEngagementRating" runat="server" visible="false"> 
           <h3>EBP Exit</h3>
                 <asp:label ID="litEBPMeassage" runat="server" ForeColor="red"></asp:label>
        
        	<p>
		        <label for="txtEbpexitdate">EBP Exit Date</label>
		        <asp:textbox id="txtEbpexitdate" runat="server" ></asp:textbox>
        
              <%--  <asp:RequiredFieldValidator ID="reqEbpexitdate" runat="server" ValidationGroup="Save1"
                     ControlToValidate="txtEbpexitdate" ErrorMessage="EBP Exit Date is required"
                     ForeColor="Red">Required</asp:RequiredFieldValidator>--%>
                 <asp:CompareValidator ID="cmpEBPexitdate" ControlToCompare="txtEbpenrolldate" ControlToValidate="txtEbpexitdate" 
                        Type="Date" Operator="GreaterThanEqual" ForeColor="Red" ValidationGroup="Save1"
                    ErrorMessage="EBP Exit date should be greater than or equal to EBP Enrollment Date" runat="server">*</asp:CompareValidator>
                 <asp:CompareValidator ID="cmEBPExitdt" ControlToCompare="txtCaseWorkerStartDt" ControlToValidate="txtEbpexitdate" 
                        Type="Date" Operator="GreaterThanEqual" ForeColor="Red" ValidationGroup="Save1"
                    ErrorMessage="EBP Exit date should be greater than or equal to Case Worker Start Date" runat="server">*</asp:CompareValidator>
 	        </p>
        <div id="PEngFocalEBP" runat="server">
           <h3>Participant Engagement Rating</h3>
                <p  style="width:900px"> Rate the specified case's engagement to date in evidence based program.(Complete after 2nd Service Log entry for EBP and at EBP Exit Mark only one) </p>
 
                    <asp:RadioButtonList ID="chkERatings" runat="server" RepeatColumns="1"></asp:RadioButtonList>
                
                        <ul>
                            <li>
                                <b>4. Participants were consistently highly involved in services:</b> The participants kept most appointments and actively participated in discussions and activities. If homework was assigned, the participants completed it.

                            </li>
                            <li>
                                <b>3. Participants&#39; involvement varied:</b> The participant(s) sometimes kept appointments and sometimes actively participated in discussions and activities. If homework was assigned, the participants sometimes completed it. At other times, the participants&#39; involvement was low.</li>
                            <li>
                        <b>2. Participants&#39; involvement was consistently low:</b> The participant(s) kept some appointments but missed or cancelled frequently. The participant(s) rarely actively participated in discussions and activities. If homework was assigned, the participant(s) frequently did not complete it.

                            </li>
                            <li>
                                <b>1. Participants were minimally or not involved at all:</b> The participant(s) kept few appointments. The participant(s) did not actively participate in discussions and activities. If homework was assigned, the participant(s) did not complete it.</li>

                        </ul>
            </div>
         </div>
    <h2>Case Members Participating in EBP</h2>
    <div>
        <asp:CheckBoxList ID="chkCaseMembers" runat="server"></asp:CheckBoxList>

    </div>

     <h2>Case Workers</h2>
    <div>

        
        <asp:GridView ID="grdCaseWorkerList" Visible="true"   AutoGenerateColumns="False" CssClass="gridTbl" 
             AutoPostBack="true"  OnRowCommand="grdCaseWorkerList_RowCommand"  runat="server"  OnRowDataBound="grdCaseWorkerList_RowDataBound">
            <Columns>
                

                <asp:BoundField  Visible="false"  DataField="GranteeContactId" HeaderText="GranteeContactID" DataFormatString="{0:g}">
		            <HeaderStyle Width="10px"></HeaderStyle>
		        </asp:BoundField>

                <asp:BoundField   DataField="CaseWorkerName" HeaderText="Name" DataFormatString="{0:g}">
		            <HeaderStyle Width="150px"></HeaderStyle>
		        </asp:BoundField>

		        <asp:BoundField DataField="Email" HeaderText="Email" DataFormatString="{0:g}">
		            <HeaderStyle Width="100px"></HeaderStyle>
		         </asp:BoundField>
		            
                <asp:BoundField DataField="Phone" HeaderText="Phone #" DataFormatString="{0:g}">
                    <HeaderStyle Width="100px"></HeaderStyle>
		         </asp:BoundField>

                <asp:BoundField DataField="CaseWorker_StartDate" HeaderText="CaseWorker Start Date" DataFormatString="{0:d}">
                    <HeaderStyle Width="100px"></HeaderStyle>
		         </asp:BoundField>

                <asp:BoundField DataField="CaseWorker_ExitDate" HeaderText="CaseWorker End Date" DataFormatString="{0:d}">
                    <HeaderStyle Width="100px"></HeaderStyle>
		         </asp:BoundField>

                 <asp:TemplateField>
                       <HeaderTemplate>                     
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdits" runat="server" CommandName="EditCaseWorker" 
                                    visible="false"  CssClass="otherstyle" CommandArgument='<%# Eval("GranteeContactId") %>'>Edit</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                 </asp:TemplateField>


                 <asp:TemplateField>
                       <HeaderTemplate>Remove                    
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnRemove" runat="server" CommandName="RemoveCaseWorker" 
                                    visible="true"  CssClass="otherstyle" CommandArgument='<%# Eval("GranteeContactId") %>'>Remove</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                 </asp:TemplateField>
            </Columns>
        </asp:GridView>


    </div>


    <h2>Assign Caseworker(s)</h2>
    <div>
        <p>
        <asp:CheckBoxList ID="chkCaseWorkers" runat="server"  Visible="false"></asp:CheckBoxList></p>

      <p>
          

         
        <asp:DropDownList ID="ddlCaseWorkers" runat="server" Width="130" AutoPostBack="true"></asp:DropDownList>  
              
		<label for="txtCaseWorkerStartDt">Start Date</label>
		<asp:textbox id="txtCaseWorkerStartDt"  Width="75" runat="server" ></asp:textbox>
           <%--<asp:RequiredFieldValidator ID="rfvCaseWorkerStartDt" runat="server" ValidationGroup="Save1"
             ControlToValidate="txtCaseWorkerStartDt" ErrorMessage="CaseWorker start date is required"
             ForeColor="Red">*</asp:RequiredFieldValidator>--%>


          <asp:CompareValidator ID="cmpVSdt" ControlToCompare="txtEbpenrolldate" ControlToValidate="txtCaseWorkerStartDt" 
                Type="Date" Operator="GreaterThanEqual" ForeColor="Red" ValidationGroup="Save1"
            ErrorMessage="Start date should be greater than or equal to EBP Enrollment Date" runat="server">*</asp:CompareValidator>
           <asp:CompareValidator ID="cmpVSdt2" ControlToCompare="txtEbpexitdate" ControlToValidate="txtCaseWorkerStartDt" 
                Type="Date" Operator="LessThanEqual" ForeColor="Red" ValidationGroup="Save1"
            ErrorMessage="Start date should be less than or equal to EBP exit date" runat="server">*</asp:CompareValidator>


 	    <label for="txtCaseWorkerExitDt">End Date</label>
		<asp:textbox id="txtCaseWorkerExitDt" Width="75" runat="server" ></asp:textbox>

           <%--<asp:RequiredFieldValidator ID="rqvCaseWorkerExitDt" runat="server" ValidationGroup="Save1"
             ControlToValidate="txtCaseWorkerExitDt" ErrorMessage="CaseWorker Exit date is required"
             ForeColor="Red">*</asp:RequiredFieldValidator>--%>

          <asp:CompareValidator ID="cmpVal1" ControlToCompare="txtCaseWorkerStartDt" ControlToValidate="txtCaseWorkerExitDt" 
                Type="Date" Operator="GreaterThanEqual" ForeColor="Red" ValidationGroup="Save1"
            ErrorMessage="Exit date should be greater than or equal to Start date" runat="server">*</asp:CompareValidator>
          <asp:CompareValidator ID="cmpVal2" ControlToCompare="txtEbpexitdate" ControlToValidate="txtCaseWorkerExitDt" 
                Type="Date" Operator="LessThanEqual" ForeColor="Red" ValidationGroup="Save1"
            ErrorMessage="Exit date should be less than or equal to EBP exit date" runat="server">*</asp:CompareValidator>

           <asp:Button ID="btnAddCsaeWorker" ValidationGroup="Save1" runat="server"   Text="Add case worker assignment" OnClick="btnAddCsaeWorker_Click"  />
 	</p>

    </div>

              <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" ValidationGroup="Save1" />&nbsp;
                <asp:Button ID="btnSaveClose" runat="server" Text="Save And Close" onclick="btnSaveClose_Click"  ValidationGroup="Save1"/>&nbsp;                  
                  <asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 

                   <asp:ValidationSummary id="valSumm2" runat="server" ForeColor="Red" ValidationGroup="Save1" HeaderText="Please fill in the correct dates for required fields."></asp:ValidationSummary>

          <asp:ValidationSummary id="vsum1" runat="server" ForeColor="Red" ValidationGroup="Save" HeaderText="Please fill in the correct dates for required fields."></asp:ValidationSummary>
           
        </p>
            <p>
               <asp:Label ID="lblAddCwMessage" runat="server" ForeColor="Red"></asp:Label>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
          
        </p>
</asp:Content>

