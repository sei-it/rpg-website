﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EBPEdit : System.Web.UI.Page
{
    int lngPkID;
    int intCaseID;
    int? intGranteeID;
    int GranteeContactid;
    int CaseWorkerid;
    bool blClose  ;
    UserInfo oUI;
    int ebpid1;
    string val;
     int granteeID;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;
        rfvCaseWorkerStartDt.Visible = false;
        lblSelectCWMesage.Text = "";

        Session["IsPageRefresh"] = false;
       if (!IsPostBack)
        {
            string currentDate = DateTime.Today.ToShortDateString();
            cvBPMaxEnroll.ValueToCompare = currentDate;
            // cvEBPExitDate.ValueToCompare = currentDate;
            cmpEBPMinExitDate.ValueToCompare = currentDate;
            cvCWMaxEDt.ValueToCompare = currentDate;
            cwEndMaxDate.ValueToCompare = currentDate;


            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["intCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
            }

            if (Page.Request.QueryString["CaseWorkerid"] != null)
            {
                CaseWorkerid = Convert.ToInt32(Page.Request.QueryString["CaseWorkerid"]);
            }

            if (Page.Request.QueryString["GranteeContactid"] != null)
            {
                GranteeContactid = Convert.ToInt32(Page.Request.QueryString["GranteeContactid"]);
            }
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);

            loadrecords();

            ViewState["ViewStateId"] = System.Guid.NewGuid().ToString();

            Session["SessionId"] = ViewState["ViewStateId"].ToString();
        }
        else
        {
            if (ViewState["ViewStateId"].ToString() != Session["SessionId"].ToString())
            {

                Session["IsPageRefresh"] = true;
            }

            Session["SessionId"] = System.Guid.NewGuid().ToString();

            ViewState["ViewStateId"] = Session["SessionId"].ToString();
        }
        DisableBtnSave();
         
        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();
            loadCaseMembers();
            loadCaseWorkers();          
            displayRecords();
           // displayCaseWorkerl();
          
            ViewState["IsLoaded1"] = true;
        }

        CompareDate();

        Page.MaintainScrollPositionOnPostBack = true;
        

    }

    private void CompareDate()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            // compare RPG enrollment date

            object objValue;
            var oPages = from pg in db.B_CASE_ENROLLMENTs
                         where pg.CaseNoId == intCaseID
                         select pg;

            foreach (var oCase in oPages)
            {
                objValue = oCase.RPG_ENROLL_DATE;
                if (objValue != null)
                {

                    txtDtCmp.Text = String.Format("{0:MM/dd/yyyy}", objValue);
                    cvEbpEnrollDt.ValueToCompare = txtDtCmp.Text;

                }
                objValue = oCase.GranteeID_fk;
                if (objValue != null)
                {
                    granteeID = Convert.ToInt32(objValue);
                }


             // EBP exitdate compare value hide for nonfocal EBP
                object objValue4;

                var oPages4 = from fs in db.F_ENROLLMENT_EBPs
                              join ga in db.A_GranteeEBPs
                              on fs.EBPID_FK equals ga.EBPID_FK
                              where fs.CaseID_FK == intCaseID && fs.CaseEBPID == lngPkID && ga.GranteeID_FK == granteeID
                              select new { fs.CaseEBPID, fs.EBPID_FK, fs.CaseID_FK, ga.FocalEBPYN };

                foreach (var oCase4 in oPages4)
                {
                    objValue4 = oCase4.FocalEBPYN;
                    if (Convert.ToBoolean(objValue4) == false)
                    {
                        cvEBPExitCaseWrkEndDt.Visible = false;
                    }

                }

                //Compare caseworker start date

                object objValue2;
                var oPages2 = from pg in db.F_EBPCaseWorkers_AssignedLists
                              where pg.EBPEnrollID_FK == lngPkID
                              orderby pg.CaseWorker_StartDate ascending
                              select pg;

                if (string.IsNullOrEmpty(val))
                {
                    foreach (var oCase2 in oPages2)
                    {
                        objValue2 = oCase2.CaseWorker_StartDate;
                        if (objValue2 != null)
                        {
                            txtDtCmp.Text = String.Format("{0:MM/dd/yyyy}", objValue2);
                            cvEBPExitCaseWrkEndDt.ValueToCompare = txtDtCmp.Text;
                        }
                    }

                }

                //Comapre caseworker start date
                foreach (var oCase3 in oPages2)
                {
                    objValue2 = oCase3.CaseWorker_StartDate;
                    if (objValue2 != null)
                    {
                        txtDtCmp.Text = String.Format("{0:MM/dd/yyyy}", objValue2);
                        cvCaseWStartDt.ValueToCompare = txtDtCmp.Text;
                        btnEBPClose.Enabled = true;
                    }
                }

                //Compare date of services
                object objValue1;

                var oPages1 = from pg in db.G_SERVICE_LOGs
                              where pg.CaseID_FK == intCaseID && pg.CaseEBPID_FK == lngPkID
                              orderby pg.DATE_OF_SERVICE ascending
                              select pg;

                foreach (var oCase1 in oPages1)
                {
                    objValue1 = oCase1.DATE_OF_SERVICE;
                    if (objValue1 != null)
                    {
                        txtDtCmp.Text = String.Format("{0:MM/dd/yyyy}", objValue1);
                        cvEBPExitCaseWrkEndDt.ValueToCompare = txtDtCmp.Text;
                        val = cvEBPExitCaseWrkEndDt.ValueToCompare;
                    }
                    else
                    {
                        cvEBPExitCaseWrkEndDt.Visible = false;
                    }

                }

            }
        }
    }
    private void DisableBtnSave()
    {

        string name=oUI.sUserID.ToString();
        bool CWassigned = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oPages = from pg in db.F_ENROLLMENT_EBPs
                         where pg.CaseEBPID == lngPkID
                         select pg;
            foreach (var oCase in oPages)
            {
                if (oCase.EBP_EXIT_DATE!=null)
                {
                    //if (oUI.intUserRoleID == 3) // added on 27th june
                    //{
                        btnSave.Visible = false;
                        //btnEBPClose.Visible = false;// changed on 8th july
                        btnAddCsaeWorker.Visible = false; //changed on 8th july

                  //}
                    //else
                    //{
                    //    btnEBPClose.Visible=false;
                    //    btnSave.Visible = true;
                    //    btnAddCsaeWorker.Visible = true;
                    //}
                   // btnSaveClose.Visible = false;
                }
            }

            var oPages1 = from pg in db.E_CaseClosures
                         where pg.CaseNoId_FK == intCaseID
                         select pg;

            foreach (var oCase1 in oPages1)
            {
                if (oCase1.CASE_CLOSE_COMPLETED == true)
                {
                    if (!(oUI.intUserRoleID == 3)) // added on 27th june
                    {
                        btnEBPClose.Visible = false;
                        btnAddCsaeWorker.Visible = false;
                    }
                    else
                    {
                        // changed on 8th july

                       btnEBPClose.Visible = false;
                       btnAddCsaeWorker.Visible = false;
                    }
                }
            }



            //"Exit CASE From EBP" button Not Available for other caseworker added on 6/11
            var oPages2 = from pg in db.F_EBPCaseWorkers_AssignedLists
                         join G in db.A_Grantee_Contacts
                         on pg.GranteeContactId_FK equals G.GranteeContactId
                          where pg.EBPEnrollID_FK == lngPkID
                         select new { G.GranteeContactId, CaseWorkerName = G.FirstName + " " + G.LastName, G.Phone, G.Email, pg.EBPAssignedCaseWorkerID, pg.CaseWorker_StartDate, pg.CaseWorker_ExitDate };

            if (oUI.intUserRoleID == 2)
            {
                foreach (var oCase2 in oPages2)
                {
                    if (oCase2.Email == name)
                    {
                        CWassigned = true;                       
                    }                  
                }

                if (CWassigned == true)
                {
                    btnEBPClose.Visible = true;
                }
                else
                {
                    btnEBPClose.Visible = false;
                }
            }
            //-----------------------

        }
    }

    //-----Load--------------------//
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from e in db.F_EBPLists
                      join gebp in db.A_GranteeEBPs
                      on e.EBPID equals gebp.EBPID_FK
                      join cs1 in db.B_CASE_ENROLLMENTs
                      on gebp.GranteeID_FK equals cs1.GranteeID_fk
                      where cs1.CaseNoId == intCaseID
                      select e;
            cboEBP.DataSource = qry;
            cboEBP.DataTextField = "EBPName";
            cboEBP.DataValueField = "EBPID";
            cboEBP.DataBind();
            cboEBP.Items.Insert(0, "");

            /*ERating  04/03/2014*/
            var qry3 = from e in db.L_ENGAGEMENT_RATINGs
                       select e;
            chkERatings.DataSource = qry3;
            chkERatings.DataTextField = "CASE_EBP_ENGAGEMENT";
            chkERatings.DataValueField = "ER_ID";
            chkERatings.DataBind();
        }
    }
    protected void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                intGranteeID = oCase.GranteeID_fk;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
            }
        }
    }

   

    //-----Display---------------------------//
    protected void displayRecords()
    {
        object objVal = null;
        if (oUI.intUserRoleID == 2)
        {
           // litMessagePermissions.Text = "You do not have permissions to edit this page.";
            btnSave.Visible = false;
            //btnSaveClose.Visible = false;
           
        }

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
        var oPages = from pg in db.F_ENROLLMENT_EBPs 
                     where pg.CaseEBPID == lngPkID
                     select pg;
        foreach (var oCase in oPages)
        {
            objVal = oCase.EBPID_FK;
            if (objVal != null)
            {
                cboEBP.SelectedIndex = cboEBP.Items.IndexOf(cboEBP.Items.FindByValue(oCase.EBPID_FK.ToString()));
                ebpid1 = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                cboEBP.Enabled = false;
            }
            objVal = oCase.EBP_ENROLL_DATE;
            if (objVal != null)
            {
                txtEbpenrolldate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
                txtEbpenrolldate.Enabled = false;

            }

            //objVal = oCase.EBP_EXIT_DATE;
            //if (objVal != null)
            //{
            //    txtEbpexitdate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            //    btnSave.Visible = false;
            //    btnSaveClose.Visible = false;
               
            //}

            //--------to hide the Participant Engagement rating questions April 8th-------------
            
                var qry4 = from pg in db.A_GranteeEBPs                          
                           where   pg.GranteeID_FK == intGranteeID && pg.EBPID_FK == ebpid1
                           select new { pg.FocalEBPYN,pg.EBPID_FK };

                foreach (var c in qry4)
                {
                    if (c.FocalEBPYN == false)
                    {
                        PEngFocalEBP.Visible = false;
                    }
                    else
                    {
                        PEngFocalEBP.Visible = true;
                    }
                }
            

            //--------------------------

            /*PE 04/03/2014*/
            objVal = oCase.EBP_EXIT_DATE;
            if (objVal != null)
            {
                txtEbpexitdate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
                btnSave.Visible = false;
                //btnSaveClose.Visible = false;
                divEngagementRating.Visible = true;
                btnEBPClose.Text = "Cancel EBP Close"; 
                displayEngagementRatings();
            }
            else
            {
                btnSave.Visible = true;
               // btnSaveClose.Visible = true;
                divEngagementRating.Visible = false;
                btnEBPClose.Text = "Exit Case from EBP";
               // displayEngagementRatings();
            }
            /*------------------*/
        }

        }
       //displayCaseWorkers(lngPkID);
       // displayCaseWorkerList(lngPkID);
        displayCaseMembers(lngPkID);
        displayCaseWorkerl(lngPkID);

        if (oUI.intUserRoleID ==3)
        {
            btnSave.Visible = true;
            txtEbpenrolldate.Enabled = true;
            cboEBP.Enabled = true;
        }
    }


    /// Participnt Engagemnet Rating 04/03/2014
    protected void displayEngagementRatings()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {  
         


            var oPages = from pg in db.F_EBP_ParticipantRatings
                         where pg.CaseEBPID_FK == lngPkID && pg.ServiceLogID_FK==null
                         select pg;
            
            foreach (var oCase in oPages)
            {
                objVal = oCase.ER_ID_FK;
                if (objVal != null)
                {
                    chkERatings.SelectedIndex = chkERatings.Items.IndexOf(chkERatings.Items.FindByValue(oCase.ER_ID_FK.ToString()));
                }
            }

        }
    }   
    


    //-----Update------------------------------//
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            F_ENROLLMENT_EBP oCase = (from c in db.F_ENROLLMENT_EBPs where c.CaseEBPID == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_ENROLLMENT_EBP();
                blNew = true;
                oCase.CaseID_FK = intCaseID;
            }

            if (!(cboEBP.SelectedItem == null))
            {
                if (!(cboEBP.SelectedItem.Value.ToString() == ""))
                {
                    oCase.EBPID_FK = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.EBPID_FK = null;
                }
            }

            DateTime.TryParse(txtEbpenrolldate.Text, out dtTmp);
            if (txtEbpenrolldate.Text != "")
                oCase.EBP_ENROLL_DATE = dtTmp;

            DateTime.TryParse(txtEbpexitdate.Text, out dtTmp);
            if (txtEbpexitdate.Text != "")
            {
                oCase.EBP_EXIT_DATE = dtTmp;
                oCase.EBP_ENROLL_COMPLETED = true;
            }

            if (blNew == true)
            {
                db.F_ENROLLMENT_EBPs.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();
            lngPkID = oCase.CaseEBPID;

            saveCaseMembers();
            saveCaseWorker1();

            /*PE 04/03/2014*/
            if (txtEbpexitdate.Text != "")
            {
                updateEngagementRating();
                updateCaseWorker1(dtTmp);
            }
            /*--------*/
            // saveCaseWorkers();

        }
        //LitJS.Text = " showSuccessToast();";
        // litMessage.Text = "Record saved! ID=" + lngPkID;

    } 
  
    public void updateEngagementRating()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBP_ParticipantRating oCase = (from c in db.F_EBP_ParticipantRatings where c.ServiceLogID_FK == null && c.CaseEBPID_FK == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBP_ParticipantRating();
                blNew = true;
                oCase.CaseEBPID_FK = lngPkID;
               // oCase.ServiceLogID_FK = lngPkID; //not needed lets you identify where it is comng from, SL or EBP
            }

            if (!(chkERatings.SelectedItem == null))
            {
                if (!(chkERatings.SelectedItem.Value.ToString() == ""))
                {
                    oCase.ER_ID_FK = Convert.ToInt32(chkERatings.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.ER_ID_FK = null;
                }
            }
            oCase.updatedBy = HttpContext.Current.User.Identity.Name;
            oCase.updatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.F_EBP_ParticipantRatings.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
            //lngPkID = oCase.EBP_Part_ID;

        }

    }
  

    //-----ViewState-----------------------------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }
        if (((this.ViewState["intGranteeID"] != null)))
        {
            intGranteeID = Convert.ToInt32(this.ViewState["intGranteeID"]);
        }

        if (((this.ViewState["GranteeContactid"] != null)))
        {
            GranteeContactid = Convert.ToInt32(this.ViewState["GranteeContactid"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        if (((this.ViewState["blClose"] != null)))
        {
            blClose = Convert.ToBoolean(this.ViewState["blClose"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["intGranteeID"] = intGranteeID;
        this.ViewState["blClose"] = blClose;
        this.ViewState["GranteeContactid"] = GranteeContactid;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }
   

    //-----Save/Close/SaveClose/------------------------------//
    protected void btnSave_Click(object sender, EventArgs e)
    {
        rfvCaseWorkerStartDt.Visible = false;
        /*----check Case Worker Assigned----*/
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                        F_EBPCaseWorkers_AssignedList oPages = (from pg in db.F_EBPCaseWorkers_AssignedLists
                                     where pg.EBPCaseWorker_Assigned != false && pg.EBPEnrollID_FK == lngPkID 
                                                  select pg).FirstOrDefault();
                        if ((oPages == null))
                        {
                            rfvCaseWorkerStartDt.Visible = true;
                            lblAddCwMessage.Text = "Add a caseworker and start date prior to enrolling a case in an EBP";
                        }
                        else
                        {           
                             if(!(oPages.CaseWorker_StartDate==null) )
                                {
                                    if ((blClose == true ) && (txtEbpexitdate.Text != ""))
                                      {
                                           btnAddCsaeWorker.Visible = false;
                                           litEBPMeassage.Text = "";                                       
                                           updateUser();                        
                                           btnEBPClose.Enabled = true;
                                           displayRecords();
                                           Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                                       }
                                     else
                                        {
                                             //--added for Sup Admin Role=3----//
                                            //if (oUI.intUserRoleID == 3)
                                            {
                                                //var oCase= from pg in db.F_ENROLLMENT_EBPs
                                                //                        where pg.CaseEBPID==lngPkID
                                                //                        select pg;
                                                updateUser();
                                                btnEBPClose.Enabled = true;
                                                displayRecords();
                                                Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                                            }
                                            //else
                                            //{

                                            //    litEBPMeassage.Text = "Required EBP Exit Date";
                                            //}
        
                                        }
                                          lblAddCwMessage.Text = "";
                                  }
                               else
                                  {
                                      lblAddCwMessage.Text = "Add a caseworker and start date prior to enrolling a case in an EBP"; 
                                      
                                  }
                          }
                   }      
        /*----------------------------------------------------------------------*/
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("EBPList.aspx?intCaseID="+ intCaseID );
    }
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        lblSelectCWMesage.Text = "";
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPCaseWorkers_AssignedList oPages = (from pg in db.F_EBPCaseWorkers_AssignedLists
                                      where pg.EBPCaseWorker_Assigned != false && pg.EBPEnrollID_FK == lngPkID
                                      select pg).FirstOrDefault();

            if ((oPages == null))
            {
                lblAddCwMessage.Text = "Add a caseworker and start date prior to enrolling a case in an EBP";
            }
            else
            {
                if (!(oPages.CaseWorker_StartDate == null))
                {
                    if ((blClose == true) && (txtEbpexitdate.Text != "") || (blClose == false))
                    {
                        litEBPMeassage.Text = "";
                        updateUser();
                        Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                    }
                    else 
                    {
                        litEBPMeassage.Text = "Input EBP Exit Date";
                    }
                    lblAddCwMessage.Text = "";
                }
                else
                {
                    lblAddCwMessage.Text = "Add a caseworker and start date prior to enrolling a case in an EBP";
                }
            }

        }      
       

    }
    

    //-----Case Members-----------------------------------//
    /// <summary>
    /// Case member operations
    /// </summary>
    /// 
    private void displayCaseMembers(int intCaseEBPID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.F_EBPEnrollments
                         where pg.EBPID_FK == intCaseEBPID
                         select pg;
            foreach (var oCase in oPages)
            {
                for (int i = 0; i < chkCaseMembers.Items.Count; i++)
                {
                    if (Convert.ToInt32(chkCaseMembers.Items[i].Value) == oCase.IndividualID_FK)
                    {
                        chkCaseMembers.Items[i].Selected = true;

                        if (oUI.intUserRoleID == 3)
                        {
                            chkCaseMembers.Items[i].Enabled = true;
                            cboEBP.Enabled = true;
                        }
                        else
                        {
                            chkCaseMembers.Items[i].Enabled = false;
                        }
                    }
                    if (oUI.intUserRoleID == 3)
                    {
                        chkCaseMembers.Items[i].Enabled = true;                       
                    }
                    else
                    {
                        chkCaseMembers.Items[i].Enabled = false;
                    }
                }

            }

           
        }

    }
    protected void loadCaseMembers()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs where p.CaseNoId_FK==intCaseID select p;
            chkCaseMembers.DataSource = qry;
            chkCaseMembers.DataTextField = "FIRST_NAME";
            chkCaseMembers.DataValueField = "IndividualID";
            chkCaseMembers.DataBind();
        }
    }
    protected void saveCaseMembers()
    {
        for (int i = 0; i < chkCaseMembers.Items.Count; i++)
        {
            if (chkCaseMembers.Items[i].Selected==true )
            {
                updateCaseMembers(Convert.ToInt32  ( chkCaseMembers.Items[i].Value));
            }
            else
            {
                removeCaseMembers(Convert.ToInt32  ( chkCaseMembers.Items[i].Value));
            }
        }
    }
    protected void updateCaseMembers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew=false;
            F_EBPEnrollment oCase = (from c in db.F_EBPEnrollments where c.IndividualID_FK == IndividualID
                                         && c.EBPID_FK ==lngPkID   select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBPEnrollment();
                blNew = true;
                oCase.EBPID_FK = lngPkID;
                oCase.IndividualID_FK = IndividualID;
            }

            if (blNew == true)
            {
                db.F_EBPEnrollments.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();
             
        }
    }
    protected void removeCaseMembers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew=false;
            F_EBPEnrollment oCase = (from c in db.F_EBPEnrollments where c.IndividualID_FK == IndividualID
                                         && c.EBPID_FK ==lngPkID   select c).FirstOrDefault();
            if ((oCase != null))
            {
                db.F_EBPEnrollments.DeleteOnSubmit(oCase);
            }
            db.SubmitChanges();
             
        }
    }

   

    //--- Case Workers In EBP---------------------------//   
    private void displayCaseWorkerl(int intEBPEnrollID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.F_EBPCaseWorkers_AssignedLists
                         join G in db.A_Grantee_Contacts
                         on pg.GranteeContactId_FK equals G.GranteeContactId
                         where pg.EBPEnrollID_FK == intEBPEnrollID
                         select new { G.GranteeContactId, CaseWorkerName = G.FirstName + " " + G.LastName, G.Phone, G.Email, pg.EBPAssignedCaseWorkerID, pg.CaseWorker_StartDate, pg.CaseWorker_ExitDate };

            grdCaseWorkerList.DataSource = oPages;
            grdCaseWorkerList.DataBind();
        }

    }
    protected void loadCaseWorkers()
    {

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from gc in db.A_Grantee_Contacts
                      where gc.GranteeId_fk == intGranteeID && gc.isActive
                      select new { gc.GranteeContactId, CaseWorkerName = gc.FirstName + " " + gc.LastName };
            ddlCaseWorkers.DataSource = qry;
            ddlCaseWorkers.DataTextField = "CaseWorkerName";
            ddlCaseWorkers.DataValueField = "GranteeContactId";
            ddlCaseWorkers.DataBind();
            ddlCaseWorkers.Items.Insert(0, "Select");

        }

    }
    private void saveCaseWorker1()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            DateTime dtTmp1;

            if (!(ddlCaseWorkers.SelectedItem.Value == "Select"))
            {
                GranteeContactid = Convert.ToInt32(ddlCaseWorkers.SelectedValue);

                //-------------updated on 18th May

                //F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
                //                         where c.GranteeContactId_FK == GranteeContactid
                //                         && c.EBPEnrollID_FK == lngPkID
                //   
                //---------------------------

                F_EBPCaseWorkers_AssignedList oCase = (from c in db.F_EBPCaseWorkers_AssignedLists
                                                       where c.EBPAssignedCaseWorkerID == 0
                                                       select c).FirstOrDefault();
                if ((oCase == null))
                {
                    oCase = new F_EBPCaseWorkers_AssignedList();
                    blNew = true;
                    oCase.EBPEnrollID_FK = lngPkID;
                    oCase.GranteeContactId_FK = GranteeContactid;
                }

                DateTime.TryParse(txtCaseWorkerStartDt.Text, out dtTmp1);

                if (txtCaseWorkerStartDt.Text != "")
                {
                    oCase.CaseWorker_StartDate = dtTmp1;
                    oCase.EBPCaseWorker_Assigned = true;
                }

                DateTime.TryParse(txtCaseWorkerExitDt.Text, out dtTmp1);

                if (txtCaseWorkerExitDt.Text != "")
                    oCase.CaseWorker_ExitDate = dtTmp1;
                // added on 18th may

                oCase.SubmitedBy = HttpContext.Current.User.Identity.Name;
                oCase.SubmitedDate = DateTime.Now;


                if (blNew == true)
                {
                    db.F_EBPCaseWorkers_AssignedLists.InsertOnSubmit(oCase);
                }

                db.SubmitChanges();

                //  displayCaseWorkerl(lngPkID);
                ClearDates();
            }
            else if (ddlCaseWorkers.SelectedItem.Value == "Select")
            {
                //lblSelectCWMesage.Text = "Please select the Caseworker";
                F_EBPCaseWorkers_AssignedList oPages = (from pg in db.F_EBPCaseWorkers_AssignedLists
                                                        where pg.EBPCaseWorker_Assigned != false && pg.EBPEnrollID_FK == lngPkID
                                                        select pg).FirstOrDefault();
                if (!(oPages == null))
                {
                    if (!(oPages.GranteeContactId_FK == null))
                    {
                        GranteeContactid = Convert.ToInt32(oPages.GranteeContactId_FK);
                        if (!(String.IsNullOrEmpty(txtCaseWorkerStartDt.Text)))
                        {
                            lblSelectCWMesage.Text = "Please select the Caseworker";
                        }
                    }
                    else
                    {

                    }
                }
            }


        }

    }
    private void updateCaseWorker1(DateTime dtTmp)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oCase = from c in db.F_EBPCaseWorkers_AssignedLists
                        where c.EBPEnrollID_FK == lngPkID
                        select c;

            foreach (var cw in oCase)
            {
                if (!(cw.CaseWorker_ExitDate == null))
                {

                    if (cw.CaseWorker_ExitDate > dtTmp)
                    {
                        cw.CaseWorker_ExitDate = dtTmp;
                    }
                }
                else
                {
                    cw.CaseWorker_ExitDate = dtTmp;

                }

                if (!(cw.CaseWorker_StartDate == null))
                {
                    if (cw.CaseWorker_StartDate > dtTmp)
                    {
                        //  txtCmpStartDt.Text = "";
                    }
                }
            }

            db.SubmitChanges();

            displayCaseWorkerl(lngPkID);
            ClearDates();



        }
    }   
    protected void removeCaseWorkers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            F_EBPCaseWorkers_AssignedList oCase = (from c in db.F_EBPCaseWorkers_AssignedLists
                                     where c.GranteeContactId_FK == IndividualID
                                         && c.EBPEnrollID_FK == lngPkID
                                     select c).FirstOrDefault();
            if ((oCase != null))
            {
                db.F_EBPCaseWorkers_AssignedLists.DeleteOnSubmit(oCase);
            }
            db.SubmitChanges();

        }
    }
    protected void btnAddCsaeWorker_Click(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(Session["IsPageRefresh"]))
            return;

        updateUser();
        displayRecords();
        lblAddCwMessage.Text = "";
       

    }
    private void ClearDates()
    {

        lblSelectCWMesage.Text = "";
        ddlCaseWorkers.SelectedIndex = 0;
        txtCaseWorkerStartDt.Text = "";
        txtCaseWorkerExitDt.Text = "";
    }
    protected void grdCaseWorkerList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RemoveCaseWorker")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                Boolean blNew = false;
                F_EBPCaseWorkers_AssignedList oCase = (from c in db.F_EBPCaseWorkers_AssignedLists
                                                       where c.EBPAssignedCaseWorkerID == intResponseID
                                         select c).FirstOrDefault();
                if ((oCase != null))
                {
                    db.F_EBPCaseWorkers_AssignedLists.DeleteOnSubmit(oCase);
                }
                db.SubmitChanges();
            }
            displayCaseWorkerl(lngPkID);
           
        }

        if (e.CommandName == "View")
        {
            int EBPAssignedCaseWorkerID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("CaseWorker.aspx?EBPAssignedCaseWorkerID=" + EBPAssignedCaseWorkerID + "&lngPkID=" + lngPkID + "&intCaseID=" + intCaseID);
        }
        displayCaseWorkerl(lngPkID);
    }
    protected bool NeedPE()
    {
        bool retVal = false;
        return retVal;
    }
    protected void grdCaseWorkerList_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if ((oUI.intUserRoleID == 1) || (oUI.intUserRoleID == 4))
        {
            e.Row.Cells[7].Visible = false;
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                object objValue;
                var oPages = from pg in db.F_ENROLLMENT_EBPs
                             where pg.CaseEBPID == lngPkID
                             select pg;
                if (oPages == null)
                {
                    e.Row.Cells[6].Visible = true;
                }

                foreach (var oCase in oPages)
                {
                    objValue = oCase.EBP_EXIT_DATE;
                    if (objValue != null)
                    {
                        e.Row.Cells[6].Visible = false;

                    }
                    else
                    {
                        e.Row.Cells[6].Visible = true;
                    }

                }
            }
        }
        if (oUI.intUserRoleID == 2)
        {
            e.Row.Cells[7].Visible = false;
            divAssignCaseworker.Visible = false;
            e.Row.Cells[6].Visible = false;
        }




    }
    protected void ddlCaseWorkers_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCaseWorkers.SelectedItem.Value == "Select")
        {
            rfvCaseWorkerStartDt.Visible = false;
            txtCaseWorkerStartDt.Text = "";
            txtCaseWorkerExitDt.Text = "";
        }
        else
        {
            rfvCaseWorkerStartDt.Visible = true;
        }
    }
   
  
    //-----EBP Exit Date----------------------------------//
    protected void btnEBPClose_Click(object sender, EventArgs e)
    {
        litEBPMeassage.Text = "";
        if (btnEBPClose.Text != "Cancel EBP Close")
        {
            PEngQuestions();
            chkERatings.SelectedIndex = -1;
            divEngagementRating.Visible = true;
           
            blClose = true;
            btnEBPClose.Text = "Cancel EBP Close";           
        }
        else
        {
            chkERatings.SelectedIndex = -1;
            PEngQuestions();
            btnAddCsaeWorker.Visible = true;
            removeExitDateAndEnggRating();
            displayCaseWorkerl(lngPkID);
            divEngagementRating.Visible = false ;
            btnSave.Visible = true;
           // btnSaveClose.Visible = true;
            blClose = false ;
            btnEBPClose.Text = "Exit Case from EBP";
           
        }
    }
    private void PEngQuestions()
    {
     //--------to hide the Participant Engagement rating questions April 8th-------------
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (!(cboEBP.SelectedItem.Value.ToString() == ""))
            {
                int ebpid = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                var qry4 = from pg in db.A_GranteeEBPs                          
                           where   pg.GranteeID_FK == intGranteeID && pg.EBPID_FK == ebpid
                           select new { pg.FocalEBPYN,pg.EBPID_FK };

                foreach (var c in qry4)
                {
                    if (c.FocalEBPYN == false)
                    {
                        PEngFocalEBP.Visible = false;
                    }
                    else
                    {
                        PEngFocalEBP.Visible = true;
                    }
                }
            }
        }

      //-------------------------------------------------------

    }
    private void removeExitDateAndEnggRating()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;

        
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_ENROLLMENT_EBP oCase = (from c in db.F_ENROLLMENT_EBPs where c.CaseEBPID == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_ENROLLMENT_EBP();
                blNew = true;
                oCase.CaseID_FK = intCaseID;
            }

            if (!(cboEBP.SelectedItem == null))
            {
                if (!(cboEBP.SelectedItem.Value.ToString() == ""))
                {
                    oCase.EBPID_FK = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.EBPID_FK = null;
                }
            }

            DateTime.TryParse(txtEbpenrolldate.Text, out dtTmp);
            if (txtEbpenrolldate.Text != "")
                oCase.EBP_ENROLL_DATE = dtTmp;

            txtEbpexitdate.Text = "";
            DateTime.TryParse(txtEbpexitdate.Text, out dtTmp);
            if (txtEbpexitdate.Text == "")
            {
                oCase.EBP_EXIT_DATE = null;
                oCase.EBP_ENROLL_COMPLETED = null;
            }

            if (blNew == true)
            {
                db.F_ENROLLMENT_EBPs.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();
            lngPkID = oCase.CaseEBPID;
           
            if (txtEbpexitdate.Text == "")
            {
                RemoveEngagementRating();
            }

        }
       
    }
    private void RemoveEngagementRating()
    {

      
            bool blNew = false;
            int strTmp;
            DateTime dtTmp;

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                              
                F_EBP_ParticipantRating oCase = (from c in db.F_EBP_ParticipantRatings
                                                 where c.CaseEBPID_FK == lngPkID && c.ServiceLogID_FK == null  
                                                 select c).FirstOrDefault();

                if ((oCase != null))
                {
                    db.F_EBP_ParticipantRatings.DeleteOnSubmit(oCase);
                }
                db.SubmitChanges();
            }

        }
    //----------------------------------------------------//
    public bool isCaseworkerinUse(string caseworkemail)
    {
        bool showRmbutton = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var srvlog = from rcd in db.G_SERVICE_LOGs
                         where rcd.CaseworkerEmailID == caseworkemail
                         select rcd;
            if (srvlog != null && srvlog.Count() == 0)
                showRmbutton = true;
        }

        return showRmbutton;
    }

    public string getButtonName(int id)
    {
        string name = "View/Edit";
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase = (from pg in db.F_EBPCaseWorkers_AssignedLists
                         where pg.EBPAssignedCaseWorkerID == id
                         select pg).SingleOrDefault();
            if (oCase !=null)
            {
                if (oCase.CaseWorker_ExitDate != null && !string.IsNullOrEmpty(oCase.CaseWorker_ExitDate.ToString()))
                    name = "View";
            }

        }

        return name;
    }
}
