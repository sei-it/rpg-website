﻿<%@ Page Title=""  Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ServiceLogEdit.aspx.cs" Inherits="ServiceLogEdit"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">


    
          <script type="text/javascript" src="jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="jquery-ui-1.8.2.custom.min.js"></script>
   
    <script type="text/javascript">
    
        String.Format = function()
        {
            var s = arguments[0];
            for (var i = 0; i < arguments.length - 1; i++)
            {
                var reg = new RegExp("\\{" + i + "\\}", "gm");
                s = s.replace(reg, arguments[i + 1]);
            }
            return s;
        }

        var dialogConfirmed = false;
        function ConfirmDialog(obj, dialogText11, title) {
            var dialogText = "Closing the service log without saving the record means any entered data will be lost. If you would like to save this record, you must click SAVE. Would you still like to close this service log  ";
            var italictxt = "without";
            dialogText += italictxt.italics() + " saving the data?";
            if (!dialogConfirmed) {
                $('body').append(String.Format("<div id='dialog' title='{0}'><p>{1}</p></div>",
                    title, dialogText));



                $('#dialog').dialog
                ({
                    height: 250,
                    width: 800,
                    modal: true,
                    resizable: false,
                    draggable: false,
                    close: function (event, ui) { $('body').find('#dialog').remove(); },
                    buttons:
                    {
                        'Yes': function () {
                            $(this).dialog('close');
                            dialogConfirmed = true;
                            if (obj) obj.click();
                        },
                        'No': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }

            return dialogConfirmed;
        }
    

                 $(function () {
                     $("#<%=txtDateofservice.ClientID%>").datepicker({
                         dateFormat: 'mm/dd/yy',
                         minDate: '01/01/2014',
                         maxDate: 'dateToday'
                     })
                
                 });


              function ValidateCaseMembersCheckBoxList(sender, args) {
                  var checkBoxList = document.getElementById("<%=chkCaseMembers.ClientID %>");
                  var checkboxes = checkBoxList.getElementsByTagName("input");
                  var isValid = false;
                  for (var i = 0; i < checkboxes.length; i++) {
                      if (checkboxes[i].checked) {
                          isValid = true;
                          break;
                      }
                  }
                  args.IsValid = isValid;
              }


        //window.onbeforeunload = confirmExit;
        //function confirmExit() {
        //    return "your changes will be lost if not saved.  Are you sure you want to exit this   page?";
        //}

//        var g_isPostBack = false;

//        function windowOnBeforeUnload() {
//            if (g_isPostBack == true)
//                return; // Let the page unload

//            if (window.event)
//                window.event.returnValue = 'Navigating away from the service log without saving the record means any entered data will be lost. If you would like to save this record, you must click SAVE. Would you still like to navigate away from this service log without saving data?'; // IE
//            else
//                return 'Navigating away from the service log without saving the record means any entered data will be lost. If you would like to save this record, you must click SAVE. Would you still like to navigate away from this service log without saving data?'; // FX
//        }

//        window.onbeforeunload = windowOnBeforeUnload;


            
</script>
  
  


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <h1>EBP Service Log</h1>
    <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal>


    </h2>
    <h2>EBP: <asp:Literal ID="litEBP" runat="server"></asp:Literal></h2>
    <p style="color:red">All data for a service log must be entered at one time. Closing the service log without saving the record means any entered data will be lost. Please make sure you have all necessary data ready for entry prior to completing the service log.</p>
     <div class="formLayout">
         <p>
              &nbsp;<asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Save" runat="server" ForeColor="Red"  HeaderText="Please fill in all the required fields." />
         </p>
         <p>             
		        <label for="txtDateofservice">Date of Service
 	            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Save" ControlToValidate="txtDateofservice" ErrorMessage="Date of Service" ForeColor="Red">Required </asp:RequiredFieldValidator>
 	            </label>
		        &nbsp;<asp:textbox id="txtDateofservice" runat="server" AutoPostBack="true" ></asp:textbox>
            
                 <asp:CompareValidator ID="cvDateFService" runat="server"
        ControlToValidate="txtDateofservice"  ErrorMessage="Date of Service must be greater than or equal to 01/01/2014"           
        Operator="GreaterThanEqual" ValidationGroup="Save" SetFocusOnError="True"
         ForeColor="Red" Display="Dynamic"  ValueToCompare="01/01/2014" Type="Date" >*
       </asp:CompareValidator>
       <asp:CompareValidator ID="cvMaxDateFService" runat="server"  ValidationGroup="Save" SetFocusOnError="True" ErrorMessage="Date of Service must be less than or equal to current date"
       Operator="LessThanEqual" ControlToValidate="txtDateofservice" ForeColor="Red" Type="date" Display="Dynamic" >*</asp:CompareValidator>
         <asp:CompareValidator ID="cvDateOfServices" runat="server"  ValidationGroup="Save" SetFocusOnError="True" ErrorMessage="Date of Service must be greater than or equal to EBP Enrollment Date"
       Operator="GreaterThanEqual" ControlToValidate="txtDateofservice" ForeColor="Red" Type="date" Display="Dynamic" >*</asp:CompareValidator>
            <asp:textbox id="txtDtCmp" runat="server" visible="false"></asp:textbox>
 	        </p>
         <p>    
                <label for="ddlCaseWorkers">Caseworker
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Save" InitialValue=""  ControlToValidate="ddlCaseWorkers" ErrorMessage="Caseworker" ForeColor="Red">Required </asp:RequiredFieldValidator>
                </label>
                <asp:DropDownList ID="ddlCaseWorkers" runat="server" ValidationGroup="Save" >
                <asp:ListItem Value="">First select a Date of Service above</asp:ListItem>
                </asp:DropDownList>
                         
        </p>
	     <p style="display:none;">
		        <label for="cboEBP">Which EBP did you use during this session?</label>
		        <asp:DropDownList id="cboEBP" runat="server"  RepeatDirection="Horizontal"  > 
		        </asp:DropDownList>
	        </p>

	        <p>
		        <label for="rblSessionlocation">Where were these services provided? 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Save" ErrorMessage="Where were these services provided?" ControlToValidate="rblSessionlocation" ForeColor="Red">Required</asp:RequiredFieldValidator>
	            </label>
                
		        &nbsp;<asp:RadioButtonList id="rblSessionlocation" runat="server" AutoPostBack="true"  RepeatDirection="Vertical"  OnSelectedIndexChanged="rblSessionlocation_SelectedIndexChanged" > 
		        </asp:RadioButtonList>
	        </p>
         <p>
		        <label for="txtSpecifSessionlocation">If services provided in other location, please specify</label>
		        <asp:textbox id="txtSpecifSessionlocation" runat="server" TextMode="MultiLine" ></asp:textbox>
              <asp:RequiredFieldValidator ID="rfvtxtSpecifSessionlocation" runat="server" Enabled="false" ValidationGroup="Save"
              ControlToValidate="txtSpecifSessionlocation" ForeColor="Red" ErrorMessage="Where these services provided if other">Required</asp:RequiredFieldValidator>
        

                 <asp:RegularExpressionValidator ID="revSessOther" runat="server" ControlToValidate="txtSpecifSessionlocation"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed.
                    </asp:RegularExpressionValidator>
	        </p>
         <br />
	        <p>
		        <label for="txtSessionminutes">How many minutes did this session last?
 <asp:RequiredFieldValidator ID="rfvSess" runat="server" ValidationGroup="Save"
              ControlToValidate="txtSessionminutes" ForeColor="Red" ErrorMessage="How many minutes did this session last?">Required</asp:RequiredFieldValidator>
        
                    
		        </label>
		        <asp:textbox id="txtSessionminutes" runat="server" ></asp:textbox> minutes 
                 <asp:RangeValidator ID="rangevSessionmin"  ValidationGroup="Save" runat="server" 
                        ForeColor="Red" ErrorMessage="Session minutes must be greater than 0 and<br/> less than 1000" ControlToValidate="txtSessionminutes" 
                        MaximumValue="999" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
	     
                  </p>
         </div>
    <h3>Individuals Present During Session</h3>
          <div  class="formLayout" >
    	    <p><label for="chkCaseMembers">Which case members were present during this session?<br /> (Mark all that apply)
                <asp:CustomValidator ID="cvalCaseMember" ErrorMessage="Please select at least one case member."
                ForeColor="Red" ClientValidationFunction="ValidateCaseMembersCheckBoxList" runat="server" ValidationGroup="Save">Required</asp:CustomValidator>
	          

    	       </label>
            
              
                        &nbsp;<asp:CheckBoxList ID="chkCaseMembers" runat="server"></asp:CheckBoxList>
	        </p>
          </div>
             <p>Which other individuals were present during this session?<br />(Mark all that apply)</p>
          <div  class="formLayout" >
	        <p>
		        <label for="chkOtherspresentfostprntguardyn"></label>
		        <asp:checkbox id="chkOtherspresentfostprntguardyn" runat="server"   Text="Foster parent/guardian"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresentinterpreteryn"></label>
		        <asp:checkbox id="chkOtherspresentinterpreteryn" runat="server"   Text="Interpreter"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresentgrantstaffmembyn"></label>
		        <asp:checkbox id="chkOtherspresentgrantstaffmembyn" runat="server"   Text="Other grantee staff member"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresentothrelatyn"></label>
		        <asp:checkbox id="chkOtherspresentothrelatyn" runat="server"   Text="Other relative of case member(s)"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresentrpgpartstaffyn"></label>
		        <asp:checkbox id="chkOtherspresentrpgpartstaffyn" runat="server"   Text="RPG partner staff"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresentfidobsyn"></label>
		        <asp:checkbox id="chkOtherspresentfidobsyn" runat="server"   Text="Staff conducting fidelity observation"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresenthlthprofyn"></label>
		        <asp:checkbox id="chkOtherspresenthlthprofyn" runat="server"   Text="Health professional (nurse, early interventionist/Part C staff) "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresentsupervisyn"></label>
		        <asp:checkbox id="chkOtherspresentsupervisyn" runat="server"   Text="Supervisor"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkOtherspresentothprofstaffyn"></label>
		        <asp:checkbox id="chkOtherspresentothprofstaffyn" runat="server" AutoPostBack="true" OnCheckedChanged="chkOtherspresentothprofstaffyn_CheckedChanged"   Text="Other"></asp:checkbox>
	        </p>
             <p>

        <label for="txtOtherspresentothprofstaffyn">If others were present, please specify</label>
		<asp:textbox id="txtOtherspresentothprofstaffyn" runat="server" TextMode="MultiLine" ></asp:textbox>
                  <asp:RequiredFieldValidator ID="rfvtxtOtherspresentothprofstaffyn" runat="server" Enabled="false" ValidationGroup="Save"
              ControlToValidate="txtOtherspresentothprofstaffyn" ForeColor="Red" ErrorMessage="If others were present, please specify">Required</asp:RequiredFieldValidator>
        
        <asp:RegularExpressionValidator ID="regOtherspresentothprofstaffyn" runat="server" ControlToValidate="txtOtherspresentothprofstaffyn"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed.
                    </asp:RegularExpressionValidator>
    
    </p>
        </div>
    <h3>Session Activities</h3>

        <div class="formLayout">
            <p> Which activities occurred during the session? 
                <br />
                (Mark all that apply) 
                <asp:Label ID="lblMessSessionActivity" runat="server" ForeColor="Red"></asp:Label>
            </p>
	        <p>
		        <label for="chkSessionactivitiesgrpdiscyn"></label>
		        <asp:checkbox id="chkSessionactivitiesgrpdiscyn" runat="server"   Text="Group discussion "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesinddiscyn"></label>
		        <asp:checkbox id="chkSessionactivitiesinddiscyn" runat="server"   Text="One-on-one discussion"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesfamactinter"></label>
		        <asp:checkbox id="chkSessionactivitiesfamactinter" runat="server"   Text=" Case activity/interaction"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesfammtng"></label>
		        <asp:checkbox id="chkSessionactivitiesfammtng" runat="server"   Text="Family meeting "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesroleplay"></label>
		        <asp:checkbox id="chkSessionactivitiesroleplay" runat="server"   Text="Role playing "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesreenact"></label>
		        <asp:checkbox id="chkSessionactivitiesreenact" runat="server"   Text="Re-enactments"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesexpact"></label>
		        <asp:checkbox id="chkSessionactivitiesexpact" runat="server"   Text="Exposure to trauma-related triggers "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesgames"></label>
		        <asp:checkbox id="chkSessionactivitiesgames" runat="server"   Text="Games/play"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesworksheet"></label>
		        <asp:checkbox id="chkSessionactivitiesworksheet" runat="server"   Text="Worksheets "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitieswatchvid"></label>
		        <asp:checkbox id="chkSessionactivitieswatchvid" runat="server"   Text="Watching videos "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesgoalset"></label>
		        <asp:checkbox id="chkSessionactivitiesgoalset" runat="server"   Text="Goal setting/planning "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesguidprac"></label>
		        <asp:checkbox id="chkSessionactivitiesguidprac" runat="server"   Text="Guided practice "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiescoachfeed"></label>
		        <asp:checkbox id="chkSessionactivitiescoachfeed" runat="server"   Text=" Coaching/feedback"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesprovemotsupp"></label>
		        <asp:checkbox id="chkSessionactivitiesprovemotsupp" runat="server"   Text="Provision of emotional support "></asp:checkbox>
	        </p>
	        <p style="display:none;">
		        <label for="chkSessionactivitiesprobsolv"></label>
		        <asp:checkbox id="chkSessionactivitiesprobsolv" runat="server"   Text="Problem solving "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiescrisinter"></label>
		        <asp:checkbox id="chkSessionactivitiescrisinter" runat="server"   Text="Crisis intervention"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesprntskillscr"></label>
		        <asp:checkbox id="chkSessionactivitiesprntskillscr" runat="server"   Text="Parenting skills screening "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitieschilddevscr"></label>
		        <asp:checkbox id="chkSessionactivitieschilddevscr" runat="server"   Text="Child development screening "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitieshlthass"></label>
		        <asp:checkbox id="chkSessionactivitieshlthass" runat="server"   Text="Health assessment"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkSessionactivitiesmenthlthsubabuse"></label>
		        <asp:checkbox id="chkSessionactivitiesmenthlthsubabuse" runat="server"   Text="Mental health/substance use disorder screening "></asp:checkbox>
	        </p>
            <p>
                <label for="chkSessionactivitieother"></label>
		        <asp:checkbox id="chkSessionactivitieother" runat="server" AutoPostBack="true" OnCheckedChanged="chkSessionactivitieother_CheckedChanged"   Text="Other"></asp:checkbox>
            
            </p>
            <p>
                    <label for="txtSessionactivitiespecifyother">If other, please specify</label>
                    <asp:textbox id="txtSessionactivitiespecifyother" runat="server" TextMode="MultiLine" ></asp:textbox>

                <asp:RequiredFieldValidator ID="rfvtxtSessionactivitiespecifyother" runat="server"  ValidationGroup="Save" Enabled="false"
              ControlToValidate="txtSessionactivitiespecifyother" ForeColor="Red" ErrorMessage="If services provided in other location, please specify">Required</asp:RequiredFieldValidator>
        
                    <asp:RegularExpressionValidator ID="regtSessionactivitiespecifyother" runat="server" ControlToValidate="txtSessionactivitiespecifyother"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed.
                    </asp:RegularExpressionValidator>

            
            </p>
           <%--  <p><label for="">Did you make any referrals to other services during the course of service provision?</label>
                 <asp:RadioButtonList id="rblReferralsServiceProvision" runat="server"  RepeatDirection="vertical">
                   <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                    <asp:ListItem Text="No" Value="2"></asp:ListItem>

                 </asp:RadioButtonList>

             </p>--%>
            </div>
            <h3>Session Alignment</h3>
            <div class="formLayout">
               
	        <p>
		        <label for="rblSessionalignmentfk">How well-aligned was this session with what you planned to accomplish?
		        <asp:RequiredFieldValidator ID="rfvSessionAlig" runat="server" ValidationGroup="Save" ErrorMessage="How well-aligned was this session?" ControlToValidate="rblSessionalignmentfk" ForeColor="Red">Required</asp:RequiredFieldValidator>
	            </label>
                 <asp:RadioButtonList id="rblSessionalignmentfk" runat="server"  RepeatDirection="vertical" AutoPostBack="true" OnSelectedIndexChanged="rblSessionalignmentfk_SelectedIndexChanged"  > 
                   <asp:ListItem Text="(3) Very well aligned" Value="1"></asp:ListItem>
                    <asp:ListItem Text="(2) Somewhat aligned" Value="2"></asp:ListItem>
                     <asp:ListItem Text="(1) Not well aligned" Value="3"></asp:ListItem>   
		        </asp:RadioButtonList>
                
	        </p>
                <p><label for="">If only somewhat or not well aligned, why was the session not very well aligned?<br /> (Mark all that apply)
	       
                <asp:Label ID="lblSessionMessage" runat="server" ForeColor="Red" ></asp:Label></label></p>
                <p>
		        <label for="chkReasonnotalignfamcris"></label>
		        <asp:checkbox id="chkReasonnotalignfamcris" runat="server"   Text="Crisis among case members"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkReasonnotalignpartnotengage"></label>
		        <asp:checkbox id="chkReasonnotalignpartnotengage" runat="server"   Text="Participants not engaged in activity "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkReasonnotalignpartinterothtop"></label>
		        <asp:checkbox id="chkReasonnotalignpartinterothtop" runat="server"   Text="Participants interested in topic other than one planned "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkReasonnotalignpresothindinhibsess"></label>
		        <asp:checkbox id="chkReasonnotalignpresothindinhibsess" runat="server"   Text="Presence of other individuals inhibited session activities"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkReasonnotalignpartsick"></label>
		        <asp:checkbox id="chkReasonnotalignpartsick" runat="server"   Text="Participant(s) were sick "></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkReasonnotalignphyscconst"></label>
		        <asp:checkbox id="chkReasonnotalignphyscconst" runat="server"   Text="Physical space constraints"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkReasonnotalignother"></label>
		        <asp:checkbox id="chkReasonnotalignother" runat="server" AutoPostBack="true" OnCheckedChanged="chkReasonnotalignother_CheckedChanged"   Text=" Other"></asp:checkbox>
	        </p>
	        <p>
		        <label for="txtSpecifyreasonnotalign">If other reason, please specify</label>
		        <asp:textbox id="txtSpecifyreasonnotalign" runat="server" TextMode="MultiLine" ></asp:textbox>
                 <asp:RequiredFieldValidator ID="rfvtxtSpecifyreasonnotalign" runat="server" Enabled="false" ValidationGroup="Save"
              ControlToValidate="txtSpecifyreasonnotalign" ForeColor="Red" ErrorMessage="If other reason, please specify">Required</asp:RequiredFieldValidator>
        
                 <asp:RegularExpressionValidator ID="regSpecifyreasonnotalign" runat="server" ControlToValidate="txtSpecifyreasonnotalign"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed.
                    </asp:RegularExpressionValidator>
	        </p>
    </div>
   
    <h3 id="Pbhere">Topics Covered</h3>
   
    <div class="formLayout" >
         <p>Which of the following substantive areas were covered with adult(s) in the RPG case during the session?
               <%--<asp:RequiredFieldValidator ID="rfvAdultTopics" runat="server" ValidationGroup="Save" ErrorMessage="Required Adult Topics" ControlToValidate="rblAdultTopics" ForeColor="Red">Required</asp:RequiredFieldValidator>        
           --%>
         </p>

       <table style="padding-left:100px">
           <tr>
               <td> <label for="rblAdultSubAbuse"  >Parents’/other adults’ substance abuse treatment</label>
                  <asp:RequiredFieldValidator ID="rfvAdultSubAbuse" runat="server" ValidationGroup="Save" ErrorMessage="Required Parents’/other adults’ substance abuse treatment information" ControlToValidate="rblAdultSubAbuse" ForeColor="Red">Required</asp:RequiredFieldValidator>        
                </td>
                <td> <asp:RadioButtonList ID="rblAdultSubAbuse" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblAdultSubAbuse_SelectedIndexChanged">
               <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
               <asp:ListItem Text="No" Value="2"></asp:ListItem>
              </asp:RadioButtonList></td>
                <td> <asp:LinkButton ID="lnkAdulttopicssubabuse" runat="server"  Visible="false" OnClick="lnkAdulttopicssubabuse_Click">Matrix</asp:LinkButton>
                
		          </td>

           </tr>

           <tr>
               <td> <label for="rblAdultPrntSkills" >Parents’/other adults’ parenting skills</label>
                 <asp:RequiredFieldValidator ID="rfvAdultprntSkills" runat="server" ValidationGroup="Save" ErrorMessage="Parents’/other adults’ parenting skills" ControlToValidate="rblAdultPrntSkills" ForeColor="Red">Required</asp:RequiredFieldValidator>        
        </td>
               <td>  <asp:RadioButtonList ID="rblAdultPrntSkills" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblAdultPrntSkills_SelectedIndexChanged">
               <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
               <asp:ListItem Text="No" Value="2"></asp:ListItem>
              </asp:RadioButtonList></td>
               <td> <asp:LinkButton ID="lnkAdulttopicsprntskills" runat="server"  Visible="false" OnClick="lnkAdulttopicsprntskills_Click">Matrix</asp:LinkButton>
                </td>
           </tr>
           <tr>
               <td> <label for="rblAdultPersonalDevp">Parents’/other adults’ personal development </label>
       
                 <asp:RequiredFieldValidator ID="rfvAdultPersonalDevp" runat="server" ValidationGroup="Save" ErrorMessage="Required Parents’/other adults’ personal development information" ControlToValidate="rblAdultPersonalDevp" ForeColor="Red">Required</asp:RequiredFieldValidator>        
          </td>
                <td> <asp:RadioButtonList ID="rblAdultPersonalDevp" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblAdultPersonalDevp_SelectedIndexChanged">
               <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
               <asp:ListItem Text="No" Value="2"></asp:ListItem>
               </asp:RadioButtonList></td>
               <td> <asp:LinkButton ID="lnkAdulttopicspersdev" runat="server"  Visible="false" OnClick="lnkAdulttopicspersdev_Click">Matrix</asp:LinkButton>
	       </td>

           </tr>
       </table>
        
	      
           
    </div>
        
        <div class="formLayout" >
            <p>Which of the following substantive areas were covered with child(ren) in the RPG case during the session? 
                <%--  <asp:RequiredFieldValidator ID="reqChildTopic" runat="server" ValidationGroup="Save" ErrorMessage="Required Child Topic" ControlToValidate="rblChildtopics" ForeColor="Red">Required</asp:RequiredFieldValidator>        --%>

            </p>


            <table style="padding-left:100px">
             <tr>
                 <td> <label for="rblChildYouthTheDevp">Youth therapy and development</label>
                 <asp:RequiredFieldValidator ID="rfvChildYouth" runat="server" ValidationGroup="Save" ErrorMessage="Required Youth therapy information" ControlToValidate="rblChildYouthTheDevp" ForeColor="Red">Required</asp:RequiredFieldValidator>        
                 </td>
                 <td> <asp:RadioButtonList ID="rblChildYouthTheDevp" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblChildYouthTheDevp_SelectedIndexChanged">
               <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
               <asp:ListItem Text="No" Value="2"></asp:ListItem>
               </asp:RadioButtonList></td>
                 <td><asp:LinkButton ID="lnkChildtopicsdevanded" runat="server"  Visible="false" OnClick="lnkChildtopicsdevanded_Click">Matrix</asp:LinkButton>
	         
                  </td>
             </tr>

              <tr>
                 <td> <label for="rblChildSubDisRecovery">Education of youth on substance use disorders and recovery </label>
                 <asp:RequiredFieldValidator ID="rfvChildSubstance" runat="server" ValidationGroup="Save" ErrorMessage="Required Education of youth on substance information" ControlToValidate="rblChildSubDisRecovery" ForeColor="Red">Required</asp:RequiredFieldValidator>        
                </td>
                 <td><asp:RadioButtonList ID="rblChildSubDisRecovery" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblChildSubDisRecovery_SelectedIndexChanged">
               <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
               <asp:ListItem Text="No" Value="2"></asp:ListItem>
               </asp:RadioButtonList></td>
                 <td><asp:LinkButton ID="lnkChildtopicsedadultsubabuse" runat="server"  Visible="false" OnClick="lnkChildtopicsedadultsubabuse_Click">Matrix</asp:LinkButton><br />
	         </td>
             </tr>
            </table>      
	       
       
                 
   </div>
              <div class="formLayout" >
             <p style="width:750px"> Did you provide educational information on substance use disorders and recovery during the session to parents/other adults who are not receiving substance use disorder treatment but are a member of, or affiliated with, the RPG case?"
                
          
                   <asp:RequiredFieldValidator ID="reqREcBiology" runat="server" ValidationGroup="Save" ErrorMessage="Required Educational information" ControlToValidate="rblRecoverBiologyAbuseManagement" ForeColor="Red">Required</asp:RequiredFieldValidator>        

       
          <p>
           <label for="rblRecoverBiologyAbuseManagement"> </label>
                   
            <asp:RadioButtonList ID="rblRecoverBiologyAbuseManagement" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblRecoverBiologyAbuseManagement_SelectedIndexChanged">
               <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
               <asp:ListItem Text="No" Value="2"></asp:ListItem>
            </asp:RadioButtonList>

                
              </p>
                <p style="margin-left: 770px">
                             <asp:LinkButton ID="lnkOtherRelative"  Visible="false" runat="server" OnClick="lnkOtherRelative_Click">Matrix</asp:LinkButton>
                    </p>

            </div>
        <div class="formLayout"  style="display:none">
	        <p>
		        <label for="chkEdothrelat"> </label>
		        <asp:checkbox id="chkEdothrelat" runat="server"   Text="Other Related "></asp:checkbox>
	        </p>
       </div>
         <div class="formLayout" runat="server" id="divPE"> 
              <h3>Participant Engagement Rating</h3>

             <asp:RequiredFieldValidator ID="reqPartEnggRating" runat="server" ValidationGroup="Save"
                ControlToValidate="chkERatings" ErrorMessage="Participant Engagement Rating is required"
                ForeColor="Red">Required</asp:RequiredFieldValidator>
                <p style="width:900px"> Rate the specified case's engagement to date in evidence based program.(Complete after 2nd Service Log entry for EBP and at EBP Exit Mark only one) </p>
 <p></p>
                    <asp:RadioButtonList ID="chkERatings" runat="server" AutoPostBack="true" RepeatColumns="1"></asp:RadioButtonList>
                
                        <ul>
                            <li>
                                <b>4. Participants were consistently highly involved in services:</b> The participants kept most appointments and actively participated in discussions and activities. If homework was assigned, the participants completed it.

                            </li>
                            <li>
                                <b>3. Participants&#39; involvement varied:</b> The participant(s) sometimes kept appointments and sometimes actively participated in discussions and activities. If homework was assigned, the participants sometimes completed it. At other times, the participants&#39; involvement was low.</li>
                            <li>
                        <b>2. Participants&#39; involvement was consistently low:</b> The participant(s) kept some appointments but missed or cancelled frequently. The participant(s) rarely actively participated in discussions and activities. If homework was assigned, the participant(s) frequently did not complete it.

                            </li>
                            <li>
                                <b>1. Participants were minimally or not involved at all:</b> The participant(s) kept few appointments. The participant(s) did not actively participate in discussions and activities. If homework was assigned, the participant(s) did not complete it.</li>

                        </ul>
         </div>  
    
                  <p>
                <asp:Button ID="btnSave" runat="server"  ValidationGroup="Save" Text="Save" onclick="btnSave_Click" CausesValidation="true" />&nbsp;
                <asp:Button ID="btnSaveClose" runat="server" Visible="false"  ValidationGroup="Save" Text="Save and Close" onclick="btnSaveClose_Click" CausesValidation="true" />&nbsp;
                <asp:Button ID="btnClose" runat="server" Visible="true" Text="Close" 
                    onclick="btnClose_Click" CausesValidation="false"   
                    OnClientClick="return ConfirmDialog(this, 'Navigating away from the service log without saving the record means any entered data will be lost. If you would like to save this record, you must click SAVE. Would you still like to navigate away from this service log without saving data?', 'Are you sure?');" />&nbsp;&nbsp;&nbsp; 
                  <asp:Button ID="btnCloseHide" runat="server" Visible="false" Text="Close" OnClick="btnCloseHide_Click" CausesValidation="false"  />&nbsp;&nbsp;&nbsp; 
                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible="false" onclick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this record')" />&nbsp;&nbsp;&nbsp; 
          
           
        </p>
            <p>
            <asp:Literal ID="litMessage" Visible="true" runat="server"></asp:Literal></p><p>
                <asp:Label ID="lblMessageSession" runat="server" ForeColor="red"></asp:Label>
          
        </p>
        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red"  HeaderText="Please fill in all the required fields." />

     

   

</asp:Content>

