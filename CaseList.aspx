﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseList.aspx.cs" Inherits="CaseList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>Case List</h2>
    <p>
        <asp:Button ID="btnNew" runat="server" Text="Create New Case" OnClick="btnNew_Click" />


        <div id="divexcel" runat="server" style="padding-left: 800px;">

            <asp:LinkButton runat="server" Visible="false" ID="lbtnAdminMain" Text="Export Case Data" PostBackUrl="ReportInExcel.aspx"></asp:LinkButton>

        </div>

        <div id="div1" runat="server" visible="false" style="padding-left: 800px;">

            <asp:LinkButton runat="server" ID="lbtGranteeReport" Text="Grantee Case Data" PostBackUrl="GranteeExtractDataFile.aspx"></asp:LinkButton>

        </div>
        <div style="padding-left: 720px;">

            <asp:Button runat="server" ID="btnExportToExcel" Text="Export to Excel" OnClick="btnExportToExcel_Click" />

        </div>
        <div style="padding-left:450px;">

            <asp:Label ID="lblCasecErrorMessage" runat="server" ForeColor="Red"></asp:Label>
        </div>
    </p>

    <asp:GridView ID="grdVwList" runat="server" CssClass="gridTb2" Width="920px"
        AutoGenerateColumns="False" 
        AllowSorting="false" OnRowCommand="grdVwList_RowCommand" OnRowDataBound="grdVwList_RowDataBound">
        <Columns>
            <asp:BoundField Visible="false" DataField="CaseNoId" HeaderText="ID" DataFormatString="{0:g}">
                <HeaderStyle Width="10px"></HeaderStyle>
            </asp:BoundField>

            <asp:BoundField DataField="GRANTEE_ID_NO" HeaderText="Grantee ID"  DataFormatString="{0:g}">
                <HeaderStyle Width="50px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="CASE_ID" HeaderText="CASE ID" DataFormatString="{0:g}">
                <HeaderStyle Width="40px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="SURNAME" HeaderText="Surname" DataFormatString="{0:g}">
                <HeaderStyle Width="60px"></HeaderStyle>
            </asp:BoundField>

            <asp:TemplateField>
                <HeaderTemplate>
                    Case Members                     
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Repeater ID="repCaseMembers" runat="server">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>

                            <li><%# Eval("FIRST_NAME") %></li>
                        </ItemTemplate>

                        <FooterTemplate></ul></FooterTemplate>

                    </asp:Repeater>
                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>



            <asp:TemplateField>
                <HeaderTemplate>
                    RPG Case Enrollment Details                      
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" Visible="true" CssClass="otherstyle"
                         CommandArgument='<%# Eval("CaseNoId") %>'>Enrollment Details</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>


            <asp:TemplateField>
                <HeaderTemplate>
                    Identify Case Member Relationships                     
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnComplCaseEnroll" runat="server" CommandName="CompleteCaseEnroll" 
                        Visible="true" CssClass="otherstyle" CommandArgument='<%# Eval("CaseNoId") %>'>Identify Relationships</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="150px" />
            </asp:TemplateField>



            <asp:TemplateField>
                <HeaderTemplate>
                    EBPs                 
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnEBPS" runat="server" CommandName="EBP" Visible="true" CssClass="otherstyle" 
                        CommandArgument='<%# Eval("CaseNoId") %>'>EBP List</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    Service Logs                 
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnSLs" runat="server" CommandName="ServiceLog" Visible="true" CssClass="otherstyle" 
                        CommandArgument='<%# Eval("CaseNoId") %>'>View</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>

            <asp:TemplateField>
                       <HeaderTemplate>   
                           Case Closure Date                  
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:Repeater ID="repCaseClose" runat="server">
                                    <HeaderTemplate>
                                        <ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li><asp:HyperLink ID="lnkBtnCCloseDate"  Text='<%# Eval("RPG_CLOSE_DATE", "{0:MM/dd/yyyy}") %>' 
                                             NavigateUrl='<%# String.Format("~/CaseClosure.aspx?lngPkID={0}&lngCaseID={1}",Eval("CaseCloseID"), Eval("CaseNoId_FK"))%>' runat="server" /></li>
                                    </ItemTemplate> 
                                    <FooterTemplate></ul></FooterTemplate>                                    
                                </asp:Repeater>                         
                            </ItemTemplate>
                           <HeaderStyle width="150px" />
                        </asp:TemplateField>          
                     

            <asp:TemplateField>
                <HeaderTemplate>
                    Close RPG Case                       
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnClose" runat="server" CommandName="CloseCase" Visible="true" CssClass="otherstyle" CommandArgument='<%# Eval("CaseNoId") %>'>Close Case</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="10px" />
            </asp:TemplateField>


            <asp:TemplateField>
                <HeaderTemplate>
                                   
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnDelete" runat="server" CommandName="Delete" Visible="false" CssClass="otherstyle" CommandArgument='<%# Eval("CaseNoId") %>'>Delete</asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle Width="10px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>

