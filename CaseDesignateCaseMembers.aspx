﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseDesignateCaseMembers.aspx.cs" Inherits="CaseDesignateCaseMembers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        $(function () {
            var checked = $('input:checkbox').click(function (e) {

            });

        })

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<asp:Literal ID="ltlvldddl1" runat="server" ></asp:Literal>
    <h1>Identify Case Member Relationships</h1>
    <h2>Case ID:
        <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname:
        <asp:Literal ID="litSurname" runat="server"></asp:Literal></h2>
    <p>
        <asp:Literal ID="litMessagePermissions" runat="server"></asp:Literal>

    </p>
    <asp:GridView ID="grdVwList" runat="server" CssClass="gridTbl"
        AutoGenerateColumns="False" AllowPaging="false"
        AllowSorting="True" OnRowDataBound="grdVwList_RowDataBound">
        <Columns>


            <asp:BoundField DataField="FIRST_NAME" HeaderText="Individual Name" DataFormatString="{0:g}">
                <HeaderStyle Width="200px"></HeaderStyle>
            </asp:BoundField>

             <asp:BoundField DataField="PersonType" HeaderText="Person Type" DataFormatString="{0:g}">
                <HeaderStyle Width="200px"></HeaderStyle>
            </asp:BoundField>


            <asp:TemplateField HeaderText="Focal Child? (Mark only one per case)" HeaderStyle-Width="75px">
                <ItemTemplate>
                    <asp:CheckBox ID="chkGrdFocalchild" CssClass="txtBoxFont" OnCheckedChanged="chkGrdFocalchild_CheckedChanged" AutoPostBack="true" Width="75px" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.FOCAL_CHILD_YN") %>'></asp:CheckBox>
               <asp:Literal ID="ltlvldFocalchild" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Relationship to Focal Child" HeaderStyle-Width="200px">
                <ItemTemplate>
                    <asp:DropDownList ID="cboRelationshiptofocalchild" AutoPostBack="true" OnSelectedIndexChanged="cboRelationshiptofocalchild_SelectedIndexChanged" CssClass="txtBoxFont" Width="150px" runat="server"></asp:DropDownList><br/>
                    <asp:Literal ID="ltlvldddl" runat="server"></asp:Literal>
                </ItemTemplate>
              
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Relationship to Focal Child if other" HeaderStyle-Width="200px">
                <ItemTemplate>
                    <asp:TextBox ID="txtRelatfocalchildIfOther" Visible="false" runat="server" MaxLength="100" OnTextChanged="txtRelatfocalchildIfOther_TextChanged" CssClass="txtBoxFont" Width="150px" Text='<%# DataBinder.Eval(Container,"DataItem.RELATIONSHIP_TO_FOCAL_CHILD_IF_OTHER") %>'> </asp:TextBox>
                  <br/><asp:Literal ID="ltlvldOther" runat="server"></asp:Literal>
                </ItemTemplate><HeaderStyle Width="200px"></HeaderStyle>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Family Functioning Adult? (Mark only one per case)" HeaderStyle-Width="100px">
                <ItemTemplate>                   
                     <asp:CheckBox ID="chkGrdFamilyfunctioningadult" CssClass="txtBoxFont" Width="100px" OnCheckedChanged="chkGrdFamilyfunctioningadult_CheckedChanged" AutoPostBack="true" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.FAMILY_FUNCTIONING_ADULT_YN") %>'></asp:CheckBox>
               <br /><asp:Literal ID="ltlvldFamilyfunctioningadult" runat="server"></asp:Literal>
                      </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recovery Domain Adult? (Mark only one per case)" HeaderStyle-Width="75px">
                <ItemTemplate>
                    <asp:CheckBox ID="chkGrdDomainrecoveryadult" CssClass="txtBoxFont" Width="75px" OnCheckedChanged="chkGrdDomainrecoveryadult_CheckedChanged" AutoPostBack="true" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.DOMAIN_RECOVERY_ADULT_YN") %>'></asp:CheckBox>
                    <br /><asp:Literal ID="ltlvldDomainrecoveryadult" runat="server"></asp:Literal>
                    
                     <asp:HiddenField ID="hidIndividualID" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
<div class="formLayout" id="AdultChild" runat="server">
   
  <p style="width:900px">
        <Label for="rblchildAdult" runat="server">Is the focal child currently in the care of the family functioning adult?

             <asp:RequiredFieldValidator ID="rfvChildAdultCare" runat="server" ValidationGroup="Save" ControlToValidate="rblchildAdult" 
                 ErrorMessage="Is the focal child currently in the care of the family functioning adult?" ForeColor="Red">Required!</asp:RequiredFieldValidator>
	
        </Label>
        
        <asp:RadioButtonList id="rblchildAdult" runat="server"  RepeatDirection="Horizontal"  AutoPostBack="true" > 
		</asp:RadioButtonList><br />&nbsp;
      </p>
          </div>
    <p>
        <asp:Label ID="lblValMessage" runat="server" ForeColor="Red"></asp:Label>
         <asp:Label ID="ltErrorMessage" runat="server" ForeColor="Red"></asp:Label>

         

    </p>
    <p>
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="Save" />

        <asp:Button ID="btnSaveClose" Visible="false" runat="server" Text="Save and Close" OnClick="btnSaveClose_Click" />

        <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" CausesValidation="false" />

    </p>
</asp:Content>

