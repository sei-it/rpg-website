﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class CaseWorker : System.Web.UI.Page
{
    int lngPkID;
    int intCaseID;
    int IdenRelationship;
    int EBPAssignedCaseWorkerID;
    int GranteeContactId;
    int? intGranteeID;
    int ValInd;
    string ErrorMsg1;
    protected void Page_Load(object sender, EventArgs e)
    {
          string strJS = null;

          if (!IsPostBack)
          {
               string currentDate = DateTime.Today.ToShortDateString();
               cmpCWMinEndDate.ValueToCompare = currentDate;              

              if (Page.Request.QueryString["lngPkID"] != null)
              {
                  lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
              }
              if (Page.Request.QueryString["GranteeContactId"] != null)
              {
                  GranteeContactId = Convert.ToInt32(Page.Request.QueryString["GranteeContactId"]);
              }
              if (Page.Request.QueryString["EBPAssignedCaseWorkerID"] != null)
              {
                  EBPAssignedCaseWorkerID = Convert.ToInt32(Page.Request.QueryString["EBPAssignedCaseWorkerID"]);
              }
              if (Page.Request.QueryString["intCaseID"] != null)
              {
                  intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
              }
          }
          loadrecords();
          if (ViewState["IsLoaded1"] == null)
          {
              displayRecords();
              ViewState["IsLoaded1"] = true;
          }

          Page.MaintainScrollPositionOnPostBack = true;

    }
    protected void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                intGranteeID = oCase.GranteeID_fk;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
            }
        }
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["GranteeContactId"] != null)))
        {
            GranteeContactId = Convert.ToInt32(this.ViewState["GranteeContactId"]);
        }
        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }

        if (((this.ViewState["EBPAssignedCaseWorkerID"] != null)))
        {
            EBPAssignedCaseWorkerID = Convert.ToInt32(this.ViewState["EBPAssignedCaseWorkerID"]);
        }
        //if (((this.ViewState["oUI"] != null)))
        //{
        //    oUI = (UserInfo)(this.ViewState["oUI"]);
        //}
        //if (((this.ViewState["blClose"] != null)))
        //{
        //    blClose = Convert.ToBoolean(this.ViewState["blClose"]);
        //}

    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["GranteeContactId"] = GranteeContactId;
        this.ViewState["intCaseID"] = intCaseID;
        //this.ViewState["blClose"] = blClose;
        this.ViewState["EBPAssignedCaseWorkerID"] = EBPAssignedCaseWorkerID;
        //this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }
    protected void displayRecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            
            object objVal1 = null;
            var oPages1 = from pg in db.F_EBPCaseWorkers_AssignedLists
                          where pg.EBPAssignedCaseWorkerID == EBPAssignedCaseWorkerID
                          select pg;

            foreach(var oCase1 in oPages1)
            {
                
               objVal1 = oCase1.GranteeContactId_FK;
                 if (objVal1 != null)
                {
                   GranteeContactId= Convert.ToInt32(objVal1.ToString());
                }
                    
            }
            

            object objVal = null;
            var oPages = from pg in db.F_EBPCaseWorkers_AssignedLists
                         join G in db.A_Grantee_Contacts
                         on pg.GranteeContactId_FK equals G.GranteeContactId
                         where pg.EBPAssignedCaseWorkerID == EBPAssignedCaseWorkerID && pg.EBPEnrollID_FK == lngPkID
                         select new { G.GranteeContactId, G.GranteeRole_fk,CaseWorkerName = G.FirstName + " " + G.LastName, G.Phone, G.Email, pg.CaseWorker_StartDate, pg.CaseWorker_ExitDate };


            foreach (var oCase in oPages)
            {

            objVal = oCase.CaseWorkerName;
            if (objVal != null)
            {
                txtCasewName.Text = objVal.ToString();
            }
            objVal = oCase.Phone;
            if (objVal != null)
            {
                txtCasewPhone.Text = objVal.ToString();

            }
            objVal = oCase.Email;
            if (objVal != null)
            {
                txtCasewEmail.Text = objVal.ToString();

            }
            objVal = oCase.CaseWorker_StartDate;
            if (objVal != null)
            {
                txtCasewStartDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);

            }
            objVal = oCase.CaseWorker_ExitDate;
            if (objVal != null)
            {
                txtCasewEndDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);

                string loginName = HttpContext.Current.User.Identity.Name;

                AppUser user = db.AppUsers.SingleOrDefault(x => x.sUserID == loginName);
                if (user != null && user.AdminRoleId_fk != 3) //supper admin
                {
                    txtCasewEndDate.Enabled = false;
                    btnSave.Visible = false;
                }
            }


            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        btnSave.Visible = false;
    }

    private void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp1;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {


            //F_EBPCaseWorker oCase = (from c in db.F_EBPCaseWorkers
            //                         where c.GranteeContactId_FK == GranteeContactId
            //                         && c.EBPEnrollID_FK == lngPkID
            //                         select c).FirstOrDefault();


            F_EBPCaseWorkers_AssignedList oCase = (from c in db.F_EBPCaseWorkers_AssignedLists
                                                   where c.GranteeContactId_FK == GranteeContactId
                                                   && c.EBPEnrollID_FK == lngPkID && c.EBPAssignedCaseWorkerID == EBPAssignedCaseWorkerID
                                                   select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBPCaseWorkers_AssignedList();
                blNew = true;
                oCase.EBPEnrollID_FK = lngPkID;
                oCase.GranteeContactId_FK = GranteeContactId;
            }

            DateTime.TryParse(txtCasewStartDate.Text, out dtTmp1);

            if (txtCasewStartDate.Text != "")
            {
                oCase.CaseWorker_StartDate = dtTmp1;
               
            }

            DateTime.TryParse(txtCasewEndDate.Text, out dtTmp1);

            if (txtCasewEndDate.Text != "")
                oCase.CaseWorker_ExitDate = dtTmp1;

            oCase.UpdatedBy = HttpContext.Current.User.Identity.Name;
            oCase.UpdatedDate = DateTime.Now;

            if (blNew == true)
            {
                db.F_EBPCaseWorkers_AssignedLists.InsertOnSubmit(oCase);
            }
                   
            db.SubmitChanges();
            lngPkID =Convert.ToInt32( oCase.EBPEnrollID_FK);
           
        }
        //LitJS.Text = " showSuccessToast();";
        // litMessage.Text = "Record saved! ID=" + lngPkID;

    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("EBPEdit.aspx?GranteeContactId=" + GranteeContactId + "&lngPkID=" + lngPkID + "&intCaseID=" + intCaseID);
    }
}