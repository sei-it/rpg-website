﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CaseClosure : System.Web.UI.Page
{
    int lngPkID;
    int intCaseID;
    int Valcount = 0;
    bool allchecked = false;
    UserInfo oUI;

    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;
        //lbltxtOther.Text = "";

        if (chkCasecloseother.Checked == false)
        {
            rfvtxtSpecifyrpgcaseclose.EnableClientScript = false;
            rfvtxtSpecifyrpgcaseclose.Enabled = false;
            txtSpecifyrpgcaseclose.Text = ""; // added on july 24
            txtSpecifyrpgcaseclose.Enabled = false;
        }
        else 
        {
            rfvtxtSpecifyrpgcaseclose.EnableClientScript = true;
            rfvtxtSpecifyrpgcaseclose.Enabled = true;
            txtSpecifyrpgcaseclose.Enabled = true;
        }
       

        if (!IsPostBack)
        {
            string currentDate = DateTime.Today.ToShortDateString();
          cvMaxCaseClose.ValueToCompare = currentDate;
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["lngCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["lngCaseID"]);
            }
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);

            if (oUI.intUserRoleID == 3)
            {
                btnDelete.Visible = true;
            }
            else
            {               
                btnDelete.Visible = false;
            }
        }
        
        loadrecords();
        DisplayChildNameAdultName();
        DisableBtnSave();
        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();             
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        CompareDate();
        Page.MaintainScrollPositionOnPostBack = true;
       
    }
    private void DisplayChildNameAdultName()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            object objVal=null;
            var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                       where p.CaseNoId_FK == intCaseID && p.FOCAL_CHILD_YN == true
                       select new { p.FIRST_NAME };

            foreach (var oCase in qry2)
            {
                objVal=oCase.FIRST_NAME;
                if (! (objVal == null)    )  
                {
                    lblFocalChild.Text = objVal.ToString();
                }
            }

            object objVal2 = null;
            var qry3 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                       where p.CaseNoId_FK == intCaseID && p.FAMILY_FUNCTIONING_ADULT_YN == true
                       select new { p.FIRST_NAME };

            foreach (var oCase1 in qry3)
            {
                objVal2 = oCase1.FIRST_NAME;
                if (!(objVal == null))
                {
                    lblfamilyAdult.Text = objVal2.ToString();
                }
            }

        }
    }
    private void CompareDate()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            object objValue;
            var oPages = from pg in db.F_ENROLLMENT_EBPs
                         where pg.CaseID_FK == intCaseID
                         orderby pg.EBP_EXIT_DATE ascending 
                         select pg;

            foreach (var oCase in oPages)
            {
                objValue = oCase.EBP_EXIT_DATE;
                if (objValue != null)
                {

                    txtDtCmp.Text = String.Format("{0:MM/dd/yyyy}", objValue);
                    cvEbpExitDate.ValueToCompare = txtDtCmp.Text;

                }

            }

            F_ENROLLMENT_EBP oCase1 = (from c in db.F_ENROLLMENT_EBPs where c.CaseID_FK == intCaseID select c).FirstOrDefault();

            if ((oCase1 == null))
            {
                object objValue1;
                var oPages1 = from pg in db.B_CASE_ENROLLMENTs
                         where pg.CaseNoId == intCaseID                       
                         select pg;
                 foreach (var oCase2 in oPages1)
                    {
                        objValue1 = oCase2.RPG_ENROLL_DATE;
                        if (objValue1 != null)
                        {

                            txtDtCmp.Text = String.Format("{0:MM/dd/yyyy}", objValue1);
                            cvEbpExitDate.ValueToCompare = txtDtCmp.Text;

                        }

                    }
                
              
            }

        }
    }      
    private void DisableBtnSave()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oPages = from pg in db.E_CaseClosures
                         where pg.CaseNoId_FK == intCaseID
                         select pg;
            foreach (var oCase in oPages)
            {
                if (oCase.CASE_CLOSE_COMPLETED == true)
                {
                    if (oUI.intUserRoleID == 3)
                    {
                        btnSave.Visible = true;
                        btnSaveClose.Visible = false;
                        btnDelete.Visible = true;
                    }
                    else
                    {
                        btnSave.Visible = false;
                        btnDelete.Visible = false;
                    }
                }
            }

        }
    }

    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.C_ChildAdultCares select p;

            rblchildAdult.DataSource = qry;
            rblchildAdult.DataTextField = "ChildAdultCare";
            rblchildAdult.DataValueField = "ChildAdultID";
            rblchildAdult.DataBind();
        }
    }
    protected void loadrecords()
    {
    }

    //----Display/Update------------//
    protected void displayRecords()
    {
        object objVal = null;
       
        using (DataClassesDataContext db = new DataClassesDataContext())
        {          

            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                if (oCase.RPG_ENROLL_DATE!=null )
                {
                    // litEnrollmentDate.Text  =Convert.ToString( oCase.RPG_ENROLL_DATE);
                    objVal = oCase.RPG_ENROLL_DATE;
                    litEnrollmentDate.Text = String.Format("{0:MM/dd/yyyy}", objVal); ;
                }
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
                 
            }

            var oPages = from pg in db.E_CaseClosures 
                         where pg.CaseNoId_FK == intCaseID 
                         select pg;
            foreach (var oCase in oPages)
            {


                objVal = oCase.RPG_CLOSE_DATE;
                if (objVal != null)
                {
                    txtRpgclosedate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
                }
                objVal = oCase.COMPLETED_PROGRAM;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkCompletedprogram.Checked = true;                      
                        
                    }
                    else
                    {
                        chkCompletedprogram.Checked = false;
                       
                    }
                }
                objVal = oCase.FAMILY_MOVE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkFamilymove.Checked = true;
                    }
                    else
                    {
                        chkFamilymove.Checked = false;
                    }
                }
                objVal = oCase.UNABLE_TO_LOCATE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkUnabletolocate.Checked = true;
                    }
                    else
                    {
                        chkUnabletolocate.Checked = false;
                    }
                }
                objVal = oCase.UNRESPONSIVE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkUNRESPONSIVE.Checked = true;
                    }
                    else
                    {
                        chkUNRESPONSIVE.Checked = false;
                    }
                }
                objVal = oCase.FAMILY_DECLINED;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkFamilydeclined.Checked = true;
                    }
                    else
                    {
                        chkFamilydeclined.Checked = false;
                    }
                }
                objVal = oCase.TRANSFERRED;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkTRANSFERRED.Checked = true;
                    }
                    else
                    {
                        chkTRANSFERRED.Checked = false;
                    }
                }
                objVal = oCase.MISC_OR_CHILD_DEATH;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkMiscorchilddeath.Checked = true;
                    }
                    else
                    {
                        chkMiscorchilddeath.Checked = false;
                    }
                }
                objVal = oCase.PARENT_DEATH;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkParentdeath.Checked = true;
                    }
                    else
                    {
                        chkParentdeath.Checked = false;
                    }
                }
                objVal = oCase.CASE_CLOSE_OTHER;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        chkCasecloseother.Checked = true;
                       
                    }
                    else
                    {
                        chkCasecloseother.Checked = false;
                       
                    }
                }
                objVal = oCase.SPECIFY_RPG_CASE_CLOSE;
                if (objVal != null)
                {
                    txtSpecifyrpgcaseclose.Text = objVal.ToString();
                }

                objVal = oCase.ChildAdultID_FK;
                if (objVal != null)
                {
                    rblchildAdult.SelectedIndex = rblchildAdult.Items.IndexOf(rblchildAdult.Items.FindByValue(oCase.ChildAdultID_FK.ToString()));
                   
                }


            }

        }
        // displayGrid(lngPkID);
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            E_CaseClosure oCase = (from c in db.E_CaseClosures where c.CaseNoId_FK == intCaseID  select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new E_CaseClosure();
                blNew = true;
                oCase.CaseNoId_FK = intCaseID;
            }


            DateTime.TryParse(txtRpgclosedate.Text, out dtTmp);
            oCase.RPG_CLOSE_DATE = dtTmp;
            if (chkCompletedprogram.Checked == true)
            {
                oCase.COMPLETED_PROGRAM = true;
            }
            else
            {
                oCase.COMPLETED_PROGRAM = false;
            }
            if (chkFamilymove.Checked == true)
            {
                oCase.FAMILY_MOVE = true;
            }
            else
            {
                oCase.FAMILY_MOVE = false;
            }
            if (chkUnabletolocate.Checked == true)
            {
                oCase.UNABLE_TO_LOCATE = true;
            }
            else
            {
                oCase.UNABLE_TO_LOCATE = false;
            }
            if (chkUNRESPONSIVE.Checked == true)
            {
                oCase.UNRESPONSIVE = true;
            }
            else
            {
                oCase.UNRESPONSIVE = false;
            }
            if (chkFamilydeclined.Checked == true)
            {
                oCase.FAMILY_DECLINED = true;
            }
            else
            {
                oCase.FAMILY_DECLINED = false;
            }
            if (chkTRANSFERRED.Checked == true)
            {
                oCase.TRANSFERRED = true;
            }
            else
            {
                oCase.TRANSFERRED = false;
            }
            if (chkMiscorchilddeath.Checked == true)
            {
                oCase.MISC_OR_CHILD_DEATH = true;
            }
            else
            {
                oCase.MISC_OR_CHILD_DEATH = false;
            }
            if (chkParentdeath.Checked == true)
            {
                oCase.PARENT_DEATH = true;
            }
            else
            {
                oCase.PARENT_DEATH = false;
            }

            oCase.SPECIFY_RPG_CASE_CLOSE = txtSpecifyrpgcaseclose.Text;

            if (chkCasecloseother.Checked == true)
            {
                oCase.CASE_CLOSE_OTHER = true;
            }
            else
            {
                oCase.CASE_CLOSE_OTHER = false;
            }

            if (!(rblchildAdult.SelectedItem == null))
            {
                if (!(rblchildAdult.SelectedItem.Value.ToString() == ""))
                {
                    oCase.ChildAdultID_FK = Convert.ToInt32(rblchildAdult.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.ChildAdultID_FK = null;
                }
            }
            //oCase.SPECIFY_RPG_CASE_CLOSE = txtSpecifyrpgcaseclose.Text;

            oCase.updatedDT = DateTime.Now;
            oCase.updatedBy = "";
            oCase.CASE_CLOSE_COMPLETED = true;

            if (blNew == true)
            {
                db.E_CaseClosures.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
            lngPkID = oCase.CaseCloseID;
             
        }
        //LitJS.Text = " showSuccessToast();";
       // litMessage.Text = "Record saved! ID=" + lngPkID;
        litMessage.Text = "Record saved!";



    }

    //-------View State----------------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }

    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }

    //-------Save/Close/SaveClose---------------------//
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Validatecheckbox();
        if (allchecked == true)
        {
            lblrequiredMess.Text = "";
            if (Valcount == 1)
            {
                lblMessage.Text = "You must enter a value in the Required field";
            }
            else
            {
                updateUser();
                displayRecords();
                btnSaveClose.Visible = false;               
                if (oUI.intUserRoleID == 3)
                {
                    btnSave.Visible = true;
                }
                else
                {
                    btnSave.Visible = false;
                }  
            }
        }
        else
        {
            lblrequiredMess.Text= "Required";
        }

        Response.Redirect("CaseList.aspx?intCaseID=" + intCaseID);
    }  
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("CaseList.aspx?intCaseID=" + intCaseID);
    } 
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();
        Response.Redirect("CaseList.aspx?intCaseID=" + intCaseID);
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            E_CaseClosure oca9 = (from c in db.E_CaseClosures
                                  where c.CaseNoId_FK == intCaseID
                                  select c).FirstOrDefault();
            if ((oca9 != null))
            {
                db.E_CaseClosures.DeleteOnSubmit(oca9);
            }
            db.SubmitChanges();
        }
        Response.Redirect("CaseList.aspx?intCaseID=" + intCaseID);
    }

    //------Validate-----------------//
    private void Validatecheckbox()
    {
        if (chkCompletedprogram.Checked)
        {
            allchecked = true;
            //chkFamilymove.Enabled = false;
            //chkUnabletolocate.Enabled = false;
            //chkUNRESPONSIVE.Enabled = false;
            //chkFamilydeclined.Enabled = false;
            //chkTRANSFERRED.Enabled = false;
            //chkMiscorchilddeath.Enabled = false;
            //chkCasecloseother.Enabled = false;
        }

        if (chkFamilymove.Checked)
            allchecked = true;
        if (chkUnabletolocate.Checked)
            allchecked = true;
        if (chkUNRESPONSIVE.Checked)
            allchecked = true;
        if (chkFamilydeclined.Checked)
            allchecked = true;
        if (chkTRANSFERRED.Checked)
            allchecked = true;
        if (chkMiscorchilddeath.Checked)
            allchecked = true;
        if (chkCasecloseother.Checked)
            allchecked = true;

    }
    protected void chkCasecloseother_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCasecloseother.Checked == true)
        {
            rfvtxtSpecifyrpgcaseclose.EnableClientScript = true;
            rfvtxtSpecifyrpgcaseclose.Enabled = true;
            txtSpecifyrpgcaseclose.Enabled = true;
        }
        else
        {
            txtSpecifyrpgcaseclose.Enabled = false;
            rfvtxtSpecifyrpgcaseclose.EnableClientScript = false;
            rfvtxtSpecifyrpgcaseclose.Enabled = false;
        }

    }
    protected void chkCompletedprogram_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCompletedprogram.Checked)
        {

            chkFamilymove.Enabled = false;
            chkUnabletolocate.Enabled = false;
            chkUNRESPONSIVE.Enabled = false;
            chkFamilydeclined.Enabled = false;
            chkTRANSFERRED.Enabled = false;
            chkParentdeath.Enabled = false;
            chkMiscorchilddeath.Enabled = false;
            chkCasecloseother.Enabled = false;
            txtSpecifyrpgcaseclose.Enabled = false;
            rfvtxtSpecifyrpgcaseclose.Enabled = false;

            chkFamilymove.Checked = false;
            chkUnabletolocate.Checked = false;
            chkUNRESPONSIVE.Checked = false;
            chkFamilydeclined.Checked = false;
            chkTRANSFERRED.Checked = false;
            chkParentdeath.Checked = false;
            chkMiscorchilddeath.Checked = false;
            chkCasecloseother.Checked = false;
            txtSpecifyrpgcaseclose.Text = "";
            lblrequiredMess.Text = "";
            
        }
        else
        {

            chkFamilymove.Enabled = true;
            chkUnabletolocate.Enabled = true;
            chkUNRESPONSIVE.Enabled = true;
            chkFamilydeclined.Enabled = true;
            chkTRANSFERRED.Enabled = true;
            chkParentdeath.Enabled = true;
            chkMiscorchilddeath.Enabled = true;
            chkCasecloseother.Enabled = true;
           // txtSpecifyrpgcaseclose.Enabled = true;
           // rfvtxtSpecifyrpgcaseclose.Enabled = true;
            lblrequiredMess.Text = "Required";

        }
    }
    
}