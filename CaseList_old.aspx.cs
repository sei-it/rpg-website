﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Configuration;
using System.IO;


public partial class CaseList : System.Web.UI.Page
{
    private int? intUserGranteeID ;
    private int? intUserRole;
    int IdenRelationship;
    int lngPkID;
    int EBPexitdate = 1;
    UserInfo oUI;
    //100-Admin
    //1-Grantee Admin
    //2-Case Workers
    //3-Super System Admin
    //4-System Admin
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

           
             
            oUI=Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            intUserGranteeID = oUI.intGranteeID;
            intUserRole = oUI.intUserRoleID;

            if (ActivateTickelr())
            {
               // Response.Redirect("CaseActivityTickler.aspx"); // commented  push the changes to production without Tickler
            }
            //else
            //{
            //    Response.Redirect("CaseList.aspx");
            //}

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["IdenRelationship"] != null)
            {
                IdenRelationship = Convert.ToInt32(Page.Request.QueryString["IdenRelationship"]);
            }
            displayRecords();
        }
        
        
        string strJS;
        //strJS = ("window.open(\"UserEdit.aspx?lngPkID=" + ("0" + "\",\'\',\"width=950,height=700,resizable,scrollbars\");"));
        //btnNew.Attributes.Add("onclick", strJS);
    }

    protected bool ActivateTickelr()
    {
        bool retval = false;

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (intUserRole == 3 || intUserRole == 4)
            {
                retval = false;
            }
            else if(intUserRole == 1) 
            {
               // var vCases = db.TicklerCount_Modified(intUserGranteeID);
                var vCases = db.TicklerCount(intUserGranteeID);
                            
                    foreach (var ocase1 in vCases)
                    {
                        if (ocase1.ActivityAlertCount >= 1)
                        {
                            retval = true;
                        }
                        else
                        {
                            retval = false;
                        }
                     }
               }
            else if (intUserRole == 2)
            {
                var vCases = db.TicklerCount_ForCase(intUserGranteeID, HttpContext.Current.User.Identity.Name);

                foreach (var ocase1 in vCases)
                {
                    if (ocase1.ActivityAlertCount >= 1)
                    {
                        retval = true;
                    }
                    else
                    {
                        retval = false;
                    }
                }
            }
        }
        return retval;

    }
    
    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

             
            if (intUserRole == 4) //RPG Admin
            {
                //btnNew.Visible = false;
        
                var vCases = from p in db.B_CASE_ENROLLMENTs
                         orderby p.CaseNoId descending
                         select p;
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
            else if(intUserRole == 1)//Grantee Admin 
            {
                var vCases = from p in db.B_CASE_ENROLLMENTs
                         where p.GranteeID_fk == intUserGranteeID
                         orderby p.CaseNoId descending
                         select p;
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
            else if (intUserRole == 2) //Case Worker
            {
                btnNew.Visible = false;
                btnExportToExcel.Visible = false;
              //  var vCases = db.getCasesForCaseWorker(HttpContext.Current.User.Identity.Name);  // commented on 18th May
                var vCases = db.getCasesForCaseWorker_AssignedList(HttpContext.Current.User.Identity.Name);
                
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
            else if (intUserRole == 3)
            {
                var vCases = from p in db.B_CASE_ENROLLMENTs
                             orderby p.CaseNoId descending
                             select p;
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }

        }

    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       

        if (e.CommandName == "Begin")
        {
            int intCaseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("CaseEdit.aspx?lngPkID=" + intCaseID + "&IdenRelationship=" + IdenRelationship);
        }
        if (e.CommandName == "EBP")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("EBPList.aspx?intCaseID=" + intResponseID);
        }
        if (e.CommandName == "ServiceLog")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("ServiceLogList.aspx?intCaseID=" + intResponseID);
        }
        if (e.CommandName == "CompleteCaseEnroll")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);             
            Response.Redirect("CaseDesignateCaseMembers.aspx?lngCaseID=" + intResponseID);
        }
        if (e.CommandName == "CloseDate")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("CaseClosure.aspx?lngCaseID=" + intResponseID);
        }
        if (e.CommandName == "CloseCase")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                F_ENROLLMENT_EBP oCase1 = (from c in db.F_ENROLLMENT_EBPs where c.CaseID_FK == intResponseID select c).FirstOrDefault();

                if ((oCase1 == null))
                {
                    Response.Redirect("CaseClosure.aspx?lngCaseID=" + intResponseID);
                }

                var qry1 = from c in db.F_ENROLLMENT_EBPs
                           where c.CaseID_FK == intResponseID
                           select c;
               
                foreach (var oCase in qry1)
                {
                    if (oCase.EBP_ENROLL_COMPLETED == null)
                    {
                        EBPexitdate = EBPexitdate+1;
                       
                    }                   
                }           
              

                if (EBPexitdate == 1)
                {
                    lblCasecErrorMessage.Text = "";
                    Response.Redirect("CaseClosure.aspx?lngCaseID=" + intResponseID);
                }
                else
                {
                    lblCasecErrorMessage.Text = "Ensure that all EBPs are exited prior to closing a case from RPG";
                }
                
            }
           
        }
        if (e.CommandName == "Delete")
        {
             int intResponseID = Convert.ToInt32(e.CommandArgument);
             DeleteCase(intResponseID);
             Response.Redirect("CaseList.aspx");
        }
    }

    private void DeleteCase(int intResponseID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            int GrnteeId = 0;
            int ServiceLogId = 0;
            //----------------get Granteeid for the Case to be deleted------------------------------//
            object objVal = null;
            var qry1 = from c in db.B_CASE_ENROLLMENTs
                       where c.CaseNoId == intResponseID
                       select new { c.GranteeID_fk };
            if (qry1 != null)
            {
                foreach (var var1 in qry1)
                {
                    if (!(var1.GranteeID_fk == null))
                    {
                        GrnteeId = Convert.ToInt32(var1.GranteeID_fk);
                    }
                }
            }



       //var qry11 = from c1 in db.G_SERVICE_LOGs
       //            where c1.CaseID_FK == intResponseID
       //                select new { c1.ServiceID };
       //     if (qry11 != null)
       //     {
       //         foreach (var var11 in qry11)
       //         {
       //             if (!(var11.ServiceID == null))
       //             {
       //                 ServiceLogId = Convert.ToInt32(var11.ServiceID);
       //             }
       //         }
       //     }
            //------------------delet case from B_CASE_ENROLLMENT----------------------------------------//
            Boolean blNew = false;
            B_CASE_ENROLLMENT oCase1 = (from c in db.B_CASE_ENROLLMENTs
                                                   where c.CaseNoId == intResponseID
                                                       && c.GranteeID_fk == GrnteeId
                                                   select c).FirstOrDefault();
            if ((oCase1 != null))
            {
                db.B_CASE_ENROLLMENTs.DeleteOnSubmit(oCase1);
            }
            db.SubmitChanges();

            //------------------delet  from C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASET----------------------------------------//

            C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE oCase2 = (from c in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                                        where c.CaseNoId_FK == intResponseID                                           
                                       select c).FirstOrDefault();
            if ((oCase2 != null))
            {
                db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs.DeleteOnSubmit(oCase2);
            }
            db.SubmitChanges();

            //------------------delet  from F_ENROLLMENT_EBP----------------------------------------//

            F_ENROLLMENT_EBP oCase3 = (from c in db.F_ENROLLMENT_EBPs
                                       where c.CaseID_FK == intResponseID
                                        select c).FirstOrDefault();
            if ((oCase3 != null))
            {
                db.F_ENROLLMENT_EBPs.DeleteOnSubmit(oCase3);
            }
            db.SubmitChanges();

            //------------------delete  from F_ENROLLMENT----------------------------------------//
           
            var oca4 = ( from p in db.vw_deleteFrom_F_EBPEnrollments
                                                   where p.CaseNoId_FK==intResponseID
                                                   select new {p.IndividualID_FK});
            if ((oca4 != null))
            {
                foreach (var var1 in oca4)
                {
                    if (!(var1.IndividualID_FK == null))
                    {
                        deleteF_EBPEnrollment(Convert.ToInt32(var1.IndividualID_FK));
                    }
                }                
            }
           

            //-------------------delete from F_EBPCaseWorkers_AssignedList-----------------//

            var oca5 = (from p in db.vw_deleteFrom_F_EBPCaseWorkers_AssignedLists
                              where p.CaseID_FK == intResponseID
                        select new {p.CaseID_FK, p.EBPEnrollID_FK });
            if ((oca5 != null))
            {
                 foreach (var var2 in oca5)
                {
                    if (!(var2.EBPEnrollID_FK == null))
                    {
                        deleteFrom_F_EBPCaseWorkers_AssignedLists(Convert.ToInt32(var2.EBPEnrollID_FK));
                    }
                 }
            }
            db.SubmitChanges();

            //-----------------delete  from G_ServiceLogCaseMembers---------------//
            var oca6 = (from p in db.vw_deleteFrom_G_ServiceLogCaseMembers
                                        where p.CaseID_FK == intResponseID
                        select new { p.ServiceID });
            if ((oca6 != null))
            {
                foreach (var var3 in oca6)
                {
                    if (!(var3.ServiceID == null))
                    {
                        deleteFrom_G_ServiceLogCaseMembers(Convert.ToInt32(var3.ServiceID));
                    }
                }
            }
            db.SubmitChanges();

            //----------------delete from G_SERVICE_LOG-------------------------------//
              G_SERVICE_LOG oca7 = (from c in db.G_SERVICE_LOGs
                                        where c.CaseID_FK == intResponseID                                           
                                       select c).FirstOrDefault();
            if ((oca7 != null))
            {
                db.G_SERVICE_LOGs.DeleteOnSubmit(oca7);
            }
            db.SubmitChanges();

            //-----------------delete  from vw_deleteFrom_F_EBP_ParticipantRating---------------//

            var oca8 = (from p in db.vw_deleteFrom_F_EBP_ParticipantRatings
                        where p.CaseID_FK == intResponseID
                        select new { p.CaseEBPID_FK });
            if ((oca8 != null))
            {
                foreach (var var4 in oca8)
                {
                    if (!(var4.CaseEBPID_FK == null))
                    {
                        deleteFrom_F_EBP_ParticipantRatings(Convert.ToInt32(var4.CaseEBPID_FK));
                    }
                }
            }
            db.SubmitChanges();

            //-------------------delete from E_CaseClosure---------------------------------------//
              E_CaseClosure oca9 = (from c in db.E_CaseClosures
                                        where c.CaseNoId_FK == intResponseID                                           
                                       select c).FirstOrDefault();
            if ((oca9 != null))
            {
                db.E_CaseClosures.DeleteOnSubmit(oca9);
            }
            db.SubmitChanges();

            //------------------deletee from N_SubstanceAbuseMatrix--------------------------//
            var oca10 = (from p in db.vw_deleteFrom_N_SubstanceAbuseMatrixes
                         where p.CaseID_FK == intResponseID
                                   select new { p.ServiceID_FK });
            if ((oca10 != null))
            {
                foreach (var var5 in oca10)
                {
                    if (!(var5.ServiceID_FK == null))
                    {
                        deleteFrom_N_SubstanceAbuseMatrixes(Convert.ToInt32(var5.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();

            //------------------deletee from O_ParentingSkillsMatrix--------------------------//
            var oca11 = (from p in db.vw_deleteFrom_O_ParentingSkillsMatrixes
                         where p.CaseID_FK == intResponseID
                         select new { p.ServiceID_FK });
            if ((oca11 != null))
            {
                foreach (var var6 in oca11)
                {
                    if (!(var6.ServiceID_FK == null))
                    {
                        deleteFrom_O_ParentingSkillsMatrixes(Convert.ToInt32(var6.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();


            //------------------deletee from P_PersonalDevelopmentMatrix--------------------------//
            var oca12 = (from p in db.vw_deleteFrom_P_PersonalDevelopmentMatrixes
                         where p.CaseID_FK == intResponseID
                         select new { p.ServiceID_FK });
            if ((oca12 != null))
            {
                foreach (var var7 in oca12)
                {
                    if (!(var7.ServiceID_FK == null))
                    {
                        deleteFrom_P_PersonalDevelopmentMatrixes(Convert.ToInt32(var7.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();


            //------------------deletee from Q_DevelopmentEducationMatrix--------------------------//
            var oca13 = (from p in db.vw_deleteFrom_Q_DevelopmentEducationMatrixes
                         where p.CaseID_FK == intResponseID
                         select new { p.ServiceID_FK });
            if ((oca13 != null))
            {
                foreach (var var8 in oca13)
                {
                    if (!(var8.ServiceID_FK == null))
                    {
                        deleteFrom_Q_DevelopmentEducationMatrixes(Convert.ToInt32(var8.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();


            //------------------deletee from R_AdultSubstanceAbuseMatrix--------------------------//
            var oca14 = (from p in db.vw_deleteFrom_R_AdultSubstanceAbuseMatrixes
                         where p.CaseID_FK == intResponseID
                         select new { p.ServiceID_FK });
            if ((oca14 != null))
            {
                foreach (var var9 in oca14)
                {
                    if (!(var9.ServiceID_FK == null))
                    {
                        deleteFrom_R_AdultSubstanceAbuseMatrixes(Convert.ToInt32(var9.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();

            //------------------deletee from S_OtherFamilySubAbuseMatrix--------------------------//

            var oca15 = (from p in db.vw_deleteFrom_S_OtherFamilySubAbuseMatrixes
                         where p.CaseID_FK == intResponseID
                         select new { p.ServiceID_FK });
            if ((oca15 != null))
            {
                foreach (var var10 in oca15)
                {
                    if (!(var10.ServiceID_FK == null))
                    {
                        deleteFrom_S_OtherFamilySubAbuseMatrixes(Convert.ToInt32(var10.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        
        }
    } 
   
   
    //----------------Delete function--------------------------//



    private void deleteFrom_S_OtherFamilySubAbuseMatrixes(int y)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            S_OtherFamilySubAbuseMatrix oCase15 = (from c in db.S_OtherFamilySubAbuseMatrixes
                                                   where c.ServiceID_FK == y
                                                   select c).FirstOrDefault();
            if ((oCase15 != null))
            {
                db.S_OtherFamilySubAbuseMatrixes.DeleteOnSubmit(oCase15);
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_R_AdultSubstanceAbuseMatrixes(int x)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            R_AdultSubstanceAbuseMatrix oCase14 = (from c in db.R_AdultSubstanceAbuseMatrixes
                                                    where c.ServiceID_FK == x
                                                    select c).FirstOrDefault();
            if ((oCase14!= null))
            {
                db.R_AdultSubstanceAbuseMatrixes.DeleteOnSubmit(oCase14);
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_Q_DevelopmentEducationMatrixes(int w)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Q_DevelopmentEducationMatrix oCase13 = (from c in db.Q_DevelopmentEducationMatrixes
                                                   where c.ServiceID_FK == w
                                                   select c).FirstOrDefault();
            if ((oCase13 != null))
            {
                db.Q_DevelopmentEducationMatrixes.DeleteOnSubmit(oCase13);
            }
            db.SubmitChanges();
        }
    }
   
    private void deleteFrom_P_PersonalDevelopmentMatrixes(int v)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            P_PersonalDevelopmentMatrix oCase12 = (from c in db.P_PersonalDevelopmentMatrixes
                                               where c.ServiceID_FK == v
                                               select c).FirstOrDefault();
            if ((oCase12 != null))
            {
                db.P_PersonalDevelopmentMatrixes.DeleteOnSubmit(oCase12);
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_O_ParentingSkillsMatrixes(int u)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            O_ParentingSkillsMatrix oCase11 = (from c in db.O_ParentingSkillsMatrixes
                                              where c.ServiceID_FK == u
                                              select c).FirstOrDefault();
            if ((oCase11 != null))
            {
                db.O_ParentingSkillsMatrixes.DeleteOnSubmit(oCase11);
            }
            db.SubmitChanges();
        }
       
    }

    private void deleteFrom_N_SubstanceAbuseMatrixes(int t)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            N_SubstanceAbuseMatrix oCase10 = (from c in db.N_SubstanceAbuseMatrixes
                                              where c.ServiceID_FK == t
                                              select c).FirstOrDefault();
            if ((oCase10 != null))
            {
                db.N_SubstanceAbuseMatrixes.DeleteOnSubmit(oCase10);
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_F_EBP_ParticipantRatings(int s)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBP_ParticipantRating oCase8 = (from c in db.F_EBP_ParticipantRatings
                                              where c.CaseEBPID_FK == s
                                             select c).FirstOrDefault();
            if ((oCase8 != null))
            {
                db.F_EBP_ParticipantRatings.DeleteOnSubmit(oCase8);
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_G_ServiceLogCaseMembers(int r)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            G_ServiceLogCaseMember oCase6 = (from c in db.G_ServiceLogCaseMembers
                                                    where c.ServiceID_FK == r
                                                    select c).FirstOrDefault();
            if ((oCase6 != null))
            {
                db.G_ServiceLogCaseMembers.DeleteOnSubmit(oCase6);
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_F_EBPCaseWorkers_AssignedLists(int q)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPCaseWorkers_AssignedList oCase5 = (from c in db.F_EBPCaseWorkers_AssignedLists
                                                    where c.EBPEnrollID_FK == q
                                      select c).FirstOrDefault();
            if ((oCase5 != null))
            {
                db.F_EBPCaseWorkers_AssignedLists.DeleteOnSubmit(oCase5);
            }
            db.SubmitChanges();
        }
    }

    private void deleteF_EBPEnrollment(int p)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPEnrollment oCase4 = (from c in db.F_EBPEnrollments
                                      where c.IndividualID_FK == p
                                   select c).FirstOrDefault();
            if ((oCase4 != null))
            {
                db.F_EBPEnrollments.DeleteOnSubmit(oCase4);
            }
            db.SubmitChanges();
        }
    }

   
//--------------------------------------------------------------//
    protected void btnNew_Click(object sender, EventArgs e)
    {
         
        Response.Redirect("CaseEdit.aspx");
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        if (((this.ViewState["IdenRelationship"] != null)))
        {
            IdenRelationship = Convert.ToInt32(this.ViewState["IdenRelationship"]);
        }

    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["oUI"] = oUI;
        this.ViewState["IdenRelationship"] = IdenRelationship;
        return (base.SaveViewState());
    }


    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
             int intCaseID=0;
 
        Repeater  repCasemember;
        Repeater repCaseClose;
        LinkButton imgBtnClose;
        LinkButton lnkBtnCCloseDate;
        LinkButton imgBtnDelete;
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "CaseNoId") != null)
            {
                intCaseID = (int)DataBinder.Eval(e.Row.DataItem, "CaseNoId");
            }
            repCasemember = (Repeater)e.Row.FindControl("repCaseMembers");
          

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs where p.CaseNoId_FK==intCaseID
                           select new { p.FIRST_NAME, p.IndividualID }  ;



                //-----------find control--------------------

                repCaseClose = (Repeater)e.Row.FindControl("repCaseClose");

                var qry3 = from cc in db.E_CaseClosures
                           where cc.CaseNoId_FK == intCaseID
                           select new { cc.CaseCloseID,cc.RPG_CLOSE_DATE,cc.CaseNoId_FK };              
                repCaseClose.DataSource = qry3;
                repCaseClose.DataBind();

                //-------------------------------------------

                var qry1 = from c in db.F_ENROLLMENT_EBPs
                           where c.CaseID_FK == intCaseID
                           select c;
                foreach (var oCase in qry1)
                    {

                        if (oCase.EBP_ENROLL_COMPLETED == true)
                        {
                            //imgBtnClose = (LinkButton)e.Row.FindControl("imgBtnClose");
                            //imgBtnClose.Visible = true;
                            lblCasecErrorMessage.Text = "";
                        }
                        else
                        {

                           // imgBtnClose = (LinkButton)e.Row.FindControl("imgBtnClose");
                           // imgBtnClose.Visible = false;
                           // lblCasecErrorMessage.Text = "Please complete EBP Exit date";
                        }

                    }


                var qry4 = from cc in db.E_CaseClosures
                           where cc.CaseNoId_FK == intCaseID
                           select new { cc.CASE_CLOSE_COMPLETED };
                foreach (var caseClose in qry4)
                {
                    if (caseClose.CASE_CLOSE_COMPLETED == true)
                    {
                       // imgBtnClose = (LinkButton)e.Row.FindControl("imgBtnClose");
                        //imgBtnClose.Visible = true;

                        //lnkBtnCCloseDate = (LinkButton)e.Row.FindControl("lnkBtnCCloseDate");
                        //lnkBtnCCloseDate.Visible = true;

                       //lnkBtnCCloseDate = (LinkButton)e.Row.FindControl("lnkBtnCCloseDate");
                       //lnkBtnCCloseDate.Visible = true;

                       
                    }
                   
                }

                
                repCasemember.DataSource = qry2;
                repCasemember.DataBind();
            }


            if (intUserRole == 2)
            {
                imgBtnClose = (LinkButton)e.Row.FindControl("imgBtnClose");
                imgBtnClose.Visible = false;
                grdVwList.HeaderRow.Cells[9].Visible = false;
            }

            if (intUserRole != 3)
            {
                imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                imgBtnDelete.Visible = false;
                
            }
            else
            {
                imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                imgBtnDelete.Visible = true;
                imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
                                                    "confirm('Are you sure you want to delete this record')");

            }


        }
    }
   
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
        intUserGranteeID = oUI.intGranteeID;
        intUserRole = oUI.intUserRoleID;

     
            if (intUserRole == 4) //RPG Admin
            {
                Response.Redirect("ReportInExcel.aspx");
            }
            else if (intUserRole == 3)
            {
                Response.Redirect("ReportInExcel.aspx");
            }
            else if (intUserRole == 1)//Grantee Admin 
            {
                Response.Redirect("GranteeExtractDataFile.aspx?intUserGranteeID=" + intUserGranteeID);
            }
            else if (intUserRole == 2) //Case Worker
            {
                Response.Redirect("CaseList.aspx");
            }
        
    }  
   
}