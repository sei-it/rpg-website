﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions; 

public partial class CaseEdit : System.Web.UI.Page
{
    int lngPkID;
    int IdenRelationship;
    int? intUserGranteeID;
    UserInfo oUI;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        Session["IsPageRefresh"] = false;
        if (!IsPostBack)
        {

            ViewState["ViewStateId"] = System.Guid.NewGuid().ToString();

            Session["SessionId"] = ViewState["ViewStateId"].ToString();

        }

        else
        {

            if (ViewState["ViewStateId"].ToString() != Session["SessionId"].ToString())
            {

                Session["IsPageRefresh"] = true;

            }

            Session["SessionId"] = System.Guid.NewGuid().ToString();

            ViewState["ViewStateId"] = Session["SessionId"].ToString();

        }     

        
        if (!IsPostBack)
        {
           
           string currentDate = DateTime.Today.ToShortDateString();
           cvRCEnroll.ValueToCompare = currentDate;


            if (!string.IsNullOrEmpty(Page.Request.QueryString["lngPkID"]))
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }

            if (!string.IsNullOrEmpty(Page.Request.QueryString["IdenRelationship"]))
            {
                IdenRelationship = Convert.ToInt32(Page.Request.QueryString["IdenRelationship"]);
           
            }

            if (!string.IsNullOrEmpty(Page.Request.QueryString["ErrorMsg"]))
            {
                lblError.Text = Page.Request.QueryString["ErrorMsg"].ToString();
            } 
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            intUserGranteeID = oUI.intGranteeID;
        }
        loadrecords();
        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

        if (!String.IsNullOrEmpty(txtCaseID.Text))
        {
            btnSave.Visible = false;
            txtCaseID.Enabled = false;  // added on 26th june
            txtEnrollDate.Enabled = false;
            txtSurname.Enabled = false;
            cboTreatmentControl.Enabled = false;
        }

        if (oUI.intUserRoleID == 3) // added on 27th june
        {
            btnSave.Visible = true;
            txtCaseID.Enabled = true;
            txtEnrollDate.Enabled = true;
            txtSurname.Enabled = true;
            cboTreatmentControl.Enabled = true;
        }

        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            // when Identify Case Member Relationships is completed

            var qryFlag = from Flag in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                          where Flag.CaseNoId_FK == lngPkID 
                          select Flag;

            
            foreach (var oCase in qryFlag)
            {
                btnCompleteCaseEnrollmnet.Enabled = true;
                //btnSave.Visible = false;
                //txtCaseID.Enabled = false;  // added on 26th june
                //txtEnrollDate.Enabled = false;
                //txtSurname.Enabled = false;
                //cboTreatmentControl.Enabled = false;
                if (oCase.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED == true)
                {
                    if (oUI.intUserRoleID == 3)// added on 27th june
                    {
                        btnNew.Visible = true;
                    }
                    else
                    {
                        btnNew.Visible = false;
                    }
                }
            }

            // when case is closed

            var oPages = from pg in db.E_CaseClosures
                         where pg.CaseNoId_FK == lngPkID
                         select pg;
            foreach (var oCase in oPages)
            {
                if (oCase.CASE_CLOSE_COMPLETED == true)
                {
                    if (oUI.intUserRoleID == 3)
                    {
                        btnNew.Visible = true;
                    }
                    else
                    {
                        btnNew.Visible = false;
                    }
                }
            }


        }



    }

    private void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from pg in db.B_TreatmentControls                     
                      select pg;
            cboTreatmentControl.DataSource = qry;
            cboTreatmentControl.DataTextField = "TreatmentControl";
            cboTreatmentControl.DataValueField = "TreatControlID";
            cboTreatmentControl.DataBind();          

        }
        
    } 
    protected void loadrecords()
    {       
    }
    protected void displayRecords()
    {
        object objVal = null;
        DataClassesDataContext db = new DataClassesDataContext();
        var oPages = from pg in db.B_CASE_ENROLLMENTs
                     where pg.CaseNoId == lngPkID && pg.CaseNoId != 0 
                     select pg;
        foreach (var oCase in oPages)
        {


 
            objVal = oCase.CASE_ID;
            if (objVal != null)
            {
                txtCaseID.Text = objVal.ToString();
            }
            objVal = oCase.ORIGINAL_ID;
            if (objVal != null)
            {
                txtCaseOriginalID.Text = objVal.ToString();
            }
            objVal = oCase.SURNAME;
            if (objVal != null)
            {
                txtSurname.Text = objVal.ToString();
            }
            objVal = oCase.RPG_ENROLL_DATE;
            if (objVal != null)
            {
                txtEnrollDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            }
            objVal=oCase.TreatControlID_fk;
            if (objVal != null)
            {
                cboTreatmentControl.SelectedIndex = cboTreatmentControl.Items.IndexOf(cboTreatmentControl.Items.FindByValue(oCase.TreatControlID_fk.ToString()));
            }



        }
        if (lngPkID==0)
        {
            cboTreatmentControl.SelectedIndex = 0;
        }

        db.Dispose();
        displayGrid(lngPkID);
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            B_CASE_ENROLLMENT oCase = (from c in db.B_CASE_ENROLLMENTs where c.CaseNoId == lngPkID select c).FirstOrDefault();
            //ResourceSubmission oCase = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oCase == null))
            {
                oCase = new B_CASE_ENROLLMENT();
                blNew = true;
            }




            oCase.CASE_ID = txtCaseID.Text;
            oCase.ORIGINAL_ID = txtCaseOriginalID.Text;
            oCase.SURNAME = txtSurname.Text;
            DateTime.TryParse(txtEnrollDate.Text, out dtTmp);
            if (txtEnrollDate.Text != "")
            oCase.RPG_ENROLL_DATE = dtTmp;

            if (!(cboTreatmentControl.SelectedItem == null))
            {
                if (!(cboTreatmentControl.SelectedItem.Value.ToString() == "Select"))
                {
                    oCase.TreatControlID_fk = Convert.ToInt32(cboTreatmentControl.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TreatControlID_fk = null;
                }
            }

          
            oCase.UpdatedBy = HttpContext.Current.User.Identity.Name;

            oCase.UpdatedDate = DateTime.Now;




            if (blNew == true)
            {
                oCase.GranteeID_fk = intUserGranteeID ;
                db.B_CASE_ENROLLMENTs.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
            lngPkID = oCase.CaseNoId;
        }
        //LitJS.Text = " showSuccessToast();";
        //litMessage.Text = "Record saved! ID=" + lngPkID;

        litMessage.Text = "Record saved!";



        //btnSave.Visible = false;
        //txtCaseID.Enabled = false;  // added on 31st Aug
        //txtEnrollDate.Enabled = false;
        //txtSurname.Enabled = false;
        //cboTreatmentControl.Enabled = false;

 
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intUserGranteeID"] != null)))
        {
            intUserGranteeID = Convert.ToInt32(this.ViewState["intUserGranteeID"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        if (((this.ViewState["IdenRelationship"] != null)))
        {
            IdenRelationship = Convert.ToInt32(this.ViewState["IdenRelationship"]);
        }



    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intUserGranteeID"] = intUserGranteeID;
        this.ViewState["IdenRelationship"] = IdenRelationship;
        this.ViewState["oUI"] = oUI;

        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Convert.ToBoolean(Session["IsPageRefresh"]))
        {
            Response.Redirect("CaseList.aspx");
            return;
        }

        if (string.IsNullOrEmpty(txtCaseID.Text.Trim()))
        {
            lblError.Text = "";
            return;
        }


        if (checkCaseIDvalid())
        {
            //btnSave.Visible = false;
            //txtCaseID.Enabled = false;  // added on 26th june
            //txtEnrollDate.Enabled = false;
            //txtSurname.Enabled = false;
            //cboTreatmentControl.Enabled = false;

            updateUser();
            displayRecords();
            txtCaseID.Text = "";
            txtEnrollDate.Text = "";
            txtSurname.Text = "";
            btnSave.Enabled = false;
            if (oUI.intUserRoleID == 3)// added on 27th june
            {
                btnSave.Visible = true;
                btnNew.Visible = true;
            }
            //else
            //{
            //    btnNew.Visible = false;
            //}
            Response.Redirect("CaseList.aspx");
        } 

    } 
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("CaseList.aspx");
    }

    private void displayGrid(int intCaseID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var vCases = db.getCaseMemberForCaseID(intCaseID) ;
 
            grdVwList.DataSource = vCases;
            grdVwList.DataBind();
        }

    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
        if (e.CommandName == "Begin")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("CaseEnrollmentDemographics.aspx?lngPkID=" + intResponseID + "&lngCaseID=" + lngPkID + "&IdenRelationship=" + IdenRelationship);
        }

        if (e.CommandName == "View")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("CaseEnrollmentDemographics.aspx?lngPkID=" + intResponseID + "&lngCaseID=" + lngPkID + "&IdenRelationship=" + IdenRelationship);
        }

        if (e.CommandName == "Delete")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            DeleteIndividual(intResponseID);
            Response.Redirect("CaseEdit.aspx?lngPkID=" + lngPkID + "&IdenRelationship=" + IdenRelationship);
        }
    }

    private void DeleteIndividual(int intResponseID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE oCase2 = (from c in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                                                                where c.IndividualID == intResponseID
                                                                select c).FirstOrDefault();
            if ((oCase2 != null))
            {
                db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs.DeleteOnSubmit(oCase2);
            }
            db.SubmitChanges();
        }

    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        if (checkCaseIDvalid())
        {

            updateUser();
            btnCompleteCaseEnrollmnet.Enabled = true;



            if (oUI.intUserRoleID == 3) // added on 27th june
            {
                btnSave.Visible = true;
                txtCaseID.Enabled = true;
                txtEnrollDate.Enabled = true;
                txtSurname.Enabled = true;
                cboTreatmentControl.Enabled = true;
            }
            else
            {
                
                    btnSave.Visible = false;
                    txtCaseID.Enabled = false;  // added on 26th june
                    txtEnrollDate.Enabled = false;
                    txtSurname.Enabled = false;
                    cboTreatmentControl.Enabled = false;
               
            }
            Response.Redirect("CaseEnrollmentDemographics.aspx?lngCaseID=" + lngPkID);
        }
    }

    private bool checkCaseIDvalid()
    {
        bool isValid = true;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            object objVal = null;
            lblError.Text = "";
            string pattern = @"^[a-zA-Z0-9]{6}$";

            if (string.IsNullOrEmpty(txtCaseID.Text.Trim()))
            {
                lblError.Text = "Required!";
                isValid = false;

                rfvCaseID.Visible = false;
            }
            else
            {
                if (!Regex.IsMatch(txtCaseID.Text.Trim(), pattern))
                {
                    lblError.Text = "Input 6 alphanumeric characters";
                    isValid = false;
                }
                else
                {

                    var oPages = from pg in db.B_CASE_ENROLLMENTs
                                 where pg.CaseNoId != lngPkID && pg.CASE_ID == txtCaseID.Text && pg.GranteeID_fk == intUserGranteeID
                                 select pg;
                    foreach (var oCase1 in oPages)
                    {

                        objVal = oCase1.CASE_ID;
                        if (objVal.ToString() == txtCaseID.Text)
                        {
                            isValid = false;
                            string ErrorMsg = "Case ID already exists.";
                            lblError.Text = ErrorMsg;

                            
                        }

                    }
                }
            }
        }
        if (!isValid)
        {
            btnSave.Visible = true;
            txtEnrollDate.Enabled = true;
            txtSurname.Enabled = true;
            cboTreatmentControl.Enabled = true;
        }
        if (lngPkID==0)
             txtCaseID.Enabled = true;
         return isValid;
    }
    protected void btnCompleteCaseEnrollmnet_Click(object sender, EventArgs e)
    {
        if (checkCaseIDvalid())
        {
            // updateUser(); //commented on 23 sep 2014 for timestamp not to change on click
            Response.Redirect("CaseDesignateCaseMembers.aspx?lngCaseID=" + lngPkID + "&IdenRelationship=" + IdenRelationship);
        }
    }
    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int IndividualID = 0;
        LinkButton imgBtnView;
        LinkButton imgBtnEdit;
        LinkButton imgBtnDelete;
        Repeater repRace;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "IndividualID") != null)
            {
                IndividualID = (int)DataBinder.Eval(e.Row.DataItem, "IndividualID");
            }

           repRace = (Repeater)e.Row.FindControl("repRace");


           using (DataClassesDataContext db = new DataClassesDataContext())
           {
               var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                          where p.IndividualID == IndividualID 
                          select new { p.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED};


               foreach (var oCase in qry2)
               {
                   if (oCase.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED == true)
                   {
                       imgBtnView = (LinkButton)e.Row.FindControl("imgBtnView");
                       imgBtnView.Visible = true;

                       imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
                       imgBtnEdit.Visible = false;

                       if (oUI.intUserRoleID == 3)// added on 27th june
                       {
                           imgBtnView = (LinkButton)e.Row.FindControl("imgBtnView");
                           imgBtnView.Visible = false;

                           imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
                           imgBtnEdit.Visible = true;

                           imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                           imgBtnDelete.Visible = true;
                           imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
                                                    "confirm('Are you sure you want to delete this record')");
                           
                       }
                   }
               }


               var qry3 = from p in db.vw_Races
                          where p.IndividualID == IndividualID
                          select p;
               foreach (var oCase in qry3)
               {
                   repRace.DataSource = qry3;
                   repRace.DataBind();
               }

               if (oUI.intUserRoleID == 3)// added on 27th june
               {
                   imgBtnView = (LinkButton)e.Row.FindControl("imgBtnView");
                   imgBtnView.Visible = false;

                   imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
                   imgBtnEdit.Visible = true;

                   imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                   imgBtnDelete.Visible = true;
                   imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
                                                    "confirm('Are you sure you want to delete this record')");

               }

           }                           
         }              
     }

    protected void txtCaseID_TextChanged(object sender, EventArgs e)
    {
        if (checkCaseIDvalid())
            btnSave.Enabled = true;
        else
            btnSave.Enabled = false;
    }
 }
  