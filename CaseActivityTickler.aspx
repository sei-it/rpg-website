﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseActivityTickler.aspx.cs" Inherits="CaseActivityTickler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
   <h2>Case Activity Alert</h2>
    <p>You do not have any activity for the following open EBPs for the past 14 days. Please input the reason for case inactivity  by clicking on the links below. You will  not be able to access any case until  you input reason for inactivity for all the EBPs listed below.</p>
   
    <asp:GridView ID="grdVwList" runat="server" CssClass="gridTb2" Width="920px"
        AutoGenerateColumns="False"
        AllowSorting="false" OnRowCommand="grdVwList_RowCommand" OnRowDataBound="grdVwList_RowDataBound">
        <Columns>
            <asp:BoundField Visible="false" DataField="CaseEBPID" HeaderText="ID" DataFormatString="{0:g}">
                <HeaderStyle Width="10px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="CASE_ID" HeaderText="CASE ID" DataFormatString="{0:g}">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="SURNAME" HeaderText="Surname" DataFormatString="{0:g}">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
             <asp:BoundField DataField="EBPName"  Visible="true" HeaderText="EBP Name" DataFormatString="{0:g}">
                <HeaderStyle Width="200px"></HeaderStyle>
            </asp:BoundField>

            <asp:TemplateField>
                <HeaderTemplate>
                    Case Members Enrolled in EBP Services                     
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Repeater ID="repCaseMembers" runat="server">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>

                            <li><%# Eval("FIRST_NAME") %></li>
                        </ItemTemplate>

                        <FooterTemplate></ul></FooterTemplate>

                    </asp:Repeater>
                </ItemTemplate>
                <HeaderStyle Width="150px" />
            </asp:TemplateField>

             <asp:BoundField DataField="DATE_OF_SERVICE" Visible="false" HeaderText="Lastest Service Log Date" DataFormatString="{0:MM/dd/yyyy}">
                <HeaderStyle Width="75px"></HeaderStyle>
            </asp:BoundField>


              <asp:TemplateField>
                       <HeaderTemplate>   
                          Lastest Service Log Date                    
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:Repeater ID="repServiceLogs" runat="server">
                                    <HeaderTemplate>
                                        <ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                       <%-- <li><asp:HyperLink ID="Button1" Text='<%# Eval("DATE_OF_SERVICE", "{0:MM/dd/yyyy}") %>'  NavigateUrl='<%# String.Format("~/ServiceLogEdit.aspx?lngPkID={0}&intCaseID={1}&intEBPEnrollID={2}", Eval("ServiceID"),Eval("CaseID_FK"),Eval("CaseEBPID_FK"))%>'    runat="server" /></li>--%>
                                    <li><%# Eval("DATE_OF_SERVICE", "{0:MM/dd/yyyy}") %></li>
                                    </ItemTemplate> 
                                    <FooterTemplate></ul></FooterTemplate>                                    
                                </asp:Repeater>                         
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField> 




            <asp:TemplateField HeaderText=" " HeaderStyle-Width="10%"> 
                <HeaderTemplate>
                    Confirm Case Activity 
                </HeaderTemplate>
                <ItemTemplate> 
                    <a href="CaseConfirmCaseActivity.aspx?intCaseID=<%#Eval("CaseNoId") %>&intCaseEBPID=<%#Eval("CaseEBPID") %>">Case Activity</a>
                    </ItemTemplate>
 
                <ItemStyle  Width="100px"></ItemStyle>
              </asp:TemplateField>  
          <%--  <asp:TemplateField>
                <HeaderTemplate>
                    Service Log Date                
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnSLs" runat="server" CommandName="ServiceLog" Visible="true" CssClass="otherstyle"
                        CommandArgument='<%# Eval("CaseNoId") %>'>View</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="150px" />
            </asp:TemplateField>--%>

<%--             <asp:TemplateField>
                <HeaderTemplate>
                    EBPs                 
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnEBPS" runat="server" CommandName="EBP" Visible="true" CssClass="otherstyle" 
                        CommandArgument='<%# Eval("CaseNoId") %>'>EBP List</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>--%>

           <%-- <asp:TemplateField>
                <HeaderTemplate>
                    Service Log                   
                </HeaderTemplate>
                <ItemTemplate>               
                    <asp:LinkButton ID="imgBtnCreate" runat="server" CommandName="CreateSL" Visible="false" CommandArgument='<%# Eval("CaseNoId") %>'>Create</asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>--%>

            <%--  <asp:TemplateField>
                <HeaderTemplate>
                    Confirm Case Activity                   
                </HeaderTemplate>
                <ItemTemplate>               
                    <asp:LinkButton ID="imgBtnCaseConfirmAct" runat="server" CommandName="CreateCaseConfimAct" Visible="true" CommandArgument='<%# Eval("CaseNoId") %>'>Case Confirm Ativity</asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>--%>




        </Columns>
    </asp:GridView>
</asp:Content>

