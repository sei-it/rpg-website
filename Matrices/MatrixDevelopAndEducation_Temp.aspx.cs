﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Matrices_MatrixDevelopAndEducation_Temp : System.Web.UI.Page
{
    int lngPkID;
    int intEBPEnrollID;
    int intCaseID;// added on feb 20th
    int intC;
    int TopicFreez;
    UserInfo oUI;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;
        this.ClientScript.RegisterOnSubmitStatement(this.GetType(), "OnSubmitScript", "g_isPostBack = true;");

        if (!IsPostBack)
        {
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            //added on feb 20th--
            if (Page.Request.QueryString["intCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
            }
            if (Page.Request.QueryString["intC"] != null)
            {
                intC = Convert.ToInt32(Page.Request.QueryString["intC"]);
            }
            if (Page.Request.QueryString["TopicFreez"] != null)
            {
                TopicFreez = Convert.ToInt32(Page.Request.QueryString["TopicFreez"]);
            }

            //-------
            if (Page.Request.QueryString["intEBPEnrollID"] != null)
            {
                intEBPEnrollID = Convert.ToInt32(Page.Request.QueryString["intEBPEnrollID"]);
            }

            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);

            loadDropdown();
        }
        loadrecords();
       // DisableBtnSave();
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

    }

    //---Disable Save button--------//
    private void DisableBtnSave()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var oPages = from pg in db.G_Create_SERVICE_LOGs
                     select pg;
        foreach (var oCase in oPages)
        {
            if (oCase.SERVICE_LOG_COMPLETED == true)
            {
                if (oUI.intUserRoleID == 3)
                {
                    btnSave.Visible = true;
                }
                else
                {
                    btnSave.Visible = false;
                }
            }
        }
    }

    //-----Load------------------//
    protected void loadrecords()
    {
    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.Matrixvals select p;
            rblTopicid1fk.DataSource = qry;
            rblTopicid1fk.DataTextField = "MatrixVals";
            rblTopicid1fk.DataValueField = "MatrixValID";
            rblTopicid1fk.DataBind();
            rblTopicid1fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid2fk.DataSource = qry;
            rblTopicid2fk.DataTextField = "MatrixVals";
            rblTopicid2fk.DataValueField = "MatrixValID";
            rblTopicid2fk.DataBind();
            rblTopicid2fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid3fk.DataSource = qry;
            rblTopicid3fk.DataTextField = "MatrixVals";
            rblTopicid3fk.DataValueField = "MatrixValID";
            rblTopicid3fk.DataBind();
            rblTopicid3fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid4fk.DataSource = qry;
            rblTopicid4fk.DataTextField = "MatrixVals";
            rblTopicid4fk.DataValueField = "MatrixValID";
            rblTopicid4fk.DataBind();
            rblTopicid4fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid5fk.DataSource = qry;
            rblTopicid5fk.DataTextField = "MatrixVals";
            rblTopicid5fk.DataValueField = "MatrixValID";
            rblTopicid5fk.DataBind();
            rblTopicid5fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid6fk.DataSource = qry;
            rblTopicid6fk.DataTextField = "MatrixVals";
            rblTopicid6fk.DataValueField = "MatrixValID";
            rblTopicid6fk.DataBind();
            rblTopicid6fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid7fk.DataSource = qry;
            rblTopicid7fk.DataTextField = "MatrixVals";
            rblTopicid7fk.DataValueField = "MatrixValID";
            rblTopicid7fk.DataBind();
            rblTopicid7fk.SelectedIndex = 3;

            rblTopicid8fk.DataSource = qry;
            rblTopicid8fk.DataTextField = "MatrixVals";
            rblTopicid8fk.DataValueField = "MatrixValID";
            rblTopicid8fk.DataBind();
            rblTopicid8fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid9fk.DataSource = qry;
            rblTopicid9fk.DataTextField = "MatrixVals";
            rblTopicid9fk.DataValueField = "MatrixValID";
            rblTopicid9fk.DataBind();
            rblTopicid9fk.SelectedIndex = 3;
            //'''''''''''''

            rblTopicid10fk.DataSource = qry;
            rblTopicid10fk.DataTextField = "MatrixVals";
            rblTopicid10fk.DataValueField = "MatrixValID";
            rblTopicid10fk.DataBind();
            rblTopicid10fk.SelectedIndex = 3;

            rblTopicid11fk.DataSource = qry;
            rblTopicid11fk.DataTextField = "MatrixVals";
            rblTopicid11fk.DataValueField = "MatrixValID";
            rblTopicid11fk.DataBind();
            rblTopicid11fk.SelectedIndex = 3;


        }
    }

    //---------Display/Update--------//
    protected void displayRecords()
    {
        object objVal = null;
        DataClassesDataContext db = new DataClassesDataContext();
        var oPages = from pg in db.Q_Temp_DevelopmentEducationMatrixes
                     where pg.ServiceID_FK == lngPkID
                     select pg;
        foreach (var oCase in oPages)
        {



            objVal = oCase.TopicID1_FK;
            if (objVal != null)
            {
                rblTopicid1fk.SelectedIndex = rblTopicid1fk.Items.IndexOf(rblTopicid1fk.Items.FindByValue(oCase.TopicID1_FK.ToString()));
            }
            objVal = oCase.TopicID2_FK;
            if (objVal != null)
            {
                rblTopicid2fk.SelectedIndex = rblTopicid2fk.Items.IndexOf(rblTopicid2fk.Items.FindByValue(oCase.TopicID2_FK.ToString()));
            }
            objVal = oCase.TopicID3_FK;
            if (objVal != null)
            {
                rblTopicid3fk.SelectedIndex = rblTopicid3fk.Items.IndexOf(rblTopicid3fk.Items.FindByValue(oCase.TopicID3_FK.ToString()));
            }
            objVal = oCase.TopicID4_FK;
            if (objVal != null)
            {
                rblTopicid4fk.SelectedIndex = rblTopicid4fk.Items.IndexOf(rblTopicid4fk.Items.FindByValue(oCase.TopicID4_FK.ToString()));
            }
            objVal = oCase.TopicID5_FK;
            if (objVal != null)
            {
                rblTopicid5fk.SelectedIndex = rblTopicid5fk.Items.IndexOf(rblTopicid5fk.Items.FindByValue(oCase.TopicID5_FK.ToString()));
            }
            objVal = oCase.TopicID6_FK;
            if (objVal != null)
            {
                rblTopicid6fk.SelectedIndex = rblTopicid6fk.Items.IndexOf(rblTopicid6fk.Items.FindByValue(oCase.TopicID6_FK.ToString()));
            }
            objVal = oCase.TopicID7_FK;
            if (objVal != null)
            {
                rblTopicid7fk.SelectedIndex = rblTopicid7fk.Items.IndexOf(rblTopicid7fk.Items.FindByValue(oCase.TopicID7_FK.ToString()));
            }
            objVal = oCase.TopicID8_FK;
            if (objVal != null)
            {
                rblTopicid8fk.SelectedIndex = rblTopicid8fk.Items.IndexOf(rblTopicid8fk.Items.FindByValue(oCase.TopicID8_FK.ToString()));
            }
            objVal = oCase.TopicID9_FK;
            if (objVal != null)
            {
                rblTopicid9fk.SelectedIndex = rblTopicid9fk.Items.IndexOf(rblTopicid9fk.Items.FindByValue(oCase.TopicID9_FK.ToString()));
            }
            objVal = oCase.TopicID10_FK;
            if (objVal != null)
            {
                rblTopicid10fk.SelectedIndex = rblTopicid10fk.Items.IndexOf(rblTopicid10fk.Items.FindByValue(oCase.TopicID10_FK.ToString()));
            }
            objVal = oCase.TopicID11_FK;
            if (objVal != null)
            {
                rblTopicid11fk.SelectedIndex = rblTopicid11fk.Items.IndexOf(rblTopicid11fk.Items.FindByValue(oCase.TopicID11_FK.ToString()));
            }


        }

        db.Dispose();

    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Q_Temp_DevelopmentEducationMatrix oCase = (from c in db.Q_Temp_DevelopmentEducationMatrixes where c.ServiceID_FK == lngPkID select c).FirstOrDefault();
            //ResourceSubmission oCase = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oCase == null))
            {
                oCase = new Q_Temp_DevelopmentEducationMatrix();
                blNew = true;
            }

            if (!(rblTopicid1fk.SelectedItem == null))
            {
                if (!(rblTopicid1fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID1_FK = Convert.ToInt32(rblTopicid1fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID1_FK = null;
                }
            }
            if (!(rblTopicid2fk.SelectedItem == null))
            {
                if (!(rblTopicid2fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID2_FK = Convert.ToInt32(rblTopicid2fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID2_FK = null;
                }
            }
            if (!(rblTopicid3fk.SelectedItem == null))
            {
                if (!(rblTopicid3fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID3_FK = Convert.ToInt32(rblTopicid3fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID3_FK = null;
                }
            }
            if (!(rblTopicid4fk.SelectedItem == null))
            {
                if (!(rblTopicid4fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID4_FK = Convert.ToInt32(rblTopicid4fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID4_FK = null;
                }
            }
            if (!(rblTopicid5fk.SelectedItem == null))
            {
                if (!(rblTopicid5fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID5_FK = Convert.ToInt32(rblTopicid5fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID5_FK = null;
                }
            }
            if (!(rblTopicid6fk.SelectedItem == null))
            {
                if (!(rblTopicid6fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID6_FK = Convert.ToInt32(rblTopicid6fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID6_FK = null;
                }
            }
            if (!(rblTopicid7fk.SelectedItem == null))
            {
                if (!(rblTopicid7fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID7_FK = Convert.ToInt32(rblTopicid7fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID7_FK = null;
                }
            }
            if (!(rblTopicid8fk.SelectedItem == null))
            {
                if (!(rblTopicid8fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID8_FK = Convert.ToInt32(rblTopicid8fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID8_FK = null;
                }
            }
            if (!(rblTopicid9fk.SelectedItem == null))
            {
                if (!(rblTopicid9fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID9_FK = Convert.ToInt32(rblTopicid9fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID9_FK = null;
                }
            }
            if (!(rblTopicid10fk.SelectedItem == null))
            {
                if (!(rblTopicid10fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID10_FK = Convert.ToInt32(rblTopicid10fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID10_FK = null;
                }
            }
            if (!(rblTopicid11fk.SelectedItem == null))
            {
                if (!(rblTopicid11fk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.TopicID11_FK = Convert.ToInt32(rblTopicid11fk.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.TopicID11_FK = null;
                }
            }





            if (blNew == true)
            {
                oCase.ServiceID_FK = lngPkID;
                db.Q_Temp_DevelopmentEducationMatrixes.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
            //lngPkID = oCase.ServiceID_FK;
        }
        //LitJS.Text = " showSuccessToast();";
       // litMessage.Text = "Record saved! ID=" + lngPkID;

        litMessage.Text = "Record saved!";



    }

    //------View State-----------------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }

        if (((this.ViewState["intC"] != null)))
        {
            intC = Convert.ToInt32(this.ViewState["intC"]);
        }
        if (((this.ViewState["TopicFreez"] != null)))
        {
            TopicFreez = Convert.ToInt32(this.ViewState["TopicFreez"]);
        }
        if (((this.ViewState["intEBPEnrollID"] != null)))
        {
            intEBPEnrollID = Convert.ToInt32(this.ViewState["intEBPEnrollID"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }

    }
    protected override object SaveViewState()
    {

        this.ViewState["intEBPEnrollID"] = intEBPEnrollID;
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["intC"] = intC;
        this.ViewState["TopicFreez"] = TopicFreez;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }

    //------Save/Close-----------------------------------//
    protected void btnSave_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();

        if (oUI.intUserRoleID == 3)
        {
            btnSave.Visible = true;
        }
        else
        {
            btnSave.Visible = false;
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("../ServiceLogAdd.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez + "&#Pbhere");//added on feb 20th

    }

}