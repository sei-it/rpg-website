﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MatrixPersonalDevelopment.aspx.cs" Inherits="Matrices_MatrixPersonalDevelopment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
 
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Parents'/Other Adults' Personal Development</h1>
    <table class="gridTbl" style="font-size: .8em">
        <tr>
            <th style="width:35%" >Topic</th>
            <th style="width:13%">Primary topic for session</th>
            <th style="width:13%">One of several topics</th>
            <%--<th style="width:13%">Spent some time discussing topic</th>--%>
            <th style="width:13%">Touched on topic</th>
            <th style="width:13%">Did not discuss</th>
        </tr>
        <tr>
            <td >Fostering communication and social skills </td>
            <td colspan="4" style="padding:0px;margin:0px;">
		        <asp:RadioButtonList id="rblTopicid1fk" runat="server"   RepeatDirection="Horizontal"   CssClass="radionButtonMatrixList" Width="100%" > 
		        </asp:RadioButtonList>
            </td>
        </tr>

        <tr>
            <td>Fostering resiliency </td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid2fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Fostering empathy and kindness </td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid3fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>

        <tr>
            <td>Learning to identify and express feelings&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid4fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Fostering skills to manage emotions&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid5fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Developing life management skills&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid6fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Fostering ability and commitment to making healthy choices&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid7fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Fostering healthy, safe relationships and boundaries&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid8fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Processing trauma and developing a trauma narrative&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid9fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        
 
    </table>
   
    <div class="formLayout">
 
 
        <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;<asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 
          
           
        </p>
            <p>
            <asp:Literal ID="litMessage" Visible="true" runat="server"></asp:Literal>
          
        </p>
    </div>
</asp:Content>



