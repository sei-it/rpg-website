﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MatrixAdultSubstanceAbuse.aspx.cs" Inherits="Matrices_MatrixAdultSubstanceAbuse" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Education of Youth on Substance Use Disorders and Recovery</h1>
    <table class="gridTbl" style="font-size: .8em">
        <tr>
            <th style="width:35%" >Topic</th>
            <th style="width:13%">Primary topic for session</th>
            <th style="width:13%">One of several topics</th>
           <%-- <th style="width:13%">Spent some time discussing topic</th>--%>
            <th style="width:13%">Touched on topic</th>
            <th style="width:13%">Did not discuss</th>
        </tr>
        <tr>
            <td >Discussing risk factors for youth developing substance use disorder </td>
            <td colspan="4" style="padding:0px;margin:0px;">
		        <asp:RadioButtonList id="rblTopicid1fk" runat="server"   RepeatDirection="Horizontal"   CssClass="radionButtonMatrixList" Width="100%" > 
		        </asp:RadioButtonList>
            </td>
        </tr>

        <tr>
            <td>Discussing impact of substance use disorders on family, friends, and relationships</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid2fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Discussing relapse prevention </td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid3fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>

        <tr>
            <td>Educating on biology of addiction&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid4fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Educating on medical effects of substance use&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid5fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
   
 
    </table>
   
    <div class="formLayout">
 
 
        <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;<asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 
          
           
        </p>
            <p>
            <asp:Literal ID="litMessage" Visible="true" runat="server"></asp:Literal>
          
        </p>
    </div>
</asp:Content>

