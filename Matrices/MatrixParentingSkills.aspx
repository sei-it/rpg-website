﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MatrixParentingSkills.aspx.cs" Inherits="Matrices_MatrixParentingSkills" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Parents'/Other Adults' Parenting Skills</h1>
    <table class="gridTbl" style="font-size: .8em">
        <tr>
            <th style="width:35%" >Topic</th>
            <th style="width:13%">Primary topic for session</th>
            <th style="width:13%">One of several topics</th>
            <%--<th style="width:13%">Spent some time discussing topic</th>--%>
            <th style="width:13%">Touched on topic</th>
            <th style="width:13%">Did not discuss</th>
        </tr>
        <tr>
            <td >Fostering parent's ability to effectively communicate with child </td>
            <td colspan="5" style="padding:0px;margin:0px;">
		        <asp:RadioButtonList id="rblTopicid1fk" runat="server"   RepeatDirection="Horizontal"   CssClass="radionButtonMatrixList" Width="100%" > 
		        </asp:RadioButtonList>
            </td>
        </tr>

        <tr>
            <td>Teaching parent how to develop child's communication and social skills </td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid2fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Teaching parent about child growth and development </td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid3fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>

        <tr>
            <td>Teaching parent how to establish care-giving routines&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid4fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Teaching parent to serve as a secure emotional base for child&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid5fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Fostering parent's understanding of and ability to develop child autonomy&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid6fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Teaching parent strategies to promote positive family interactions&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid7fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Teaching parent to manage child's misbehavior, foster positive behavior, and set developmentally appropriate rules and consequences&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid8fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Educating parent about pre-teen and teen sex and STIs&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid9fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Educating parent about child/adolescent substance use&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid10fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Educating parent on child/adolescent depression and suicide&nbsp;</td>
            <td colspan="5" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid11fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
 
 
    </table>
   
    <div class="formLayout">
 
 
        <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;<asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 
          
           
        </p>
            <p>
            <asp:Literal ID="litMessage" Visible="true" runat="server"></asp:Literal>
          
        </p>
    </div>
</asp:Content>


