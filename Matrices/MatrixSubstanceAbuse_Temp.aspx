﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MatrixSubstanceAbuse_Temp.aspx.cs" Inherits="Matrices_MatrixSubstanceAbuse_Temp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
       <script type="text/javascript" src="jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="jquery-ui-1.8.2.custom.min.js"></script>
   
    <script type="text/javascript">
        var g_isPostBack = false;

        function windowOnBeforeUnload() {
            if (g_isPostBack == true)
                return; // Let the page unload

            if (window.event) {
                //var dialogText = 'Navigating away from the service log without saving the record means any entered data will be lost. If you would like to save this record, you must click SAVE. Would you still like to navigate away from this service log';
                //var italictxt = 'without';
                //dialogText += italictxt.italics() + " saving data?";

                //window.event.returnValue =  dialogText; // IE

                window.event.returnValue = 'Navigating away from the service log without saving the record means any entered data will be lost. If you would like to save this record, you must click SAVE. Would you still like to navigate away from this service log without saving data?'; // FX

            }
            else {
                return 'Navigating away from the service log without saving the record means any entered data will be lost. If you would like to save this record, you must click SAVE. Would you still like to navigate away from this service log without saving data?'; // FX
            }
        }

        window.onbeforeunload = windowOnBeforeUnload;
        </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Parents'/Other Adults' Substance Abuse Treatment</h1>
    <table class="gridTbl" style="font-size: .8em">
        <tr>
            <th style="width:35%">Topic</th>
            <th style="width:13%">Primary topic for session</th>
            <th style="width:13%">One of several topics</th>
           
            <th style="width:13%">Touched on topic</th>
            <th style="width:13%">Did not discuss</th>
        </tr>
        <tr>
            <td >Acknowledging a substance use problem</td>
            <td colspan="4" style="padding:0px;margin:0px;">
		        <asp:RadioButtonList id="rblTopicid1fk" runat="server"   RepeatDirection="Horizontal"   CssClass="radionButtonMatrixList" Width="100%" > 
		        </asp:RadioButtonList>
            </td>
        </tr>

        <tr>
            <td>Discussing readiness to change </td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid2fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Discussing past successful behavioral changes </td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid3fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>

        <tr>
            <td>Identifying and preventing destructive behaviors</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid4fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Identifying triggers and cravings&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid5fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Enacting plan for change and recovery&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid6fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Developing a relapse plan&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid7fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Fostering honesty and responsibility&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid8fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Fostering self-help skills&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid9fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Providing information on abuse and trauma&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid10fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Developing understanding of substance use disorders and their effects&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid11fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Addressing guilt, loss, and grief&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid12fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>Developing support networks
&nbsp;</td>
            <td colspan="4" style="padding:0px;margin:0px;">		    
                <asp:RadioButtonList id="rblTopicid13fk" runat="server"   RepeatDirection="Horizontal"  CssClass="radionButtonMatrixList" Width="100%"> </asp:RadioButtonList> 
            </td>
        </tr>
 
    </table>
   
    <div class="formLayout">
 
 
        <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />&nbsp;<asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 
          
           
        </p>
            <p>
            <asp:Literal ID="litMessage" Visible="true" runat="server"></asp:Literal>
          
        </p>
    </div>
</asp:Content>

