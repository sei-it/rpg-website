﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class CaseConfirmCaseActivity : System.Web.UI.Page
{
 
    int lngPkID, intCaseEBPID,intCaseID;
    int? intUserGranteeID;
    UserInfo oUI;
    int checkR3to7true = 0;
    int checkR7false = 1;

    int vald1 = 0;
    int vald2 = 0;
    int vald3 = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        if (!IsPostBack)
        {
            R2.Visible = false;
            R3to7.Visible = false;
            R8.Visible = false;
            R9.Visible = false;
            R10.Visible = false;

            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["intCaseEBPID"] != null)
            {
                intCaseEBPID = Convert.ToInt32(Page.Request.QueryString["intCaseEBPID"]);
            }
            if (Page.Request.QueryString["intCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
            }
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            intUserGranteeID = oUI.intGranteeID;
        }
       
        //Validatecheckbox();
        if (ViewState["IsLoaded1"] == null)
        {
           
            displayRecords();
            displayAutoPopText();
            loadDropdown();
            ViewState["IsLoaded1"] = true;
        }

        loadrecords();
        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void displayAutoPopText()
    {
        object objVal = null;
        object objValEBPDate = null;
        DataClassesDataContext db = new DataClassesDataContext();

        //var vCases = from p in db.getCaseConfirmTickler(intUserGranteeID)
        //             where p.CaseNoId==intCaseID select p;

        var vCases = from p in db.getCaseConfirmTickler(intUserGranteeID)
                     where p.CaseEBPID == intCaseEBPID
                     select p;

        foreach (var oCase in vCases)
        {

            //---- R1
            litCaseID.Text = oCase.CASE_ID;
            R1EBPName.Text = oCase.EBPName;
            R1CaseSurname.Text = oCase.SURNAME;
            objVal = oCase.DATE_OF_SERVICE;
            objValEBPDate = oCase.EBP_ENROLL_DATE;
            if(!(objVal==null))
            {
            R1LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objVal); 
            }
            else
            {
                R1LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objValEBPDate); 
            }
            //---R2

            R2CaseSurname.Text = oCase.SURNAME;
            objVal = oCase.DATE_OF_SERVICE;
            if (!(objVal == null))
            {
                R2LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            }
            else
            {
                R2LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objValEBPDate);
            } 

            //---R3to7
            R3to7CaseSurname.Text = oCase.SURNAME;

            //--R10
            R10LastSerDate.Text = oCase.SURNAME;

            //--R8
            R8CaseSurname.Text = oCase.SURNAME;
             objVal = oCase.DATE_OF_SERVICE;
             if (!(objVal == null))
             {
                 R8LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
             }
             else
             {
                 R8LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objValEBPDate);
             }
         
            //---R9
            objVal = oCase.DATE_OF_SERVICE;
            if (!(objVal == null))
            {
                R9LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            }
            else
            {
                R9case1.Text = "the case was enrolled in";
                R9FocalEBp.Text=oCase.EBPName;
                R9service.Text="services on";
                R9EbpEnroll.Text = String.Format("{0:MM/dd/yyyy}", objValEBPDate);
                //R9LastSerDate.Text = String.Format("{0:MM/dd/yyyy}", objValEBPDate);
            }
        }
    }
 
    protected void loadrecords()
    {
    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            object objVal = null;

            var vCases = from p in db.getCaseConfirmTickler(intUserGranteeID)
                         where p.CaseEBPID == intCaseEBPID
                         select p;

            string caseID = "", caseSurname = "", FocalEbpName="";
            foreach (var itm in vCases)
            {
                caseID = itm.CASE_ID;
                caseSurname = itm.SURNAME;
                FocalEbpName = itm.EBPName;
                break;
            }
            var qry = from e in db.M_ReasonNoContactTypes
                                            select e;

            foreach (var data in qry)
            {
                data.ContactType = data.ContactType.ToString().Replace("[focal EBP]", FocalEbpName).Replace("[Case surname]", caseSurname);
            }
            rblREASONNOCASECONTACTFK.DataSource = qry;
            rblREASONNOCASECONTACTFK.DataTextField = "ContactType";
            rblREASONNOCASECONTACTFK.DataValueField = "ContactTypeID";
            rblREASONNOCASECONTACTFK.DataBind();
        }
    }
    protected void displayRecords()
    {
        object objVal = null;
        DataClassesDataContext db = new DataClassesDataContext();

        var oCases = from css in db.B_CASE_ENROLLMENTs
                     where css.CaseNoId == intCaseID
                     select css;
        foreach (var oCase in oCases)
        {
            litCaseID.Text = oCase.CASE_ID;
            litSurname.Text = oCase.SURNAME;
            //intGranteeID = oCase.GranteeID_fk;
        }



        var oPages = from pg in db.M_CONFIRM_CASE_ACTIVITies
                     where pg.ConfirmCaseActivityID == lngPkID
                     select pg;
        foreach (var oCase in oPages)
        {

 

            objVal = oCase.CONTACT_CONFIRMATION_YN;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true)
                { 
                    rblCONTACTCONFIRMATIONYN.Items[0].Selected = true;
                } 
                else if (Convert.ToBoolean(objVal) == false) 
                { 
                    rblCONTACTCONFIRMATIONYN.Items[1].Selected = true; 
                }
            }
            objVal = oCase.IN_PERSON_CONFIRMATION_YN;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true) 
                { 
                    rblINPERSONCONFIRMATIONYN.Items[0].Selected = true; 
                } 
                else if (Convert.ToBoolean(objVal) == false) 
                { 
                    rblINPERSONCONFIRMATIONYN.Items[1].Selected = true; 
                }
            }

            objVal = oCase.CONTACT_TYPE_PROV_REF_SERV;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true)
                {
                    chkCONTACTTYPEPROVREFSERV.Checked = true;
                }
                else
                {
                    chkCONTACTTYPEPROVREFSERV.Checked = false;
                }
            }
            objVal = oCase.CONTACT_TYPE_ATT_SCHED_APP;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true)
                {
                    chkCONTACTTYPEATTSCHEDAPP.Checked = true;
                }
                else
                {
                    chkCONTACTTYPEATTSCHEDAPP.Checked = false;
                }
            }
            objVal = oCase.CONTACT_TYPE_CHECK_FAM_NOT_IN_PERS;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true)
                {
                    chkCONTACTTYPECHECKFAMNOTINPERS.Checked = true;
                }
                else
                {
                    chkCONTACTTYPECHECKFAMNOTINPERS.Checked = false;
                }
            }
            objVal = oCase.CONTACT_TYPE_OTHER;
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true)
                {
                    chkCONTACTTYPEOTHER.Checked = true;
                }
                else
                {
                    chkCONTACTTYPEOTHER.Checked = false;
                }
            }
            objVal = oCase.SPECIFY_CONTACT_TYPE;
            if (objVal != null)
            {
                txtSPECIFYCONTACTTYPE.Text = objVal.ToString();
            }
            objVal = oCase.REASON_NO_CASE_CONTACT_FK;
            if (objVal != null)
            {
                rblREASONNOCASECONTACTFK.SelectedIndex = rblREASONNOCASECONTACTFK.Items.IndexOf(rblREASONNOCASECONTACTFK.Items.FindByValue(oCase.REASON_NO_CASE_CONTACT_FK.ToString()));
            }
            objVal = oCase.MISSED_VISIT_COUNT;
            if (objVal != null)
            {
                txtMISSEDVISITCOUNT.Text = objVal.ToString();
                //if (Convert.ToInt32(txtMISSEDVISITCOUNT.Text) > 0)
                //{
                //    R10.Visible = true;
                //}
                //else
                //{
                //    R10.Visible = false;
                //}
            }
            objVal = oCase.ABILITY_SCHED_APP_YN;
 
            if (objVal != null)
            {
                if (Convert.ToBoolean(objVal) == true)
                { 
                    rblABILITYSCHEDAPPYN.Items[0].Selected = true; 
                } 
                else if (Convert.ToBoolean(objVal) == false)
                { 
                    rblABILITYSCHEDAPPYN.Items[1].Selected = true; 
                }
            }


        }

        db.Dispose();
          
    }
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            M_CONFIRM_CASE_ACTIVITY  oCase = (from c in db.M_CONFIRM_CASE_ACTIVITies where c.ConfirmCaseActivityID == lngPkID select c).FirstOrDefault();
            //ResourceSubmission oCase = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
            if ((oCase == null  || lngPkID==0))
            {
                oCase = new M_CONFIRM_CASE_ACTIVITY();
                blNew = true;
            }
 
            if (rblCONTACTCONFIRMATIONYN.SelectedValue == "1") {
                oCase.CONTACT_CONFIRMATION_YN = true;
		    }else if (rblCONTACTCONFIRMATIONYN.SelectedValue == "0") {
                oCase.CONTACT_CONFIRMATION_YN = false;
		    }
		    if (rblINPERSONCONFIRMATIONYN.SelectedValue == "1") {
                oCase.IN_PERSON_CONFIRMATION_YN = true;
		    }else if (rblINPERSONCONFIRMATIONYN.SelectedValue == "0") {
                oCase.IN_PERSON_CONFIRMATION_YN = false;
		    }
            if (chkCONTACTTYPEPROVREFSERV.Checked == true)
            {
                oCase.CONTACT_TYPE_PROV_REF_SERV = true;
            }
            else
            {
                oCase.CONTACT_TYPE_PROV_REF_SERV = false;
            }
            if (chkCONTACTTYPEATTSCHEDAPP.Checked == true)
            {
                oCase.CONTACT_TYPE_ATT_SCHED_APP = true;
            }
            else
            {
                oCase.CONTACT_TYPE_ATT_SCHED_APP = false;
            }
            if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked == true)
            {
                oCase.CONTACT_TYPE_CHECK_FAM_NOT_IN_PERS = true;
            }
            else
            {
                oCase.CONTACT_TYPE_CHECK_FAM_NOT_IN_PERS = false;
            }
            if (chkCONTACTTYPEOTHER.Checked == true)
            {
                oCase.CONTACT_TYPE_OTHER = true;
            }
            else
            {
                oCase.CONTACT_TYPE_OTHER = false;
            }
            oCase.SPECIFY_CONTACT_TYPE = txtSPECIFYCONTACTTYPE.Text;
            if (!(rblREASONNOCASECONTACTFK.SelectedItem == null))
            {
                if (!(rblREASONNOCASECONTACTFK.SelectedItem.Value.ToString() == ""))
                {
                    oCase.REASON_NO_CASE_CONTACT_FK = Convert.ToInt32(rblREASONNOCASECONTACTFK.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.REASON_NO_CASE_CONTACT_FK = null;
                }
            }
            int.TryParse(txtMISSEDVISITCOUNT.Text, out strTmp);
            oCase.MISSED_VISIT_COUNT = strTmp;
             
            if (rblABILITYSCHEDAPPYN.SelectedValue == "1")
            {
                oCase.ABILITY_SCHED_APP_YN = true;
            }
            else if (rblABILITYSCHEDAPPYN.SelectedValue == "0")
            {
                oCase.ABILITY_SCHED_APP_YN = false;
            }
            oCase.updatedBy = HttpContext.Current.User.Identity.Name;

            oCase.updatedDT = DateTime.Now;




            if (blNew == true)
            {
                oCase.EBPEnrollID_fk = intCaseEBPID ;
                 
                db.M_CONFIRM_CASE_ACTIVITies.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
            lngPkID = oCase.ConfirmCaseActivityID;
        }
        //LitJS.Text = " showSuccessToast();";
        litMessage.Text = "Record saved! ID=" + lngPkID;


 
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intCaseEBPID"] != null)))
        {
            intCaseEBPID = Convert.ToInt32(this.ViewState["intCaseEBPID"]);
        }
        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intCaseEBPID"] = intCaseEBPID;
        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["oUI"] = oUI;

        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        updateUser();
        displayRecords();

    } 
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("CaseList.aspx");
    }
    
    protected void btnNew_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("CaseEnrollmentDemographics.aspx?lngCaseID=" + lngPkID );
    }
    protected void btnCompleteCaseEnrollmnet_Click(object sender, EventArgs e)
    {
        updateUser();
        Response.Redirect("CaseDesignateCaseMembers.aspx?lngCaseID=" + lngPkID);
    }
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        updateUser();
    }
   

    protected void rblCONTACTCONFIRMATIONYN_SelectedIndexChanged(object sender, EventArgs e)//R1
    {     

        int i = rblCONTACTCONFIRMATIONYN.SelectedIndex;

        if (i == 0)
        {
            R2.Visible = true;
            R8.Visible = false;
            R9.Visible = false;
            txtMISSEDVISITCOUNT.Text = "";
            foreach (ListItem item in rblREASONNOCASECONTACTFK.Items)
            {
                item.Selected = false;
            }
            //rblREASONNOCASECONTACTFK. ="0";
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }
            lblR8.Text = "";

        }
        else if(i == 1)
        {
            R8.Visible = true;
            R2.Visible = false;
            lblR2.Text = "";
            foreach (ListItem item in rblINPERSONCONFIRMATIONYN.Items)
            {
                item.Selected = false;
            }
            R3to7.Visible = false;           
            chkCONTACTTYPEPROVREFSERV.Checked = false;
            chkCONTACTTYPEATTSCHEDAPP.Checked = false;
            chkCONTACTTYPECHECKFAMNOTINPERS.Checked = false;
            chkCONTACTTYPEOTHER.Checked = false;
            rfvtxtSPECIFYCONTACTTYPE.EnableClientScript = false;
            rfvtxtSPECIFYCONTACTTYPE.Enabled = false;
            othertxt.Visible = false;
            txtSPECIFYCONTACTTYPE.Text = "";
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }

        }
    }
    protected void rblREASONNOCASECONTACTFK_SelectedIndexChanged(object sender, EventArgs e) //R8
    {
        if (rblREASONNOCASECONTACTFK.SelectedValue == "3")
        {
            R9.Visible = true;
            lblR8.Text = "";
        }
        else if (rblREASONNOCASECONTACTFK.SelectedValue == "1")
        {
            R9.Visible = false;
            txtMISSEDVISITCOUNT.Text = "";
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }
            lblR8.Text = "Please ensure service logs are created for any services provided by collaborating caseworker(s).";
        }
        else if (rblREASONNOCASECONTACTFK.SelectedValue == "2")
        {
            R9.Visible = false;
            txtMISSEDVISITCOUNT.Text = "";
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }
            lblR8.Text = "If you are no longer providing these services to the case, please end your assignment to this case.";
        }
        else if (rblREASONNOCASECONTACTFK.SelectedValue == "5")
        {
            R9.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }
            txtMISSEDVISITCOUNT.Text = "";
            R10.Visible = false;
            lblR8.Text = "Please exit case from the EBP. If other caseworkers are collaboratively providing these services with you to this case, please ensure they are no longer providing these services before exiting the case from the EBP.";
        }
        else
        {
            R9.Visible = false;
            txtMISSEDVISITCOUNT.Text = "";
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }
            lblR8.Text = "";
        }
        
    }
    protected void rblINPERSONCONFIRMATIONYN_SelectedIndexChanged(object sender, EventArgs e)//R2

    {
        int i = rblINPERSONCONFIRMATIONYN.SelectedIndex;

        if (i == 0)
        {
            R3to7.Visible = false;
            chkCONTACTTYPEPROVREFSERV.Checked = false;
            chkCONTACTTYPEATTSCHEDAPP.Checked= false;
            chkCONTACTTYPECHECKFAMNOTINPERS.Checked= false;
            chkCONTACTTYPEOTHER.Checked = false;
            rfvtxtSPECIFYCONTACTTYPE.EnableClientScript = false;
            rfvtxtSPECIFYCONTACTTYPE.Enabled = false;
            othertxt.Visible = false;
            txtSPECIFYCONTACTTYPE.Text = "";
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }

            lblR2.Text = "Please complete a service log for each encounter you've had with the case. If you provide these services collaboratively with other caseworker(s), please ensure there is only one service log created for each session provided together.";
        }
        else if (i == 1)
        {
            lblR2.Text = "";
            R3to7.Visible = true;
            Validatecheckbox();
            if (checkR3to7true > 0)
            {
                R10.Visible = false;
                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
            }

        }
    }

    private void Validatecheckbox()
    {

        if (chkCONTACTTYPEPROVREFSERV.Checked)
        {
            checkR3to7true = 0;
        }
        else
        {
            checkR3to7true = 1;
        }
        if (chkCONTACTTYPEATTSCHEDAPP.Checked)
        {
            checkR3to7true = 0;
        }
        else
        {
            checkR3to7true = checkR3to7true+1;
        }
        if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked)
        {
            checkR3to7true = 0;
        }
        else
        {
            checkR3to7true = checkR3to7true + 1;
        }
        if (chkCONTACTTYPEOTHER.Checked)
        {
            checkR3to7true = 0;
        }
        else
        {
            checkR3to7true = checkR3to7true + 1;
        }

       // displayR10(checkR3to7true);
    }
  

   
    //private void displayR10(int checktrue)
    //{
    //    if (checktrue==100)
    //    {
    //        rfvtxtSPECIFYCONTACTTYPE.EnableClientScript = true;
    //        rfvtxtSPECIFYCONTACTTYPE.Enabled = true;
    //        othertxt.Visible = true;
    //    }
    //    else if (checktrue >= 1)
    //    {
    //        R10.Visible = true;
    //    }
    //}
    protected void txtMISSEDVISITCOUNT_TextChanged(object sender, EventArgs e)//R9
    {
        if (!(String.IsNullOrEmpty(txtMISSEDVISITCOUNT.Text)))
        {

            string str = txtMISSEDVISITCOUNT.Text;
            Regex includesNonNumeric = new Regex(@"[^\d]");
            if (includesNonNumeric.IsMatch(str))
            {
                R10.Visible = false;

                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
            }
            else if (!(str == "0"))
            {
                R10.Visible = true;
            }
            else
            {
                R10.Visible = false;
                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
            }
            
        }
        else
        {
            R10.Visible = false;
         
                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
        }
    } 

    //R3to7
   
    protected void chkCONTACTTYPEPROVREFSERV_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCONTACTTYPEPROVREFSERV.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPEATTSCHEDAPP.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPEOTHER.Checked == true)
        {

            R10.Visible = true;
        }
        else
        {
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }

        }
        

    }
    protected void chkCONTACTTYPEATTSCHEDAPP_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCONTACTTYPEATTSCHEDAPP.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPEPROVREFSERV.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPEOTHER.Checked == true)
        {

            R10.Visible = true;
        }
        else
        {
           // Validatecheckbox2();
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }
        }
       
    }
    protected void chkCONTACTTYPECHECKFAMNOTINPERS_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPEATTSCHEDAPP.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPEPROVREFSERV.Checked)
        {
            R10.Visible = true;
        }
        else if (chkCONTACTTYPEOTHER.Checked == true)
        {

            R10.Visible = true;
        }
        else
        {
           // Validatecheckbox3();
            R10.Visible = false;
            foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            {
                item.Selected = false;
            }
        }
    }
    protected void chkCONTACTTYPEOTHER_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCONTACTTYPEOTHER.Checked == true)
        {
            rfvtxtSPECIFYCONTACTTYPE.EnableClientScript = true;
            rfvtxtSPECIFYCONTACTTYPE.Enabled = true;
            othertxt.Visible = true;
            R10.Visible = true;
            txtSPECIFYCONTACTTYPE.Visible = true;
            //Validatecheckbox();
            //if (checkR3to7true > 0)
            //{
            //    R10.Visible = false;
            //    foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
            //    {
            //        item.Selected = false;
            //    }
            //}

        }
        //else if (chkCONTACTTYPEATTSCHEDAPP.Checked)
        //{
        //    R10.Visible = true;
        //}
        //else if (chkCONTACTTYPEPROVREFSERV.Checked)
        //{
        //    R10.Visible = true;
        //}
        //else if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked == true)
        //{

        //    R10.Visible = true;
        //}
        else
        {
                      
            rfvtxtSPECIFYCONTACTTYPE.EnableClientScript = false;
            rfvtxtSPECIFYCONTACTTYPE.Enabled = false;
            othertxt.Visible = false;
            txtSPECIFYCONTACTTYPE.Text = "";

            if (chkCONTACTTYPEATTSCHEDAPP.Checked)
            {
                R10.Visible = true;
            }
            else if (chkCONTACTTYPEPROVREFSERV.Checked)
            {
                R10.Visible = true;
            }
            else if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked == true)
            {
                R10.Visible = true;
            }
            else
            {
                R10.Visible = false;
                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
            }

        }
    }

    private void Validatecheckbox1()
    {

        if (chkCONTACTTYPEPROVREFSERV.Checked ==true)
        {
            vald1= 0;
        }
        else
        {
            Validatecheckbox2();
            Validatecheckbox3();
            vald1= 1;
            if ((vald3 == 1) && (vald2 == 1))
            {
                R10.Visible = false;
                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
            }
            else
            {
                R10.Visible = true;
            }
        }

        //if (chkCONTACTTYPEATTSCHEDAPP.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = checkR3to7true + 1;
        //}
        //if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = checkR3to7true + 1;
        //}
        //if (chkCONTACTTYPEOTHER.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = checkR3to7true + 1;
        //}

        // displayR10(checkR3to7true);
    }
    private void Validatecheckbox2()
    {

        //if (chkCONTACTTYPEPROVREFSERV.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = 1;
        //}
        if (chkCONTACTTYPEATTSCHEDAPP.Checked ==true)
        {
            vald2 = 0;
        }
        else
        {
            Validatecheckbox1();
            Validatecheckbox3();
            vald2 = 1;
            if ((vald1 == 1) && (vald3 == 1))
            {
                R10.Visible = false;
                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
            }
            else
            {
                R10.Visible = true;
            }
        }
        //if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = checkR3to7true + 1;
        //}
        //if (chkCONTACTTYPEOTHER.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = checkR3to7true + 1;
        //}

        // displayR10(checkR3to7true);
    }

    private void Validatecheckbox3()
    {

        //if (chkCONTACTTYPEPROVREFSERV.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = 1;
        //}
        //if (chkCONTACTTYPEATTSCHEDAPP.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = checkR3to7true + 1;
        //}
        if (chkCONTACTTYPECHECKFAMNOTINPERS.Checked==true)
        {
            vald3 = 0;
        }
        else
        {
            Validatecheckbox1();
            Validatecheckbox2();
            vald3 = 1;

            if ((vald1 == 1) && (vald2 == 1))
            {
                R10.Visible = false;
                foreach (ListItem item in rblABILITYSCHEDAPPYN.Items)
                {
                    item.Selected = false;
                }
            }
            else
            {
                R10.Visible = true;
            }
        }
        //if (chkCONTACTTYPEOTHER.Checked)
        //{
        //    checkR3to7true = 0;
        //}
        //else
        //{
        //    checkR3to7true = checkR3to7true + 1;
        //}

        // displayR10(checkR3to7true);
    }
}