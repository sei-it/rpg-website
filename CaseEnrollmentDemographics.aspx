﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseEnrollmentDemographics.aspx.cs" Inherits="CaseEnrollmentDemographics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
   <link rel="stylesheet" href="Styles/jquery-ui1_11_3.css" />
  <script type="text/javascript" src="Scripts/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="Scripts/jquery-ui1_11_3.js"></script>
<script type="text/javascript">
    (function ($) {
        $.extend($.datepicker, {

            // Reference the orignal function so we can override it and call it later
            _inlineDatepicker2: $.datepicker._inlineDatepicker,

            // Override the _inlineDatepicker method
            _inlineDatepicker: function (target, inst) {

                // Call the original
                this._inlineDatepicker2(target, inst);

                var beforeShow = $.datepicker._get(inst, 'beforeShow');

                if (beforeShow) {
                    beforeShow.apply(target, [target, inst]);
                }
            }
        });
    } (jQuery));

    $(function () {

        $(".selectchange").change(function () {
            $("#<%=txtDOB.ClientID%>").val("");
        });
        $("#<%=txtDOB.ClientID%>").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1930:2050',
            beforeShow: customRange
        });

        function customRange(input) {

            if ($("#<%=hfEditMode.ClientID %>").val == "1") {  //disable datepicker in view mode
                return false;
            }
            if ($("#<%=hfEnrollDate.ClientID%>").val() == null || $("#<%=hfEnrollDate.ClientID%>").val() == "") {
                alert("EBP Enrollment date can not empty!");
                return false;
            }

            var enrolldate = $("#<%=hfEnrollDate.ClientID%>").val().split("/");
            var enrollday, enrollyear, enrollmonth;
            var maxdate, mindate, topyear, bottomyear;

            for (var i in enrolldate) {
                if (i == 0)
                    enrollmonth = enrolldate[i];
                if (i == 1)
                    enrollday = enrolldate[i];
                if (i == 2)
                    enrollyear = enrolldate[i];
            }

            var Persontype = $('#<%= rblPersontype.ClientID %> input:checked').val();

            if (Persontype == null) {
                alert("Select person type first.");
                return false;
            }

            if (Persontype == "1") {    //Adult:  01/01/1930 =< DOB <= EnrollDate - 15years

                maxdate = new Date(enrollyear - (15), enrollmonth - 1, enrollday);
                $("#<%=txtDOB.ClientID%>").datepicker("option", "maxDate", maxdate);
            }
            else {                      //Child:  Enrollday -20 <= DOB <= [today's date] + 9 months 
                var todaydate = new Date($("#<%=hfTodayDate.ClientID%>").val());
                var thismonth = todaydate.getMonth() + 1;
                var thisyear = todaydate.getYear() + 1900;
                var thisday = todaydate.getDate();
                var newMonth;
                mindate = new Date(enrollyear - (20), enrollmonth - 1, enrollday);
                $("#<%=txtDOB.ClientID%>").datepicker("option", "minDate", mindate);
                //var todaydate = new Date();

                if (thismonth + 9 > 12) {
                    newMonth = (thismonth + 9) - 12;
                    thisyear = thisyear + 1;
                }
                else {
                    newMonth = thismonth + 9;
                }

                maxdate = new Date(thisyear, newMonth, 0);
                // $("#<%=txtDOB.ClientID%>").datepicker("option", "maxDate", maxdate);
                $("#<%=txtDOB.ClientID%>").datepicker("option", "maxDate", "+9m");
            }


        }
    });
  </script>
</asp:Content>
 
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Add Case Member Information</h1>
        <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal></h2>
    <div class="formLayout">
 <p>
 
      <asp:Label ID="lblDateError" runat="server" ForeColor="Red" Text=""></asp:Label>
      <br />
 </p>
	        <p>
		        <label for="txtIndid">Individual ID</label>
		        <asp:textbox id="txtIndid" runat="server" MaxLength="6" ></asp:textbox>
                 <asp:Label ID="lblError1" runat="server" ForeColor="Red" Text=""></asp:Label>
                 <asp:RequiredFieldValidator ID="ref" runat="server" ValidationGroup="Save" ControlToValidate="txtIndid" ErrorMessage="Individual ID" ForeColor="Red">Required!</asp:RequiredFieldValidator>
	
	            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtIndid" ErrorMessage="Input 6  alphanumeric characters" ValidationExpression="^[a-zA-Z0-9]{6}$" ForeColor="Red" ValidationGroup="Save"></asp:RegularExpressionValidator>
	       
            
            </p>
 


        	<p>
		<label for="txtFirstname">Individual Name</label>
		<asp:textbox id="txtFirstname" runat="server" MaxLength="25" ></asp:textbox>
	            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Save" ControlToValidate="txtFirstname" ErrorMessage="Individual Name" ForeColor="Red">Required!</asp:RequiredFieldValidator>
	</p>

             <p>
		<label for="rblGender">Gender

  <asp:RequiredFieldValidator 
        ID="reqGender" 
        runat="server" 
        ValidationGroup="Save" 
        ControlToValidate="rblGender" 
        ErrorMessage="Select Gender" 
        ForeColor="Red">Required!</asp:RequiredFieldValidator>
		</label>
		<asp:RadioButtonList id="rblGender" runat="server"  RepeatDirection="Vertical"  AutoPostBack="true" > 
		</asp:RadioButtonList><br />&nbsp;
	</p> 
      
     <p>
		<label for="rblPersontype">Person Type

    <asp:RequiredFieldValidator 
        ID="rfvPersontype" 
        runat="server" 
        ValidationGroup="Save" 
        ControlToValidate="rblPersontype" 
        ErrorMessage="Select Person type" 
        ForeColor="Red">Required!</asp:RequiredFieldValidator>

		</label>
		<asp:RadioButtonList id="rblPersontype" runat="server" CssClass="selectchange" 
             AutoPostBack="true"  RepeatDirection="Vertical" 
             onselectedindexchanged="rblPersontype_SelectedIndexChanged"  > 
		</asp:RadioButtonList><br />
	</p> 
     <p >
      <label for="txtDOB">Date of Birth
      <asp:RequiredFieldValidator 
        ID="rfvDOB" 
        runat="server" 
        ValidationGroup="Save" 
        ControlToValidate="txtDOB" 
        ErrorMessage="Date of birth day is required" 
        ForeColor="Red">Required!</asp:RequiredFieldValidator>
      
      
      </label>
       <asp:textbox id="txtDOB"  runat="server" ></asp:textbox>
         &nbsp;&nbsp;<asp:Label ID="lblDOBmess" runat="server" text="Format (mm/dd/yyyy)" ></asp:Label>
          <asp:Label ID="lblEnrollYear" runat="server" Visible="false"></asp:Label>
         <asp:Label ID="lblNowMonth" runat="server" Visible="false"></asp:Label>
         <asp:HiddenField ID="hfEnrollDate" runat="server" ></asp:HiddenField>
         <asp:HiddenField ID="hfEditMode" Value="0" runat="server" ></asp:HiddenField>
         <asp:HiddenField ID="hfTodayDate" Value="" runat="server" ></asp:HiddenField>
         </p>
	 <p>
		
         

       
       <%-- <asp:Label ID="lblDobMonth" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblDobDay" runat="server" Visible="false"></asp:Label>--%>
        <%-- <asp:RegularExpressionValidator ID="rexpvST" runat="server" ControlToValidate="txtDOB"
                        ErrorMessage="Please Enter date in  (mm/dd/yyyy) format" ForeColor="Red" ValidationExpression="^(((0[13578]|10|12)(/)(0[1-9]|[12][0-9]|3[01]))(/)(\d{4})|((0[469]|11)(/)([0][1-9]|[12][0-9]|30))(/)(\d{4})|((02)(/)(0[1-9]|1[0-9]|2[0-8])(/)(\d{4}))|((02)(/)(29)(/)([02468][048]00))|((02)(/)(29)(/)([13579][26]00))|((02)(/)(29)(/)([0-9][0-9][0][48]))|((02)(/)(29)(/)([0-9][0-9][2468][048]))|((02)(/)(29)(/)([0-9][0-9][13579][26])))$"
                        ValidationGroup="Save">*</asp:RegularExpressionValidator>--%>
 	</p>

     <p >


            <asp:label  id="hfEnrollDateErrorMsg" runat="server" ForeColor="red"></asp:label>
            <%--<asp:CompareValidator 
                ID="cmpMonth" 
                runat="server" 
                  ControlToValidate="ddlMonth" 
                    ErrorMessage="Must be less than or equal RPG Enrollment Date"
                    Operator="LessThanEqual"
                    ValidationGroup="Save" 
                    SetFocusOnError="True"
                     ForeColor="Red" 
                     Display="Dynamic" 
               Type="Date"
                >*

            </asp:CompareValidator>--%>
           

        </p>

   <%--     <p>
		<label for="rblGender">Gender

  <asp:RequiredFieldValidator 
        ID="reqGender" 
        runat="server" 
        ValidationGroup="Save" 
        ControlToValidate="rblGender" 
        ErrorMessage="Select Gender" 
        ForeColor="Red">Required!</asp:RequiredFieldValidator>
		</label>
		<asp:RadioButtonList id="rblGender" runat="server"  RepeatDirection="Vertical"  AutoPostBack="true" > 
		</asp:RadioButtonList><br />&nbsp;
	</p>   
     <p>
		<label for="rblPersontype">Person Type

    <asp:RequiredFieldValidator 
        ID="rfvPersontype" 
        runat="server" 
        ValidationGroup="Save" 
        ControlToValidate="rblPersontype" 
        ErrorMessage="Select Person type" 
        ForeColor="Red">Required!</asp:RequiredFieldValidator>

		</label>
		<asp:RadioButtonList id="rblPersontype" runat="server"  AutoPostBack="true" 
            OnSelectedIndexChanged="rblPersontype_SelectedIndexChanged"  RepeatDirection="Vertical"  > 
		</asp:RadioButtonList>
	</p> 
  --%>

  </div>
    <div class="formLayout">
    <label>Race</label> <br />
        (Mark all that apply)
	<p>
		<label for="chkRaceamerindoraknat"></label>
		<asp:checkbox id="chkRaceamerindoraknat" runat="server"   Text="American Indian or Alaskan Native"></asp:checkbox>
	</p>
	<p>
		<label for="chkRaceasian"></label>
		<asp:checkbox id="chkRaceasian" runat="server"   Text="Asian"></asp:checkbox>
	</p>
	<p>
		<label for="chkRaceblack"></label>
		<asp:checkbox id="chkRaceblack" runat="server"   Text="Black or African American"></asp:checkbox>
	</p>
	<p>
		<label for="chkRacenathiorothpacisle"></label>
		<asp:checkbox id="chkRacenathiorothpacisle" runat="server"   Text="Native Hawaiian or Other Pacific Islander"></asp:checkbox>
	</p>
	<p>
		<label for="chkRacewhite"></label>
		<asp:checkbox id="chkRacewhite" runat="server"   Text="White"></asp:checkbox>
	</p>
	<p>
		<label for="rblEthnicity">Ethnicity</label>
		<asp:RadioButtonList id="rblEthnicity" runat="server"  RepeatDirection="Vertical"  > 
		</asp:RadioButtonList>
	</p>
    </div>
    <div class="formLayout">
 
	<p>
		<label for="rblPrimaryhomelang">Primary Language Spoken at Home</label>
		<asp:RadioButtonList id="rblPrimaryhomelang" runat="server"  RepeatDirection="Vertical" AutoPostback="true"  OnSelectedIndexChanged="rblPrimaryhomelang_SelectedIndexChanged"> 
		</asp:RadioButtonList>
	</p>
	<p>
		<label for="txtPrimaryhomelangspec">If other than English or Spanish, please specify</label>
		<asp:textbox id="txtPrimaryhomelangspec" runat="server" TextMode="MultiLine" ></asp:textbox>
       
        <asp:RequiredFieldValidator ID="valtxtOther" runat="server" Enabled="false" ValidationGroup="Save"
          ControlToValidate="txtPrimaryhomelangspec" ForeColor="Red" ErrorMessage="If other than English or Spanish, please specify">Required</asp:RequiredFieldValidator>
        
        <asp:RegularExpressionValidator ID="regPrimaryhomelangspec" runat="server" ControlToValidate="txtPrimaryhomelangspec"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed. </asp:RegularExpressionValidator>
	</p>
        <p>

        </p>
    </div>
    <div class="formLayout">
     
    <%-- <p>
		<label for="rblPersontype">Person Type</label>
		<asp:RadioButtonList id="rblPersontype" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="rblPersontype_SelectedIndexChanged"  RepeatDirection="Vertical"  > 
		</asp:RadioButtonList>
	</p>--%>
	<p>
		<label for="rblCurrentresidence">Current Type of Residence</label>
		<asp:RadioButtonList id="rblCurrentresidence" runat="server"  RepeatDirection="Vertical"  AutoPostBack="true" OnSelectedIndexChanged="rblCurrentresidence_SelectedIndexChanged" > 
		</asp:RadioButtonList>
       
	</p>
    <p>

    <label for="txtCurrentresidencOther">If other type of residence, please specify</label>
		<asp:textbox id="txtCurrentresidencOther" runat="server" TextMode="MultiLine" ></asp:textbox>
       
         <asp:RequiredFieldValidator ID="rvfCurrentResidencOther" runat="server" Enabled="false" ValidationGroup="Save"
              ControlToValidate="txtCurrentresidencOther" ForeColor="Red" ErrorMessage="If other type of residence, please specify">Required</asp:RequiredFieldValidator>
        
        <asp:RegularExpressionValidator ID="regCurrentresidencOther" runat="server" ControlToValidate="txtCurrentresidencOther"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed. </asp:RegularExpressionValidator>
    
    </p>
    </div>
    <div id="divAdultOnly" runat="server">
    <div class="formLayout">
    

	<p>
		<label for="rblHighesteducation">Highest Education Level</label>
		<asp:RadioButtonList id="rblHighesteducation" runat="server"  RepeatDirection="Vertical"  > 
		</asp:RadioButtonList>
	</p>
    </div>
    <div class="formLayout">
	<p>
		<label for="rblIncomelevel">Annual Income (past 12 months)</label>
		<asp:RadioButtonList id="rblIncomelevel" runat="server"  RepeatDirection="Vertical"  > 
		</asp:RadioButtonList>
	</p>
        </div>
    <div class="formLayout">
        <label>Income Source <br />(Mark all that apply)</label>
	<p>
		<label for="chkIncomesourcewagesalary"></label>
		<asp:checkbox id="chkIncomesourcewagesalary" runat="server"   Text=" Wages/salary"></asp:checkbox>
	</p>
	<p>
		<label for="chkIncomesourcepubass"></label>
		<asp:checkbox id="chkIncomesourcepubass" runat="server"   Text="Public assistance "></asp:checkbox>
	</p>
	<p>
		<label for="chkIncomesourceretirpens"></label>
		<asp:checkbox id="chkIncomesourceretirpens" runat="server"   Text="Retirement/pension "></asp:checkbox>
	</p>
	<p>
		<label for="chkIncomesourcedisab"></label>
		<asp:checkbox id="chkIncomesourcedisab" runat="server"   Text="Disability "></asp:checkbox>
	</p>
	
	<p>
		<label for="chkIncomesourcenone"></label>
		<asp:checkbox id="chkIncomesourcenone" runat="server"   Text="None "></asp:checkbox>
	</p>
    <p>
		<label for="chkIncomesourceother"></label>
		<asp:checkbox id="chkIncomesourceother" runat="server" AutoPostBack="true"  OnCheckedChanged="chkIncomesourceother_CheckedChanged"   Text="Other "></asp:checkbox>
	</p>
         <p>

    <label for="txtIncomesourceother">If other income source, please specify</label>
            
		<asp:textbox id="txtIncomesourceother" runat="server" TextMode="MultiLine" ></asp:textbox>
              <asp:RequiredFieldValidator ID="rfvtxtIncomeSourceOther" runat="server" Enabled="false" ValidationGroup="Save"
              ControlToValidate="txtIncomesourceother" ForeColor="Red" ErrorMessage="If other income source, please specify">Required</asp:RequiredFieldValidator>
        
              
        <asp:RegularExpressionValidator ID="regIncomesourceother" runat="server" ControlToValidate="txtIncomesourceother"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed. </asp:RegularExpressionValidator>
    
    </p>
    </div>
    <div class="formLayout">
	<p>
		<label for="rblEmploymentstatus">Employment Status </label>
		<asp:RadioButtonList id="rblEmploymentstatus" runat="server"  RepeatDirection="Vertical"  > 
		</asp:RadioButtonList>
	</p>
    </div>
    <div class="formLayout">
	<p>
		<label for="rblRelationshipstatus">Relationship Status </label>
		<asp:RadioButtonList id="rblRelationshipstatus" runat="server"  RepeatDirection="Vertical"  > 
		</asp:RadioButtonList>
	</p>
    </div>
      </div>
    <div class="formLayout">
            <p>
                <asp:Button ID="btnSave" runat="server"  ValidationGroup="Save" Text="Save" onclick="btnSave_Click"  CausesValidation="true" />&nbsp;
                &nbsp;                
                
                <asp:Button ID="btnSaveandClose" Visible="false" runat="server" ValidationGroup="Save" Text="Save and Close" onclick="btnSaveandClose_Click"  CausesValidation="true" />&nbsp;&nbsp;&nbsp; 
                &nbsp;  

                <asp:Button ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click"  CausesValidation="false" />          
        </p>
        <p>
            <asp:Literal ID="litMessage" runat="server" ></asp:Literal>
          
        </p>          
            <asp:ValidationSummary ID="ValidationSummary1"  ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="You must enter a value in the following fields:" />
    </div>
</asp:Content>

