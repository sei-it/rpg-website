﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseClosure.aspx.cs" Inherits="CaseClosure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
             <script> 
                 $(function () {
                     $("#<%=txtRpgclosedate.ClientID%>").datepicker({
                         dateFormat: 'mm/dd/yy',
                         minDate: '01/01/2014',
                         maxDate: 'dateToday'
                     })
                  
             });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <h1>Close RPG Case</h1>
    <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal><br />
        RPG Enrollment Date: <asp:Literal ID="litEnrollmentDate" runat="server"></asp:Literal>

    </h2>
    <div  class="formLayout">
        	<p>
		         <label for="txtRpgclosedate">RPG Case Closure Date</label>
		         <asp:textbox id="txtRpgclosedate" runat="server" ></asp:textbox>

                 <asp:RequiredFieldValidator ID="rfvRpgclosedate" runat="server" ValidationGroup="Save" 
                     ControlToValidate="txtRpgclosedate" ForeColor="Red" ErrorMessage="Required RPG Case Closure Date">Required</asp:RequiredFieldValidator>
                 <asp:textbox id="txtDtCmp" runat="server" visible="false"></asp:textbox>

                 <asp:CompareValidator 
                     ID="cvCaseClose" 
                     runat="server"
                     ControlToValidate="txtRpgclosedate" 
                     ErrorMessage="RPG Case Closure Date must be greater than or equal to 01/01/2014"           
                     Operator="GreaterThanEqual" 
                     ValidationGroup="Save" 
                     SetFocusOnError="True"
                     ForeColor="Red" 
                     Display="Dynamic"  
                     ValueToCompare="01/01/2014" 
                     Type="Date">
                 </asp:CompareValidator><br />

                 <asp:CompareValidator 
                     ID="cvMaxCaseClose" 
                     runat="server"  
                     ValidationGroup="Save" 
                     SetFocusOnError="True" 
                     ErrorMessage="Not greater than current date"
                     Operator="LessThanEqual" 
                     ControlToValidate="txtRpgclosedate" 
                     ForeColor="Red" 
                     Type="date" 
                     Display="Dynamic">*</asp:CompareValidator><br />


                 <asp:CompareValidator 
                     ID="cvEbpExitDate" 
                     runat="server"  
                     ValidationGroup="Save" 
                     SetFocusOnError="True" 
                     ErrorMessage="RPG Case Closure Date must be greater than or equal to EBP Exit Date"
                     Operator="GreaterThanEqual" 
                     ControlToValidate="txtRpgclosedate" 
                     ForeColor="Red" 
                     Type="date" 
                     Display="Dynamic"></asp:CompareValidator>
  	            </p>
        <div style="height:10px;">&nbsp;</div>
 
    <p></p>
 
	            <p>
		            <label for="chkCompletedprogram">Reason for Case Closure   <br />(Mark all that apply)
                       <asp:Label ID="lblrequiredMess" runat="server" ForeColor="Red"></asp:Label>

		            </label>
		            <asp:checkbox id="chkCompletedprogram" runat="server"  OnCheckedChanged="chkCompletedprogram_CheckedChanged"  AutoPostBack="true"  Text="Successfully completed RPG program" ></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkFamilymove"></label>
		            <asp:checkbox id="chkFamilymove" runat="server"   Text="Family moved out of area "></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkUnabletolocate"></label>
		            <asp:checkbox id="chkUnabletolocate" runat="server"   Text="Unable to locate "></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkUNRESPONSIVE"></label>
		            <asp:checkbox id="chkUNRESPONSIVE" runat="server"   Text="Excessive missed appointments/unresponsive "></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkFamilydeclined"></label>
		            <asp:checkbox id="chkFamilydeclined" runat="server"   Text="Family declined further participation "></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkTRANSFERRED"></label>
		            <asp:checkbox id="chkTRANSFERRED" runat="server"   Text="Transferred to another service provider "></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkMiscorchilddeath"></label>
		            <asp:checkbox id="chkMiscorchilddeath" runat="server"   Text="Miscarriage or fetal/child death "></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkParentdeath"></label>
		            <asp:checkbox id="chkParentdeath" runat="server"   Text="Parental death "></asp:checkbox>
	            </p>
	            <p>
		            <label for="chkCasecloseother"></label>
		            <asp:checkbox id="chkCasecloseother" runat="server" OnCheckedChanged="chkCasecloseother_CheckedChanged" AutoPostBack="true"  Text="Other" ></asp:checkbox>
                    
	            </p>
	            <p>
		            <label for="txtSpecifyrpgcaseclose">If other reason, please specify</label>
		            <asp:textbox id="txtSpecifyrpgcaseclose" enabled="false" TextMode="MultiLine"  runat="server" ></asp:textbox>
                     <asp:RequiredFieldValidator ID="rfvtxtSpecifyrpgcaseclose" runat="server" Enabled="false" ValidationGroup="Save"
              ControlToValidate="txtSpecifyrpgcaseclose" ForeColor="Red" ErrorMessage="If other reason, please specify">Required</asp:RequiredFieldValidator>
        
                   <%-- <asp:Label ID="lbltxtOther" runat="server" ForeColor="Red"></asp:Label>--%>
                     <asp:RegularExpressionValidator ID="regSpecifyrpgcaseclose" runat="server" ControlToValidate="txtSpecifyrpgcaseclose"
                        ValidationGroup="Save" ValidationExpression="^[\s\S]{0,240}$" ErrorMessage="Maximum 240 characters are allowed."
                        ForeColor="Red"> Maximum 240 characters are allowed.
                    </asp:RegularExpressionValidator>
                    
    </div>

  

         <div class="formLayout">
   
  <p >
        <Label id="Label1" for="rblchildAdult" runat="server">Is the focal child <asp:Label ID="lblFocalChild" runat="server"></asp:Label>  currently in the care of the family functioning adult <asp:label ID="lblfamilyAdult" runat="server"></asp:label>?

             <asp:RequiredFieldValidator ID="rfvChildAdultCare" runat="server" ValidationGroup="Save" ControlToValidate="rblchildAdult" 
                 ErrorMessage="Is the focal child currently in the care of the family functioning adult?" ForeColor="Red">Required!</asp:RequiredFieldValidator>
	
        </Label>
        
        <asp:RadioButtonList id="rblchildAdult" runat="server"  RepeatDirection="Horizontal"  AutoPostBack="true" > 
		</asp:RadioButtonList><br />&nbsp;
      </p>
          </div>
  
           <p>
                <asp:Button ID="btnSave" runat="server" ValidationGroup="Save" Text="Save" onclick="btnSave_Click" CausesValidation="true" />&nbsp;
                <asp:Button ID="btnSaveClose" runat="server" Visible="false" ValidationGroup="Save" Text="Save and Close" onclick="btnSaveClose_Click" CausesValidation="true" />&nbsp;        
                <asp:Button ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp;         
                <asp:Button ID="btnDelete" runat="server" Text="Delete" Visible="false" onclick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this record')" />&nbsp;&nbsp;&nbsp; 
           </p> 
         <asp:ValidationSummary ID="ValidationSummary1"  ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="You must enter a value in the following fields:" />
   
            <p>
            <asp:Literal ID="litMessage" Visible="true" runat="server"></asp:Literal>
                 <asp:Label Id="lblMessage" runat="server" ForeColor="Red"></asp:Label>
          
        </p>
</asp:Content>

