﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseConfirmCaseActivity.aspx.cs" Inherits="CaseConfirmCaseActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <script type="text/javascript">


        function allowonlynumbers() {
            if (event.keyCode >= 48 && event.keyCode <= 57) {
                return true;
            }

            else {
                alert('Only numbers can be entered.'); return false;
            }

        }




        $("#txtMISSEDVISITCOUNT").change(function () {
            $(".formLayout1").hide();
            var myValue = $(this).val();
            if (myValue <= 1) {
                $(".formLayout1").show();
            }
           
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
         <h1>Confirm Case Activity</h1>
    <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal></h2>
        <p>
            <asp:Literal ID="litMessagePermissions" runat="server"></asp:Literal>
          
        </p>
    <div class="formLayout">
 
        	<p>
		        <label for="rblCONTACTCONFIRMATIONYN">Have you had any contact with the 
                    <asp:Label ID="R1CaseSurname" runat="server" ></asp:Label> case, for which you provide 
                    <asp:Label ID="R1EBPName" runat="server" > </asp:Label> services, since
                    <asp:Label ID="R1LastSerDate" runat="server" > </asp:Label>?<br />
                     (Note: If you provide this EBP collaboratively with other caseworker(s), this date is the last date a service log was 
                    created for the case and may not reflect the last date you provided these services directly. 
                    If you have not provided any services to the case, this date is the date of the case’s enrollment into the EBP.) (check only one)
		        <asp:RequiredFieldValidator ID="rfvCONTACTCONFIRMATIONYN" runat="server" ValidationGroup="Save" 
                    ErrorMessage="Required have you had any contact "
                     ControlToValidate="rblCONTACTCONFIRMATIONYN" ForeColor="Red">Required</asp:RequiredFieldValidator>        
               
                     </label>
		        <asp:RadioButtonList id="rblCONTACTCONFIRMATIONYN" runat="server"  RepeatDirection="Vertical"  AutoPostBack="true" OnSelectedIndexChanged="rblCONTACTCONFIRMATIONYN_SelectedIndexChanged" > 
		        <asp:ListItem Value="1">Yes</asp:ListItem>
		        <asp:ListItem Value="0">No</asp:ListItem>
		        </asp:RadioButtonList>
	        </p>
        </div>
 <div class="formLayout" id="R2" runat="server" Visible="false" >
	        <p>
		        <label for="rblINPERSONCONFIRMATIONYN">Did you provide in-person services to the 
                    <asp:Label ID="R2CaseSurname" runat="server" > </asp:Label> case since 
                    <asp:Label ID="R2LastSerDate" runat="server" > </asp:Label>?<br /> (check only one)</label>
		        <asp:RadioButtonList id="rblINPERSONCONFIRMATIONYN" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="rblINPERSONCONFIRMATIONYN_SelectedIndexChanged" RepeatDirection="Vertical"  > 
		        <asp:ListItem Value="1">Yes</asp:ListItem>
		        <asp:ListItem Value="0">No</asp:ListItem>
		        </asp:RadioButtonList>

                
	        </p>
     
      
            <asp:label ID="lblR2" ForeColor="Red" runat="server" Text=""></asp:label>
          
        
      </div>
       <div class="formLayout" id="R3to7" runat="server" Visible="false">
            <p>What contact did you have with the
              <asp:Label ID="R3to7CaseSurname" runat="server" > </asp:Label> case?<br /> (check all that apply)</p>

	        <p>
		        <label for="chkCONTACTTYPEPROVREFSERV"  > </label>
		        <asp:checkbox id="chkCONTACTTYPEPROVREFSERV" runat="server"  AutoPostBack="true"  OnCheckedChanged="chkCONTACTTYPEPROVREFSERV_CheckedChanged" Text="Provided referral services"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkCONTACTTYPEATTSCHEDAPP">  </label>
		        <asp:checkbox id="chkCONTACTTYPEATTSCHEDAPP" runat="server" AutoPostBack="true" OnCheckedChanged="chkCONTACTTYPEATTSCHEDAPP_CheckedChanged"   Text="Tried to schedule appointment"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkCONTACTTYPECHECKFAMNOTINPERS"  > </label>
		        <asp:checkbox id="chkCONTACTTYPECHECKFAMNOTINPERS" runat="server" AutoPostBack="true" OnCheckedChanged="chkCONTACTTYPECHECKFAMNOTINPERS_CheckedChanged"  Text="Checked in with case by phone/email/other mode of contact not in person"></asp:checkbox>
	        </p>
	        <p>
		        <label for="chkCONTACTTYPEOTHER">   </label>
		        <asp:checkbox id="chkCONTACTTYPEOTHER" runat="server" AutoPostBack="true"  OnCheckedChanged="chkCONTACTTYPEOTHER_CheckedChanged" Text="Other"></asp:checkbox>
	        </p>
	        <p id="othertxt" runat="server" Visible="false">
		        <label for="txtSPECIFYCONTACTTYPE">If other, please specify 
             <asp:RequiredFieldValidator ID="rfvtxtSPECIFYCONTACTTYPE" runat="server" Enabled="false" ValidationGroup="Save" SetFocusOnError="true"
              ControlToValidate="txtSPECIFYCONTACTTYPE" ForeColor="Red" ErrorMessage="If other reason, please specify">Required</asp:RequiredFieldValidator>
       

		        </label>
		        <asp:textbox id="txtSPECIFYCONTACTTYPE" runat="server" ></asp:textbox>
	        </p>
           <p></p>
        </div>
       <div class="formLayout" id="R8" runat="server" Visible="false">
	        <p>
		        <label for="rblREASONNOCASECONTACTFK">What was the main reason for there being no contact with the 
                 <asp:Label ID="R8CaseSurname" runat="server" > </asp:Label> case since 
                  <asp:Label ID="R8LastSerDate" runat="server" > </asp:Label>?(check only one)</label>
		        <asp:RadioButtonList id="rblREASONNOCASECONTACTFK" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblREASONNOCASECONTACTFK_SelectedIndexChanged"  RepeatDirection="Vertical"  > 
		        </asp:RadioButtonList>
	        </p>
             <asp:label ID="lblR8" ForeColor="Red" runat="server" Text=""></asp:label>
        </div>

       <div class="formLayout" id="R9" runat="server" Visible="false">
	        <p>
		        <label for="txtMISSEDVISITCOUNT">How many times did a scheduled visit not occur since 
                    <asp:Label ID="R9LastSerDate" runat="server" > </asp:Label>
                    <asp:Label ID="R9case1" runat="server" > </asp:Label>
                    <asp:Label ID="R9FocalEBp" runat="server" > </asp:Label>
                    <asp:Label ID="R9service" runat="server" > </asp:Label>
                    <asp:Label ID="R9EbpEnroll" runat="server" > </asp:Label>
                    ?</label>
		        <asp:textbox id="txtMISSEDVISITCOUNT" runat="server" AutoPostBack="true" onkeypress="return allowonlynumbers();"   OnTextChanged="txtMISSEDVISITCOUNT_TextChanged" ></asp:textbox>
                <asp:RangeValidator ID="rangeMISSEDVISITCOUNT"  ValidationGroup="Save" runat="server" 
                        ForeColor="Red" ErrorMessage="scheduled visit count must be greater than 0 and<br/> less than 1000" ControlToValidate="txtMISSEDVISITCOUNT" 
                        MaximumValue="999" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                <%--<asp:RegularExpressionValidator id="RegularExpressionValidatorcount" 
                 ControlToValidate="txtMISSEDVISITCOUNT"
                 ValidationExpression="^\d+"                
                 ErrorMessage="Only Numbers"
                 EnableClientScript="true"
                 ValidationGroup="Save"
                   
              runat="server"></asp:RegularExpressionValidator>--%>
                <%--   <asp:RegularExpressionValidator ID="rgfldvalidator" ControlToValidate="txtMISSEDVISITCOUNT"
runat="server" ErrorMessage="Please enter the numbers only" ValidationExpression="^[0-9]*\.?[0-9]+$">
</asp:RegularExpressionValidator>--%>
	        </p>
           <p></p>
                </div>
        <div class="formLayout" id="R10" runat="server" visible="false">
	        <p>
		        <label for="rblABILITYSCHEDAPPYN">Were you able to schedule an appointment with the
                 <asp:Label ID="R10LastSerDate" runat="server" > </asp:Label> case for an in-person session? (check only one)</label>
		        <asp:RadioButtonList id="rblABILITYSCHEDAPPYN" runat="server"  RepeatDirection="Vertical"  > 
		        <asp:ListItem Value="1">Yes</asp:ListItem>
		        <asp:ListItem Value="0">No</asp:ListItem>
		        </asp:RadioButtonList>
	        </p>
        </div>
  
    <div class="formLayout">
                  <p>
                <asp:Button ID="btnSave" runat="server" Text="Save"  ValidationGroup="Save" onclick="btnSave_Click" />&nbsp;
                <asp:Button ID="btnSaveClose" runat="server" Text="Save And Close"  Visible="false" />&nbsp;                  
                  <asp:Button 
                    ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" />&nbsp;&nbsp;&nbsp; 
          
           
        </p>
            <p>
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
          
        </p>

        </div>
</asp:Content>

