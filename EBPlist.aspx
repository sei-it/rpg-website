﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EBPlist.aspx.cs" Inherits="EBPlist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>EBP List</h1>
    <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal>
    </h2><p></p>
    <div>
            <asp:Button ID="btnNew" runat="server"   Text="Enroll Case in New EBP" OnClick="btnNew_Click" /></div><p></p>
     <p>
         <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
          
    </p>

         <asp:GridView ID="grdVwList" runat="server"   CssClass="gridTbl" 
            AutoGenerateColumns="False"     AllowPaging="false"  
            AllowSorting="True" OnRowCommand="grdVwList_RowCommand"  OnRowDataBound="grdVwList_RowDataBound"  >
            <Columns>
            		<asp:BoundField DataField="CaseEBPID" HeaderText="ID" Visible="false" DataFormatString="{0:g}">
		            <HeaderStyle Width="50px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="EBPName" HeaderText="EBP Name" DataFormatString="{0:g}">
		            <HeaderStyle Width="200px"></HeaderStyle>
		            </asp:BoundField>



                      <asp:TemplateField>
                       <HeaderTemplate>   
                          Case Members Enrolled in EBP Services                    
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:Repeater ID="repCaseMembers" runat="server">
                                    <HeaderTemplate>
                                        <ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li><%# Eval("FIRST_NAME") %></li>
                                    </ItemTemplate> 
                                    <FooterTemplate></ul></FooterTemplate>                                    
                                </asp:Repeater>                         
                            </ItemTemplate>
                           <HeaderStyle width="150px" />
                        </asp:TemplateField>    
		            <asp:BoundField DataField="EBP_ENROLL_DATE" HeaderText="Enrollment Date" DataFormatString="{0:MM/dd/yyyy}">
                        		            <HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="EBP_EXIT_DATE" HeaderText="Exit Date" DataFormatString="{0:MM/dd/yyyy}">
                        		            <HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>

                   
                     <asp:TemplateField>
                       <HeaderTemplate> 
                              Manage EBP Enrollment and Exit                    
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit2" runat="server" Text="Manage" CommandName="Begin" visible=true  CssClass="otherstyle" CommandArgument='<%# Eval("CaseEBPID") %>'></asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="250px" />
                        </asp:TemplateField>

                      <asp:TemplateField>
                       <HeaderTemplate>   
                           Service Logs                    
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:Repeater ID="repServiceLogs" runat="server">
                                    <HeaderTemplate>
                                        <ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li><asp:HyperLink ID="Button1" Text='<%# Eval("DATE_OF_SERVICE", "{0:MM/dd/yyyy}") %>'  NavigateUrl='<%# String.Format("~/ServiceLogEdit.aspx?lngPkID={0}&intCaseID={1}&intEBPEnrollID={2}", Eval("ServiceID"),Eval("CaseID_FK"),Eval("CaseEBPID_FK"))%>'    runat="server" /></li>
                                    </ItemTemplate> 
                                    <FooterTemplate></ul></FooterTemplate>                                    
                                </asp:Repeater>                         
                            </ItemTemplate>
                           <HeaderStyle width="150px" />
                        </asp:TemplateField>                 
                  
                        <asp:TemplateField>
                           <HeaderTemplate>Create Service Log                   
                            </HeaderTemplate>                        
                                <ItemTemplate>               
<%--                                    <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="ServiceLogs" visible=true   CommandArgument='<%# Eval("CaseID_FK") %>'>View</asp:LinkButton>--%>
                                    <asp:LinkButton ID="imgBtnCreate" runat="server" CommandName="CreateSL" visible=true   CommandArgument='<%# Eval("CaseEBPID") %>'>Create</asp:LinkButton>                                                          
                                </ItemTemplate>
                               <HeaderStyle width="20px" />
                            </asp:TemplateField>  

                 <asp:TemplateField>
                <HeaderTemplate>
                                   
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnDelete" runat="server" CommandName="Delete" Visible="false" CommandArgument='<%# Eval("CaseEBPID") %>'>Delete</asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle Width="50px" />
            </asp:TemplateField>
            </Columns>
        </asp:GridView>
   
</asp:Content>

