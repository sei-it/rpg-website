﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CaseWorker.aspx.cs" Inherits="CaseWorker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <script>
         $(function () {
             $("#<%=txtCasewEndDate.ClientID%>").datepicker({
                 dateFormat: 'mm/dd/yy',
                 minDate: '01/01/2014',
                 maxDate: 'dateToday'
             })

         });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <h1>Manage Caseworker</h1>
     <h2>Case ID: <asp:Literal ID="litCaseID" runat="server"></asp:Literal><br />
        RPG Case Surname: <asp:Literal ID="litSurname" runat="server"></asp:Literal></h2>

     <div class="formLayout">
 
	    <p>
		    <label for="txtCasewName">Caseworker Name</label>
		    <asp:textbox id="txtCasewName" runat="server" Enabled="false" ></asp:textbox>
             

             </p>
	     <p >
		    <label for="txtCasewEmail">Caseworker Email</label>
		    <asp:textbox id="txtCasewEmail" runat="server" Enabled="false" ></asp:textbox>
	    </p>
	    <p>
		    <label for="txtCasewPhone">Caseworker Phone</label>
            
		    <asp:textbox id="txtCasewPhone"  runat="server" Enabled="false" ></asp:textbox>
             </p>
	    <p>
		    <label for="txtCasewStartDate">Caseworker Start Date</label>
		    <asp:textbox id="txtCasewStartDate" runat="server"  Enabled="false"></asp:textbox>
            </p>
         <p>

              <label for="txtCasewEndDate">Caseworker End Date</label>
		    <asp:textbox id="txtCasewEndDate" runat="server" ></asp:textbox>
              <asp:RequiredFieldValidator ID="rvfCasewEndDate" runat="server" 
                  ErrorMessage="Caseworker End Date is required" ForeColor="Red"   SetFocusOnError="True"
                  ControlToValidate="txtCasewEndDate" ValidationGroup="Save">Required!</asp:RequiredFieldValidator>
             </p>
         <p>
              <asp:CompareValidator 
                     ID="cmpCWStartdate" 
                     ControlToCompare="txtCasewStartDate" 
                     ControlToValidate="txtCasewEndDate" 
                     Type="Date" 
                     Operator="GreaterThanEqual" 
                     ForeColor="Red" 
                     ValidationGroup="Save"
                     ErrorMessage="Caseworker End Date must be greater than or equal to Caseworker Start Date" 
                     runat="server">Caseworker End Date must be greater than or equal to Caseworker Start Date</asp:CompareValidator><br />
           
                 <asp:CompareValidator 
                     ID="cmpCWMinEndDate" 
                     runat="server"
                     ControlToValidate="txtCasewEndDate"  
                     ErrorMessage="Caseworker End Date must be less than or equal to current date"           
                     Operator="LessThanEqual" 
                     ValidationGroup="Save" 
                     SetFocusOnError="True"
                     ForeColor="Red" 
                     Display="Dynamic" 
                     Type="Date">Caseworker End Date must be less than or equal to current date</asp:CompareValidator><br />
	  
         </p>
          <p>
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save"  onclick="btnSave_Click" />&nbsp;
              <asp:Button  ID="btnClose" runat="server" Text="Close" onclick="btnClose_Click" CausesValidation="False" />
           </p>
         <p>

             <asp:ValidationSummary id="vsum1" runat="server" ForeColor="Red" ValidationGroup="Save" HeaderText="Please fill in the correct dates for required fields."></asp:ValidationSummary>
          
         </p>
      </div>
</asp:Content>

