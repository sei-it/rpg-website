﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EBPlist : System.Web.UI.Page
{
    int intCaseID;
    UserInfo oUI;
    int? intUserGranteeID;
    int Granteeid;
    int GranteeContactId;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Page.Request.QueryString["intCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
                oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
                intUserGranteeID = oUI.intGranteeID;
                if (Page.Request.QueryString["GranteeContactId"] != null)
                {
                    GranteeContactId = Convert.ToInt32(Page.Request.QueryString["GranteeContactId"]);
                }
            }
        }
       // btnNew.Visible = false;
        DisableBtnSave();
         
        if (!IsPostBack)
        {
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }

       
    }

    private void DisableBtnSave()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var qryFlag = from Flag in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                          where Flag.CaseNoId_FK == intCaseID
                          select Flag;             
            foreach (var oCase in qryFlag)
            {
                if (oCase.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED == true)
                {
                    btnNew.Visible = true;
                    //if (oUI.intUserRoleID == 3) // added on 27th june
                    //{
                    //    btnNew.Visible = true;
                    //}
                    //else
                    //{
                    //    btnNew.Visible =false;
                    //}
                }               
            }

            // if not Case member added EBP new button should be disabled

            //var oCaseNEW = (from Flag in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
            //              where Flag.CaseNoId_FK == intCaseID
            //             select new { Flag.IND_ID });

            C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASE oCaseNEW = (from c in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                                                                 where c.CaseNoId_FK == intCaseID
                                                   select c).FirstOrDefault();

            if (oCaseNEW != null)
            {
                btnNew.Visible = true;
            }
            else
            {
                btnNew.Visible = false;
            }

            //----------------------------------------------------------------//
            var oPages = from pg in db.E_CaseClosures
                         where pg.CaseNoId_FK == intCaseID
                         select pg;
            foreach (var oCase in oPages)
            {
                if (oCase.CASE_CLOSE_COMPLETED == true)
                {
                    if (oUI.intUserRoleID == 3) // added on 27th june
                    {
                        btnNew.Visible = true;
                    }
                    else
                    {
                        btnNew.Visible = false;
                    }
                   
                }
            }

        }
    }


    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
            }

               var vCases = db.getEBPsForCaseID(intCaseID);

            
                var qryFlag = from Flag in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                              where Flag.CaseNoId_FK == intCaseID
                              select Flag;

                foreach (var oCase in qryFlag)
                {
                    if (oCase.IDENTIFY_CASE_MEM_RELATIONSHIP_COMPLETED == true)
                    {
                            grdVwList.DataSource = vCases;
                            grdVwList.DataBind();                       
                        
                    }
                    else
                    {
                        lblMessage.Text = "Before enrolling the case in EBPs please complete the Identify Case Member Relationships information";
                        btnNew.Visible = false;
                    }

               }

           
           // grdVwList.DataSource = vCases;
            //grdVwList.DataBind();
        }

    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }

        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        if (((this.ViewState["GranteeContactId"] != null)))
        {
            GranteeContactId = Convert.ToInt32(this.ViewState["GranteeContactId"]);
        }
    }
    protected override object SaveViewState()
    {

        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["oUI"] = oUI;
        this.ViewState["GranteeContactId"] = GranteeContactId;
        return (base.SaveViewState());
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("EBPEdit.aspx?intCaseID="+ intCaseID );
    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Begin")
        {
            int intResponseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("EBPEdit.aspx?intCaseID=" + intCaseID + "&lngPkID=" + intResponseID);

        }

        if (e.CommandName == "CreateSL")
        {
            int TopicFreez = 1;
            int intEBPEnrollID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("ServiceLogAdd.aspx?intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&TopicFreez=" +TopicFreez+ "&lngPkID=0");

        }

        if (e.CommandName == "Delete")
        {
            int intEBPEnrollID = Convert.ToInt32(e.CommandArgument);
             //--------------------B_CASE_ENROLLMENT---------------//
            using (DataClassesDataContext db = new DataClassesDataContext())
            {

                F_ENROLLMENT_EBP oCase1 = (from c in db.F_ENROLLMENT_EBPs
                                          where c.CaseEBPID == intEBPEnrollID
                                           select c).FirstOrDefault();
                if ((oCase1 != null))
                {
                    db.F_ENROLLMENT_EBPs.DeleteOnSubmit(oCase1);
                }
                db.SubmitChanges();

               
                           
                GranteeDeletefrom_F_ENROLLMENT(intEBPEnrollID);
                GranteeDeletefrom_F_EBPCaseWorkers_AssignedList(intEBPEnrollID);
                GranteeDeletefrom_G_SERVICE_LOG(intEBPEnrollID);
                GranteeDeletefrom_G_ServiceLogCaseMembers(intEBPEnrollID);
                GranteeDeletefrom_F_EBP_ParticipantRatings(intEBPEnrollID);
                GreanteeDeletefrom_N_SubstanceAbuseMatrix(intEBPEnrollID);

                GreanteeDeletefrom_O_ParentingSkillsMatrix(intEBPEnrollID);
                GreanteeDeletefrom_P_PersonalDevelopmentMatrix(intEBPEnrollID);
                GreanteeDeletefrom_Q_DevelopmentEducationMatrix(intEBPEnrollID);
                GreanteeDeletefrom_R_AdultSubstanceAbuseMatrix(intEBPEnrollID);
                GreanteeDeletefrom_S_OtherFamilySubAbuseMatrix(intEBPEnrollID);
               
                       
                }

                Response.Redirect("EBPlist.aspx?intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID);

           
        }
    }

    private void DeleteEBPName_ServiceLog()
    {
       
                       
    }
    //------------------------Delete---------------------------------------------------//

    //------------------deletee from S_OtherFamilySubAbuseMatrix--------------------------//
    private void GreanteeDeletefrom_S_OtherFamilySubAbuseMatrix(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase27 = (from p in db.vw_deleteFrom_S_OtherFamilySubAbuseMatrixes
                           where p.CaseEBPID_FK == CaseEBPID_FK
                           select new { p.ServiceID_FK });
            if ((oCase27 != null))
            {
                foreach (var var10 in oCase27)
                {
                    if (!(var10.ServiceID_FK == null))
                    {
                        deleteFrom_S_OtherFamilySubAbuseMatrixes(Convert.ToInt32(var10.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_S_OtherFamilySubAbuseMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            S_OtherFamilySubAbuseMatrix oCase28 = (from c in db.S_OtherFamilySubAbuseMatrixes
                                                   where c.ServiceID_FK == ServiceID_FK
                                                   select c).FirstOrDefault();
            if ((oCase28 != null))
            {
                db.S_OtherFamilySubAbuseMatrixes.DeleteOnSubmit(oCase28);
            }
            db.SubmitChanges();
        }
    }



    //------------------deletee from R_AdultSubstanceAbuseMatrix--------------------------//
    private void GreanteeDeletefrom_R_AdultSubstanceAbuseMatrix(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase25 = (from p in db.vw_deleteFrom_R_AdultSubstanceAbuseMatrixes
                           where p.CaseEBPID_FK == CaseEBPID_FK
                           select new { p.ServiceID_FK });
            if ((oCase25 != null))
            {
                foreach (var var9 in oCase25)
                {
                    if (!(var9.ServiceID_FK == null))
                    {
                        deleteFrom_R_AdultSubstanceAbuseMatrixes(Convert.ToInt32(var9.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_R_AdultSubstanceAbuseMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            R_AdultSubstanceAbuseMatrix oCase26 = (from c in db.R_AdultSubstanceAbuseMatrixes
                                                   where c.ServiceID_FK == ServiceID_FK
                                                   select c).FirstOrDefault();
            if ((oCase26 != null))
            {
                db.R_AdultSubstanceAbuseMatrixes.DeleteOnSubmit(oCase26);
            }
            db.SubmitChanges();
        }
    }

    //------------------deletee from Q_DevelopmentEducationMatrix--------------------------//


    private void GreanteeDeletefrom_Q_DevelopmentEducationMatrix(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase23 = (from p in db.vw_deleteFrom_Q_DevelopmentEducationMatrixes
                           where p.CaseEBPID_FK == CaseEBPID_FK
                           select new { p.ServiceID_FK });
            if ((oCase23 != null))
            {
                foreach (var var8 in oCase23)
                {
                    if (!(var8.ServiceID_FK == null))
                    {
                        deleteFrom_Q_DevelopmentEducationMatrixes(Convert.ToInt32(var8.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_Q_DevelopmentEducationMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Q_DevelopmentEducationMatrix oCase24 = (from c in db.Q_DevelopmentEducationMatrixes
                                                    where c.ServiceID_FK == ServiceID_FK
                                                    select c).FirstOrDefault();
            if ((oCase24 != null))
            {
                db.Q_DevelopmentEducationMatrixes.DeleteOnSubmit(oCase24);
            }
            db.SubmitChanges();
        }
    }


    //------------------deletee from P_PersonalDevelopmentMatrix--------------------------//

    private void GreanteeDeletefrom_P_PersonalDevelopmentMatrix(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase21 = (from p in db.vw_deleteFrom_P_PersonalDevelopmentMatrixes
                           where p.CaseEBPID_FK == CaseEBPID_FK
                           select new { p.ServiceID_FK });
            if ((oCase21 != null))
            {
                foreach (var var7 in oCase21)
                {
                    if (!(var7.ServiceID_FK == null))
                    {
                        deleteFrom_P_PersonalDevelopmentMatrixes(Convert.ToInt32(var7.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_P_PersonalDevelopmentMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            P_PersonalDevelopmentMatrix oCase22 = (from c in db.P_PersonalDevelopmentMatrixes
                                                   where c.ServiceID_FK == ServiceID_FK
                                                   select c).FirstOrDefault();
            if ((oCase22 != null))
            {
                db.P_PersonalDevelopmentMatrixes.DeleteOnSubmit(oCase22);
            }
            db.SubmitChanges();
        }
    }






    //------------------deletee from O_ParentingSkillsMatrix--------------------------//

    private void GreanteeDeletefrom_O_ParentingSkillsMatrix(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase19 = (from p in db.vw_deleteFrom_O_ParentingSkillsMatrixes
                           where p.CaseEBPID_FK == CaseEBPID_FK
                           select new { p.ServiceID_FK });
            if ((oCase19 != null))
            {
                foreach (var var6 in oCase19)
                {
                    if (!(var6.ServiceID_FK == null))
                    {
                        deleteFrom_O_ParentingSkillsMatrixes(Convert.ToInt32(var6.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();

        }
    }

    private void deleteFrom_O_ParentingSkillsMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            O_ParentingSkillsMatrix oCase20 = (from c in db.O_ParentingSkillsMatrixes
                                               where c.ServiceID_FK == ServiceID_FK
                                               select c).FirstOrDefault();
            if ((oCase20 != null))
            {
                db.O_ParentingSkillsMatrixes.DeleteOnSubmit(oCase20);
            }
            db.SubmitChanges();
        }
    }

    //------------------deletee from N_SubstanceAbuseMatrix--------------------------//

    private void GreanteeDeletefrom_N_SubstanceAbuseMatrix(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase17 = (from p in db.vw_deleteFrom_N_SubstanceAbuseMatrixes
                           where p.CaseEBPID_FK == CaseEBPID_FK
                           select new { p.ServiceID_FK });
            if ((oCase17 != null))
            {
                foreach (var var5 in oCase17)
                {
                    if (!(var5.ServiceID_FK == null))
                    {
                        deleteFrom_N_SubstanceAbuseMatrixes(Convert.ToInt32(var5.ServiceID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_N_SubstanceAbuseMatrixes(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            N_SubstanceAbuseMatrix oCase18 = (from c in db.N_SubstanceAbuseMatrixes
                                              where c.ServiceID_FK == ServiceID_FK
                                              select c).FirstOrDefault();
            if ((oCase18 != null))
            {
                db.N_SubstanceAbuseMatrixes.DeleteOnSubmit(oCase18);
            }
            db.SubmitChanges();
        }
    }


    //-------------------delete from E_CaseClosure---------------------------------------//

   

    //-----------------delete  from vw_deleteFrom_F_EBP_ParticipantRating---------------//


    private void GranteeDeletefrom_F_EBP_ParticipantRatings(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase14 = (from p in db.vw_deleteFrom_F_EBP_ParticipantRatings
                           where p.CaseID_FK == CaseEBPID_FK
                           select new { p.CaseEBPID_FK });
            if ((oCase14 != null))
            {
                foreach (var var4 in oCase14)
                {
                    if (!(var4.CaseEBPID_FK == null))
                    {
                        deleteFrom_F_EBP_ParticipantRatings(Convert.ToInt32(var4.CaseEBPID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }
    }

    private void deleteFrom_F_EBP_ParticipantRatings(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBP_ParticipantRating oCase15 = (from c in db.F_EBP_ParticipantRatings
                                               where c.CaseEBPID_FK == CaseEBPID_FK
                                               select c).FirstOrDefault();
            if ((oCase15 != null))
            {
                db.F_EBP_ParticipantRatings.DeleteOnSubmit(oCase15);
            }
            db.SubmitChanges();
        }
    }


    //----------------delete from G_SERVICE_LOG-------------------------------//

    private void GranteeDeletefrom_G_SERVICE_LOG(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            G_SERVICE_LOG oCase13 = (from c in db.G_SERVICE_LOGs
                                     where c.CaseEBPID_FK == CaseEBPID_FK
                                     select c).FirstOrDefault();
            if ((oCase13 != null))
            {
                db.G_SERVICE_LOGs.DeleteOnSubmit(oCase13);
            }
            db.SubmitChanges();
        }
    }


    //----------------- G_ServiceLogCaseMembers---------------//

    private void GranteeDeletefrom_G_ServiceLogCaseMembers(int CaseEBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase11 = (from p in db.vw_deleteFrom_G_ServiceLogCaseMembers
                           where p.CaseEBPID_FK == CaseEBPID_FK
                           select new { p.ServiceID });
            if ((oCase11 != null))
            {
                foreach (var var3 in oCase11)
                {
                    if (!(var3.ServiceID == null))
                    {
                        deleteFrom_G_ServiceLogCaseMembers(Convert.ToInt32(var3.ServiceID));
                    }
                }
            }
            db.SubmitChanges();
        }

    }
    private void deleteFrom_G_ServiceLogCaseMembers(int ServiceID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            G_ServiceLogCaseMember oCase12 = (from c in db.G_ServiceLogCaseMembers
                                              where c.ServiceID_FK == ServiceID_FK
                                              select c).FirstOrDefault();
            if ((oCase12 != null))
            {
                db.G_ServiceLogCaseMembers.DeleteOnSubmit(oCase12);
            }
            db.SubmitChanges();
        }
    }




    //------- F_EBPCaseWorkers_AssignedList---------------------------------------------//

    private void GranteeDeletefrom_F_EBPCaseWorkers_AssignedList(int EBPEnrollID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase9 = (from p in db.F_EBPCaseWorkers_AssignedLists
                          where p.EBPEnrollID_FK == EBPEnrollID_FK
                          select p);
            if ((oCase9 != null))
            {
                foreach (var var9 in oCase9)
                {
                    if (!(var9.EBPEnrollID_FK == null))
                    {
                        deleteFrom_F_EBPCaseWorkers_AssignedLists(Convert.ToInt32(var9.EBPEnrollID_FK));
                    }
                }
            }
            db.SubmitChanges();
        }

    }

    private void deleteFrom_F_EBPCaseWorkers_AssignedLists(int EBPEnrollID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPCaseWorkers_AssignedList oCase10 = (from c in db.F_EBPCaseWorkers_AssignedLists
                                                     where c.EBPEnrollID_FK == EBPEnrollID_FK
                                                     select c).FirstOrDefault();
            if ((oCase10 != null))
            {
                db.F_EBPCaseWorkers_AssignedLists.DeleteOnSubmit(oCase10);
            }
            db.SubmitChanges();
        }
    }



    //------------------F_ENROLLMENT---------------------------------------------------//


    private void GranteeDeletefrom_F_ENROLLMENT(int EBPID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oCase7 = (from p in db.F_EBPEnrollments
                          where p.EBPID_FK == EBPID_FK
                          select p);
            if ((oCase7 != null))
            {
                foreach (var var7 in oCase7)
                {
                    if (!(var7.IndividualID_FK == null))
                    {
                        deleteF_EBPEnrollment(Convert.ToInt32(var7.IndividualID_FK));
                    }
                }
            }
        }
    }

    private void deleteF_EBPEnrollment(int IndividualID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBPEnrollment oCase8 = (from c in db.F_EBPEnrollments
                                      where c.IndividualID_FK == IndividualID_FK
                                      select c).FirstOrDefault();
            if ((oCase8 != null))
            {
                db.F_EBPEnrollments.DeleteOnSubmit(oCase8);
            }
            db.SubmitChanges();
        }
    }


    //------------------F_ENROLLMENT_EBP----------------------//


    private void GranteeDeletefrom_F_ENROLLMENT_EBP(int CaseID_FK)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_ENROLLMENT_EBP oCase6 = (from c in db.F_ENROLLMENT_EBPs
                                       where c.CaseID_FK == CaseID_FK
                                       select c).FirstOrDefault();
            if ((oCase6 != null))
            {
                db.F_ENROLLMENT_EBPs.DeleteOnSubmit(oCase6);
            }
            db.SubmitChanges();
        }
    }

    //---------------------------------------------------------------------------------

    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int CaseEBPID = 0;
       

        Repeater repCasemember;
        Repeater repServiceLogs;
        LinkButton imgBtnCreate;
        LinkButton imgBtnEdit2;
        LinkButton imgBtnDelete;
        


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "CaseEBPID") != null)
            {
                CaseEBPID = (int)DataBinder.Eval(e.Row.DataItem, "CaseEBPID");
            }

            repCasemember = (Repeater)e.Row.FindControl("repCaseMembers");

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                var qry2 = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs join
                                      ebp in db.F_EBPEnrollments on p.IndividualID equals ebp.IndividualID_FK
                           where ebp.EBPID_FK == CaseEBPID
                           select new { p.FIRST_NAME, p.IndividualID };
                repCasemember.DataSource = qry2;
                repCasemember.DataBind();


                var GID = oUI.intGranteeID;
                
                var qry4 = from pg in db.B_CASE_ENROLLMENTs                           
                           where pg.CaseNoId == intCaseID 
                           select new { pg.GranteeID_fk };
                foreach (var c in qry4)
                {
                    if (c.GranteeID_fk != null)
                    {
                        Granteeid = Convert.ToInt32(c.GranteeID_fk);
                    }
                }
               
                var qry5 = from pg in db.A_GranteeEBPs
                           join epb in db.F_ENROLLMENT_EBPs on pg.EBPID_FK equals epb.EBPID_FK
                           where epb.CaseEBPID == CaseEBPID && epb.CaseID_FK == intCaseID && pg.GranteeID_FK == Granteeid
                           select new { pg.FocalEBPYN, epb.EBPID_FK };

               // only show creat link for focal EPB

                foreach (var c in qry5)
                {
                    if (c.FocalEBPYN == false)
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = false;
                    }
                    else
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = true;
                    }

                }

              //-------Caseworker can creat ServiceLog for EBP assinged to them-----------

                object objVal2 = null;
                var qry6 = from pg in db.A_Grantee_Contacts
                           where pg.Email == HttpContext.Current.User.Identity.Name
                           select pg;
                foreach (var b in qry6)
                {
                    objVal2 = b.GranteeContactId;
                    if (objVal2 != null)
                    {
                        GranteeContactId = Convert.ToInt32(objVal2.ToString());
                    }
                }

   
                /////----Service Logs------////////////////
                repServiceLogs = (Repeater)e.Row.FindControl("repServiceLogs");

 
                if (oUI.intUserRoleID == 2)
                {
                    btnNew.Visible = false;
                    //e.Row.Cells[5].Visible = false;
                    //grdVwList.HeaderRow.Cells[5].Visible = false;
                    //e.Row.Cells[6].Visible = false;
                    //grdVwList.HeaderRow.Cells[6].Visible = false;
                    //e.Row.Cells[7].Visible = false;
                    //grdVwList.HeaderRow.Cells[7].Visible = false;                   


                    //var qry3 = from p in db.G_SERVICE_LOGs
                    //           where p.CaseEBPID_FK == CaseEBPID && p.created_by == HttpContext.Current.User.Identity.Name
                    //           select new { p.ServiceID, p.DATE_OF_SERVICE, p.CaseID_FK, p.CaseEBPID_FK };
                    //repServiceLogs.DataSource = qry3;
                    //repServiceLogs.DataBind();

                    //*********************************Added on july 28th which show all the services logs************//
                    var qry3 = from p in db.G_SERVICE_LOGs
                               where p.CaseEBPID_FK == CaseEBPID
                               select new { p.ServiceID, p.DATE_OF_SERVICE, p.CaseID_FK, p.CaseEBPID_FK };
                    repServiceLogs.DataSource = qry3;
                    repServiceLogs.DataBind();
                }
                else
                {
                    var qry3 = from p in db.G_SERVICE_LOGs
                               where p.CaseEBPID_FK == CaseEBPID
                               select new { p.ServiceID, p.DATE_OF_SERVICE, p.CaseID_FK, p.CaseEBPID_FK };
                    repServiceLogs.DataSource = qry3;
                    repServiceLogs.DataBind();
                }

               

                // CaseWorkers CSL--------------------------
                if (oUI.intUserRoleID == 2)
                {

                    object objVal3 = null;

                    F_EBPCaseWorkers_AssignedList oCase2 = (from pg in db.F_EBPCaseWorkers_AssignedLists where pg.GranteeContactId_FK == GranteeContactId && pg.EBPEnrollID_FK == CaseEBPID select pg).FirstOrDefault();
                    //var oPages4 = from pg in db.F_EBPCaseWorkers_AssignedLists
                    //              where pg.GranteeContactId_FK == GranteeContactId && pg.EBPEnrollID_FK == CaseEBPID
                    //              select new { pg.EBPEnrollID_FK, pg.GranteeContactId_FK };

                    if (oCase2 != null)
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = true;
                        e.Row.Cells[7].Visible = true;
                    }
                    else
                    {
                        imgBtnEdit2 = (LinkButton)e.Row.FindControl("imgBtnEdit2");
                        imgBtnEdit2.Text = "View";
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = false;
                        e.Row.Cells[7].Visible = false;
                    }
                    
                }
                else
                {

                }


                var qry05 = from pg in db.A_GranteeEBPs
                           join epb in db.F_ENROLLMENT_EBPs on pg.EBPID_FK equals epb.EBPID_FK
                           where epb.CaseEBPID == CaseEBPID && epb.CaseID_FK == intCaseID && pg.GranteeID_FK == Granteeid
                           select new { pg.FocalEBPYN, epb.EBPID_FK };

                // only show creat link for focal EPB

                foreach (var c in qry05)
                {
                    if (c.FocalEBPYN == false)
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = false;
                    }
                    else
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = true;
                    }

                }

                //========================================================================

                // when the EPB exit date is not null creat link disappear

                var oPages = from pg in db.F_ENROLLMENT_EBPs
                             where pg.CaseEBPID == CaseEBPID && pg.EBP_EXIT_DATE != null
                             select new { pg.EBP_EXIT_DATE };
                foreach (var oCase in oPages)
                {
                    if (oCase.EBP_EXIT_DATE != null)
                    {
                        //imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        //imgBtnCreate.Visible = false;

                        if (!(oUI.intUserRoleID == 3))
                        {
                            foreach (var c in qry05)
                            {
                                if (c.FocalEBPYN == false)
                                {
                                    imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                                    imgBtnCreate.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                            imgBtnCreate.Visible = true;
                        }
                    }
                }

                // hide create services log when case is closed added on Aug12-----//
                var qryhide = from p in db.E_CaseClosures
                              where p.CaseNoId_FK == intCaseID
                              select new { p.CASE_CLOSE_COMPLETED };
                foreach (var oCase1 in qryhide)
                {
                    if (oCase1.CASE_CLOSE_COMPLETED != null)
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = false;
                        e.Row.Cells[7].Visible = false;
                        grdVwList.HeaderRow.Cells[7].Visible = false; 
                    }
                    else if (oCase1.CASE_CLOSE_COMPLETED == false)
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = true;
                        e.Row.Cells[7].Visible = true;
                        grdVwList.HeaderRow.Cells[7].Visible = true; 
                    }
                }

                //------------------------------------------Aug12-----------------//


                
                       


                //--------------Delete link access---------------
                if (!(oUI.intUserRoleID == 3))
                {
                    imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                    imgBtnDelete.Visible = false;
                   // grdVwList.HeaderRow.Cells[5].Visible = false;
                   // imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                   // imgBtnCreate.Visible = false;
                }
                else
                {
                    imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
                    imgBtnDelete.Visible = true;
                    imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
                                                  "confirm('Are you sure you want to delete this record')");
                   // grdVwList.HeaderRow.Cells[5].Visible = true;
                    //imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                    //imgBtnCreate.Visible = true;
                }               


                var qry06 = from pg in db.A_GranteeEBPs
                            join epb in db.F_ENROLLMENT_EBPs on pg.EBPID_FK equals epb.EBPID_FK
                            where epb.CaseEBPID == CaseEBPID && epb.CaseID_FK == intCaseID && pg.GranteeID_FK == Granteeid 
                            select new { pg.FocalEBPYN, epb.EBPID_FK, epb.EBP_EXIT_DATE };



                // only show creat link for focal EPB and EBP exit date is null

                foreach (var c in qry06)
                {
                    if (c.FocalEBPYN == false)
                    {
                        imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                        imgBtnCreate.Visible = false;
                        
                    }
                    else
                    {
                        string exitdate = Convert.ToString(c.EBP_EXIT_DATE);

                        if( (String.IsNullOrEmpty(exitdate) && (c.FocalEBPYN == true)))
                        {
                            imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                            imgBtnCreate.Visible = true;
                           
                        }
                        else if (!(String.IsNullOrEmpty(exitdate)) && (c.FocalEBPYN == true))
                        {
                            imgBtnCreate = (LinkButton)e.Row.FindControl("imgBtnCreate");
                            imgBtnCreate.Visible = false;
                        }
                    }

                }







            }
        }
    }
}