﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ServiceLogEdit : System.Web.UI.Page
{
    int lngPkID;
    int intCaseID;
    int intEBPEnrollID;
    int count1 ;
    UserInfo oUI;
    string currentDate;
    int intC;
    int allchecked = 1;
    bool valSessActivities = false;
    int TopicFreez;
    int SessAlligment;
    int sActivities = 0;
    int GetEBPIDfromSL;
    string caseworkEmail = "";
    
   
    protected void Page_Load(object sender, EventArgs e)
    {
      
        string strJS = null;
       // this.ClientScript.RegisterOnSubmitStatement(this.GetType(), "OnSubmitScript", "g_isPostBack = true;");

        if (!IsPostBack)
        {
             currentDate = DateTime.Today.ToShortDateString();
             cvMaxDateFService.ValueToCompare = currentDate;
         
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);          
          
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
            if (Page.Request.QueryString["intCaseID"] != null)
            {
                intCaseID = Convert.ToInt32(Page.Request.QueryString["intCaseID"]);
            }
            if (Page.Request.QueryString["intEBPEnrollID"] != null)
            {
                intEBPEnrollID = Convert.ToInt32(Page.Request.QueryString["intEBPEnrollID"]);
            }
           
            // delete function access to super system admin role----//
            if (!(oUI.intUserRoleID == 3))
            {

                btnDelete.Visible = false;
               
            }
            else
            {

                btnDelete.Visible = true;
                btnDelete.Attributes.Add("onclick", "javascript:return " +
                                              "confirm('Are you sure you want to delete this record')");
                
            }




            //-------------from------------------------//
            if (Page.Request.QueryString["intC"] != null)
            {
                intC = Convert.ToInt32(Page.Request.QueryString["intC"]);
            }
                if (intC == 1)
                {
                    TopicEnabled();               
                }
                else
                {
                    TopicEnabledTrue();               
                }

            //---------------from same page--------------//
            if (Page.Request.QueryString["TopicFreez"] != null)
            {
                TopicFreez = Convert.ToInt32(Page.Request.QueryString["TopicFreez"]);
            }

                if (TopicFreez == 0)
                {
                    TopicEnabled();
                }
                else
                {
                    TopicEnabledTrue();
                }
            //--------------------------------------------//

            if (Page.Request.QueryString["allchecked"] != null)
            {
                allchecked = Convert.ToInt32(Page.Request.QueryString["allchecked"]);
            }         
           
            DisableBtnSave();
        }
        loadrecords();


        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();
            loadCaseMembers();
            
            displayRecords();           
            ViewState["IsLoaded1"] = true;
        }

        if (!string.IsNullOrEmpty(txtDateofservice.Text))
            loadCaseWorkers();  

        CompareDate();      
        Page.MaintainScrollPositionOnPostBack = true;
    }

    protected void loadCaseWorkers()
    {
        if (ddlCaseWorkers.SelectedValue != "")
            caseworkEmail = ddlCaseWorkers.SelectedValue;

        ddlCaseWorkers.Items.Clear();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            int? intUserGranteeID = oUI.intGranteeID;

            var qry = from pg in db.F_EBPCaseWorkers_AssignedLists
                      join G in db.A_Grantee_Contacts
                      on pg.GranteeContactId_FK equals G.GranteeContactId
                      where pg.EBPEnrollID_FK == intEBPEnrollID && (pg.CaseWorker_StartDate == null || pg.CaseWorker_StartDate <= DateTime.Parse(txtDateofservice.Text))
                      && (pg.CaseWorker_ExitDate == null || pg.CaseWorker_ExitDate >= DateTime.Parse(txtDateofservice.Text))
                      group pg by new { CaseWorkerName = G.FirstName + " " + G.LastName, G.Email } into pggroup
                      orderby pggroup.Key.CaseWorkerName
                      select new
                      {
                          pggroup.Key.CaseWorkerName,
                          pggroup.Key.Email
                      };

            ddlCaseWorkers.DataSource = qry;
            ddlCaseWorkers.DataTextField = "CaseWorkerName";
            ddlCaseWorkers.DataValueField = "Email";
            ddlCaseWorkers.DataBind();
            ddlCaseWorkers.Items.Insert(0, new ListItem("Select caseworker", ""));

            if (ddlCaseWorkers.Items.FindByValue(caseworkEmail) == null)
            {
               ddlCaseWorkers.SelectedValue = "";
            }
            else
            {
                ddlCaseWorkers.SelectedValue = caseworkEmail;
            }
        }

    }

    private void TopicEnabledTrue()
    {
        rblAdultSubAbuse.Enabled = true;
        rblAdultPrntSkills.Enabled = true;
        rblAdultPersonalDevp.Enabled = true;
        rblChildYouthTheDevp.Enabled = true;
        rblChildSubDisRecovery.Enabled = true;
        rblRecoverBiologyAbuseManagement.Enabled = true;
    }
    private void TopicEnabled()
    {
        if (oUI.intUserRoleID == 3)
        {
            rblAdultSubAbuse.Enabled = true;
            rblAdultPrntSkills.Enabled = true;
            rblAdultPersonalDevp.Enabled = true;
            rblChildYouthTheDevp.Enabled = true;
            rblChildSubDisRecovery.Enabled = true;
            rblRecoverBiologyAbuseManagement.Enabled = true;
        }
        else
        {
            rblAdultSubAbuse.Enabled = false;
            rblAdultPrntSkills.Enabled = false;
            rblAdultPersonalDevp.Enabled = false;
            rblChildYouthTheDevp.Enabled = false;
            rblChildSubDisRecovery.Enabled = false;
            rblRecoverBiologyAbuseManagement.Enabled = false;
        }
    }
    private void CompareDate()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            object objValue;
            //var oCases = from css in db.B_CASE_ENROLLMENTs
            //             where css.CaseNoId == intCaseID
            //             select css;


            F_ENROLLMENT_EBP oCase1 = (from c in db.F_ENROLLMENT_EBPs where c.CaseEBPID == intEBPEnrollID select c).FirstOrDefault();

            if ((oCase1 != null))
            {
                var oCases = from pg in db.F_ENROLLMENT_EBPs
                             where pg.CaseEBPID == intEBPEnrollID
                             select pg;
                foreach (var oCase in oCases)
                {
                    objValue = oCase.EBP_ENROLL_DATE;
                    if (objValue != null)
                    {

                        txtDtCmp.Text = String.Format("{0:MM/dd/yyyy}", objValue);
                        cvDateOfServices.ValueToCompare = txtDtCmp.Text;

                    }
                    else
                    {
                        cvDateOfServices.ValueToCompare = currentDate;
                    }

                }

            }
            else
            {
                cvDateOfServices.ValueToCompare = currentDate;
            }
        }
           
       
    }
    private void DisableBtnSave()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var qryFlag = from Flag in db.G_SERVICE_LOGs
                          where Flag.ServiceID == lngPkID
                          select Flag;
            foreach (var oCase in qryFlag)
            {
                if (oCase.SERVICE_LOG_COMPLETED == true)
                {
                    if (oUI.intUserRoleID == 3)// added on 1 july
                    {
                        btnSave.Visible = true;
                        btnSaveClose.Visible = false;
                        // added on Aug 12 fro popup message rule same changed on 18th sep(reverted back)
                        btnClose.Visible = false;
                        btnCloseHide.Visible = true;
                    }
                    else
                    {
                        btnSave.Visible = false;
                        btnSaveClose.Visible = false;
                        btnClose.Visible = false;
                        btnCloseHide.Visible = true;
                    }
                }
            }

        }
    }
    protected void loadrecords()
    {
    }
    protected void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.F_EBPLists select p;
            cboEBP.DataSource = qry;
            cboEBP.DataTextField = "EBPName";
            cboEBP.DataValueField = "EBPID";
            cboEBP.DataBind();

            var qry2 = from p in db.G_Service_Locations select p;
            rblSessionlocation.DataSource = qry2;
            rblSessionlocation.DataTextField = "SessionLocation";
            rblSessionlocation.DataValueField = "SessLocationID";
            rblSessionlocation.DataBind();

            /*Participnt Engagemnet Rating 4/2/2014*/
            var qry3 = from e in db.L_ENGAGEMENT_RATINGs
                       select e;
            chkERatings.DataSource = qry3;
            chkERatings.DataTextField = "CASE_EBP_ENGAGEMENT";
            chkERatings.DataValueField = "ER_ID";
            chkERatings.DataBind();


        }


    }  
    protected bool IsFocalEBP(int intEBPEnrollID)
    {
            bool blRet =false ;       
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oEBPStatus = db.getFocalEBPStatusForCaseEBPID(intEBPEnrollID);
            foreach (var oEBP in oEBPStatus)
            {
                if (oEBP.FocalEBPYN != null)
                {
                    if (oEBP.FocalEBPYN == true)
                    {
                        blRet = true;
                    }
                }
            }
        }
        return blRet;
    }
    protected void displayRecords()
    {
        object objVal = null;
 
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oCases = from css in db.B_CASE_ENROLLMENTs
                         where css.CaseNoId == intCaseID
                         select css;
            foreach (var oCase in oCases)
            {
                litCaseID.Text = oCase.CASE_ID;
                if (oCase.SURNAME != null)
                {
                    litSurname.Text = Convert.ToString(oCase.SURNAME);
                }
            }
            //var oEBPs = from ebp in db.F_EBPLists
            //           join ebpe in db.F_ENROLLMENT_EBPs
            //           on ebp.EBPID equals  ebpe.EBPID_FK
            //           where ebpe.CaseEBPID == intEBPEnrollID
            //           select ebp;
            //foreach (var oEBP in oEBPs)
            //{
            //    //litCaseID.Text = oEBP.EBPName;
            //    if (oEBP.EBPName != null)
            //    {
            //        litEBP.Text = Convert.ToString(oEBP.EBPName);
            //    }
            //}


            // for view services log (added on 6/11)

            
            var ocaGet = from c in db.G_SERVICE_LOGs
                         where c.ServiceID == lngPkID
                         select new { c.CaseEBPID_FK };
            if (ocaGet != null)
            {

                foreach (var oEBPId in ocaGet)
                {
                    objVal = oEBPId.CaseEBPID_FK;
                    if (objVal != null)
                    {
                        GetEBPIDfromSL = Convert.ToInt32(objVal);
                    }
                }
            }

            if (intC == 1)
            {
                var oEBPsv = from ebp in db.F_EBPLists
                             join ebpe in db.F_ENROLLMENT_EBPs
                             on ebp.EBPID equals ebpe.EBPID_FK
                             where ebpe.CaseEBPID == GetEBPIDfromSL
                             select ebp;
                foreach (var oEBP in oEBPsv)
                {
                    //litCaseID.Text = oEBP.EBPName;
                    if (oEBP.EBPName != null)
                    {
                        litEBP.Text = Convert.ToString(oEBP.EBPName);
                    }
                }
            }
            else
            {
                var oEBPs = from ebp in db.F_EBPLists
                            join ebpe in db.F_ENROLLMENT_EBPs
                            on ebp.EBPID equals ebpe.EBPID_FK
                            where ebpe.CaseEBPID == intEBPEnrollID
                            select ebp;
                foreach (var oEBP in oEBPs)
                {
                    //litCaseID.Text = oEBP.EBPName;
                    if (oEBP.EBPName != null)
                    {
                        litEBP.Text = Convert.ToString(oEBP.EBPName);
                    }
                }

            }

            //var oEBPsv = from ebp in db.F_EBPLists
            //             join ebpe in db.F_ENROLLMENT_EBPs
            //             on ebp.EBPID equals ebpe.EBPID_FK
            //             join sl in db.G_SERVICE_LOGs
            //             on ebpe.CaseID_FK equals sl.CaseID_FK
            //             where sl.ServiceID == lngPkID
            //             select ebp;
            //foreach (var oEBP in oEBPsv)
            //{
            //    //litCaseID.Text = oEBP.EBPName;
            //    if (oEBP.EBPName != null)
            //    {
            //        litEBP.Text = Convert.ToString(oEBP.EBPName);
            //    }
            //}

            //----------------------


            var oPages = from pg in db.G_SERVICE_LOGs
                         where pg.ServiceID   == lngPkID
                         select pg;
            foreach (var oCase in oPages)
            {

                if (oCase.SERVICE_LOG_COMPLETED == true)
                {

                    if (oUI.intUserRoleID == 3)// added on 27th june
                    {
                        btnSave.Visible = true;
                    }
                    else
                    {
                        btnSave.Visible = false;
                    }
                }

                objVal = oCase.DATE_OF_SERVICE;
                if (objVal != null)
                {
                    txtDateofservice.Text = String.Format("{0:MM/dd/yyyy}", objVal);
                }
                objVal = oCase.EBP_SERVICE_FK;
                if (objVal != null)
                {
                    cboEBP.SelectedIndex = cboEBP.Items.IndexOf(cboEBP.Items.FindByValue(oCase.EBP_SERVICE_FK.ToString()));
                }
    
                objVal = oCase.SESSION_LOCATION_FK;
                if (objVal != null)
                {
                    rblSessionlocation.SelectedIndex = rblSessionlocation.Items.IndexOf(rblSessionlocation.Items.FindByValue(oCase.SESSION_LOCATION_FK.ToString()));
                    if (rblSessionlocation.SelectedIndex == 10)
                    {
                        rfvtxtSpecifSessionlocation.EnableClientScript = true;
                        rfvtxtSpecifSessionlocation.Enabled = true;
                    }
                }


                //ADDED ON 31ST MARCH

                objVal = oCase.SESSION_LOCATION_SPECIFY_IF_OTHER;
                if (objVal != null)
                {
                    txtSpecifSessionlocation.Text = objVal.ToString();
                }

                objVal = oCase.SESSION_MINUTES;
                if (objVal != null)
                {
                    txtSessionminutes.Text = objVal.ToString();
                }
                objVal = oCase.OTHERS_PRESENT_FOST_PRNT_GUARD_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentfostprntguardyn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresentfostprntguardyn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_INTERPRETER_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentinterpreteryn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresentinterpreteryn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_GRANT_STAFF_MEMB_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentgrantstaffmembyn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresentgrantstaffmembyn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_OTH_RELAT_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentothrelatyn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresentothrelatyn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_RPG_PART_STAFF_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentrpgpartstaffyn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresentrpgpartstaffyn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_FID_OBS_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentfidobsyn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresentfidobsyn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_HLTH_PROF_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresenthlthprofyn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresenthlthprofyn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_SUPERVIS_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentsupervisyn.Checked = true;
                    }
                    else
                    {
                        chkOtherspresentsupervisyn.Checked = false;
                    }
                }
                objVal = oCase.OTHERS_PRESENT_OTH_PROF_STAFF_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkOtherspresentothprofstaffyn.Checked = true;
                        rfvtxtOtherspresentothprofstaffyn.EnableClientScript = true;
                        rfvtxtOtherspresentothprofstaffyn.Enabled = true;
                    }
                    else
                    {
                        chkOtherspresentothprofstaffyn.Checked = false;
                    }
                }

                //added by vars on 18th feb
                objVal = oCase.OTHERS_PRESENT_SPECIFY_IF_OTHER;
                if (objVal != null)
                {
                   txtOtherspresentothprofstaffyn.Text = objVal.ToString();
                }
              

                objVal = oCase.SESSION_ACTIVITIES_GRP_DISC_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesgrpdiscyn.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesgrpdiscyn.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_IND_DISC_YN;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesinddiscyn.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesinddiscyn.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_FAM_ACT_INTER;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesfamactinter.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesfamactinter.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_FAM_MTNG;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesfammtng.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesfammtng.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_ROLE_PLAY;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesroleplay.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesroleplay.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_REENACT;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesreenact.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesreenact.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_EXP_ACT;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesexpact.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesexpact.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_GAMES;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesgames.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesgames.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_WORKSHEET;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesworksheet.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesworksheet.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_WATCH_VID;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitieswatchvid.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitieswatchvid.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_GOAL_SET;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesgoalset.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesgoalset.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_GUID_PRAC;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesguidprac.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesguidprac.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_COACH_FEED;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiescoachfeed.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiescoachfeed.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_PROV_EMOT_SUPP;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesprovemotsupp.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesprovemotsupp.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_PROB_SOLV;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesprobsolv.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesprobsolv.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_CRIS_INTER;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiescrisinter.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiescrisinter.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_PRNT_SKILL_SCR;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesprntskillscr.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesprntskillscr.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_CHILD_DEV_SCR;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitieschilddevscr.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitieschilddevscr.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_HLTH_ASS;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitieshlthass.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitieshlthass.Checked = false;
                    }
                }
                objVal = oCase.SESSION_ACTIVITIES_MENT_HLTH_SUB_ABUSE;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkSessionactivitiesmenthlthsubabuse.Checked = true;
                        sActivities = 1;
                    }
                    else
                    {
                        chkSessionactivitiesmenthlthsubabuse.Checked = false;
                    }
                }

                //added on 21st feb by varsha
               
                objVal = oCase.SESSION_ACTIVITIES_OTHER;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        sActivities = 1;
                        chkSessionactivitieother.Checked = true;
                        rfvtxtSessionactivitiespecifyother.EnableClientScript = true;
                        rfvtxtSessionactivitiespecifyother.Enabled = true;
                    }
                    else
                    {
                        chkSessionactivitieother.Checked = false;
                    }
                }

                objVal = oCase.SESSION_ACTIVITIES_SPECIFY_OTHER;
                if (objVal != null)
                {
                    txtSessionactivitiespecifyother.Text = objVal.ToString();
                }

                //-----------------------added on 1st july for validation to take place after matrix--------------------------------------
               
                if (sActivities == 1)
                {
                    valSessActivities = true;
                    lblMessSessionActivity.Text = "";
                    lblMessageSession.Text = "";
                }
                else
                {
                    valSessActivities = false;
                    lblMessSessionActivity.Text = "Required";
                    lblMessageSession.Text = "Which activities occurred during the session? is required";
                }

                //--------------------------------------------------------------


                objVal = oCase.SESSION_ALIGNMENT_FK;
                if (objVal != null)
                {
                    rblSessionalignmentfk.SelectedIndex = rblSessionalignmentfk.Items.IndexOf(rblSessionalignmentfk.Items.FindByValue(oCase.SESSION_ALIGNMENT_FK.ToString()));
                    SessAlligment = rblSessionalignmentfk.SelectedIndex;
                    allchecked = 0;
                }
                objVal = oCase.REASON_NOT_ALIGN_FAM_CRIS;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkReasonnotalignfamcris.Checked = true;
                        allchecked = 1;
                    }
                    else
                    {
                        chkReasonnotalignfamcris.Checked = false;
                    }
                }
                objVal = oCase.REASON_NOT_ALIGN_PART_NOT_ENGAGE;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkReasonnotalignpartnotengage.Checked = true;
                        allchecked = 1;
                    }
                    else
                    {
                        chkReasonnotalignpartnotengage.Checked = false;
                    }
                }
                objVal = oCase.REASON_NOT_ALIGN_PART_INTER_OTH_TOP;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkReasonnotalignpartinterothtop.Checked = true;
                        allchecked = 1;
                    }
                    else
                    {
                        chkReasonnotalignpartinterothtop.Checked = false;
                    }
                }
                objVal = oCase.REASON_NOT_ALIGN_PRES_OTH_IND_INHIB_SESS;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkReasonnotalignpresothindinhibsess.Checked = true;
                        allchecked = 1;
                    }
                    else
                    {
                        chkReasonnotalignpresothindinhibsess.Checked = false;
                    }
                }
                objVal = oCase.REASON_NOT_ALIGN_PART_SICK;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkReasonnotalignpartsick.Checked = true;
                        allchecked = 1;
                    }
                    else
                    {
                        chkReasonnotalignpartsick.Checked = false;
                    }
                }
                objVal = oCase.REASON_NOT_ALIGN_PHYSC_CONST;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkReasonnotalignphyscconst.Checked = true ;
                        allchecked = 1;
                    }
                    else
                    {
                        chkReasonnotalignphyscconst.Checked = false;
                    }
                }
                objVal = oCase.REASON_NOT_ALIGN_OTHER;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        chkReasonnotalignother.Checked = true;
                        rfvtxtSpecifyreasonnotalign.EnableClientScript = true;
                        rfvtxtSpecifyreasonnotalign.Enabled = true;
                        allchecked = 1;
                    }
                    else
                    {
                        chkReasonnotalignother.Checked = false;
                    }
                }
                objVal = oCase.SPECIFY_REASON_NOT_ALIGN;
                if (objVal != null)
                {
                    txtSpecifyreasonnotalign.Text = objVal.ToString();
                }

                //------------------added on 1st july for Session Alligment validation take place---------------------------------//

                //allchecked = 0;

                if (SessAlligment == 2)
                {
                    Validatecheckbox();
                    if (allchecked == 0)
                    {
                        lblSessionMessage.Text = "Required";
                    }
                    else
                    {
                        lblSessionMessage.Text = "";
                    }
                    chkReasonnotalignfamcris.Enabled = true;
                    chkReasonnotalignpartnotengage.Enabled = true;
                    chkReasonnotalignpartinterothtop.Enabled = true;
                    chkReasonnotalignpresothindinhibsess.Enabled = true;
                    chkReasonnotalignpartsick.Enabled = true;
                    chkReasonnotalignphyscconst.Enabled = true;
                    chkReasonnotalignother.Enabled = true;
                    txtSpecifyreasonnotalign.Enabled = true;

                }
                else if (SessAlligment == 1)
                {
                    Validatecheckbox();
                    if (allchecked == 0)
                    {
                        lblSessionMessage.Text = "Required";
                    }
                    else
                    {
                        lblSessionMessage.Text = "";
                    }

                    chkReasonnotalignfamcris.Enabled = true;
                    chkReasonnotalignpartnotengage.Enabled = true;
                    chkReasonnotalignpartinterothtop.Enabled = true;
                    chkReasonnotalignpresothindinhibsess.Enabled = true;
                    chkReasonnotalignpartsick.Enabled = true;
                    chkReasonnotalignphyscconst.Enabled = true;
                    chkReasonnotalignother.Enabled = true;
                    txtSpecifyreasonnotalign.Enabled = true;

                }
                else if (SessAlligment == 0)
                {
                    allchecked = 1;
                    lblMessageSession.Text = "";
                    lblSessionMessage.Text = "";

                    chkReasonnotalignfamcris.Enabled = false;
                    chkReasonnotalignpartnotengage.Enabled = false;
                    chkReasonnotalignpartinterothtop.Enabled = false;
                    chkReasonnotalignpresothindinhibsess.Enabled = false;
                    chkReasonnotalignpartsick.Enabled = false;
                    chkReasonnotalignphyscconst.Enabled = false;
                    chkReasonnotalignother.Enabled = false;
                    txtSpecifyreasonnotalign.Enabled = false;
                    rfvtxtSpecifyreasonnotalign.Enabled = false; // added

                    chkReasonnotalignfamcris.Checked = false;
                    chkReasonnotalignpartnotengage.Checked = false;
                    chkReasonnotalignpartinterothtop.Checked = false;
                    chkReasonnotalignpresothindinhibsess.Checked = false;
                    chkReasonnotalignpartsick.Checked = false;
                    chkReasonnotalignphyscconst.Checked = false;
                    chkReasonnotalignother.Checked = false;
                    txtSpecifyreasonnotalign.Text = "";
                }
                else
                {
                    allchecked = 1;
                    lblMessageSession.Text = "";
                    lblSessionMessage.Text = "";
                }


                //---------------------------------------------------//







               
                objVal = oCase.ED_OTH_RELAT;
                if (objVal != null)
                {
                    if (objVal.ToString()=="True")
                    {
                        //chkEdothrelat.Checked = true;
                        lnkOtherRelative.Visible = true; // added by var
                        rblRecoverBiologyAbuseManagement.SelectedIndex = rblRecoverBiologyAbuseManagement.Items.IndexOf(rblRecoverBiologyAbuseManagement.Items.FindByValue("1"));

                    }
                    else
                    {
                       // chkEdothrelat.Checked = false;
                        lnkOtherRelative.Visible = false; // added by var
                        rblRecoverBiologyAbuseManagement.SelectedIndex = rblRecoverBiologyAbuseManagement.Items.IndexOf(rblRecoverBiologyAbuseManagement.Items.FindByValue("2"));

                       
                    }
                }
                
                /*-----------------------------------------------------*/

                objVal = oCase.ADULT_TOPICS_SUB_ABUSE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        lnkAdulttopicssubabuse.Visible = true;                        
                        rblAdultSubAbuse.SelectedIndex = rblAdultSubAbuse.Items.IndexOf(rblAdultSubAbuse.Items.FindByValue("1"));
                    }
                    else 
                    {                       
                        lnkAdulttopicssubabuse.Visible = false;             
                        rblAdultSubAbuse.SelectedIndex = rblAdultSubAbuse.Items.IndexOf(rblAdultSubAbuse.Items.FindByValue("2"));
                    }
                }

                objVal = oCase.ADULT_TOPICS_PRNT_SKILLS;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        lnkAdulttopicsprntskills.Visible = true;
                        rblAdultPrntSkills.SelectedIndex = rblAdultPrntSkills.Items.IndexOf(rblAdultPrntSkills.Items.FindByValue("1"));
                    }
                    else 
                    {
                        lnkAdulttopicsprntskills.Visible = false;
                        rblAdultPrntSkills.SelectedIndex = rblAdultPrntSkills.Items.IndexOf(rblAdultPrntSkills.Items.FindByValue("2"));
                    }
                }

                objVal = oCase.ADULT_TOPICS_PERS_DEV;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                        lnkAdulttopicspersdev.Visible = true;
                        rblAdultPersonalDevp.SelectedIndex = rblAdultPersonalDevp.Items.IndexOf(rblAdultPersonalDevp.Items.FindByValue("1"));
                    }
                    else 
                    {
                        lnkAdulttopicspersdev.Visible = false;
                        rblAdultPersonalDevp.SelectedIndex = rblAdultPersonalDevp.Items.IndexOf(rblAdultPersonalDevp.Items.FindByValue("2"));
                    }
                }

                //child


                objVal = oCase.CHILD_TOPICS_DEV_AND_ED;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                         lnkChildtopicsedadultsubabuse.Visible = true;

                         rblChildSubDisRecovery.SelectedIndex = rblChildSubDisRecovery.Items.IndexOf(rblChildSubDisRecovery.Items.FindByValue("1"));
                    }
                    else
                    {
                         lnkChildtopicsedadultsubabuse.Visible =false;
                         rblChildSubDisRecovery.SelectedIndex = rblChildSubDisRecovery.Items.IndexOf(rblChildSubDisRecovery.Items.FindByValue("2"));
                    }
                }

                objVal = oCase.CHILD_TOPICS_ED_ADULT_SUB_ABUSE;
                if (objVal != null)
                {
                    if (objVal.ToString() == "True")
                    {
                         lnkChildtopicsdevanded.Visible = true;


                         rblChildYouthTheDevp.SelectedIndex = rblChildYouthTheDevp.Items.IndexOf(rblChildYouthTheDevp.Items.FindByValue("1"));
                    }
                    else
                    {
                         lnkChildtopicsdevanded.Visible = false;
                         rblChildYouthTheDevp.SelectedIndex = rblChildYouthTheDevp.Items.IndexOf(rblChildYouthTheDevp.Items.FindByValue("2"));
                    }
                }


               

                //objVal = oCase.ED_OTH_RELAT;
                //rename  hhe radio button  later
                if (objVal != null)
                { 
                    
                //    rblRecoverBiologyAbuseManagement.SelectedIndex = rblRecoverBiologyAbuseManagement.Items.IndexOf(rblRecoverBiologyAbuseManagement.Items.FindByValue(oCase.ED_OTH_RELAT.ToString()));
                }

                caseworkEmail = oCase.CaseworkerEmailID;
            }

        }
        // displayGrid(lngPkID);
        displayCaseMembers(lngPkID);
        /*Participnt Engagemnet Rating 4/2/2014*/
        //if (NeedPEDisplay())

        if (NeedPE_v1())
        {
            displayEngagementRatings();
        }
        else
        {
            divPE.Visible = false;
        }
    }

    /*Participnt Engagemnet Rating 4/2/2014*/
    protected void displayEngagementRatings()
    {
       
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.F_EBP_ParticipantRatings
                         where pg.ServiceLogID_FK == lngPkID
                         select pg;          

            foreach (var oCase in oPages)
            {
                objVal = oCase.ER_ID_FK;
                if (objVal != null)
                {

                    chkERatings.SelectedIndex = chkERatings.Items.IndexOf(chkERatings.Items.FindByValue(oCase.ER_ID_FK.ToString()));

                }
                              
            }
        }
    }

    //-----update-----------------------//
    public void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            G_SERVICE_LOG oCase = (from c in db.G_SERVICE_LOGs where c.ServiceID   == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new G_SERVICE_LOG();
                blNew = true;
                oCase.CaseID_FK = intCaseID;
                oCase.CaseEBPID_FK = intEBPEnrollID;
            }


            oCase.CaseworkerEmailID = ddlCaseWorkers.SelectedValue;

 
 
            DateTime.TryParse(txtDateofservice.Text, out dtTmp);
            if (txtDateofservice.Text != "")
            oCase.DATE_OF_SERVICE = dtTmp;
            if (!(cboEBP.SelectedItem == null))
            {
                if (!(cboEBP.SelectedItem.Value.ToString() == ""))
                {
                    oCase.EBP_SERVICE_FK = Convert.ToInt32(cboEBP.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.EBP_SERVICE_FK = null;
                }
            }
             
            if (!(rblSessionlocation.SelectedItem == null))
            {
                if (!(rblSessionlocation.SelectedItem.Value.ToString() == ""))
                {
                    oCase.SESSION_LOCATION_FK = Convert.ToInt32(rblSessionlocation.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.SESSION_LOCATION_FK = null;
                }
            }

            //added on 31th March
            oCase.SESSION_LOCATION_SPECIFY_IF_OTHER = txtSpecifSessionlocation.Text;


            int.TryParse(txtSessionminutes.Text, out strTmp);
            oCase.SESSION_MINUTES = strTmp;
            if (chkOtherspresentfostprntguardyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_FOST_PRNT_GUARD_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_FOST_PRNT_GUARD_YN = false;
            }
            if (chkOtherspresentinterpreteryn.Checked == true)
            {
                oCase.OTHERS_PRESENT_INTERPRETER_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_INTERPRETER_YN = false;
            }
            if (chkOtherspresentgrantstaffmembyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_GRANT_STAFF_MEMB_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_GRANT_STAFF_MEMB_YN = false;
            }
            if (chkOtherspresentothrelatyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_OTH_RELAT_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_OTH_RELAT_YN = false;
            }
            if (chkOtherspresentrpgpartstaffyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_RPG_PART_STAFF_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_RPG_PART_STAFF_YN = false;
            }
            if (chkOtherspresentfidobsyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_FID_OBS_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_FID_OBS_YN = false;
            }
            if (chkOtherspresenthlthprofyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_HLTH_PROF_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_HLTH_PROF_YN = false;
            }
            if (chkOtherspresentsupervisyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_SUPERVIS_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_SUPERVIS_YN = false;
            }
            if (chkOtherspresentothprofstaffyn.Checked == true)
            {
                oCase.OTHERS_PRESENT_OTH_PROF_STAFF_YN = true;
            }
            else
            {
                oCase.OTHERS_PRESENT_OTH_PROF_STAFF_YN = false;
            }

            //added by vars on 18th feb
            oCase.OTHERS_PRESENT_SPECIFY_IF_OTHER = txtOtherspresentothprofstaffyn.Text;

            if (chkSessionactivitiesgrpdiscyn.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_GRP_DISC_YN = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_GRP_DISC_YN = false;
            }
            if (chkSessionactivitiesinddiscyn.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_IND_DISC_YN = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_IND_DISC_YN = false;
            }
            if (chkSessionactivitiesfamactinter.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_FAM_ACT_INTER = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_FAM_ACT_INTER = false;
            }
            if (chkSessionactivitiesfammtng.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_FAM_MTNG = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_FAM_MTNG = false;
            }
            if (chkSessionactivitiesroleplay.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_ROLE_PLAY = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_ROLE_PLAY = false;
            }
            if (chkSessionactivitiesreenact.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_REENACT = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_REENACT = false;
            }
            if (chkSessionactivitiesexpact.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_EXP_ACT = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_EXP_ACT = false;
            }
            if (chkSessionactivitiesgames.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_GAMES = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_GAMES = false;
            }
            if (chkSessionactivitiesworksheet.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_WORKSHEET = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_WORKSHEET = false;
            }
            if (chkSessionactivitieswatchvid.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_WATCH_VID = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_WATCH_VID = false;
            }
            if (chkSessionactivitiesgoalset.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_GOAL_SET = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_GOAL_SET = false;
            }
            if (chkSessionactivitiesguidprac.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_GUID_PRAC = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_GUID_PRAC = false;
            }
            if (chkSessionactivitiescoachfeed.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_COACH_FEED = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_COACH_FEED = false;
            }
            if (chkSessionactivitiesprovemotsupp.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_PROV_EMOT_SUPP = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_PROV_EMOT_SUPP = false;
            }
            if (chkSessionactivitiesprobsolv.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_PROB_SOLV = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_PROB_SOLV = false;
            }
            if (chkSessionactivitiescrisinter.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_CRIS_INTER = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_CRIS_INTER = false;
            }
            if (chkSessionactivitiesprntskillscr.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_PRNT_SKILL_SCR = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_PRNT_SKILL_SCR = false;
            }
            if (chkSessionactivitieschilddevscr.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_CHILD_DEV_SCR = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_CHILD_DEV_SCR = false;
            }
            if (chkSessionactivitieshlthass.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_HLTH_ASS = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_HLTH_ASS = false;
            }
            if (chkSessionactivitiesmenthlthsubabuse.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_MENT_HLTH_SUB_ABUSE = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_MENT_HLTH_SUB_ABUSE = false;
            }

            //added on 21st feb by varsha


            if (chkSessionactivitieother.Checked == true)
            {
                oCase.SESSION_ACTIVITIES_OTHER = true;
            }
            else
            {
                oCase.SESSION_ACTIVITIES_OTHER = false;
            }

            oCase.SESSION_ACTIVITIES_SPECIFY_OTHER = txtSessionactivitiespecifyother.Text;      

       

            if (!(rblSessionalignmentfk.SelectedItem == null))
            {
                if (!(rblSessionalignmentfk.SelectedItem.Value.ToString() == ""))
                {
                    oCase.SESSION_ALIGNMENT_FK = Convert.ToInt32(rblSessionalignmentfk.SelectedItem.Value.ToString());
                    SessAlligment = Convert.ToInt32(rblSessionalignmentfk.SelectedItem.Value.ToString());

                }
                else
                {
                    oCase.SESSION_ALIGNMENT_FK = null;
                    SessAlligment = 0;
                }
            }
            if (chkReasonnotalignfamcris.Checked == true)
            {
                oCase.REASON_NOT_ALIGN_FAM_CRIS = true;
            }
            else
            {
                oCase.REASON_NOT_ALIGN_FAM_CRIS = false;
            }
            if (chkReasonnotalignpartnotengage.Checked == true)
            {
                oCase.REASON_NOT_ALIGN_PART_NOT_ENGAGE = true;
            }
            else
            {
                oCase.REASON_NOT_ALIGN_PART_NOT_ENGAGE = false;
            }
            if (chkReasonnotalignpartinterothtop.Checked == true)
            {
                oCase.REASON_NOT_ALIGN_PART_INTER_OTH_TOP = true;
            }
            else
            {
                oCase.REASON_NOT_ALIGN_PART_INTER_OTH_TOP = false;
            }
            if (chkReasonnotalignpresothindinhibsess.Checked == true)
            {
                oCase.REASON_NOT_ALIGN_PRES_OTH_IND_INHIB_SESS = true;
            }
            else
            {
                oCase.REASON_NOT_ALIGN_PRES_OTH_IND_INHIB_SESS = false;
            }
            if (chkReasonnotalignpartsick.Checked == true)
            {
                oCase.REASON_NOT_ALIGN_PART_SICK = true;
            }
            else
            {
                oCase.REASON_NOT_ALIGN_PART_SICK = false;
            }
            if (chkReasonnotalignphyscconst.Checked == true)
            {
                oCase.REASON_NOT_ALIGN_PHYSC_CONST = true;
            }
            else
            {
                oCase.REASON_NOT_ALIGN_PHYSC_CONST = false;
            }
            if (chkReasonnotalignother.Checked == true)
            {
                oCase.REASON_NOT_ALIGN_OTHER = true;
            }
            else
            {
                oCase.REASON_NOT_ALIGN_OTHER = false;
            }
            oCase.SPECIFY_REASON_NOT_ALIGN = txtSpecifyreasonnotalign.Text;
            
            /*---------------------------------------------------------------------*/

            if (!(rblAdultSubAbuse.SelectedItem == null))
            {
                if ((rblAdultSubAbuse.SelectedItem.Value == "1"))
                {
                    oCase.ADULT_TOPICS_SUB_ABUSE = true;
                }
                else
                {
                    oCase.ADULT_TOPICS_SUB_ABUSE = false;
                }
            }
            if (!(rblAdultPrntSkills.SelectedItem == null))
            {
                if ((rblAdultPrntSkills.SelectedItem.Value == "1"))
                {
                    oCase.ADULT_TOPICS_PRNT_SKILLS = true;
                }
                else
                {
                    oCase.ADULT_TOPICS_PRNT_SKILLS = false;
                }
            }
            if (!(rblAdultPersonalDevp.SelectedItem == null))
            {
                if ((rblAdultPersonalDevp.SelectedItem.Value == "1"))
                {
                    oCase.ADULT_TOPICS_PERS_DEV = true;
                }
                else
                {
                    oCase.ADULT_TOPICS_PERS_DEV = false;
                }
            }

            //child

            if (!(rblChildYouthTheDevp.SelectedItem == null))
            {
                if ((rblChildYouthTheDevp.SelectedItem.Value == "1"))
                {
                    oCase.CHILD_TOPICS_ED_ADULT_SUB_ABUSE = true;
                }
                else
                {
                    oCase.CHILD_TOPICS_ED_ADULT_SUB_ABUSE = false;
                }
            }

            if (!(rblChildSubDisRecovery.SelectedItem == null))
            {
                if ((rblChildSubDisRecovery.SelectedItem.Value == "1"))
                {
                    oCase.CHILD_TOPICS_DEV_AND_ED = true;
                }
                else
                {
                    oCase.CHILD_TOPICS_DEV_AND_ED = false;
                }
            }

          

            if (!(rblRecoverBiologyAbuseManagement.SelectedItem == null))
            {
                if ((rblRecoverBiologyAbuseManagement.SelectedItem.Value == "1"))
                {
                    oCase.ED_OTH_RELAT = true;
                }
                else
                {
                    oCase.ED_OTH_RELAT = false  ;
                }
            }
            
                oCase.updated_by = HttpContext.Current.User.Identity.Name;
               // oCase.updated_date = DateTime.Now.Date ;
                oCase.updated_date = DateTime.Now;

               // oCase.SERVICE_LOG_COMPLETED = true;


            if (blNew == true)
            {
                oCase.created_by = HttpContext.Current.User.Identity.Name;
               // oCase.created_date = DateTime.Now.Date ;
                oCase.created_date = DateTime.Now;
          
                db.G_SERVICE_LOGs.InsertOnSubmit(oCase);
              

            }

            db.SubmitChanges();
            lngPkID = oCase.ServiceID;
            saveCaseMembers();
            /*Participnt Engagemnet Rating 4/2/2014*/
            if (NeedPE_v1())
            {
                updateEngagementRating();
            }
           
        }
        //LitJS.Text = " showSuccessToast();";
        //litMessage.Text = "Record saved! ID=" + lngPkID + " at " + DateTime.Now.ToString();
        litMessage.Text = "Record saved!";



    }
    private void updateUserCompletedField()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            G_SERVICE_LOG oCase = (from c in db.G_SERVICE_LOGs where c.ServiceID == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new G_SERVICE_LOG();
                blNew = true;
                oCase.CaseID_FK = intCaseID;
                oCase.CaseEBPID_FK = intEBPEnrollID;
            }


            oCase.SERVICE_LOG_COMPLETED = true;


            if (blNew == true)
            {
                oCase.created_by = HttpContext.Current.User.Identity.Name;
               // oCase.created_date = DateTime.Now.Date;
                oCase.created_date = DateTime.Now;

                db.G_SERVICE_LOGs.InsertOnSubmit(oCase);


            }

            db.SubmitChanges();


        }
    }   
  
    //-----View State------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["intCaseID"] != null)))
        {
            intCaseID = Convert.ToInt32(this.ViewState["intCaseID"]);
        }
        if (((this.ViewState["intEBPEnrollID"] != null)))
        {
            intEBPEnrollID = Convert.ToInt32(this.ViewState["intEBPEnrollID"]);
        }

        if (((this.ViewState["intC"] != null)))
        {
            intC = Convert.ToInt32(this.ViewState["intC"]);
        }
        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
        if (((this.ViewState["allchecked"] != null)))
        {
            allchecked = Convert.ToInt32(this.ViewState["allchecked"]);
        }
        if (((this.ViewState["TopicFreez"] != null)))
        {
            TopicFreez = Convert.ToInt32(this.ViewState["TopicFreez"]);
        }
        
    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["intCaseID"] = intCaseID;
        this.ViewState["intEBPEnrollID"] = intEBPEnrollID;
        this.ViewState["intC"] = intC;
        this.ViewState["oUI"] = oUI;
        this.ViewState["allchecked"] = allchecked;
        this.ViewState["TopicFreez"] = TopicFreez;

        return (base.SaveViewState());
    }

    //-----Case Member----------------//
    protected void loadCaseMembers()
    {

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.C_DEMOGRAPHICS_ON_INDIVIDUALS_IN_THE_CASEs
                      where p.CaseNoId_FK == intCaseID
                      select new { p.IndividualID, p.FIRST_NAME, CaseMember = p.IND_ID + " - " + p.FIRST_NAME };
            chkCaseMembers.DataSource = qry;
            chkCaseMembers.DataTextField = "CaseMember";
            chkCaseMembers.DataValueField = "IndividualID";
            chkCaseMembers.DataBind();
        }
    }
    private void displayCaseMembers(int intServiceID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
 
            var oPages = from pg in db.G_ServiceLogCaseMembers
                         where pg.ServiceID_FK == intServiceID
                         select pg;
            foreach (var oCase in oPages)
            {
                for (int i = 0; i < chkCaseMembers.Items.Count; i++)
                {
                    if (Convert.ToInt32(chkCaseMembers.Items[i].Value) == oCase.IndividualID_FK)
                    {
                        chkCaseMembers.Items[i].Selected = true;
                    }
                }

            }
        }

    }
    protected void saveCaseMembers()
    {
        for (int i = 0; i < chkCaseMembers.Items.Count; i++)
        {
            if (chkCaseMembers.Items[i].Selected == true)
            {
                updateCaseMembers(Convert.ToInt32(chkCaseMembers.Items[i].Value));
            }
            else
            {
                removeCaseMembers(Convert.ToInt32(chkCaseMembers.Items[i].Value));
            }
        }
    }
    protected void updateCaseMembers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            G_ServiceLogCaseMember oCase = (from c in db.G_ServiceLogCaseMembers
                                     where c.IndividualID_FK == IndividualID
                                         && c.ServiceID_FK == lngPkID
                                     select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new G_ServiceLogCaseMember();
                blNew = true;
                oCase.ServiceID_FK = lngPkID;
                oCase.IndividualID_FK = IndividualID;
            }

            if (blNew == true)
            {
                db.G_ServiceLogCaseMembers.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();

        }
    }
    protected void removeCaseMembers(int IndividualID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Boolean blNew = false;
            G_ServiceLogCaseMember oCase = (from c in db.G_ServiceLogCaseMembers
                                     where c.IndividualID_FK == IndividualID
                                         && c.ServiceID_FK == lngPkID
                                     select c).FirstOrDefault();
            if ((oCase != null))
            {
                db.G_ServiceLogCaseMembers.DeleteOnSubmit(oCase);
            }
            db.SubmitChanges();

        }
    }

    //--------Save/SaveClose/delete--------------------//
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Validatecheckbox();
        ValidateSessionActivityCheckBox();
        if (valSessActivities == true)
        {
            if (allchecked == 1)
            {
                // Process form submit
                lblSessionMessage.Text = "";
                lblMessageSession.Text = "";
                lblMessSessionActivity.Text = "";
                updateUser();
                updateUserCompletedField();
                displayRecords();
                btnSave.Visible = false;
                btnSaveClose.Visible = false;
                //added on Aug 12 for popup aleart
                btnClose.Visible = false;
                btnCloseHide.Visible = true;

                Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                
            }
            else
            {
                lblSessionMessage.Text = "Required";
                lblMessageSession.Text = "Why was the session not very well aligned? is required";
            }

        }
        else
        {
            lblMessSessionActivity.Text = "Required";
            lblMessageSession.Text = "Which activities occurred during the session? is required";
 
        }
        //litMessage.Text = "Required field info";

        if (oUI.intUserRoleID == 3)
        {
            btnSave.Visible = true;
        }

    }
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        updateUser();
        updateUserCompletedField();
        // displayRecords();

        if (intC == 1)
        {
            Response.Redirect("ServiceLogList.aspx?intCaseID=" + intCaseID);
        }
        else
        {

            Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
       
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            
            //----------------delete from G_SERVICE_LOG-------------------------------//
            G_SERVICE_LOG oca7 = (from c in db.G_SERVICE_LOGs
                                  where c.ServiceID == lngPkID
                                  select c).FirstOrDefault();
            if ((oca7 != null))
            {
                db.G_SERVICE_LOGs.DeleteOnSubmit(oca7);
            }
            db.SubmitChanges();

            //-----------------delete  from F_EBP_ParticipantRating---------------//

            var oca8 = (from p in db.F_EBP_ParticipantRatings
                        where p.ServiceLogID_FK == lngPkID
                        select p).FirstOrDefault();
            if ((oca8 != null))
            {
                db.F_EBP_ParticipantRatings.DeleteOnSubmit(oca8);
            }
            db.SubmitChanges();           

            //------------------deletee from N_SubstanceAbuseMatrix--------------------------//
            var oca10 = (from p in db.N_SubstanceAbuseMatrixes
                         where p.ServiceID_FK == lngPkID
                         select p).FirstOrDefault();
            if ((oca10 != null))
            {
                db.N_SubstanceAbuseMatrixes.DeleteOnSubmit(oca10);
            }
            db.SubmitChanges();

            //------------------deletee from O_ParentingSkillsMatrix--------------------------//
            var oca11 = (from p in db.O_ParentingSkillsMatrixes
                         where p.ServiceID_FK == lngPkID
                         select p).FirstOrDefault();
            if ((oca11 != null))
            {
                db.O_ParentingSkillsMatrixes.DeleteOnSubmit(oca11);
            }
            db.SubmitChanges();


            //------------------deletee from P_PersonalDevelopmentMatrix--------------------------//
            var oca12 = (from p in db.P_PersonalDevelopmentMatrixes
                         where p.ServiceID_FK == lngPkID
                         select p).FirstOrDefault();
            if ((oca12 != null))
            {

                db.P_PersonalDevelopmentMatrixes.DeleteOnSubmit(oca12);

            }
            db.SubmitChanges();


            //------------------deletee from Q_DevelopmentEducationMatrix--------------------------//
            var oca13 = (from p in db.Q_DevelopmentEducationMatrixes
                         where p.ServiceID_FK == lngPkID
                         select p).FirstOrDefault();
            if ((oca13 != null))
            {

                db.Q_DevelopmentEducationMatrixes.DeleteOnSubmit(oca13);

            }
            db.SubmitChanges();


            //------------------deletee from R_AdultSubstanceAbuseMatrix--------------------------//
            var oca14 = (from p in db.R_AdultSubstanceAbuseMatrixes
                         where p.ServiceID_FK == lngPkID
                         select p).FirstOrDefault();
            if ((oca14 != null))
            {

                db.R_AdultSubstanceAbuseMatrixes.DeleteOnSubmit(oca14);

            }
            db.SubmitChanges();

            //------------------deletee from S_OtherFamilySubAbuseMatrix--------------------------//

            var oca15 = (from p in db.S_OtherFamilySubAbuseMatrixes
                         where p.ServiceID_FK == lngPkID
                         select p).FirstOrDefault();
            if ((oca15 != null))
            {

                db.S_OtherFamilySubAbuseMatrixes.DeleteOnSubmit(oca15);

            }
            db.SubmitChanges();

        }

        Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
    } 

    //------------button Close------------------------//
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

      //  ScriptManager.RegisterStartupScript(Page, typeof(Page), "Popup", "alert('This is alert Message from C#')", true);      


       
        if (intC == 1)
        {
            Response.Redirect("ServiceLogList.aspx?intCaseID=" + intCaseID);
        }
        else
        {
            //---added on 7th july for service log close button creats services log-----------//

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                //----------------Check for SERVICE_LOG completed-----------------//
                var oPages = (from c in db.G_SERVICE_LOGs
                              where c.ServiceID == lngPkID
                              select c);

                foreach (var oCase in oPages)
                {
                    if (oCase.SERVICE_LOG_COMPLETED == null)
                    {
                        //string script = "alert(\"Hello!\");";
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerControlScript", script, true);

                      
                        Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);

                    }
                    else
                    {
                        if ((oCase.ADULT_TOPICS_SUB_ABUSE == null) && (oCase.ADULT_TOPICS_PRNT_SKILLS == null) && (oCase.ADULT_TOPICS_PERS_DEV == null) && (oCase.CHILD_TOPICS_DEV_AND_ED == null) && (oCase.CHILD_TOPICS_ED_ADULT_SUB_ABUSE == null) && (oCase.ED_OTH_RELAT == null))
                        {
                            Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                        }
                        else
                        {
                            Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                        }
                    }
                }
                //string script1 = "alert(\"Hello!\");";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ServerControlScript", script1, true);
               // ScriptManager.RegisterStartupScript(Page, typeof(Page), "Popup", "alert('This is alert Message from C#')", true);

                Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
            }
        }

       

    }
    
   
    //-----Participnt Engagemnet Rating 4/2/2014------//
    public void updateEngagementRating()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            F_EBP_ParticipantRating oCase = (from c in db.F_EBP_ParticipantRatings where c.ServiceLogID_FK == lngPkID select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new F_EBP_ParticipantRating();
                blNew = true;
                oCase.CaseEBPID_FK = intEBPEnrollID;
                oCase.ServiceLogID_FK = lngPkID;
            }

           


            if (!(chkERatings.SelectedItem == null))
            {
                if (!(chkERatings.SelectedItem.Value.ToString() == ""))
                {
                    oCase.ER_ID_FK = Convert.ToInt32(chkERatings.SelectedItem.Value.ToString());
                }
                else
                {
                    oCase.ER_ID_FK = null;
                }
            }
            oCase.updatedBy = HttpContext.Current.User.Identity.Name;
            oCase.updatedDate=DateTime.Now;

            if (blNew == true)
            {
                db.F_EBP_ParticipantRatings.InsertOnSubmit(oCase);

            }

            db.SubmitChanges();
   

        }

   


    }
    protected bool NeedPE_v1()
    {
        bool retVal = false;
        object objVal = null;
        long? intServiceLogRank=0;
        int? intServiceLogCOunt=0;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (intC == 1)
            {
                intEBPEnrollID = GetEBPIDfromSL;
            }

            var LogRankquery = db.getServiceLogRank(intEBPEnrollID, lngPkID);

            foreach (getServiceLogRankResult oLogRank in LogRankquery)
            {
                intServiceLogRank = oLogRank.RowNum;
            }

            var LogCountquery = db.getServiceLogCount(intEBPEnrollID);

            foreach (getServiceLogCountResult oLogCount in LogCountquery)
            {
                intServiceLogCOunt = oLogCount.ServiceLogCount;
            }

            if (intServiceLogCOunt == 1 && lngPkID == 0)
            {
                retVal = true;
            }
            if (intServiceLogRank == 2 && lngPkID != 0)
            {
                retVal = true;
            }

        }
        return retVal;
    }
    protected bool NeedPE()
    {
        bool retVal = false;
        object objVal = null;
        //If it already exist show it
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            //for displaying ParticipantRatings from view

            if (intC == 1)
            {
                var oPag = from pg in db.F_EBP_ParticipantRatings
                             where  pg.ServiceLogID_FK == lngPkID  // changed on 1st july  from pg.ServiceLogID_FK != lngPkID  (back to != on 28th July                   
                             select pg;
                foreach (var oCa in oPag)
                {
                    if (oCa.ServiceLogID_FK == lngPkID)
                    {
                        retVal = true;
                        return retVal;
                    }
                    else
                    {
                        retVal = false;
                        return retVal;
                    }
                }

            }

            //--------------------------------------------------//


            var oPages = from pg in db.F_EBP_ParticipantRatings
                         where pg.CaseEBPID_FK == intEBPEnrollID && pg.ServiceLogID_FK != lngPkID  // changed on 1st july  from pg.ServiceLogID_FK != lngPkID  (back to != on 28th July                   
                         select pg;           
                foreach (var oCase in oPages)
                {
                    if (oCase.ServiceLogID_FK == lngPkID)
                    {
                        retVal = true;
                        return retVal;
                    }
                    else
                    {
                        retVal = false;
                        return retVal;
                    }
                }

                if (TopicFreez == 1)
                {
                    var sCounts = from pg1 in db.G_SERVICE_LOGs
                                  where pg1.CaseEBPID_FK == intEBPEnrollID  && pg1.ServiceID != lngPkID
                                  group pg1 by pg1.CaseEBPID_FK into g
                                  select new { SCounts = g.Count() };
                    foreach (var oC in sCounts)
                    {
                        if (oC.SCounts == 1)
                        {
                            retVal = true;
                            return retVal;
                        }
                        else
                        {
                            retVal = false;
                            return retVal;
                        }
                    }
                }
                else
                {
                    var sCounts = from pg1 in db.G_SERVICE_LOGs
                                  where pg1.CaseEBPID_FK == intEBPEnrollID && pg1.CaseID_FK == intCaseID && pg1.ServiceID != lngPkID
                                  group pg1 by pg1.CaseEBPID_FK into g
                                  select new { SCounts = g.Count() };
                    foreach (var oC in sCounts)
                    {
                       // if (oC.SCounts == 2 || oC.SCounts == 1)
                        if (oC.SCounts >=1)
                        {
                            retVal = true;
                            return retVal;
                        }
                        else
                        {
                            retVal = false;
                            return retVal;
                        }
                    }
                }
          }
        return retVal;
    }


    //------link to Matrix------------------lnkAdulttopicssubabuse-------//
    protected void rblAdultSubAbuse_SelectedIndexChanged(object sender, EventArgs e)
    {

      
            if (rblAdultSubAbuse.SelectedValue == "1")
            {
                lnkAdulttopicssubabuse.Visible = true;
                updateUser();
                // Response.Redirect("Matrices/MatrixSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
                Response.Redirect("Matrices/MatrixSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
            }
            else
            {              
                lnkAdulttopicssubabuse.Visible = false;
                DeleteSubAbuse(lngPkID);
            }
       
    }
    private void DeleteSubAbuse(int lngPkID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            N_SubstanceAbuseMatrix oCase10 = (from c in db.N_SubstanceAbuseMatrixes
                                              where c.ServiceID_FK == lngPkID
                                              select c).FirstOrDefault();
            if ((oCase10 != null))
            {
                db.N_SubstanceAbuseMatrixes.DeleteOnSubmit(oCase10);
            }
            db.SubmitChanges();
        }
    }
    protected void rblAdultPrntSkills_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblAdultPrntSkills.SelectedValue == "1")
        {
            lnkAdulttopicsprntskills.Visible = true;
             updateUser();

            // Response.Redirect("Matrices/MatrixParentingSkills.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
            Response.Redirect("Matrices/MatrixParentingSkills.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
        }
        else
        {
            lnkAdulttopicsprntskills.Visible = false;
            DeletePrnSkills(lngPkID);
        }
    }
    private void DeletePrnSkills(int lngPkID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            O_ParentingSkillsMatrix oCase11 = (from c in db.O_ParentingSkillsMatrixes
                                               where c.ServiceID_FK == lngPkID
                                               select c).FirstOrDefault();
            if ((oCase11 != null))
            {
                db.O_ParentingSkillsMatrixes.DeleteOnSubmit(oCase11);
            }
            db.SubmitChanges();
        }
    }
    protected void rblAdultPersonalDevp_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblAdultPersonalDevp.SelectedValue == "1")
        {
            lnkAdulttopicspersdev.Visible = true;
            updateUser();

            //Response.Redirect("Matrices/MatrixPersonalDevelopment.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
            Response.Redirect("Matrices/MatrixPersonalDevelopment.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
        }
        else
        {
            lnkAdulttopicspersdev.Visible = false;
            DeleteAdultPrsDevp(lngPkID);
        }
    }
    private void DeleteAdultPrsDevp(int lngPkID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            P_PersonalDevelopmentMatrix oCase12 = (from c in db.P_PersonalDevelopmentMatrixes
                                                   where c.ServiceID_FK == lngPkID
                                                   select c).FirstOrDefault();
            if ((oCase12 != null))
            {
                db.P_PersonalDevelopmentMatrixes.DeleteOnSubmit(oCase12);
            }
            db.SubmitChanges();
        }
    }
    protected void rblChildYouthTheDevp_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (rblChildYouthTheDevp.SelectedValue == "1")
        {
            lnkChildtopicsdevanded.Visible = true;
            updateUser();

            //Response.Redirect("Matrices/MatrixDevelopAndEducation.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
            Response.Redirect("Matrices/MatrixDevelopAndEducation.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
        }
        else
        {
            lnkChildtopicsdevanded.Visible = false;
            DeleteChildYouthDevp(lngPkID);
        }

    }
    private void DeleteChildYouthDevp(int lngPkID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            Q_DevelopmentEducationMatrix oCase13 = (from c in db.Q_DevelopmentEducationMatrixes
                                                    where c.ServiceID_FK == lngPkID
                                                    select c).FirstOrDefault();
            if ((oCase13 != null))
            {
                db.Q_DevelopmentEducationMatrixes.DeleteOnSubmit(oCase13);
            }
            db.SubmitChanges();
        }
    }
    protected void rblChildSubDisRecovery_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblChildSubDisRecovery.SelectedValue == "1")
        {
            lnkChildtopicsedadultsubabuse.Visible = true;
            updateUser();

            //Response.Redirect("Matrices/MatrixAdultSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
            Response.Redirect("Matrices/MatrixAdultSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
        }
        else
        {

            lnkChildtopicsedadultsubabuse.Visible = false;
            DeleteSubDiscovery(lngPkID);
        }

    }
    private void DeleteSubDiscovery(int lngPkID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            R_AdultSubstanceAbuseMatrix oCase14 = (from c in db.R_AdultSubstanceAbuseMatrixes
                                                   where c.ServiceID_FK == lngPkID
                                                   select c).FirstOrDefault();
            if ((oCase14 != null))
            {
                db.R_AdultSubstanceAbuseMatrixes.DeleteOnSubmit(oCase14);
            }
            db.SubmitChanges();
        }
    }
    protected void rblRecoverBiologyAbuseManagement_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblRecoverBiologyAbuseManagement.SelectedValue == "1")
        {
            lnkOtherRelative.Visible = true;
            updateUser();
            // Response.Redirect("Matrices/MatrixOtherRelatives.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID );
            Response.Redirect("Matrices/MatrixOtherRelatives.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
        }
        else
        {
            lnkOtherRelative.Visible = false;
            DeleteOtherRelative(lngPkID);
        }
    }

    private void DeleteOtherRelative(int lngPkID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            S_OtherFamilySubAbuseMatrix oCase15 = (from c in db.S_OtherFamilySubAbuseMatrixes
                                                   where c.ServiceID_FK == lngPkID
                                                   select c).FirstOrDefault();
            if ((oCase15 != null))
            {
                db.S_OtherFamilySubAbuseMatrixes.DeleteOnSubmit(oCase15);
            }
            db.SubmitChanges();
        }
    }
    protected void lnkAdulttopicssubabuse_Click(object sender, EventArgs e)
    {
        updateUser();
        // Response.Redirect("Matrices/MatrixSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
        Response.Redirect("Matrices/MatrixSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez); //added on feb 20th
    }
    protected void lnkAdulttopicsprntskills_Click(object sender, EventArgs e)
    {
        updateUser();
        //Response.Redirect("Matrices/MatrixParentingSkills.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
        Response.Redirect("Matrices/MatrixParentingSkills.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
    }
    protected void lnkAdulttopicspersdev_Click(object sender, EventArgs e)
    {
        updateUser();
        //Response.Redirect("Matrices/MatrixPersonalDevelopment.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
        Response.Redirect("Matrices/MatrixPersonalDevelopment.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
    }
    protected void lnkChildtopicsdevanded_Click(object sender, EventArgs e)
    {
        updateUser();
        //Response.Redirect("Matrices/MatrixDevelopAndEducation.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
        Response.Redirect("Matrices/MatrixDevelopAndEducation.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
    }
    protected void lnkChildtopicsedadultsubabuse_Click(object sender, EventArgs e)
    {
        updateUser();
        // Response.Redirect("Matrices/MatrixAdultSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
        Response.Redirect("Matrices/MatrixAdultSubstanceAbuse.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
    }
    protected void lnkOtherRelative_Click(object sender, EventArgs e)
    {
        updateUser();
        // Response.Redirect("Matrices/MatrixOtherRelatives.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID);
        Response.Redirect("Matrices/MatrixOtherRelatives.aspx?lngPkID=" + lngPkID + "&intEBPEnrollID=" + intEBPEnrollID + "&intCaseID=" + intCaseID + "&intC=" + intC + "&TopicFreez=" + TopicFreez);//added on Feb 20th
    }

    //--------Validation for Session --------//
    protected void rblSessionlocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = rblSessionlocation.SelectedIndex;

        if (i == 10)
        {
            rfvtxtSpecifSessionlocation.EnableClientScript = true;
            rfvtxtSpecifSessionlocation.Enabled = true;
            //count1 = 1;
        }
        else
        {
            rfvtxtSpecifSessionlocation.EnableClientScript = false;
            rfvtxtSpecifSessionlocation.Enabled = false;
          
        }
    }
    protected void chkOtherspresentothprofstaffyn_CheckedChanged(object sender, EventArgs e)
    {
        if (chkOtherspresentothprofstaffyn.Checked == true)
        {
            rfvtxtOtherspresentothprofstaffyn.EnableClientScript = true;
            rfvtxtOtherspresentothprofstaffyn.Enabled = true;
            //count1 = 2;
        }
        else
        {
            rfvtxtOtherspresentothprofstaffyn.EnableClientScript = false;
            rfvtxtOtherspresentothprofstaffyn.Enabled = false;
        }
    }
    protected void chkSessionactivitieother_CheckedChanged(object sender, EventArgs e)
    {
        if (chkSessionactivitieother.Checked == true)
        {
            rfvtxtSessionactivitiespecifyother.EnableClientScript = true;
            rfvtxtSessionactivitiespecifyother.Enabled = true;
            //count1 = 3;
        }
        else
        {
            rfvtxtSessionactivitiespecifyother.EnableClientScript = false;
            rfvtxtSessionactivitiespecifyother.Enabled = false;
        }
    }
    protected void chkReasonnotalignother_CheckedChanged(object sender, EventArgs e)
    {
        if (chkReasonnotalignother.Checked == true)
        {
            rfvtxtSpecifyreasonnotalign.EnableClientScript = true;
            rfvtxtSpecifyreasonnotalign.Enabled = true;
            //count1 = 4;
        }
        else
        {
            rfvtxtSpecifyreasonnotalign.EnableClientScript = false;
            rfvtxtSpecifyreasonnotalign.Enabled = false;
        }
    }
    protected void rblSessionalignmentfk_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = rblSessionalignmentfk.SelectedIndex;
        allchecked = 0;

        if (i == 2)
        {
            Validatecheckbox();
            if (allchecked == 0)
            {
                lblSessionMessage.Text = "Required";
            }
            else
            {
                lblSessionMessage.Text = "";
            }
            chkReasonnotalignfamcris.Enabled = true;
            chkReasonnotalignpartnotengage.Enabled = true;
            chkReasonnotalignpartinterothtop.Enabled = true;
            chkReasonnotalignpresothindinhibsess.Enabled = true;
            chkReasonnotalignpartsick.Enabled = true;
            chkReasonnotalignphyscconst.Enabled = true;
            chkReasonnotalignother.Enabled = true;
            txtSpecifyreasonnotalign.Enabled = true;
        }
        else if (i == 1)
        {
            Validatecheckbox();
            if (allchecked == 0)
            {
                lblSessionMessage.Text = "Required";
            }
            else
            {
                lblSessionMessage.Text = "";
            }

            chkReasonnotalignfamcris.Enabled = true;
            chkReasonnotalignpartnotengage.Enabled = true;
            chkReasonnotalignpartinterothtop.Enabled = true;
            chkReasonnotalignpresothindinhibsess.Enabled = true;
            chkReasonnotalignpartsick.Enabled = true;
            chkReasonnotalignphyscconst.Enabled = true;
            chkReasonnotalignother.Enabled = true;
            txtSpecifyreasonnotalign.Enabled = true;
        }
        else if (i == 0)
        {
            allchecked = 1;
            lblMessageSession.Text = "";
            lblSessionMessage.Text = "";

            chkReasonnotalignfamcris.Enabled=false;
            chkReasonnotalignpartnotengage.Enabled=false;
            chkReasonnotalignpartinterothtop.Enabled=false;
            chkReasonnotalignpresothindinhibsess.Enabled=false;
            chkReasonnotalignpartsick.Enabled=false;
            chkReasonnotalignphyscconst.Enabled=false;
            chkReasonnotalignother.Enabled = false;
            txtSpecifyreasonnotalign.Enabled = false;
            rfvtxtSpecifyreasonnotalign.Enabled = false; // added
            chkReasonnotalignfamcris.Checked = false;
            chkReasonnotalignpartnotengage.Checked = false;
            chkReasonnotalignpartinterothtop.Checked = false;
            chkReasonnotalignpresothindinhibsess.Checked = false;
            chkReasonnotalignpartsick.Checked = false;
            chkReasonnotalignphyscconst.Checked = false;
            chkReasonnotalignother.Checked = false;
            txtSpecifyreasonnotalign.Text= "";
        }
        else
        {
            allchecked = 1;
            lblMessageSession.Text = "";
            lblSessionMessage.Text = "";
        }
        
        
    }

    //-----Validation for checkbox-----------//
    private void Validatecheckbox()
    {
       
        if(chkReasonnotalignfamcris.Checked)
            allchecked=1;
        if(chkReasonnotalignpartnotengage.Checked)
            allchecked=1;
        if(chkReasonnotalignpartinterothtop.Checked)
            allchecked=1;
        if(chkReasonnotalignpresothindinhibsess.Checked)
            allchecked=1;
         if(chkReasonnotalignpartsick.Checked)
            allchecked=1;
         if(chkReasonnotalignphyscconst.Checked)
            allchecked=1;
         if(chkReasonnotalignother.Checked)
            allchecked=1;    
    
        
    }
    private void ValidateSessionActivityCheckBox()
    {
        if (chkSessionactivitiesgrpdiscyn.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesinddiscyn.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesfamactinter.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesfammtng.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesroleplay.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesreenact.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesexpact.Checked)
            valSessActivities = true;

        if (chkSessionactivitiesgames.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesworksheet.Checked)
            valSessActivities = true;
        if (chkSessionactivitieswatchvid.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesgoalset.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesguidprac.Checked)
            valSessActivities = true;
        if (chkSessionactivitiescoachfeed.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesprovemotsupp.Checked)
            valSessActivities = true;

       
        if (chkSessionactivitiescrisinter.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesprntskillscr.Checked)
            valSessActivities = true;
        if (chkSessionactivitieschilddevscr.Checked)
            valSessActivities = true;
        if (chkSessionactivitieshlthass.Checked)
            valSessActivities = true;
        if (chkSessionactivitiesmenthlthsubabuse.Checked)
            valSessActivities = true;
        if (chkSessionactivitieother.Checked)
            valSessActivities = true;   
    }


    // added on Aug 12 for popup (not to show when save butoon is freezed)
    protected void btnCloseHide_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        if (intC == 1)
        {
            Response.Redirect("ServiceLogList.aspx?intCaseID=" + intCaseID);
        }
        else
        {
            //---added on 7th july for service log close button creats services log-----------//

            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                //----------------Check for SERVICE_LOG completed-----------------//
                var oPages = (from c in db.G_SERVICE_LOGs
                              where c.ServiceID == lngPkID
                              select c);

                foreach (var oCase in oPages)
                {
                    if (oCase.SERVICE_LOG_COMPLETED == null)
                    {                       
                       
                        Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                    }
                    else
                    {
                        if ((oCase.ADULT_TOPICS_SUB_ABUSE == null) && (oCase.ADULT_TOPICS_PRNT_SKILLS == null) && (oCase.ADULT_TOPICS_PERS_DEV == null) && (oCase.CHILD_TOPICS_DEV_AND_ED == null) && (oCase.CHILD_TOPICS_ED_ADULT_SUB_ABUSE == null) && (oCase.ED_OTH_RELAT == null))
                        {
                            Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                        }
                        else
                        {
                            Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
                        }
                    }
                }               

                Response.Redirect("EBPList.aspx?intCaseID=" + intCaseID);
            }
        }

    }
}